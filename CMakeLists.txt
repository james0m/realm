cmake_minimum_required(VERSION 3.9)

project(realm)

SET(CMAKE_C_COMPILER clang)
SET(CMAKE_CXX_COMPILER clang++)
# SET(CMAKE_C_COMPILER gcc)
# SET(CMAKE_CXX_COMPI g++)
message(STATUS "Set C compiler to ${CMAKE_C_COMPILER}")
message(STATUS "Set CXX compiler to ${CMAKE_CXX_COMPILER}")

SET(CMAKE_EXPORT_COMPILE_COMMANDS 1)


set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14")

option(REALM_ASAN "enable address and leak sanitizer" OFF)

if (REALM_ASAN)
add_compile_options(-fsanitize=leak -fsanitize=address)
add_link_options(-fsanitize=leak -fsanitize=address)
endif()

link_directories(/usr/lib /usr/local/lib)

include_directories(
        /usr/include
        /usr/local/include

        /usr/include/freetype2
        ${CMAKE_SOURCE_DIR}/src
        ${CMAKE_SOURCE_DIR}/src/3p
)

add_subdirectory(src)
#add_subdirectory(tests)
