


Font Renderer


basic library based on freetype2 and/or SDL2_ttf
relies on the basic grimoire::Renderer api for drawing

* core structures and methods
struct Font {
    string name;
    int    size;
    GlyphAtlas glyphs;
};


// may not need this
struct Glyph {
    char  ch;      // the mapped character
    int   texture; // the texture that holds the real glyph data
    rectf region;  // region / tex coords 
};

struct GlyphAtlas {
    Font* font; 
    map<char, rectf> regions;
    int glyphcount;

    static GlyphAtlas from_atlas(string charset, const textureatlas &src);
};

GlyphAtlas from_texture_atlas(string charset, const texture_atlas &src)
{
    auto glyphs = new GlyphAtlas(charset.size());
    for( char ch : charset ) {
        glyphs.regions[ch] = src.region_by_id(ch);
    }

    return glyphs
}

rectf GlyphAtlas::glyph(char ch) {
    return _regions[ch];
}
 

void GlyphAtlas::phrase(in string txt, out list<rectf> &result) {
    assert(result.size() >= txt.size())

    auto region = rectf;
    for(char ch : txt) 
        region = glyph(ch)
        result.push_back(region)
} 




* TextElement (gfx::Element)
  a scene node that renders text
  its boundary size is based on char1 ... charn -> += char_size(ch)
  up until the line break size (if any)

* Other concerns
these are things that will need to be dealt with if a lot of text is 
going to be displayed. UI and console text  will require this. Simple debug
displays for FPS and value printouts are not as important and don't need real
font setting work.

** baseline offsets
font glyphs cannot just be aligned to the bottom of their rectangle. there is 
a baseline point that you need to set the text to. 

** line height
pretty straightforward

** soft line breaks
each text element has boundaries, including padding and margine, and so 
there will be a point where the text flow needs to move down and back over
 to the beginning of the active space

** letter spacing
how much space  between each glyph?

** coloring
coloring fonts will be pretty important, need some kind of masking scheme 
to map coloring onto font glyphs on the fly
