    

fsm = {}
fsm[IDLE] = unit_idle
fsm[MOVING] = unit_moving
fsm[ATTACKING] = unit_attacking
fsm[DEFENDING] = unit_defending

class Astar:
    pass

class UnitControlSystem:

    """ 
    a list of units with active states that need 
    to be updated each frame
    """
    active_units = []


    pathfinder = AStar();
    
    def frame(self):
        for unit in active_units:
	    unit.state()
				


    def unit_idle(world):
        """
        this is ALWAYS on the state stack

        check for reasons to change state

        is an enemy in sight range?
            is enemy in weapon range?
                yes: switch to attacking
                no:  switch to close_distance        
        """
        pass


    def unit_moving(entity, world):
        """
        get the unit's path from the pathfinder
        if the path is empty, 
            pop state

        look at the first node in the path
        if at the first node:      
            get the next node
            start a lerp from first node to next node, writing to unit.spatial.location            
        
        """
        pass
    
    def unit_attacking(world):
        """
        check if moving, check for unit type's ability to attack while moving
        check for target
        range and LOS check weapon
        if movement and weapons checks pass
            post event to combat resolver
        else:
            push_state(unit_moving(target.location))
        
        """
    
    def unit_defending(world):
        """ 
        stay near a location and attack enemies who come near

        location = unit.unitctl.target_loc
        tracking_entity = world.spatial_query(location)

        if tracking_entity:
            location = tracking_entity.spatial.location
        
        
        
        """          
