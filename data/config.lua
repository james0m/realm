
game = {

   startup_map = "desert.tmx",
   ui_font = "NotoSans-Regular.ttf",
   ui_font_size = 12,
   test_entities = 1200
}

window = {
   width = 1280,
   height = 768
}

movement = {
   near_proximity = 15
}

pathfinding = {
   algorithm = "AStar",
   overlay = "MapDebugOverlay"
}

console = {
   bg_r = 0.0,
   bg_g = 0.1,
   bg_b = 0.4,
   bg_a = 0.8,
   
   font = "Inconsolata-Regular.ttf",
   font_size = 11
}

