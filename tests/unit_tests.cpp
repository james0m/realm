

/*
this file exists to be the sole compilation unit for
the Catch test implementation. nothing else should go 
here.
*/
#define CATCH_CONFIG_MAIN
#include "catch/catch.hpp"
