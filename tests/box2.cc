#include <iostream>
#include <vector>

#include "vec2.h"
#include "rect.h"
#include "grimoire/element.h"
#include "color.h"
#include "renderapi_sdl.hpp"
#include "interpolation.h"

#ifdef _WIN32
#undef main
#endif

using namespace std;

struct LerpState {
	float x_start = 0;
	float x_end   = 0;
	float y_start = 0;
	float y_end   = 0;

	unsigned start_t = 0,
	         end_t   = 0;

	float t  = 0;
	bool in_motion = false;
};

using gfx::Element;

class Box : public Element {
public:
	Box(Element* parent, colorf col={1.0f, 0,0,1.0f}, rectf r={0,0,0,0}):
        Element(parent),
		_color(col)
    {
        set_bounds(r);
    }

	void set_color(const colorf &col) {
		_color = col;
	}

    virtual void draw(RenderApi& renderer) {
        renderer.draw_filled_rect(bounds(), _color);

        for( auto child : elements()) {
            child->draw(renderer);
		}
	}

	LerpState *lerp() { return &_lerp; }
	
private:
	colorf    _color;
	LerpState _lerp;
};



int main() {
	
	sdl::init();

	SDL_Window* win;
	SDL_Renderer* ren;

	SDL_CreateWindowAndRenderer(800, 600, SDL_WINDOW_RESIZABLE, &win, &ren);

	RenderApi* renderer = new RenderApi_SDL();
	renderer->set_context(win);
	renderer->set_clear_color({.4f,.4f,.4f, 1.0f});

	Element *selected_box = nullptr;

	Box *box1 = new Box( nullptr, { 1.0f, 0,0, 1.0f}, {0,0, 36,36});
	Box *box2 = new Box( box1,    { 0.0, 1.0f, 0.0, 1.0f}, {39, 39, 24, 24});
	Box *box3 = new Box( box2,    { 0,0,1,1}, {53,33, 15,15});
	Box *box4 = new Box( box1,    { 1.0f, 1.0f, 0.0f, 0.6f}, {65,65, 72, 72});

	std::vector<Element*> boxes;
	boxes.push_back(box1);
	boxes.push_back(box2);
	boxes.push_back(box3);
	boxes.push_back(box4);

    for (auto box : boxes) {
        auto sz = box->bounds().dimensions();
        box->set_anchor(sz.x/2, sz.y/2);
    }

	unsigned t, frame_end;
	constexpr unsigned ANIM_DURATION = 500;
	constexpr unsigned FPS_60hz = 1000 / 60;

	bool running = true;
	while (running) 
	{
		t = SDL_GetTicks();
		SDL_Event e;
		
		while( SDL_PollEvent(&e)) 
		{
			switch ( e.type) {
				case SDL_QUIT: {
					running = false; break;
				}

				case SDL_KEYDOWN: {
					if( e.key.keysym.sym == SDLK_ESCAPE) {
						running = false; break;
					}
				}

				case SDL_MOUSEBUTTONDOWN: {

					bool selection = false;
					for(auto box : boxes) {
						if( box->bounds().collide(e.button.x, e.button.y)) {

							cout << "Collision at (" << e.button.x << ", " << e.button.y 
								 << ") " << endl; 
							selection = true;
							selected_box = box;
							break;
						}
					}

					if( !selection && selected_box) {
						// move the box and its children to the point of the 
						// mouse click. The box's anchor point should be centered
						// over this spot
						LerpState *lerp = static_cast<Box*>(selected_box)->lerp();
						lerp->in_motion = true;

						rectf bounds = selected_box->bounds();
						auto ancpt = selected_box->anchor();

						lerp->x_start = bounds.x;
						lerp->x_end   = float(e.button.x) - ancpt.x;
						
						lerp->y_start = bounds.y;
						lerp->y_end   = float(e.button.y) - ancpt.y;

						lerp->start_t = t;
						lerp->end_t   = t + ANIM_DURATION; 
					}
				}
			}
		}

		t = SDL_GetTicks();

		for( auto box : boxes) {
			LerpState *lerp = static_cast<Box*>(box)->lerp();
			if( lerp->in_motion) {
				lerp->t = (float(t) - lerp->start_t) / (lerp->end_t - lerp->start_t);

				if( lerp->t > 1.0)
					lerp->t = 1.0;

				vec2f pos = box->position();

				float nx = interpolate(linear, lerp->x_start, lerp->x_end, lerp->t);
				float ny = interpolate(linear, lerp->y_start, lerp->y_end, lerp->t);

				float dx = nx - pos.x;
				float dy = ny - pos.y;

				selected_box->translate(dx, dy);

				if( lerp->t == 1.0) {
					lerp->in_motion = false;
				}
			}

		}

		renderer->frame_begin();
        box1->draw(*renderer);

		if ( selected_box && ((Box*)selected_box)->lerp()->in_motion)
		{
			LerpState *l = ((Box*)selected_box)->lerp();
			line_def ld;
			ld.start = {l->x_start, l->y_start};
			ld.end   = {l->x_end, l->y_end};
			ld.color = {0, 1.0f, 0, 1.0f};
			renderer->batch_lines(&ld);

			circle_def circles[3];
			circles[0].center = ld.start;
			circles[0].radius = 10.0f;
			circles[0].color = { 0,1,0};

			circles[1].center = ld.end;
			circles[1].radius = 10.0f;
			circles[1].color = { 0,1,0};

            circles[2].center = rectf(ld.end, selected_box->bounds().topleft()).center();
			circles[2].radius = 10.0f;
			circles[2].color = { 1,1,0};

			renderer->batch_circles(circles, 3);
		}
		renderer->frame_end();

		frame_end = SDL_GetTicks() - t;

		if( frame_end < FPS_60hz) {
			SDL_Delay(FPS_60hz - frame_end);
		}
	}
	return 0;
}
