#include <vector>
#include "grimoire/ioutils.h"
#include "grimoire/filesystem.h"
#include "grimoire/fontlib.h"
#include "grimoire/sdlutils.h"
#include "grimoire/renderapi_sdl.hpp"

#include "ui/textbox.h"

const char* wiki = "Wikipedia is a free online encyclopedia that aims to allow anyone "
    "to edit articles. Wikipedia is the largest and most popular general reference "
    "work on the Internet and is ranked among the ten most popular websites. "
    "Wikipedia is owned by the nonprofit Wikimedia Foundation.";

using namespace std;

int main()
{
    io::FileSystem::mount(io::exe_dir() + "/data");
    sdl::init();
    txt::init();
    
    auto window         = sdl::window_create("font test",800,600);
    auto renderer       = RenderApi_SDL::create(window.get());
    auto font_noto_18   = txt::Font::from_file(*renderer, "fonts/NotoSans-Regular.ttf", 18);
    auto font_oswald_48 = txt::Font::from_file(*renderer,"fonts/Oswald-Regular.ttf", 48);   
    auto font_oswald_24 = txt::Font::from_file(*renderer,"fonts/Oswald-Regular.ttf", 24);    

    auto txt_by_golly       = "By golly!";
    auto txt_bowel_movement = "I produced a fantastic bowel movement today, "
                              "at exactly 2:35pm!";

    renderer->set_clear_color({ 0.3f, 0.3f, 0.7f, 1.0f });       
    
    auto textboxes = vector<Unique<txt::TextBox>>(3);
                   
    textboxes[0] = txt::TextBox::create(*font_oswald_24, txt_bowel_movement, 400);
    textboxes[1] = txt::TextBox::create(*font_oswald_48, txt_by_golly, 300);
    textboxes[2] = txt::TextBox::create(*font_noto_18, wiki, 300);    
    
    textboxes[0]->place( 0,0 );
    textboxes[1]->place( 200, 275 );
    textboxes[2]->place( 450, 50 );
    
    bool running = true;

    rectf atlas_dims = {
        0,0,
        (float)font_oswald_24->glyphs.texture_width(),
        (float)font_oswald_24->glyphs.texture_height()
    };

    bool draw_glyph_rects = false;
    bool draw_glyph_atlas = false;
    bool draw_text_regions = true;

    int loop_start, loop_end;
    while(running) {
        loop_start = SDL_GetTicks();
        
        SDL_Event e;
        while(SDL_PollEvent(&e)) {
            if (e.type == SDL_QUIT) {
                running = false;
                break;                
            }
            if (e.type == SDL_KEYDOWN) {
                if (e.key.keysym.sym == SDLK_ESCAPE) {
                    running = false;
                    break;
                }
                if (e.key.keysym.sym == SDLK_r) {
                    draw_glyph_rects = !draw_glyph_rects;
                }
                if (e.key.keysym.sym == SDLK_a) {
                    draw_glyph_atlas = !draw_glyph_atlas;
                }
                if (e.key.keysym.sym == SDLK_t) {
                    draw_text_regions = !draw_text_regions;
                }
            }
        }


        auto src = rectf{};
        
        renderer->frame_begin();

        if (draw_glyph_atlas) {
            auto tex = font_oswald_24->glyphs['a'].texture;
            renderer->draw_texture(tex, src, atlas_dims);
        }

        if( draw_glyph_rects ) {

            auto rect_cmds = std::vector<rect_def>();
            for(auto glyph : font_oswald_24->glyphs) {
                rect_def r;
                r.r = glyph.second.region;
                r.color = colorf{ 1.0f, 0, 0, 1.0f};
                rect_cmds.push_back(r);
            }
            renderer->batch_rects(&rect_cmds[0], rect_cmds.size());
        }

        for(auto &tb : textboxes) {
            if (draw_text_regions) {
                auto cmds = std::vector<rect_def>(2);
                renderer->draw_rect(tb->bounds(),
                                colorf(0.0f, 1.0f, 1.0f, 1.0f));
            }
            tb->draw(*renderer);
        }
        
        renderer->frame_end();

        
        loop_end = SDL_GetTicks();
        sdl::limit60hz(loop_end - loop_start);
    }    
    
    return 0;
}
