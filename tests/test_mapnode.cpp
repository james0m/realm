#include <memory>
#include <iostream>

#include "grimoire/tiledmap.h"
#include "grimoire/tiledmapelement.h"
#include "grimoire/filesystem.h"
#include "grimoire/ioutils.h"
#include "grimoire/tmxreader.h"
#include "grimoire/renderapi_sdl.hpp"
#include "grimoire/scene.h"

using TiledMap = tmx::TiledMap;

constexpr int SCREEN_W = 800;
constexpr int SCREEN_H = 600;

int main() {


    io::FileSystem::mount(io::exe_dir());
    io::FileSystem::mount(io::exe_dir() + "/data");
   
    auto scene = gfx::Scene::create("map viewer");
    auto umap = io::load_tmx_map("desert.tmx");
    auto mapnode = util::make_unique<gfx::TiledMapElement>(scene.get(), umap.get());
    auto camera  = gfx::Camera::create(SCREEN_W, SCREEN_H, mapnode.get());


    camera->lock_to_parent_bounds(true);
    
    scene->attach(mapnode.get());
    
    auto *win = SDL_CreateWindow("map viewer",
                                 SDL_WINDOWPOS_CENTERED,
                                 SDL_WINDOWPOS_CENTERED, 800, 600, SDL_WINDOW_RESIZABLE);
    
    auto renderer = new RenderApi_SDL(win);


    auto *map = mapnode->map();
    auto &tileset = map->tileset(0);

    assert( tileset.src != "");

    auto tsfile = io::open(tileset.src);
    auto tsdata = util::make_unique<u8[]>(tsfile->size());
    tsfile->read(tsdata.get());
    
    auto sr = sdl::image_load_from_memory(tsdata.get(), tsfile->size());

    texid ts_texture = renderer->create_texture(
        sr->w, sr->h,
        from_sdl_format(sr->format),
        AccessMode::STATIC
    );

    renderer->bind_texture(ts_texture);
    renderer->update_texture(sr->pixels);
    tileset.atlas.set_texture(ts_texture);

    // generate all tileset regions for the atlas
    auto regions = util::rects_from_grid(
        tileset.tilewidth,
        tileset.tileheight,
        sr->w,
        sr->h,
        tileset.spacing,
        tileset.margin
    );
    
    tileset.atlas.add_regions(&regions[0], regions.size());
    bool drag_map = false;
    bool running = true;

    int loop_start, loop_interval;
    constexpr int FPS = 1000 / 60;
    
    while(running) {
        loop_start = SDL_GetTicks();
        
        SDL_Event e;
        while(SDL_PollEvent (&e)) {
            if (e.type == SDL_QUIT ||
                (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_ESCAPE))
            {
                running = false;
            
                break;
            }

            switch (e.type)
            {
                case SDL_KEYDOWN: {
                    switch(e.key.keysym.sym) {
                    case SDLK_UP: {
                        camera->translate(0, -10);
                        break;
                    }
                    case SDLK_DOWN: {
                        camera->translate(0, 10);
                        break;
                    }
                    case SDLK_LEFT:  { 
                        camera->translate(-10, 0);
                        break; 
                    }
                    case SDLK_RIGHT: { 
                        camera->translate(10, 0);
                        break; 
                    }

                    case SDLK_m: {
                        mapnode->toggle_map_grid();
                        break;
                    }
                    default:
                        break;
                    }
                } break;
                    
                case SDL_MOUSEBUTTONDOWN: {
                    drag_map = true;
                    break;
                }
                case SDL_MOUSEBUTTONUP: {
                   drag_map = false;
                   break;
                }
                case SDL_MOUSEMOTION: {
                    if (drag_map) {
                        camera->translate(-e.motion.xrel, -e.motion.yrel);
                    }
                    break;
                }
            }
        }


        renderer->frame_begin();
        scene->draw(*renderer, *camera);
        renderer->frame_end();

        loop_interval = SDL_GetTicks() - loop_start;
        if (loop_interval < FPS) {
            SDL_Delay(FPS - loop_interval);   
        }
    }
    
    return 0;
}

