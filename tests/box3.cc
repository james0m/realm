#include <iostream>
#include <vector>
#include <cmath>

#include "SDL.h"
#ifdef _WIN32
#undef main
#endif

#include "vec2.h"
#include "rect.h"
#include "element.h"
#include "color.h"
#include "renderapi_sdl.hpp"
#include "interpolation.h"
#include "sig.h"

using namespace std;

struct LerpState {
	int x_start = 0;
	int x_end   = 0;
	int y_start = 0;
	int y_end   = 0;

	unsigned start_t = 0,
	         end_t   = 0;

	float t  = 0;
	bool in_motion = false;
};

using gfx::Element;

class Box : public Element {
public:
	Box(Element* parent, colorf col={1.0f, 0,0,1.0f}, rectf r={0,0,0,0}):
        Element(parent),
		_color(col)
    {
        set_bounds(r);
    }

	void set_color(const colorf &col) {
		_color = col;
	}

    virtual void draw(RenderApi &renderer)  {
        renderer.draw_filled_rect(bounds(), _color);

        for( auto child : elements()) {
            child->draw(renderer);
		}
	}

	LerpState *lerp() { return &_lerp; }
	
	void setx(float x) {
		_bounds.x = x;
	}
protected:
	colorf    _color;
	LerpState _lerp;
};

struct ElemCollision {
	Element *ball = nullptr;
	Element *brick = nullptr;

	ElemCollision(Element* ba=nullptr, Element* br=nullptr):
		ball(ba),
		brick(br)
	{}

};

struct ElemDestroyed {
	Element* element;

	ElemDestroyed(Element* b=nullptr):
		element(b)
	{}
};

struct Overkill {
	Element *killed;
	int      amt;

	Overkill(Element *ko=nullptr, int a=1):
		killed(ko),
		amt(a)
	{}
};

const int OVERKILL_THRESHOLD = -3;

class Brick : public Box {
public:
	Brick(colorf col, rectf r):
		Box(nullptr, col, r)
	{
		hit.connect(fastdelegate::MakeDelegate(this, &Brick::on_hit));
		destroyed.connect(fastdelegate::MakeDelegate(this, &Brick::on_destroyed));
	}


    virtual void draw(RenderApi& renderer) {
		renderer.draw_filled_rect(bounds(), _color);
		renderer.draw_rect(bounds(), {0,0,0,1});
        for( auto child : elements()) {
            child->draw(renderer);
		}
	}

	void on_hit(const ElemCollision &event) {
		--hp;
		if(hp < 1)
			destroyed((Element*)this);
		if(hp < OVERKILL_THRESHOLD) 
			overkill(this, hp);
	}

	void on_destroyed(const ElemDestroyed &event) {

	}

	int                     hp = 1;
	signal<ElemCollision>	hit;
	signal<ElemDestroyed>   destroyed;
	signal<Overkill>  		overkill;
};

class Ball : public Element {
public:
	Ball(Element* parent, colorf col, float radius) :
		Element(parent),
		_color(col)
	{
		_bounds.w = radius*2;
		_bounds.h = radius*2;
	}

    virtual void draw(RenderApi &renderer) {
		auto r = bounds();
        renderer.draw_filled_circle(r.center(), r.w/2, _color);
	}

	virtual bool collides(float x, float y) const {
		const vec2f center = bounds().center();
		float radius = bounds().w / 2;

		float xdiff_sq = powf(x - center.x, 2),
		      ydiff_sq = powf(y - center.y, 2);

		return ( xdiff_sq + ydiff_sq < radius*radius);
	}

	bool in_motion() const { return _velocity.x != 0 || _velocity.y != 0; }

	void set_velocity(float dx, float dy) {
		_velocity.x = dx;
		_velocity.y = dy;
	}

	// used to invert velocity if wall collision
	void scale_velocity(float vx, float vy) {
		_velocity.x *= vx;
		_velocity.y *= vy;
	}

	void frame() {
		_bounds.translate(_velocity);
	}

	LerpState _lerp;

	vec2f  _velocity;
	colorf _color;
};


std::vector< Box* > create_wall(rectf bricksz, rectf region) {

	std::vector<Box*> bricks;

	vector<rectf> regions = util::rects_from_grid<float>(bricksz.w, bricksz.h, region.w, region.h, 0);

	colorf color = { 0.5f, 0.7f, 0.0f, 1.0f};
	for(auto r : regions)
		bricks.push_back(new Brick(color, r));

	return bricks;
}

struct size2f {
	size2f(float w, float h): w(w),h(h) {}
	float w=0, h=0;
};

size2f window_size(SDL_Window *window) {
	int w, h;

	SDL_GetWindowSize(window, &w, &h);
	return size2f(float(w), float(h));
}

int main () {

	// initialize
	sdl::init();

	// create a window
	SDL_Window* win;
	SDL_Renderer *ren;

	SDL_CreateWindowAndRenderer(640,480, 0, &win, &ren);
	SDL_ShowCursor(0);
	RenderApi* renderer = new RenderApi_SDL();
	renderer->set_context(win);
	renderer->set_clear_color({0.2f,0.2f,0.2f,1});
	// fill the top 1/3 of the window with Box()es. 
	
	// create a paddle
	Box* paddle = new Box(nullptr, colorf(0.5, 0.5, 0.5, .8), rectf( 30, 460, 120, 10));

	// create a ball, attached to the paddle
	Ball* ball = new Ball(paddle, colorf(1,1,1,1), 7);

	ball->translate(20, -20);

	size2f winsize = window_size(win);

	vector< Element* > objects;

	objects.push_back(ball);
	objects.push_back(paddle);

	rectf grid_region = {0,0,640, 480 * (1.0f/3.0f)};
	for( auto brick : create_wall({0,0,40,10}, grid_region)) {
		objects.push_back(brick);
	}

	unsigned t, frame_duration;
	const unsigned FPS60hz = 1000 / 60;

	bool active=true;
	while(active) 
	{
		t = SDL_GetTicks();

		SDL_Event e;
		while(SDL_PollEvent(&e)) {
			switch( e.type ) {
				case SDL_QUIT: active=false; break;
				case SDL_KEYDOWN: {
					if( e.key.keysym.sym != SDLK_ESCAPE) {

						switch(e.key.keysym.sym) {

							case SDLK_SPACE: {
								if( ball->parent() == (Element*)paddle) {

									rectf bounds = ball->bounds();
                                    paddle->detach((Element*)ball);
									ball->place(bounds.x, bounds.y);

									ball->set_velocity(-5.0f, -10.0f);
								}
							}
						}
					}
					else {
						active = false;
					}
					break;
				}
				case SDL_MOUSEMOTION: {
					paddle->setx(e.motion.x);
				}
			}
		}

		// update ball position
		ball->frame();

		rectf bounds = ball->bounds();

		rectf pbounds = paddle->bounds();

		float vx = 1, vy = 1;

		if (grid_region.collide( bounds.topleft())) {
			for (auto itr = begin(objects), finish=end(objects); itr!=finish; ++itr) {
				if ( *itr == (Element*)ball || *itr == (Element*)paddle)
					continue;

				if ( grid_region.collide((*itr)->bounds())) {
					if( (*itr)->bounds().collide(ball->bounds())) {
						
						static_cast<Brick*> (*itr)->hit( (Element*)ball, *itr);
						objects.erase(itr);
						vy = -1;
						break;
					}
				}
			}
		}

		if(bounds.x < 0 || bounds.right() >= winsize.w) {
			vx = -1;
		}

		if(bounds.y < 0 ||  pbounds.collide(bounds) ) {
			vy = -1;
		}

		else if ( bounds.top() > winsize.h) {
			ball->set_velocity(0,0);
			ball->place(0,0);
            paddle->attach((Element*)ball);
            ball->translate(vec2f(20,-20));
		}

		ball->scale_velocity(vx, vy);

		renderer->frame_begin();

		for(auto obj : objects) {
            obj->draw(*renderer);
		}

		renderer->frame_end();


		frame_duration = SDL_GetTicks() - t;
		if(frame_duration < FPS60hz) {
			SDL_Delay(FPS60hz - frame_duration);
		}
	}
	// allow space bar to add velocity to a stopped ball

	// any ball+Box collisions result in damage to the boxes

	// damage below 0 destroys the box

	return 0;
}
