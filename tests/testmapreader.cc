
#include <iostream>
#include <exception>

#include "../grimoire/tmxreader.h"
#include "../grimoire/tiledmap.h"
#include "../grimoire/filesystem.h"

using namespace std;
#include "physfs.h"
void print_search_path()
{ 
     for (char **i = PHYSFS_getSearchPath(); *i != NULL; i++)
         cout << *i << endl;
}

void list_files()
{
	for (char **dir = PHYSFS_getSearchPath(); *dir != NULL; ++dir)
		for( char **file = PHYSFS_enumerateFiles(*dir); *file != NULL; ++file)
			cout << *dir << "/" << *file << endl;
}
int main(int argc, char **argv) 
{
	PHYSFS_init(argv[0]);
	
	io::FileSystem::mount("/home/james/work/C++/realm.d/tests/data", "data");
	print_search_path();
	list_files();

	

	io::TmxMapReader reader;

	TiledMap::Ref map;
	try
	{
		// TODO: shouldn't be forcing a particular data directory name
		io::FileHandle mapfile = io::open("data/desert.tmx", io::ACCESS_READ);
		String buffa = mapfile->as_string();
		map = reader.read(buffa);
	}
	catch (io::TmxReadError &e)	
	{
		cerr << e.what() << endl;
		//assert( 0 ); // force a breakpoint here
		return -1;
	}
	catch(io::FileSystemError &e)
	{
		cerr << e.what() << endl;
		print_search_path();
		return -1;
	}
	catch (std::bad_alloc &e) {
		cerr << "FATAL: no memory available" << endl;
		return -2;
	}
	catch (std::exception &e) {
		cerr << "Unexpected exception: " << e.what() << endl;
		//assert( 0 );
		return -2;
	}

    assert( map->width() == 40);
    assert( map->height() == 40);

    assert( map->tile_width() == 32);
    assert( map->tile_height() == 32);

    assert( map->tileset_count() == 1);

    assert( map->layer_at(0).data.size() == 1600);

    // this fails because the tileset's textureatlas is invalid, which makes sense,
    // as we are not loading any image assets at this point
    assert( map->valid() );

	return 0;
}