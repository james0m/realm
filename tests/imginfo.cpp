#include <iostream>
#include "grimoire/sdlutils.h"

using namespace std;

int main(int argc, char ** argv)
{
    if (argc < 2) {
       cout << "must pass a valid path to an image file" << endl;
       return 0;
    }
    
    sdl::init();
    string image_file = argv[1];
    auto img = sdl::image_load(image_file);
    cout << sdl::surface_info(img.get()) << endl;
    
    return 0;
}
