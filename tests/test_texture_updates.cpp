


#include "grimoire/sdlutils.h"
#include "grimoire/ioutils.h"
#include "grimoire/renderapi_sdl.hpp"

int main() {

    sdl::init();

    auto window   = sdl::window_create("test texture updates", 800, 600);    
    auto bitmap   = sdl::image_load(io::exe_dir() + "/data/Huntress.png");
    
    auto scaling = 4;

    auto renderer = RenderApi_SDL::create(window.get());
    
    rectf frames[4] = {
        rectf(  4.0f, 0.0f, 49.0f, 70.0f) ,
        rectf( 58.0f, 0.0f, 49.0f, 70.0f),
        rectf( 111.0f, 0.0f, 49.0f, 70.0f),
        rectf( 164.0f, 0.0f, 49.0f, 70.0f)
    };

    int frame_idx = 0;
    
    rectf src = frames[0];
    
    rectf dst = {
        (800 / 2) - (src.w * scaling / 2),
        (600 / 2) - (src.h * scaling  / 2),
        float(53) * scaling,
        float(70) * scaling
    };

    auto texture = renderer->create_texture(bitmap->w,
                                            bitmap->h,
                                            from_sdl_format(bitmap->format),
                                            AccessMode::STREAM);

    int bytes_per_pixel = bitmap->format->BytesPerPixel;
    int f_start,
        f_time;

    int anim_interval=0;
    renderer->set_clear_color({ 255.0 / 128.0, 255.0 / 182.0f, 255.0f / 128, 1.0f });
    renderer->bind_texture(texture);
    
    bool running = true;
    while(running)
    {
        f_start = SDL_GetTicks();
        
        SDL_Event e;
        while(SDL_PollEvent(&e)) {
            if(e.type == SDL_QUIT) {
                running = false;
                break;
            }
        }
        int pixel_offset = frames[frame_idx].x * bytes_per_pixel;
        
        auto *pixels = ((char*)bitmap->pixels) + pixel_offset;        
        int pitch = (bitmap->pitch);
        

        renderer->frame_begin();       
        renderer->update_texture((void*)pixels, &src, pitch);
    
        renderer->draw_texture(texture, src, dst);
        
        renderer->frame_end();
        
        // timing calculation for fps limiting and animation updates
        f_time = SDL_GetTicks() - f_start;
        
        if (anim_interval > ( 1000 / 4) ) {
            frame_idx = (frame_idx + 1) % 4;
            anim_interval = 0;
        }        
        int frame_diff = (1000/60) - f_time;
        if (frame_diff > 0) {
            SDL_Delay(frame_diff);
            anim_interval += f_time + frame_diff;
        }
        else {
            anim_interval += f_time;
        }
    }

    sdl::quit();
}
