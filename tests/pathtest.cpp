#include <vector>

#include "grimoire/filesystem.h"
#include "grimoire/sdlutils.h"
#include "grimoire/grid.h"
#include "grimoire/log.h"
#include "grimoire/sdlrendererimpl.h"

#include "gfx/gfx.h"
#include "librealm/world.h"

#include "gfx/element.h"
#include "pathingsystem.h"

using namespace std;

template <class T>
class GridElement : gfx::Element {
public:
    
    using Cell = typename util::grid<T>::cellvalue;
    using Grid = typename util::grid<T>;
    
    GridElement(const Grid &grid);
    GridElement(u16 cols, u16 rows);

    static Unique<GridElement> create(u16 cols, u16 rows) {
        return Unique<GridElement> { new GridElement(cols, rows) };
    }

    const Grid& grid() const {
        return _grid;
    }

    void draw_grid(RenderApi &renderer)
    {
        auto commands = vector<line_def> (_grid.width() + _grid.height());

        auto i = 0;
        for(auto row=0; row<_grid.height(); ++row, ++i) {
            commands[i] = {
                vec2f(),
                vec2f(),
                _line_color
            };
        }

        for(auto col=0; col<_grid.width(); ++col) {
            commands[i] = {
                vec2f(),
                vec2f(),
                _line_color
            };
        }

        renderer.batch_lines(&commands.front(),commands.size());
    }
    
    void draw(RenderApi &r) override {
        draw_grid(r);
    }

    Cell& cell(int idx) {
        return _grid.cell(idx);
    }
    Cell& cell(int x, int y);
    
    void place_item(Cell &cell);

    void add_visited(vec2<int> location) {
        _visited.push_back(location);
    }

    void clear_visited() {
        _visited.clear();
    }
        
private:

    colorf _line_color = { 0.5f, 0.5f, 0.0f, 1.0f };
    Grid _grid;

    vector<vec2<int>> _visited;
};

auto running    = true;
bool new_position = false;
vec2i start_location = { 0, 0 };
vec2i end_location;

void events()
{
    SDL_Event e;
    while(SDL_PollEvent(&e)) {
        
    }
}

void draw_path(RenderApi &r, Path& path)
{
}
#include "entity/components.h"
#include "componentpool.h"

ComponentPool<Spatial> spatials;

int main()
{
    io::FileSystem::mount("data");
    sdl::init();
    gfx::init();

    auto window     = sdl::window_create("pathfinder test", 800, 600);
    auto world      = Realm::World::create(100);

    world->registerComponent<Spatial>(&spatials);
    world->createEntity(Realm::TypeMask<Spatial>());
    
    auto renderer   = SDL_RenderApi::create(window.get());
    auto pathfinder = new PathingSystem();
    auto mapelem    = GridElement<int>::create(45, 45);


    while(running) {
       auto frame_start = SDL_GetTicks();
       
       events();

       renderer->frame_begin();

       mapelem->draw(*renderer);
       
       if(new_position) {

           pathfinder->start_search(0, end_location);
           auto new_path = pathfinder->path_for(0);           
           draw_path(*renderer, *new_path);
           new_position = false;
       }
       
       renderer->frame_end();

       sdl::limit60hz(SDL_GetTicks() - frame_start);
    }

}


    
