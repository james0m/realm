
#include "catch/catch.hpp"

#include "grimoire/rect.h"


using recti = rect<int>;


TEST_CASE("basic rect properties", "[properties]") {
    auto r = rect<int> { 0, 0, 12, 12 };

    REQUIRE( r.valid()       == true );
    REQUIRE( r.topleft()     == vec2<int>(0,0)   );
    REQUIRE( r.topright()    == vec2<int> (12, 0) );
    REQUIRE( r.bottomleft()  == vec2<int> (0, 12) );
    REQUIRE( r.bottomright() == r.dimensions()    );
    REQUIRE( r.center()      == vec2<int> (6, 6)  );
    REQUIRE( r.top()         == 0);
    REQUIRE( r.bottom()      == 12 );
    
    auto r2 = rect<int> { 0, 0, -12, -2 };
    
    REQUIRE (r2.valid() == false);
}
TEST_CASE("rect collisions")
{

    {
        recti r1 ( 5, 5, 10, 10);
        recti r2 ( 7, 0, 5, 20);

        REQUIRE( r1.collide(r2) );
    }

    {
        recti r1 ( 0, 0, 25, 25);
        recti r2 ( 5, 5, 5, 5);

        REQUIRE( r1.collide(r2));
    }
}
/*
void test_collision() {
    recti r1 = { 0, 0, 2, 2 };
    recti r2 = { 0, 0, 2, 2 };
    recti r3 = { 3, 3, 4, 4 };

    assert ( r1.collide(vec2<int>(1,1)));
    assert ( r1.collide(r2) == true);
    assert ( r1.collide(r3) == false);

    cout << "test_collision passed"  << endl;
}
void test_contains() {
    rect<int> r1 = { 0, 0, 12, 12 };
    rect<int> r2 = { 3, 3, 4, 4};

    assert (r1.contains(r2));

    // check for false positive where there is no intersection
    rect<int> r3 = { 1, 1, 2, 2};
    rect<int> r4 = { 4, 4, 2, 2};
    assert (!r3.contains(r4));
    
    // check for false positive where an intersection occurs
    rect<int> r5 = { 1, 1, 2, 2};
    rect<int> r6 = { 2, 2, 4, 4};
    assert (!r5.contains(r6));
    
    cout << "test_contains passed" << endl;
}

void test_intersection() {
    rect<int> r1 = { 0, 0, 5, 5};
    rect<int> r2 = { 1, 1, 2, 2};

    recti r3 = r1.intersection(r2);
    recti r4 = r2.intersection(r1);
    
    assert (r3.collide(r1));
    assert (r4.collide(r1));

    assert (r1.contains(r3));
    assert (r4.contains(r2));

    // test for false-positives
    rect<int> r5 = { 6, 0, 12, 12};
    rect<int> no_intersect = r5.intersection(r1);
    assert ( no_intersect.valid() == false );


    // a smaller rect that is completely enclosed in another rect
    // should produce an intersection that is equal to itself
    rect<int> r6(0, 0, 25, 25);
    rect<int> r7(5,5,5,5);
    auto intersected = r6.intersection(r7);
    assert ( intersected == r7 );
    
    cout << "test_intersection passed" << endl;
}

void test_unions() {

    rect<int> r1 = { 0, 0, 5, 5 };
    rect<int> r2 = { 5, 5, 5, 5 };

    auto r3 = union_rect(r1, r2);

    assert( r3 == rect<int>(0, 0, 10, 10) );


    r1 = { 5, 5, 1, 1 };
    r2 = { 0, 0, 6, 6 };

    r3 = union_rect(r1, r2);
    assert( r3 == r2);
    cout << "test_unions passed" << endl;
}
int main()
{
    test_properties();
    test_contains();
    test_collision();
    test_intersection();
    test_unions();
    
    return 0;
}
*/
