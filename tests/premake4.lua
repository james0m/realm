

project "unit_tests"
kind "ConsoleApp"
language "C++"

files {
    "unit_tests.cpp",
    "test_rects.cpp",
    "test_world.cc",
    "test_file.cpp"
}
links { "grimoire", "SDL2", "physfs" }

configuration "linux"
links { "pthread" }

configuration "windows"
links { "SDL2main" }

configuration "debug"
defines { "DEBUG" }
flags { "Symbols" }

configuration "release"
defines { "NDEBUG" }
flags { "Optimize" }


project "test_mapreader"
    kind "ConsoleApp"
    language "C++"

    files { "testmapreader.cc" }
    links { "grimoire", "physfs", "SDL2"}
    configuration "linux"
    configuration "windows"
        links { "SDL2main" }
    configuration "debug"
        defines { "DEBUG" }
        
project "test_mapnode"
	kind "ConsoleApp"
	language "C++"

	files { "test_mapnode.cpp" }
	links { "grimoire", "physfs", "SDL2" }

	configuration "linux"

	configuration "windows"
		links { "SDL2main" }

	configuration "debug"
			defines { "DEBUG" }
			flags { "Symbols" }

		configuration "release"
			defines { "NDEBUG" }
			flags { "Optimize" }


	project "box1"
		kind "consoleapp"

		files {
			"box1.cc"
		}

		links { "grimoire", "SDL2"  }

		configuration "linux"
			links { "GL" }
			
		configuration "windows"
			links { "SDL2main", "opengl32" }
			
		configuration "debug"
				defines { "DEBUG" }
				flags { "Symbols" }

		configuration "release"
			defines { "NDEBUG" }
			flags { "Optimize" }



	project "box2"
		kind "consoleapp"
		files {
			"box2.cc"
		}

		links { "grimoire", "SDL2", "physfs" }

		configuration "linux"
			links { "GL" }
			
		configuration "windows"
			links { "SDL2main", "opengl32" }

		configuration "debug"
				defines { "DEBUG" }
				flags { "Symbols" }

		configuration "release"
			defines { "NDEBUG" }
			flags { "Optimize" }


	project "box3"
		kind "consoleapp"
		files {
			"box3.cc"
		}

		links { "grimoire", "SDL2", "physfs", "GL"}

		configuration "windows"
			links { "SDL2main", "opengl32" }

		configuration "debug"
				defines { "DEBUG" }
				flags { "Symbols" }

		configuration "release"
			defines { "NDEBUG" }
			flags { "Optimize" }


        project "testlogger"
		kind "consoleapp"
		files {
			"logtest.cpp"
		}

		links { "grimoire" }

		configuration "windows"
			links { "SDL2main", "opengl32" }

		configuration "debug"
				defines { "DEBUG" }
				flags { "Symbols" }

		configuration "release"
			defines { "NDEBUG" }
			flags { "Optimize" }            
            

    project "testemitters"
		kind "consoleapp"
		files {
			"test_emitter.cpp"
		}

		links { "grimoire", "physfs", "SDL2" }

		configuration "windows"
			links { "SDL2main", "opengl32" }

		configuration "debug"
				defines { "DEBUG" }
				flags { "Symbols" }

		configuration "release"
			defines { "NDEBUG" }
			flags { "Optimize" }
                                


project "testfontlib" 
kind "consoleapp"
files {
   "testfont.cpp",
   "../src/textbox.cpp"
}
links { "grimoire", "SDL2", "freetype", "physfs" }

configuration "windows"
links { "SDL2main", "opengl32" }

configuration "debug"
defines { "DEBUG" }
flags { "Symbols" }

project "test_texture_updates" 
kind "consoleapp"
files {
   "test_texture_updates.cpp"
}
links { "grimoire", "SDL2", "physfs" }

configuration "windows"
links { "SDL2main", "opengl32" }

configuration "debug"
defines { "DEBUG" }
flags { "Symbols" }


project "imginfo" 
kind "consoleapp"
files {
   "imginfo.cpp"
}
links { "grimoire", "SDL2",  "physfs" }

configuration "windows"
links { "SDL2main", "opengl32" }

configuration "debug"
defines { "DEBUG" }
flags { "Symbols" }


