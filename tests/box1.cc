
#include <iostream>
#include "SDL.h"

#include "interpolation.h"

#ifdef _WIN32
#undef main
#endif

#include <vector>

using namespace std;


struct LerpState {
	int x_start = 0;
	int x_end   = 0;
	int y_start = 0;
	int y_end   = 0;

	unsigned start_t = 0,
	         end_t   = 0;

	float t  = 0;
};

struct Box {
	bool in_motion = false;
	SDL_Rect bounds = { 0,0, 36,36 };
	SDL_Color color;
};


int main() {
	
	SDL_Window *win;
	SDL_Renderer *ren;

	SDL_CreateWindowAndRenderer(640,480, 0, &win, &ren);

	Box box;
	LerpState lerp;

	box.bounds.x = 0;
	box.bounds.y = 0;
	box.bounds.w = 36;
	box.bounds.h = 36;
	
	box.color.r = 255;
	box.color.g = 0;
	box.color.b = 0;
	box.color.a = 255;

	bool run = true;

	unsigned t;

    int loop_time, loop_interval;
    constexpr int FPS = static_cast<int>(1000.0f / 60.0f);
    
	while(run) {
        loop_time = SDL_GetTicks();
		SDL_Event e;
		while( SDL_PollEvent(&e)) 
		{
			
			switch (e.type) {
				case SDL_QUIT: run = false; break;
				case SDL_KEYDOWN: {

					if( e.key.keysym.sym == SDLK_ESCAPE) 
						run = false;
					break;
				}

				case SDL_MOUSEBUTTONDOWN: {
					box.in_motion = true;

					lerp.x_start = box.bounds.x;
					lerp.x_end   = e.button.x;
					
					lerp.y_start = box.bounds.y;
					lerp.y_end   = e.button.y;

					lerp.start_t = t;
					lerp.end_t   = t + 500; // two seconds from pt a to b, regardless of distance
				}
			}
		}

		t = SDL_GetTicks();

		if (box.in_motion) {

			lerp.t = (float(t) - lerp.start_t) / (lerp.end_t - lerp.start_t);

			if( lerp.t > 1.0)
				lerp.t = 1.0;
			
			box.bounds.x = interpolate(linear, lerp.x_start, lerp.x_end, lerp.t);
			box.bounds.y = interpolate(quad_out, lerp.y_start, lerp.y_end, lerp.t);
			
			cout << "----------------------------" << endl
			     << "t: " << lerp.t << endl
			     << "x: " << box.bounds.x << endl
			     << "y: " << box.bounds.y << endl;

			if( lerp.t == 1.0) {
				box.in_motion = false;
			}
		}

		SDL_SetRenderDrawColor(ren, 128,128,128, 255);
		SDL_RenderClear(ren);

		SDL_SetRenderDrawColor(ren, box.color.r, box.color.g, box.color.b, box.color.a);
		SDL_RenderFillRect(ren, &box.bounds);
		SDL_RenderPresent(ren);

        loop_interval = SDL_GetTicks() - loop_time;
        
        if (loop_interval < FPS) {
            SDL_Delay(FPS - loop_interval);
        }
	}

	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);
	SDL_Quit();
	return 0;
}
