#include <stdio.h>
#include <cassert>
#include <memory>
#include <vector>

#include "catch/catch.hpp"

#include "grimoire/typelib.hpp"
#include "grimoire/rect.h"
#include "grimoire/grimoire_util.h"

#include "grimoire/componentpool.h"
#include "grimoire/ecs.hpp"


using World = ecs::Registry;
using namespace ecs;

const idtype VOID_ENTITY = -1;
struct Player  {
	String name;
};

struct Spatial {
	idtype map;
	vec2i  position;
};

struct Sprite  {
	idtype texture;
	rectf  region;
	vec2i  offset;
};

struct Unit  {
	idtype owner;
	idtype unittype;

	int atk    = 1;
	int def    = 1;
	int weapon = 0;
	int speed  = 1;

	int moves = 1; //number of remaining move points for the turn
};

struct Settlement  {
	idtype owner;
	String name;
	
	int    population      = 1;
	
	int    minerals        = 0;
	int    nutrients       = 0;
	int    coin            = 0;
	int    ki              = 0;

	int    mineral_growth  = 0;
	int    nutrient_growth = 0;
	int    income          = 0;
	int    ki_growth       = 0;

	int    mineral_usage   = 0;
	int    nutrient_usage  = 0;
	int    expenses        = 0;
	int    ki_usage        = 0;

	idtype in_production   = VOID_ENTITY;
};


const int QUEUE_SIZE = 10;

struct BuildQueue {
	idtype queue[QUEUE_SIZE];
};

void on_entity_created(const EntityCreated &e){
	printf("Entity number %u was created\n", e.eid);
} 
void on_entity_destroyed(const EntityDestroyed &e){
	printf("Entity number %u was destroyed\n", e.eid);

} 
void on_component_attached(const ComponentAttached &e) {
    printf("Component: %u attached to Entity: %u\n", e.cid, e.eid);
}

void on_component_detached(const ComponentDetached &e) {
    printf("Component: %u detached from Entity: %u\n", e.cid, e.eid);
}

using SpatialPool = ComponentPool<Spatial>;
using SpritePool  = ComponentPool<Sprite>;
using UnitPool    = ComponentPool<Unit>;

TEST_CASE("world creation and component access", "[world]") {
	
    auto world    = util::make_unique<World>();
    auto spatials = util::make_unique<SpatialPool>();
    auto sprites  = util::make_unique<SpritePool>();
    auto units    = util::make_unique<UnitPool>();

    world->entity_created.connect(on_entity_created);
    world->entity_destroyed.connect(on_entity_destroyed);
    world->component_attached.connect(on_component_attached);
    world->component_detached.connect(on_component_detached);
    
    world->register_component<Spatial>(spatials.get());
    world->register_component<Sprite>(spatials.get());
    world->register_component<Unit>(units.get());

	world->entity_created.connect(&on_entity_created);
	world->entity_destroyed.connect(&on_entity_destroyed);
	world->component_attached.connect(&on_component_attached);
	world->component_detached.connect(&on_component_detached);

    ComponentMask components = tl::type_mask<Spatial, Sprite, Unit>();

	idtype entity_a = world->create_entity( components );
	idtype entity_b = world->create_entity( components );
	idtype entity_c = world->create_entity( components );

    REQUIRE( world->has_component(entity_a, tl::type_id<Spatial>()) );
    REQUIRE( world->has_component(entity_a, tl::type_id<Unit>())    );

    REQUIRE( world->has_component(entity_b, tl::type_id<Spatial>()) );
    REQUIRE( world->has_component(entity_b, tl::type_id<Unit>())    );

    REQUIRE( world->has_component(entity_c, tl::type_id<Spatial>()) );
    REQUIRE( world->has_component(entity_c, tl::type_id<Unit>()));

    // set a->spatial.x to 3 and test    
    world->component<Spatial>(entity_a)->position = {3, 3};    
    Spatial *sp = ecs::component_cast<Spatial>(world->component(entity_a, tl::type_id<Spatial>()));

    REQUIRE (sp->position.x == 3);
    REQUIRE (sp->position.y ==3);
    
	world->destroy_entity(entity_a);
	world->destroy_entity(entity_b);
	world->destroy_entity(entity_c);

    REQUIRE( world->entity_exists(entity_a) == false);
    REQUIRE( world->entity_exists(entity_b) == false);
    REQUIRE( world->entity_exists(entity_c) == false);
}
