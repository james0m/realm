
#include "../src/grimoire/file.h"
#include "../src/grimoire/filesystem.h"
#include <cassert>
#include <iostream>
#include <sstream>

#include "catch/catch.hpp"

#include "../src/grimoire/ioutils.h"

using namespace std;

TEST_CASE("basic file io", "[file io]")
{
	io::FileSystem::mount(io::exe_dir());
    auto srcfile = io::open("test_file.cpp", io::ACCESS_READ);

    REQUIRE( srcfile->exists() );
	
    REQUIRE( srcfile->name() == "test_file.cpp");
    REQUIRE( srcfile->ext() == "cpp");

    auto orig_size = srcfile->size();
	srcfile->close();

	auto newfile = io::mkfile("newfile.cc");

    REQUIRE( newfile->exists());
    REQUIRE( newfile->name() == "newfile.cc");

	newfile->writestr("I'm a new file!\n");
	newfile->writestr("This should be a second line!\n");

	// close the file, done writing
	newfile->close();
    REQUIRE( io::exists("newfile.cc") );

	// reopen the file for reading
	newfile = io::open("newfile.cc", io::ACCESS_READ);

    auto buf = Unique<char[]>(new char[newfile->size() + 1]);

    newfile->reset(); // move read head back to 0
    newfile->read(buf.get(), newfile->size());
    buf[newfile->size()] = '\0';

    REQUIRE( String(buf.get()) == "I'm a new file!\nThis should be a second line!\n" );
}
