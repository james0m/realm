

solution "Realm"
	configurations {"debug", "release"}

	-- global includes
	includedirs {
        "src",
		"src/3p",
		"src/3p/fastdelegates",
		"src/3p/rapidxml",
		"src/grimoire"
	}

	libdirs {
		"src/grimoire"
	}

	language "C++"


	configuration "linux"
        buildoptions {   -- assuming g++ or clang++
           "-Wall",
           "-Wno-reorder",
           "-Wno-unused-local-typedef",
           "-std=c++11"

        }

		includedirs {
			"/usr/include/SDL2",
			"/usr/local/include/SDL2",
            "/usr/include/freetype2"
		}
		libdirs {
			"/usr/lib",
			"/usr/local/lib/"
		}

	configuration "windows"
		includedirs {
			"C:/local/boost_1_58_0",
			"src/3p/SDL2/include"
		}
		libdirs {
			"src/3p/SDL2/lib/x86",
			"src/3p/SDL2_image/lib/x86",
			"src/3p/SDL2_ttf/lib/x86"
		}

    local buildgame   = true;
    local buildtests  = false;

    if buildgame then
       include "src/grimoire"
       include "src"
    end

    if buildtests then
		include "tests"
	end
