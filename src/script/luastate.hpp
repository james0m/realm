#ifndef _luastate_h_
#define _luastate_h_

#include "grimoire/grimoire_util.h"
#include "scripting.hpp"


/**

LuaState wraps the lua api lua_State object; As such, it
is the gateway through which native engine code communicates
to scripts and vice-versa.

This class should be treated as a stack of values that can 
be of any type defined in the LuaDataType enum. Accessing
values, running functions, passing arguments all must be 
done by pushing onto the stack in the correct order.

The scripting environment will rely on  at least one of 
these at all times to read config files and compile and 
execute scripts.

 */
class LuaState {
public:
    static constexpr const char* TAG = "LuaState";
    LuaState();
    virtual ~LuaState();

    /**
       queries for type of value at (pos) of stack*/
    bool is_number() const;
    bool is_string() const;
    bool is_table() const;

    /** set the stack index position, most other methods will
        rely on this position being set correctly to work */
    void stack_pos(int pos);

    /** pop the top of the stack */
    void pop(int n=1);


    
    /** 
     * returns the type of value on the top of the stack */
    LuaDataType get_type() const;

    const char* type_name() const;
    /** 
     * fetch a global by name and push onto the stack */
    void        get_global(const char* name);
    
    const char* get_string() const;
    int         get_int() const;
    float       get_float() const;

    /**
     * push a string onto the lua stack. 
     *
     * the string will be copied into lua's memory, 
     * so it is safe to pass temporaries
     */
    void push_string(const char* str);
    void push_string(const String& str);

    /** 
     * push an integer onto the stack
     */
    void push_int(int value);
    
    
    /* access a lua table field, if a table is currently on top */
    void get_field(const char* key) const;

    /* pull an error off of the top of the stack, if any */
    const char* get_error() const;

    /* run code */
    void        exec(const char* code);
    void        exec(const String& code);

    /* access the raw lua_State object */
    lua_State*  raw() { return vm; }

private:
    void handle_lua_error(int errcode);
    
private:    
    LuaState(const LuaState& );
    LuaState& operator=(const LuaState&);

    int        _pos = -1;
    lua_State* vm;
};


#endif //_luastate_h_
