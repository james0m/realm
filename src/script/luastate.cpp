#include <cassert>
#include <cstring>

extern "C" {
#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"
}
#include "grimoire/log.h"

#include "scripterror.hpp"
#include "luastate.hpp"

LuaState::LuaState() {
    vm = luaL_newstate();
    if (!vm)
        throw LuaInitError();
    
    luaL_openlibs(vm);
}

LuaState::~LuaState() {
    lua_close(vm);
    vm = nullptr;
}

bool LuaState::is_number() const {
    return lua_isnumber(vm, _pos) ? true : false;
}

bool LuaState::is_string() const {
    return lua_isstring(vm, _pos) ? true : false;
}

bool LuaState::is_table() const {
    return lua_istable(vm, _pos) ? true : false;
}

void LuaState::stack_pos(int pos) {
    _pos = pos;
}

void LuaState::get_global(const char* name) {
    lua_getglobal(vm, name);
}

int LuaState::get_int() const {
    return lua_tointeger(vm, _pos);
}

float LuaState::get_float() const {
    return static_cast<float>(lua_tonumber(vm, _pos));
}

LuaDataType LuaState::get_type() const {
    return static_cast<LuaDataType>(lua_type(vm, -1));
}

const char* LuaState::type_name() const {
    return lua_typename(vm, lua_type(vm,_pos));
}

const char* LuaState::get_error() const {
    return is_string() ? get_string() : "";
}

const char* LuaState::get_string() const
{
    return get_type() == TSTRING ? lua_tolstring(vm, _pos, NULL) : "";
}

void LuaState::get_field(const char* key) const
{
    assert(is_table() && "getTableField called on stack object that is not a table");
    lua_pushstring(vm, key);
    lua_gettable(vm, -2);
    lua_pop(vm, 2);
}

void LuaState::pop(int n) {
    lua_pop(vm, n);
}

void LuaState::push_string(const char* str) {
    lua_pushstring(vm, str);
}

void LuaState::push_string(const std::string& str) {
    push_string(str.c_str());
}

void LuaState::push_int(int value) {
    lua_pushinteger(vm, static_cast<lua_Integer>(value));
}

void LuaState::handle_lua_error(int errcode)
{
    if (errcode == LUA_YIELD) {
        Log::e(TAG, "LUA_YIELD");
        assert(0);
    }
    else if (errcode == LUA_ERRRUN) {
        auto lua_err_msg = get_error();

        // @TODO: can we get a backtrace? Or, at least a fucking line number???
        Log::e(TAG, sl::format("LuaRunTimeError %s", lua_err_msg));
        throw LuaRuntimeError(*this, lua_err_msg);
    }
    else if (errcode == LUA_ERRSYNTAX) {
        auto lua_err_msg = get_error();
        Log::e(TAG, sl::format("LuaSyntaxError %s", lua_err_msg));
        throw LuaSyntaxError(*this, lua_err_msg);
    }
    else if (errcode == LUA_ERRMEM) {
        Log::e(LuaState::TAG, "LUA_ERRMEM");
        assert(0);
    }
//    else if (errcode == LUA_ERRGCMM) {
//        Log::e(LuaState::TAG, "LUA_ERRGCMEM");
//        assert(0);
//    }
}

void LuaState::exec(const char* code)
{
    auto errcode = luaL_loadstring(vm, code);
    if (errcode) {
        handle_lua_error(errcode);
    }
    
    errcode = lua_pcall(vm, 0,0,0);
    
    if (errcode)
        handle_lua_error(errcode);
}

void LuaState::exec(const std::string& code)
{
    exec(code.c_str());
    /*
    lua_pushlstring(s_lua_vm, code.c_str(), code.size());
    auto errcode = lua_pcall(s_lua_vm, 0,0,0);
    if (errcode)
        handle_lua_error(errcode);
        */
}


