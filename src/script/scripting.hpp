#ifndef _scripting_h_
#define _scripting_h_

extern "C" {
#include "lua.h"
#include "lauxlib.h"    
#include "lualib.h"    
}

class LuaState;
/** mirror lua's macro-defined  type enums*/
enum LuaDataType
{
    TNONE          = -1,
    TNIL           =  0,
    TBOOLEAN       =  1,
    TLIGHTUSERDATA =  2,
    TNUMBER        =  3,
    TSTRING		   =  4,
    TTABLE         =  5,
    TFUNCTION      =  6,
    TUSERDATA      =  7,
    TTHREAD        =  8,
    TNUMTAGS       =  9
};



#endif //_scripting_h_
