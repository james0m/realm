#ifndef _scripterror_h_
#define _scripterror_h_

#include "lauxlib.h"
#include "grimoire/stringlib.h"

#include "luastate.hpp"


/**

#define LUA_OK		0
#define LUA_YIELD	1
#define LUA_ERRRUN	2
#define LUA_ERRSYNTAX	3
#define LUA_ERRMEM	4
#define LUA_ERRGCMM	5
#define LUA_ERRERR	6

 */

enum LuaErrorCode
{
    L_OK = 0,
    L_YIELD,
    L_RUN,
    L_MEM,
    L_GCMM,
    L_ERR,
    L_NONE
};

static LuaErrorCode errcode_to_enum(int err)
{
    switch (err)
    {
    case 0: return L_OK;
    case 1: return L_YIELD;
    case 2: return L_RUN;
    case 3: return L_MEM;
    case 4: return L_GCMM;
    case 5: return L_ERR;
    default:
        return L_NONE;
    }
}

static const char* err_to_str(LuaErrorCode ecode) {
    switch (ecode) {
    case L_OK: return "L_OK"; 
    case L_YIELD: return "L_YIELD"; 
    case L_RUN: return "L_RUN"; 
    case L_GCMM: return "L_GCMM";
    case L_ERR: return "L_ERR";
    default: return "Unknown Error";
    }
}

class LuaError : public std::exception {
public:
    const LuaState& state() const { return *vm; }
    
protected:
    LuaError():vm(nullptr){}
    LuaError(const LuaState& state)
        :vm(&state)
    {}

protected:
    const LuaState* vm;
    std::string msg;
};
    
struct LuaInitError : public LuaError {
public:
    LuaInitError():
        LuaError()
    {}
    LuaInitError(const LuaState& state):
        LuaError(state)
    {
        msg = sl::format("lua couldn't be initialized: %s\n", vm->get_error());
    }
    
    const char* what() const noexcept override {
        return msg.c_str();
    }
};

class LuaRuntimeError : public LuaError {
public:
    
    LuaRuntimeError(const LuaState& state, int errcode=0)
        :LuaError(state),
         error_code(errcode_to_enum(errcode))
    {
        error_msg = sl::format("LuaRuntimeError: %s", err_to_str(error_code));
    }

    LuaRuntimeError(const LuaState& state, const char* msg)
        :LuaError(state)         
    {
        build_message(type(), msg);
    }
         
    virtual const char* what() const noexcept override {        
        return error_msg.c_str();
    }

public:
    const std::string& message() const {
        return error_msg;
    }

protected:
    virtual const char*  type() const {
        return "RuntimeError";
    }
    
    virtual void build_message(const char* exception_name,
                                             const char* msg)
    {
        error_msg = sl::format("%s: %s", exception_name, msg);
    }
private:
    std::string error_msg;
    LuaErrorCode error_code;
};

class LuaSyntaxError : public LuaRuntimeError {
public:
    LuaSyntaxError(const LuaState& state, const char* msg)
        :LuaRuntimeError(state, msg)
    {
    }

    virtual  const char* type() const {
        return "LuaSyntaxError";
    }
        
};
#endif //_scripterror_h_
