#ifndef _scriptenv_h_
#define _scriptenv_h_

#include "grimoire/filesystem.h"


#include "runtime.hpp"
#include "config.hpp"
#include "scripting.hpp"

namespace script {


constexpr
const char *Script_TAG = "Script";

void
init(Realm::Runtime &rt);

//void
//Script_cleanup();
//
//void
//Script_exec(const char* code);

Config
load_global_config(const io::FileHandle file);

//
//void
//Script_load_global_config(const char* file);

} // namespace script
#endif //_scriptenv_h_
