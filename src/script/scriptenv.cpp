#include "scriptenv.hpp"
#include "scripterror.hpp"
#include "grimoire/log.h"
#include "grimoire/grimoire_util.h"


using Realm::Runtime;

static Runtime *s_runtimne;
static LuaState s_lua_vm;

HashSet<String> lua_globals;


static
Array<String>
get_globals(lua_State* L) {
    lua_pushglobaltable(L);       // Get global table
    lua_pushnil(L);               // put a nil key on stack

    auto results = Array<String>{};
    
    while (lua_next(L,-2) != 0)  
    { 
        results.emplace_back(lua_tostring(L,-2));  // Get key(-2) name
        lua_pop(L,1);               // remove value(-1), now key on top at(-1)
    }
    lua_pop(L,1);                 // remove global table(-1)
    return results;
}

#if 0
static void print_globals(lua_State* L)
{
    stringstream ss;
    auto globals = get_globals(L);
    for(auto s : globals) ss << s << endl;        
    Log::i(Script_TAG, sl::format("lua globals:\n%s", ss.str()));
}
#endif

namespace script {

    void
    init(Runtime &rt) {
        s_runtimne = &rt;

        // make a blacklist of all of LUA's global names
        auto globals = get_globals(s_lua_vm.raw());
        lua_globals.insert(begin(globals), end(globals));
    }

//void
//Script_cleanup(){
//    lua_globals.clear();
//}
//
//void
//Script_exec(const char* code) {
//    s_lua_vm.exec(code);
//}

//int conf_declare(lua_State *L) {
//    Log::i(Script_TAG, "lua script called conf_declare");
//    return 0;
//}
//
//int err_callback(lua_State *L) {
//    Log::e(Script_TAG, "err_callback");
//    return 0;
//}

Config
load_global_config(const io::FileHandle file) {
    auto conf = Config();
    const auto s = file->as_string();
    s_lua_vm.exec(s);

    lua_State *L = s_lua_vm.raw();
    for (const auto &g: get_globals(L)) {
        if (lua_globals.find(g) != end(lua_globals)) {
            continue;
        }

        // g is not a std lua global, must be a config var!
        s_lua_vm.get_global(g.c_str());
        if (s_lua_vm.is_table()) {
            lua_pushnil(L);
            while (lua_next(L, -2) != 0) {
                Any val;
                switch (s_lua_vm.get_type()) {
                    case TSTRING: {
                        val = s_lua_vm.get_string();
                        break;
                    }
                    case TNUMBER: {
                        val = s_lua_vm.get_float();
                        break;
                    }
                    case TBOOLEAN:
                    case TFUNCTION:
                    case TUSERDATA:
                    case TTHREAD:
                    case TNIL:
                    case TNONE:
                    default: {
                        Log::e(Script_TAG, sl::format("received an unexpected type: %s", s_lua_vm.type_name()));
                        continue;
                    }
                }

                const auto key = sl::format("%s.%s", g, lua_tostring(L, -2));
                conf.put(key, val);
                lua_pop(L, 1);
            }
        } else if (s_lua_vm.is_number()) {
            auto var = static_cast<float>(s_lua_vm.get_float());
            conf.put(g, var);
        } else if (s_lua_vm.is_string()) {
            auto var = s_lua_vm.get_string();
            conf.put(g, var);
        } else {
            auto tname = s_lua_vm.type_name();
            Log::e(Script_TAG, sl::format(
                    "%s is not a supported config variable type", tname));
        }
    }
    return conf;
}


} // namespace script