#ifndef _combat_h_
#define _combat_h_


#include "events.hpp"
#include "runtime.hpp"

enum Weapon_Type
{
    W_INSTANT,     // rifles, melee, attacks
    W_PROJECTILE  // arrows, missiles, bullets    
};

struct Weapon
{
    idtype id;
    Weapon_Type type;
    events::DamageType damage_type;
    int range;
    int damage;
    int cooldown_time;
};

namespace Combat
{

static constexpr const char* TAG = "Combat";

void init(Realm::Runtime& rt);
void cleanup();
void begin_attack(idtype attacker, idtype weapon);
    
void resolve_attack(idtype attacker, idtype target);
    
void resolve_melee_attack(idtype attacker, idtype target);
    
// ranged attack using a specific entity as a target
void resolve_ranged_attack(idtype attacker, idtype target);

// ranged attack form using a map location or world position as the target
void resolve_ranged_attack(idtype attacker, vec2f target);

// queue up a damage event
void report_damage(idtype victim, events::DamageType dt, int hp_loss);

void frame(World* world);

/**
 *  check if the attacker's current weapon allows for initiating an attack
 * current range
 */
bool check_in_weapon_range(vec2f attacker_location, vec2f target_locatoin, idtype weapon_id);

/**
 * check if attacker has target in unobstructed view
 */
bool check_LOS(idtype attacker, idtype target);

/**
 *
 */
void projectile_impact(idtype projectile);

idtype spawn_projectile(idtype weapon_id, vec2f start_point, vec2f direction);

const Weapon& get_weapon(idtype w);
}
#endif
