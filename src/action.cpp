#include "action.h"

const char* action_string(ActionId action)
{
    switch(action) {
        
    case A_CANCEL: return "Cancel";
    case A_SHOW_MENU: return "Show Menu";
    case A_UNIT_MOVE: return "Move";
    case A_UNIT_STOP: return "Stop";
    case A_UNIT_ATTACK: return "Attack";
    case A_UNIT_TRAIN: return "Train";
    case A_BUILD_STRUCTURE: return "Build";
    case A_TOGGLE_CONSOLE: return "Toggle Console";
    case A_NULL: return "Null";
    case DEBUG_ENABLE_MAP_GRID: return "Toggle Map Grid";
    default: return "Unknown Action";
    }
}

namespace actions {
    signal<void> toggle_console;
    signal<void> toggle_map_grid;
    signal<void>  toggle_terrain;
    signal<Action_UnitMove> unit_move;
    signal<Action_UnitStop> unit_stop;
    signal<Action_UnitAttack> unit_attack;

    signal<vec2i> scroll_map;

    signal<Realm::Prototype> spawn_entity;
    signal<idtype>        kill_entity;

    Array<Action_TrainUnit>      build_unit;
    Array<Action_BuildStructure> build_structure;
}



