#include "combat.hpp"
#include "entity/world.hpp"

using Realm::Runtime;
using events::DamageType;

namespace events {

Array<Attack> entity_attacks; // inbound event
Array<Damage> entity_damages; // outbound
Array<Killed> entity_deaths;

}

namespace Combat
{


static Realm::Runtime* _rt;
static World* _world;

static Weapon  _weapons[8];

static HashSet<idtype> projectiles_table;

static u32 last_time = 0;





void init(Runtime& rt)
{
    _rt = &rt;
    _world = rt.world();

    Weapon rifle;
    rifle.id = 1;
    rifle.cooldown_time = 250;
    rifle.damage = 10;
    rifle.range = 240;

    rifle.type = Weapon_Type::W_INSTANT;
    rifle.damage_type = DamageType::NORMAL;

    Weapon rocket_launcher;
    rocket_launcher.id = 2;
    rocket_launcher.cooldown_time = 5000;
    rocket_launcher.range = 40;
    rocket_launcher.damage = 100;
    rocket_launcher.type = Weapon_Type::W_PROJECTILE;
    rocket_launcher.damage_type = DamageType::EXPLOSIVE;

    _weapons[rifle.id] = rifle;
    _weapons[rocket_launcher.id] = rocket_launcher;
}

void cleanup()
{
    projectiles_table = HashSet<idtype>();

    _rt = nullptr;
    _world = nullptr;
}

static bool can_attack(idtype attacker)
{
    return _world->vitals[attacker].weapon_cooldown == 0;
}

void begin_attack(idtype attacker, idtype target)
{
    const auto& body_a = _world->bodies[attacker];
    const auto& body_b = _world->bodies[target];
    const auto& vitals = _world->vitals[attacker];

    if (can_attack(attacker) && check_in_weapon_range(body_a.location, body_b.location, vitals.weapon)) {
        events::entity_attacks.emplace_back(attacker, target);
    }
}


bool check_in_weapon_range(vec2f attacker_location, vec2f target_location, idtype weapon_id)
{
    const auto& weapon = _weapons[weapon_id];
    return point_in_circle(attacker_location, float(weapon.range), target_location);
}

bool check_LOS(idtype attacker, idtype target)
{
    return true;
}

void resolve_attack(idtype attacker, idtype target)
{
    auto attacker_unit   = &_world->units[attacker];
    auto target_unit     = &_world->units[target];
    auto attacker_vitals = &_world->vitals[attacker];

    // get weapon info
    const auto& weapon = _weapons[attacker_vitals->weapon];

	// check for weapon range
    if (!check_in_weapon_range(attacker_unit->location, target_unit->location, weapon.id))
    {
        Log::d(TAG, "unit attack out of range");
        return;
    }

    auto target_vitals   = &_world->vitals[target];

    if (weapon.type == W_INSTANT)
    {
        // get attacker's atk rating and target def
        auto base_damage = weapon.damage;
        auto target_def = target_vitals->defense;

        auto damage = base_damage - base_damage * target_def;
        auto prev_hp = target_vitals->hp;

        target_vitals->hp = prev_hp > damage ? prev_hp - damage : 0;

        attacker_vitals->weapon_cooldown = weapon.cooldown_time;
        events::entity_damages.emplace_back(target,
                                            weapon.damage_type,
                                            damage, target_vitals->hp);
    }

    else // projectiles mean we have to make an entity
    {
        if (weapon.type == W_PROJECTILE)
        {
            const auto& attacker_body = _world->bodies[attacker];
            //s_world->spawn_entity("", unit.location);
            attacker_vitals->weapon_cooldown = weapon.cooldown_time;
            spawn_projectile(weapon.type, attacker_body.location, norm(attacker_body.vel));
        }
    }

    attacker_vitals->weapon_cooldown = _weapons[attacker_vitals->weapon].cooldown_time;
}


void frame(World* world)
{
    events::entity_damages.clear();
    events::entity_deaths.clear();

    for(auto& collision : events::collisions)
    {
        idtype projectile_entity = NULL_ID;

        if ( projectiles_table.count(collision.e1) )
        {
            projectile_entity = collision.e1;
        }

        else if (projectiles_table.count(collision.e2))
        {
            projectile_entity = collision.e2;
        }

        if (projectile_entity != NULL_ID)
            projectile_impact(projectile_entity);
    }

    for(auto& attack : events::entity_attacks)
    {
        // resolve combat if in weapon range
        resolve_attack(attack.attacker, attack.target);
    }

    events::entity_attacks.clear();

    for(auto& damage : events::entity_damages)
    {
        if(damage.current_hp == 0)
        {
            _world->kill_entity(damage.victim);
            events::entity_deaths.emplace_back(damage.victim);
        }

    }

    auto t_delta = SDL_GetTicks() - last_time;
    for(auto& vitals : _world->vitals )
    {
        if(vitals.entity == NULL_ID)
            continue;

        else if (vitals.weapon_cooldown > 0)
        { // update weapon cooldown timers
            auto wc = vitals.weapon_cooldown;
            wc = wc >= t_delta ? wc - t_delta : 0;

            vitals.weapon_cooldown = wc;
        }

    }

    last_time = SDL_GetTicks();

}

idtype spawn_projectile(idtype weapon_id, vec2f start_point, vec2f direction)
{
    return NULL_ID;
}

void report_damage(idtype victim, DamageType dt, int hp_loss)
{
    events::entity_damages.emplace_back(victim,
                                        dt,
                                        hp_loss,
                                        _world->vitals[victim].hp);
}

#define THING_EXPLODES 1

void projectile_impact(idtype projectile)
{
    auto& body = _world->bodies[projectile];

    Log::d(TAG,
           sl::format("projectile impact @ (%d, %d)", body.location.x, body.location.y));

    //auto& quad = s_world->qtree.at_point(body.location);

    if (THING_EXPLODES)
    {
        // do splash damage in this quadrant
        // check for neighboring quadrant intersection and test those too
    }
    else
    {
        // check for hit
    }
}


const Weapon& get_weapon(idtype w)
{
    return _weapons[w];
}
} // namespace Combat
