#ifndef _unitcontrol_h_
#define _unitcontrol_h_

#include "grimoire/tiledmap.h"
#include "grimoire/interpolation.h"

#include "grimoire/ecs.hpp"
#include "grimoire/componentpool.h"

#include "forward.hh"
#include "runtime.hpp"
#include "action.h"



namespace Units {
static constexpr const char* TAG = "UnitControl";

void
init(Realm::Runtime& rt);

void
cleanup();

void
frame();

void
on_unit_move(const Action_UnitMove &data);

void
on_unit_attack(const Action_UnitAttack &data);
//    void on_unit_defend(const Action_UnitDefend &data);
void
on_unit_stop(const Action_UnitStop &stop);

void
stop_entity(idtype entity);

idtype
start_search(idtype entity, vec2f target);

const Path*
path_for(idtype entity);

const char*
state_string(UnitState state);

/**
 * assign the actual pathfinding function to call
 * during frame()
 */
void
set_algorithm(const sl::string& algo);

}
#endif //_unitcontrol_h_
