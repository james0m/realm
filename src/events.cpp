#include "events.hpp"

namespace events {

Array<Collision> collisions;
Array<TerrainCollision> terrain_collisions;
signal<idtype> entity_spawned;
signal<EntityMotion> entity_motion;
Array<EntityMotion> motion_events;

signal<PathCompleted> path_complete;
Array<PathCompleted> completed_paths;
signal<ViewResize> view_resized;

}
