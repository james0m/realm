#ifndef COMPONENTS_H_
#define COMPONENTS_H_

#include "grimoire/stringlib.h"
#include "grimoire/element.h"
#include "grimoire/color.h"
#include "grimoire/interpolation.h"


#include "common.h"
#include "forward.hh"


enum Player_Type {
    HUMAN,
    AI
};

enum Location_Type
{
    LOCAL,
    NET
};

enum Player_Tech {
     HAVE_HQ,
     HAVE_BARRACKS,
     MASS_DRIVER_WEAPONS,
     BEAM_WEAPONS,
     PULSE_WEAPONS,
     SELF_HEALING_METALS,
     HOLO_TARGETING
};

struct Player
{
    idtype               id = -1;
    Player_Type          type;
    Location_Type        location;
    idtype               team;
    
    int                  credits;
    int                  materials;

    Array<idtype>       selected_units;
    Array<idtype>       groups;

    HashSet<Player_Tech> acquired_tech;
};


struct Meta
{
    idtype entity = -1;
    u32 components;
    u32 player;
    u32 team;
    const Realm::Prototype* prototype;
    
};


// map location and object size, weight, direction
struct Spatial
{
    idtype entity = -1;
    MapLocation location;
    float radius;
    float soft_radius;
    
    //float accel;
    vec2f vel;
    float max_speed;

    idtype quad_count = 0;  // count of quadtree indexes we live in currently
    idtype quads[9];

    inline void add_quad_index(idtype index)
    {
        ASSERT(quad_count < 9);
        quads[quad_count++] = index;
    }

    inline void clear_quads()
    {
        quad_count = 0;
    }
};

// hitpoints, special move points, weapon type, etc for units and buildings
struct Vitals {
    idtype       entity = -1;
    u8           type;    // type of entity
    u8           hp;      // current health points
    u8           mp;      // current mana points
    u8           xp;      // experience points
    u8           lvl;     // level or rank of entity
    u8           weapon;    // current weapon in use
    u16          weapon_cooldown;
    float        defense;
};

enum UnitState : unsigned char
{
    IDLE      = 0,
    MOVING    = 1,
    ATTACK    = 2,
    DEFEND    = 3,
    SEEK      = 4,    // goal: an entity that has a body, that we want to close the distance to
    NEED_PATH = 5
};

constexpr u8 UNIT_AI_STATES = 8;

// unit ai structure. stores unit goals and targeting info, as well as AI state
struct Unit
{
    idtype       entity = NULL_ID;
    idtype       team;                   // the faction this entity belongs to
    float        sight_range;           // sight radius in tiles
    vec2f        location;              // position on the world map
    vec2f        last_location;
    vec2f        waypoint;              // a static location to close distance with
    vec2f        goal;                  // entity this unit is focusing on
    idtype       target;
    int          state_idx = -1;        // points to the top of the state stack
    UnitState    state[UNIT_AI_STATES]; // ai state stack

    inline UnitState current_state() const
    {
        return state[state_idx];
    }

    inline void push_state(UnitState s)
    {
        ASSERT(state_idx < UNIT_AI_STATES);
        state[++state_idx] = s;
    }

    inline void pop_state()
    {
        if(state_idx > 0)
            --state_idx;
    }

    inline void clear_state()
    {
        state_idx = 0;
        state[0] = IDLE;
    }
};

enum SpawnType
{
    ST_NONE,
    ST_UNIT,
    ST_STRUCTURE,
    ST_UPGRADE
};

// enables an entity to produce other entities
struct Spawner {
    idtype               entity = -1;
    SpawnType            spawn_type;     // what type of things do i spawn?
    vec2f                spawn_point;    // location where new entities are placed
    int                  builds = 0;     // count of concurrent builds
    int                  max_builds = 1; // maximum allowed builds
    sl::stringlist       options;        // types of entity that can be generated
};


#endif //_components_h_
