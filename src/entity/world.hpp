#ifndef _world_h_
#define _world_h_

#include "grimoire/types.h"
#include "grimoire/typelib.hpp"
#include "grimoire/ecs.hpp"
#include "grimoire/tiledmap.h"
#include "grimoire/gridindexer.hpp"
#include "grimoire/componentpool.h"

#include "forward.hh"

#include "entity/components.h"
//#include "prototype.hpp"


struct World : ecs::Registry {
//    using PrototypeTable = HashMap<String, Realm::Prototype>;
    using SpatialIndex = GridIndexer; //quadtree<idtype, 3>;

    static constexpr const char* TAG = "World";

    SpatialIndex               index;
    Unique<tmx::TiledMap>      map;
    Array<Player>              players;
    
    ComponentPool<Meta>        entity_metadata;
    ComponentPool<Spatial>     bodies;
    ComponentPool<gfx::Sprite> sprites;
    ComponentPool<Unit>        units;
    ComponentPool<Vitals>      vitals;
    ComponentPool<Spawner>     spawners;

    explicit
    World(size_t max_entities=10000);

    World(const World&) = delete;
    World(World&&) = delete;

    World& operator=(const World&) = delete;
    World&& operator=(World&&) = delete;

    ~World() override = default;

    idtype
    spawn_entity(const String& proto_name, vec2f location);

    idtype
    spawn_entity(const Realm::Prototype& proto, vec2f location);

    void
    kill_entity(idtype entity);


    template <class Vec2>
    vec2i
    tile_coords(const Vec2 world_coords) const;

    template <class Vec2>
    Vec2
    world_coords(const Vec2 tile_coords) const;

    const Realm::Prototype*
    prototype_of(idtype entity) const {
        return get<Meta>(entity).prototype;
    }

    /**
     * @brief contains_point
     * check if the given point in world space is on the map
     *
     * @param world_loc
     */
    bool
    contains_point(vec2f world_loc) const;

    /**
     * check if the given tile cell has resistance < 255
     *
     * @param cell vec2 in grid/tile coordinates
     */
    bool
    terrain_is_passable(vec2i cell) const;
    
};


template <class Vec2>
vec2i
World::tile_coords(const Vec2 world_coords) const {
    const vec2i tile_dims = map->tile_dimensions();
    return Vec2(world_coords.x / tile_dims.x,
                world_coords.y / tile_dims.y);
}


template <class Vec2>
Vec2
World::world_coords(const Vec2 tile_coords) const {
    const vec2i tile_dimensions = map->tile_dimensions();
    return Vec2(tile_coords.x * tile_dimensions.x,
                tile_coords.y * tile_dimensions.y);
}


#endif
