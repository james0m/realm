

#include "world.hpp"

//#define QUADTREE_IMPLEMENTATION
//#include "quadtree.hpp"
#define GRID_INDEXER_IMPL
#include "grimoire/gridindexer.hpp"

#include "grimoire/ecs.hpp"


#include "grimoire/sprite.h"
#include "grimoire/log.h"
#include "grimoire/stringlib.h"

#include "asset/assets.hpp"
#include "common.h"
#include "events.hpp"
#include "motion.hpp"
#include "entity/prototype.hpp"



World::World(size_t max_entities):
    Registry(max_entities)
{
    register_component<Meta>(&entity_metadata);
    register_component<Spatial>(&bodies);
    register_component<Sprite>(&sprites);
    register_component<Unit>(&units);
    register_component<Vitals>(&vitals);
    register_component<Spawner>(&spawners);

    players.resize(2); // put some players in the bucket

    players[0].id = 0;
    players[0].type = Player_Type::HUMAN;
    players[0].location = Location_Type::LOCAL;
    players[0].team = 0;
    players[0].credits = 100;
    players[0].materials = 100;

    players[1].id = 1;
    players[1].type = Player_Type::AI;
    players[1].location = Location_Type::LOCAL;
    players[1].team = 1;
    players[1].credits = 100;
    players[1].materials = 100;

    auto component_base_size = sizeof(Spatial) + sizeof(gfx::Sprite) + sizeof(Vitals) +
        sizeof(Unit) + sizeof(Spawner) + sizeof(Meta);

    auto memory_size_bytes = 10000 * component_base_size;

    Log::i(TAG, sl::format("using %d bytes for component storage", memory_size_bytes));
}


bool
World::terrain_is_passable(vec2i cell) const {
    assert( rectf({0,0}, map->dimensions()).collide(cell) );
    const auto cell_data = map->layer_at(0).data.cell(cell);
    return (map->tile_info(cell_data).resistance < TR_IMPASSABLE);
}


bool
World::contains_point(vec2f world_loc) const {
    return (world_loc.x >= 0 && world_loc.y >= 0 &&
            world_loc.x < static_cast<float>(map->pxwidth()) &&
			world_loc.y < static_cast<float>(map->pxheight()));
}


/**
 * make a new entity with data and values defined by the prototype
 * and place into the world
 */
idtype
World::spawn_entity(const String& proto_name, vec2f location) {
    if ( ! contains_point(location) )
    {
        // error or ignore?
        Log::e(TAG, sl::format("spawn_entity: location (%d, %d) is not on the map",
                               location.x, location.y));
        ASSERT(0);
    }

    return spawn_entity(*assets::get_prototype(proto_name.c_str()), location);
}


idtype
World::spawn_entity(const Realm::Prototype& proto, vec2f location) {
    assert(proto != Realm::Prototype::Null);

    idtype entity = create_entity(proto.components);

    for (auto cmp_id = 1; cmp_id < component_count(); ++cmp_id)
    {
        // if component id is not in the prototype's set, skip it
        if (!( proto.components & 1 << cmp_id))
            continue;

        if (cmp_id == tl::type_id<Sprite>()) {
            auto& sprite = sprites[entity];
            sprite.entity = entity;
            sprite = proto.sprite;

            sprite.texture = assets::texture(sprite.image);

            sprite.place(location.x, location.y);
        }
        else if (cmp_id == tl::type_id<Spatial>()) {
            auto& body  = bodies[entity];
            body = proto.spatial;
            body.entity = entity;
            Motion::place_body(entity, location);
            //body.location = location;
        }

        else if (cmp_id == tl::type_id<Unit>()) {
            auto& unitai  = units[entity];
            unitai = proto.unit;
            unitai.entity = entity;
            unitai.push_state(IDLE);
        }

        else if (cmp_id == tl::type_id<Vitals>()) {
            auto& v = vitals[entity];

            v = proto.vitals;
            v.entity = entity;
        }

        else if (cmp_id == tl::type_id<Spawner>()) {
            auto& spawner = spawners[entity];
            spawner = proto.spawner;
            spawner.entity = entity;
        }
    }

    auto meta = attach<Meta>(entity);

    meta->entity = entity;
    meta->components = components_for(entity);
    meta->prototype = &proto;

    events::entity_spawned(entity);
    return entity;
}


void
World::kill_entity(idtype entity) {
    if (! id_in_use(entity)) // don't kill entities that are already dead
        return;

    entity_destroyed(entity);

    if(has_component<Spatial>(entity)) {
        bodies[entity].entity = NULL_ID;
    }

    if(has_component<gfx::Sprite>(entity)) {
        sprites[entity].entity = NULL_ID;
    }

    if(has_component<Unit>(entity)) {
        units[entity].entity = NULL_ID;
    }

    if(has_component<Vitals>(entity)) {
        vitals[entity].entity = NULL_ID;
    }

    if(has_component<Spawner>(entity)) {
        spawners[entity].entity = NULL_ID;
    }

    destroy_entity(entity);
}
