#pragma once

#include "grimoire/ecs.hpp"
#include "grimoire/sprite.h"

#include "action.h"
#include "components.h"


using gfx::Sprite;

namespace Realm {

/**
 * Prototype
 *
 * A template for defining and storing entity component collections
 * and their default values;
 *
 * Used for easy spawning of new entities of frequently-used types.
 */
struct Prototype {
    String         name;            // identifier for lookups
    String         inherit;         // base prototype for default values
    
    ComponentMask  components = 0;  // specify which components are used

    Array<Player_Tech> prerequisites;
    Array<ActionId>    actions;
    
    int            credits_cost = 0;
    int            materials_cost = 0;
    long           build_time = 0;
    
    Spatial        spatial;
    Sprite         sprite;
    Unit           unit;
    Vitals         vitals;
    Spawner        spawner;

    static const Prototype Null;
    Prototype() {}
    
    Prototype(const String& n, ComponentMask mask):
        name(n),
        components(mask)
    {}    

    inline bool using_component(idtype component) const {
        return components & 1 << component;
    }
    
    inline bool operator == (const Prototype& o) const {
        return name == o.name && components == o.components;
    }

    inline bool operator != (const Prototype& o) const {
        return !(*this == o);
    }
};
}
