#ifndef _events_h_
#define _events_h_


#include "common.h"
#include "entity/components.h"
#include "grimoire/terrain.hpp"


struct PathSearch;

namespace events {

struct EntityMotion {
    idtype entity;
    idtype unit_type;
    MapLocation prev_loc;
    MapLocation current_loc;

    EntityMotion(idtype e=0,
              idtype utype=0,
              MapLocation prev={0,0},
              MapLocation curr={0,0}):
        entity(e),
        unit_type(utype),
        prev_loc(prev),
        current_loc(curr)
    {}
};

struct Collision {
    long   time;
    idtype e1;
    idtype e2;
    Spatial sp1;
    Spatial sp2;

    Collision(long t, idtype a, idtype b, const Spatial& body1, const Spatial& body2):
        time(t),
        e1(a),
        e2(b),
        sp1(body1),
        sp2(body2)
    {}
};

struct TerrainCollision
{
    long           time; 
    idtype         entity;
    vec2i          map_cell;
    const Terrain* terrain_type;

    TerrainCollision(long t, idtype e, vec2i cell, const Terrain* tt):
        time(t),
        entity(e),
        map_cell(cell),
        terrain_type(tt)
    {}
};

struct CompoundCollision
{
    long t;
//    EntityList entities;
};

struct ViewResize {
    int w, h;

    ViewResize(int w, int h) {
        this->w = w;
        this->h = h;
    }
};

extern signal<ViewResize> view_resized;



extern signal<idtype> entity_spawned;
extern signal<idtype> entity_destroyed;
extern signal<EntityMotion> entity_motion;

extern Array<EntityMotion> motion_events;
extern Array<Collision> collisions;
extern Array<TerrainCollision> terrain_collisions;

struct PathCompleted {
    int id;
    const PathSearch* pathop;

    PathCompleted(int id, const PathSearch* po):
        id(id),
        pathop(po)
    {}
};
extern signal<PathCompleted> path_complete;
extern Array<PathCompleted> completed_paths;


enum DamageType {
    NORMAL =    0,
    EXPLOSIVE = 1,
};

struct Killed
{
    idtype entity;

    Killed(idtype e):
        entity(e)
    {}
};

struct Damage
{
    idtype victim;
    DamageType damage_type;
    int    hp_loss;
    int    current_hp;

    Damage(idtype entity, DamageType dt, int loss, int cur_hp):
        victim(entity),
        damage_type(dt),
        hp_loss(loss),
        current_hp(cur_hp)
    {
    }
};

struct Attack
{
    idtype attacker;
    idtype target;

    Attack(idtype attkr, idtype tgt):
        attacker(attkr),
        target(tgt)
    {
    }
};


extern Array<Attack> entity_attacks; // inbound event
extern Array<Damage> entity_damages; // outbound
extern Array<Killed> entity_deaths;
}

#endif //_events_h_
