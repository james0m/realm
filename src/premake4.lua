project "game"
	kind "WindowedApp"
	language "C++"

    targetname "realm"
    targetdir ".."
    
	files { "*.h", "*.cpp"  }

	configuration "linux"

    links {
       "grimoire", 
       "lua",
       "physfs",
       "freetype",
       "SDL2", 
       "pthread",
       "profiler"
    }
    
	configuration "debug"
			defines { "DEBUG" }
			flags { "Symbols" }

		configuration "release"
			defines { "NDEBUG" }
			flags { "Optimize" }
