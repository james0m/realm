#ifndef _config_hpp_
#define _config_hpp_

#include "common.h"

#include <map>
#include <initializer_list>

/**
 * Config
 *
 * a flexible key-value mapping for the purpose of storing
 * data pulled in from config files, scripts, etc. 
 *
 */
class Config {
public:
    
    using AttrMap = std::map<std::string, Any>;
    using KV      = std::pair<String, Any>;
    using iterator = AttrMap::iterator;
    using const_iterator = AttrMap::const_iterator;
    
    Config(){ }
    
    Config(std::initializer_list<KV>&& init) {
        for(auto kv : init) {
            put(kv.first, kv.second);
        }
    }
    
//    Config(AttrMap key_val_pairs) {
//        values = key_val_pairs;
//    }


    template <class T>
    static T cast(const Any& any) {
        return boost::any_cast<T>(any);
    }

    template <class T>
    void put(const std::string& key, const T& value) {
        values[key] = value;
    }

    template <class T>
    const T& get(const std::string& key) const {
        return boost::any_cast<const T&>(values.at(key));
    }

    template <class T>
    T& get(const std::string& key) {
        return boost::any_cast<T&>(values[key]);
    }
    
    const char* get_string(const std::string& key) const {
        return get<const char*>(key);
    }

    float get_float(const std::string& key) const {
        return get<float>(key);
    }

    const Any& get_raw(const std::string& key) const {
        return values.at(key);
    }
            
    template <class T>
    T& operator[] (const std::string& key) {
        return const_cast<T&>(Config::cast<T>(values.at(key)));
    }

    size_t size() const { return values.size(); }
    
    iterator begin() { return values.begin(); }
    iterator end() { return values.end(); }

    const_iterator begin() const { return values.begin(); }
    const_iterator end() const { return values.end(); }
    
private:
    AttrMap values;
};

#endif //_config_h_
