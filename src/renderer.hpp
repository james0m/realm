#ifndef RENDERCTL_H_
#define RENDERCTL_H_

//#include "grimoire/componentpool.h"
//#include "grimoire/log.h"
//#include "grimoire/scene.h"
#include "grimoire/sdlutils.h"
//#include "grimoire/sprite.h"
//#include "grimoire/tiledmap.h"
//#include "grimoire/tiledmapelement.h"

#include "forward.hh"
#include "entity/components.h"
#include "action.h"
#include "events.hpp"
#include "runtime.hpp"
#include "entity/world.hpp"


using OverlayTable = std::map<String, std::pair<gfx::Overlay*, gfx::Element*>>;

struct Display {
    sdl::window_ptr window = nullptr;
    rect<int> viewport;

    Display() = default;
};

/**
 * A halo is simply an effect to highlight an object in the world
 */
struct HaloNode : gfx::Element {

    vec2f point;
    float radius;
    colorf color;
    bool fadeout = true;

    u32 start_t;

    HaloNode():
        Element(nullptr)
    {
    }

    virtual void draw(RenderApi& r) override;
};


namespace Render
{

constexpr const char* TAG = "Render";


void
init(Realm::Runtime& rt);

void
cleanup();

gfx::Scene*
create_scene();

void
create_display(int w=-1, int h=-1, int xoffset=-1, int yoffset=-1);

//const Display*
//current_display();

void
frame();

/**
 * create a new texture of the given dimensions and fill color
 *
 * @brief make_blank_texture
 * @param w
 * @param h
 * @param fill color in RGBA format to fill the texels with
 * @return id of the new texture
 */
texid
make_blank_texture(int w, int h, colorf fill=colorf(0,0,0,1));

//tmx::Tileset
//make_blank_tileset();

void
resize_display(int w, int h);

/**
   Loads pngs for tilesets into textures

   * @brief load_map_data
   * @param mapdata
   */
void
load_map_data(tmx::TiledMap* mapdata);

void
on_unit_spawned(const idtype& entity);

void
on_toggle_map_grid();

void
on_toggle_terrain();

void
on_toggle_waypoint_indicators();

void
on_map_scrolled(const vec2i& dir);

//void
//on_unit_moved(const events::EntityMotion& ev);
//
//void
//on_view_resize(const events::ViewResize& ev);


/**
 * get a reference to the main in-game camera
 */
gfx::Camera&
main_camera();

/**
 * get a reference to the in-game scene
 *
 * @brief current_scene
 * @return
 */
gfx::Scene&
get_scene();

/**
 * get a reference to the active drawing api
 *
 * @brief api
 * @return
 */
RenderApi&
get_api();

/**
 * get a reference to the GUI scenegraph
 * @brief ui
 * @return
 */
gfx::Scene&
get_ui();

/**
 * @brief world_coords
 * convert screen/pixel coordinates into valid world-level coordinates
 *
 * @param pixel_x
 * @param pixel_y
 * @return vec2<float>
 */
vec2f
screen_to_world_coords(int pixel_x, int pixel_y);

/**
 * @brief pixel_coords
 * convert world-level coordinates into screen/pixel coordinates
 * @param world_x
 * @param world_y
 * @return vec2<float> containing the transformed coords
 */
vec2f
world_to_pixel_coords(float world_x, float world_y);

vec2f
world_to_pixel_coords(vec2f world_coords);

/**
 * @brief world_to_grid_coords
 * @param world_x
 * @param world_y
 * @return
 */
vec2<int>
world_to_grid_coords(float world_x, float world_y);

vec2<int>
world_to_grid_coords(vec2f world_coords);

//vec2f
//grid_to_world_coords(int grid_x, int grid_y);

void
add_element(gfx::Element* root, gfx::Element* elem);

void
add_element(gfx::Element* elem);

//void
//drop_element(gfx::Element* elem);

void
drop_element(gfx::Element* root, gfx::Element* elem);

/**
 * register a new overlay to the pool of available overlays
 */
void
add_overlay(const String& name, gfx::Overlay* overlay, gfx::Element* root=nullptr);

/**
 * disable and forget about an overlay
 */
//void
//drop_overlay(const String& name);

/**
 * initialize and start rendering the registered overlay
 */
void
enable_overlay(const String& name);

/**
 * stop drawing the registered overlay
 */
void
disable_overlay(const String& name);

bool
has_overlay(const String& name);

const OverlayTable&
get_overlays();

void
add_halo(gfx::Element* root, vec2f point, float radius,
                     colorf color, bool fade = true);

void
add_halo(vec2f point, float radius, colorf color, bool fade = true);

void
drop_halo(HaloNode* halo);

} // namespace Render
#endif //_renderctl_h_
