
#include "unitcontrol.h"

#include "grimoire/grimoire_util.h"
#include "grimoire/grimoire_math.hpp"
#include "grimoire/interpolation.h"
#include "grimoire/scene.h"

#include "action.h"
#include "config.hpp"
#include "input/input.hpp"
#include "renderer.hpp"
#include "motion.hpp"
#include "runtime.hpp"
#include "combat.hpp"
#include "pathfinding/path.hpp"
#include "pathfinding/pathsearchalgo.h"


namespace Units
{

static float PROXIMITY = 1.0f;

using ecs::EntityList;
using PathOpsTable = HashMap<idtype, PathSearch>;

static World *                     _world = nullptr;
static tmx::TiledMap *             _map = nullptr;
static PathOpsTable                _searches;

static auto nearest_enemy_buffer = GridIndexer::QuadList();


void on_entity_spawned(const idtype&)
{
}


static void on_entity_destroyed(const ecs::EntityDestroyed& ev)
{
    if (_world->has_component<Spatial>(ev.eid))
    {
        auto itr = _searches.find( ev.eid);

        if (itr != _searches.end())
        {
            _searches.erase(itr);
        }
    }
}


static
bool
has_path(idtype entity)
{
    return !(_searches[entity].path.completed());
}


static
bool
outside(rect<int> bounds, vec2i pt) {
	return ! bounds.collide(pt);
}



/**
 * @brief cell_available
 *
 * check if the world coordinates are inhabited by an entity or
 * on impassible terrain
 *
 * @param world readonly pointer to the current world
 * @param tile map location in grid coordinates
 *
 * @return true if the cell is clear, false if an obstruction is found
 */
static
bool
cell_available(const World* world, vec2i tile)
{
    if (!world->terrain_is_passable(tile)) {
        return false;
    }

    // get the world coords of the tile position, offset it to the center of the grid cell
    const auto world_point = world->world_coords<vec2f>(tile) + 0.5*world->map->tile_dimensions();

    // is any entity with spatial body in this cell?
    for(auto entity : world->index.at_point(world_point).entities) {
        if (entity == NULL_ID)
            continue;

        const auto& body = world->get<Spatial>(entity);
        if ( point_in_circle(body.location, body.radius, world_point)) {
            return false;
        }
    }
    return true;
}


/**
 * given a point on the map, and a group of units
 * search around the point in increasing radius for a suitable alternative
 */
static
void
scatter_points(const World& world, const EntityList& units, vec2f chosen_point, Array<vec2i>& out)
{
    constexpr const vec2i ADJACENTS[4] = {
            vec2i(0, -1),
            vec2i(1, 0),
            vec2i(0, 1),
            vec2i(-1, 0)
    };

    const auto MAP_BOUNDS = rect<int>({0,0}, world.map->dimensions());

    auto visited          = HashSet<vec2i>();
    auto chosen_cell      = world.tile_coords(chosen_point);
    auto potentials       = std::queue<vec2i>();

    potentials.push(chosen_cell);

    int found = 0;
    while(potentials.size())
    {
        // get the next point to try, pop it off the queue, and add to visited
        const vec2i test_cell = potentials.front();
        potentials.pop();
        visited.insert(test_cell);

        if (cell_available(&world, test_cell)) {
            const vec2i tile_dims = world.map->tile_dimensions();
            out.push_back(test_cell * tile_dims + 0.5 * tile_dims);
            ++found;
        }

        if (found >= units.size()) {
            return;
        }

        for(vec2i delta : ADJACENTS) {
            const vec2i neighbor = test_cell + delta;
            if (outside(MAP_BOUNDS, neighbor) || visited.count(neighbor)) {
                continue;
            }
            potentials.push(neighbor);
        }

        if (visited.size() >= world.map->size()) {
            Log::e(TAG, sl::format(
                    "scatter_points() - search has exhausted all map cells (%lu)", world.map->size()));
        }
    }
}

void
init(Realm::Runtime &rt) {

    _world = rt.world();

    _map = rt.world()->map.get();

    PROXIMITY =  rt.config().get<float>("movement.near_proximity");

    actions::unit_move.connect(on_unit_move);
    actions::unit_attack.connect(on_unit_attack);
    actions::unit_stop.connect(on_unit_stop);

    events::entity_spawned.connect(on_entity_spawned);
    _world->entity_destroyed.connect(on_entity_destroyed);

    nearest_enemy_buffer.reserve(10);
}

void
cleanup()
{

}

//void on_unit_stop(const Action_UnitStop &stop)
void
on_unit_stop(const Action_UnitStop &)
{
    const auto& selected = input::selected_units();
    for(auto id : selected)
    {
        auto& unit = _world->units[id];
        unit.state_idx = 0;

        Motion::stop_movement(id);
    }
}

//static void execute_unit_move(World* world, idtype entity, const Path* path )
static
void
execute_unit_move(World*, idtype entity, const Path* path )
{
    if (path->completed())
    {
        Log::i(TAG, sl::format("executing a unit move for entity #%d with a path that is completed", entity));
        return;
    }

    auto& actor = _world->units[entity];

    actor.clear_state();
    actor.push_state(UnitState::MOVING);

    auto& body = _world->bodies[entity];

    if (body.max_speed <= 0)
    {
        Log::e(TAG, sl::format("executed a move on inanimate entity #%d ", entity));
        return;
    }

    actor.waypoint = path->next();

    Render::add_halo(&Render::get_scene(), path->goal(), 16.0f, { 0.0f, 1.0f, 0.0f, 1.0f});
    Motion::start_movement(entity,
                              body.max_speed * normalize(actor.waypoint - body.location));
}

void
on_unit_move(const Action_UnitMove &data)
{
    Log::i(TAG, "on_unit_move");

    if (input::selected_units().empty()) {
        Log::i(TAG, "on_unit_move: no units selected");
        return;
    }

    // get the destination and sanity check this. is it reachable?
    auto  destination     = Render::world_to_grid_coords(data.goal);
    auto& terrain_layer   = _map->layer_at(0).data;
    auto  cell            = terrain_layer.cell(destination);
    auto  cell_resistance = _map->tile_info(cell).resistance;

    // test for an off-map destination
    if( destination.x < 0 || destination.y < 0 ||
        destination.x >= _map->width() || destination.y >= _map->height())
    {
        Log::e(TAG, "destination is off the map");
        return;
    }
    // test for an obstacle as destination
    if (cell_resistance == TR_IMPASSABLE) {
        Log::e(TAG, sl::format("on_unit_move(): map location (%d, %d) is impassable",
                        destination.x, destination.y));
        return;
    }

    // TODO: instead of input::selected_units, pass a selection
    //       group through the event, or store the selection in
    //       the player's table
    auto chosen_point = data.goal;

    // check to see if an entity is blocking

    auto& quad = _world->index.at_point(chosen_point);

    for(idtype e : quad.entities)
    {
        if (e == NULL_ID)
            continue;

        auto body = &_world->bodies[e];
        if ( point_in_circle(body->location, body->radius, chosen_point))
        {
            Log::i(TAG, sl::format("point (%f, %f) is blocked by entity: %d",
                                         chosen_point.x, chosen_point.y, e));
            return;
        }
    }

    const auto& selection = input::selected_units();
    const auto N = selection.size();

    if (N == 1)
    {
        auto entity = selection.front();
        start_search(entity, chosen_point);

        const auto *path = path_for(entity);
        assert(path != nullptr);
        if (path->completed()) {
            return;
        }
        execute_unit_move(_world, entity, path);
    }
    else
    {
        auto goals = Array<vec2i>();
        goals.reserve(N);

        scatter_points(*_world, selection, chosen_point, goals);

        for(auto i=0u; i<N; ++i)
        {
            auto entity = selection[i];
            auto goal = goals[i];

            start_search(entity, goal);

            const auto *path = path_for(entity);
            assert(path != nullptr);

            execute_unit_move(_world, entity, path);
        }
    }
}

void
on_unit_attack(const Action_UnitAttack &){
    Log::i(TAG, "on_unit_attack");
}


void
on_unit_defend(const Action_UnitDefend &)
{
    Log::i(TAG, "on_unit_defend");
}


// find the nearest enemy to the unit, within its  current cell and that cell's
// immediate neighbors
static
idtype
find_nearest_enemy(World* world, const Unit& unit)
{
    static auto radius_query_buffer = GridIndexer::QuadList();

    // get the grid quadrant we are in now
    radius_query_buffer.push_back( &(world->index.at_point(unit.location)));

    // get any neighboring cells that are visible to this unit
    world->index.within_radius(unit.location, unit.sight_range, radius_query_buffer);

    idtype nearest_enemy = NULL_ID;
    float  nearest_distance = std::numeric_limits<float>::max();

    for(auto quad : radius_query_buffer)
    {
        for (auto e : quad->entities)
        {
            if (e == NULL_ID)
                continue;

            auto other = &world->units[e];

            if (other->entity != NULL_ID && other->team != unit.team)
            {
                const auto dist = distance(unit.location, other->location);

                if (dist < nearest_distance)
                {
                    nearest_enemy = other->entity;
                    nearest_distance = dist;
                }
            }
        }
    }

    radius_query_buffer.clear();
    return nearest_enemy;
}

static
void
indicate_targeting(const Unit& u)
{
    Render::add_halo(u.location, 16, {0,1,0,1});
    auto& target_body = _world->bodies[u.target];
    Render::add_halo(target_body.location, 16, {1,0,0,1});
}

static
void
navigate_path(Unit& unit)
{
    if (!is_number(unit.waypoint)) {
        auto* p = path_for(unit.entity);
        unit.waypoint = p->front();
    }

    if (nearby(unit.location, unit.waypoint, PROXIMITY)) {
        // get the next waypoint, or end the move
        const Path* p = path_for(unit.entity);
        if (!p->completed()) {
            unit.waypoint = p->next();
            auto& body = _world->bodies[unit.entity];
            body.vel = body.max_speed * normalize(unit.waypoint - unit.location);
        }
        else {
            unit.waypoint = vec2f::nan();
            stop_entity(unit.entity);
        }
    }
    else if (distance(unit.location, unit.waypoint) > distance(unit.last_location, unit.waypoint)) {
        auto& body = _world->bodies[unit.entity];
        body.vel = body.max_speed * norm(unit.waypoint - unit.location);
    }
    else if ( nearby(unit.last_location, unit.last_location, PROXIMITY * 0.5f)) {
        auto& body = _world->bodies[unit.entity];
        if (!is_number(body.vel)) {
            Motion::start_movement(unit.entity, norm(unit.waypoint - unit.location) * body.max_speed );
        }
    }
}


void
frame() {
    events::entity_attacks.clear();

    for(auto& ev : events::motion_events) {
        // update local Unit.location with the event.current_loc
        auto& v = _world->units[ev.entity];

        v.last_location = v.location;
        v.location = ev.current_loc;
    }

    static EntityList buffer;

    if (globals.units_clock < globals.TENHz)
        return;

    static bool odd_entity_ids_only = false;
    odd_entity_ids_only = ! odd_entity_ids_only;

    for(auto& unit : _world->units)
    {
        if (unit.entity == NULL_ID)
            continue;

        switch(unit.current_state())
        {
            case UnitState::IDLE:
            {
                // scanning for nearby enemies for idle units is expensive, so alternate
                // between odd/even number entity IDs
                const bool id_is_even = unit.entity % 2 == 0;
                if (odd_entity_ids_only && id_is_even || (!odd_entity_ids_only && !id_is_even)) {
                    break;
                }

                // check for nearby units on opposing team
                const idtype nearest_enemy = find_nearest_enemy(_world, unit);
                if (nearest_enemy != NULL_ID) {
                    const auto enemy_unit = &_world->units[nearest_enemy];
                    if ( distance(enemy_unit->location, unit.location) > unit.sight_range) {
                        break;
                    }

                    unit.target = nearest_enemy;
                    unit.push_state(ATTACK);
                    indicate_targeting(unit);
                }
                break;
            }
            case UnitState::MOVING:
            {
                assert(unit.waypoint != vec2f::nan());

                if (! is_number(unit.waypoint.x))
                    unit.push_state(NEED_PATH);

                navigate_path(unit);
                break;
            }

            case UnitState::ATTACK:
			{
				// no target?
				if (unit.target == NULL_ID) {
					unit.pop_state();
					break;
				}

                // if our target is dead, stop targeting him!
                const Vitals& target_vitals = _world->vitals[unit.target];
				if (target_vitals.entity == NULL_ID || target_vitals.hp == 0) {
					unit.target = NULL_ID;
					unit.pop_state();
					break;
				}

                // resolve combat if in weapon range
                const Spatial& target_body = _world->bodies[unit.target];
                const Vitals& attacker_vitals = _world->vitals[unit.entity];
				const bool resolve_combat = Combat::check_in_weapon_range(
                        unit.location,
                        target_body.location,
                        attacker_vitals.weapon
                );

				if (resolve_combat) {
					Motion::stop_movement(unit.entity);
					// face the target
					auto& attacker_body = _world->bodies[unit.entity];
//					auto target_body   = &s_world->bodies[unit.target];

					attacker_body.vel = norm(target_body.location - attacker_body.location);

					Combat::begin_attack(unit.entity, unit.target);
					//indicate_targeting(unit);
				}
				else {
                    // set state to SEEK if too far, return
                    unit.push_state(SEEK);
                }
				break;
			}

			case UnitState::DEFEND: {
				// check distance to defense subject
				// set state to SEEK if too far
				// otherwise, go IDLE
				break;
			}

			case UnitState::SEEK:
			{
				if (unit.target == NULL_ID)
				{
					unit.pop_state();
					continue;
				}

				// check distance to target
				auto target_body = &_world->bodies[unit.target];

				if (target_body->entity == NULL_ID)
				{
					unit.target = NULL_ID;
					unit.pop_state();
					continue;
				}

				const auto& vitals = _world->vitals[unit.entity];
				if (Combat::check_in_weapon_range(unit.location, target_body->location, vitals.weapon)) {
                    unit.pop_state();
				}
				else {
                    if (! has_path(unit.entity)) {
                        // need a path to get nearer to our target, set goal and push state
                        unit.goal = target_body->location;
                        unit.waypoint = unit.goal;
                        unit.push_state(NEED_PATH);

                        break;
                    }

                    // we have a path, check if we need to start moving again
                    if (!Motion::is_moving(unit.entity) ) {
                        Motion::start_movement(unit.entity, norm(unit.waypoint - unit.location) * 120);
                    }
                    navigate_path(unit);
				}
				break;
			}

			case UnitState::NEED_PATH:
			{
				start_search(unit.entity, unit.goal);
				assert( path_for(unit.entity) );
				unit.pop_state();
				break;
			}

        } // switch(unit.current_state())

    }

    globals.units_clock = 0;
}


void stop_entity(idtype entity)
{
    auto& v = _world->units[entity];
    v.clear_state();
    Motion::stop_movement(entity);
}

Path* construct_path(PathSearch& search)
{
    assert (search.trail.size() > 0);

    search.path.clear();
    auto loc = search.goal;
    const auto trail_size = search.trail.size();

    while(loc != search.start) {
        search.path.push_front(_world->world_coords(loc));
        loc = search.trail[loc];
        if (search.path.size() >= trail_size)
            break;
    }

    assert(search.path.size() < _world->map->size() && "");
    return &search.path;
}


idtype
start_search(idtype entity, vec2f target)
{
    auto body = &(_world->get<Spatial>(entity));

    auto &pathop = _searches[entity];

    pathop.init();

    pathop.id                = entity;
    pathop.last_compute_node = 0;
    pathop.map               = _map;
    pathop.status            = SearchState::NotStarted;
    pathop.real_start        = body->location;
    pathop.start             = _world->tile_coords(body->location);
    pathop.real_goal         = target;
    pathop.goal              = _world->tile_coords(target);


    events::completed_paths.clear();

    AStar(pathop);

    if (size(pathop.trail) > 0) {
        construct_path(pathop);
        events::completed_paths.emplace_back(pathop.id, &pathop);
        events::path_complete(pathop.id, &pathop);
    }

    return entity;
}


const Path*
path_for(idtype entity) {
    return &(_searches[entity].path);
}


const char*
state_string(UnitState state) {
    switch(state) {
        case IDLE:      return "idle";
        case MOVING:    return "moving";
        case ATTACK:    return "attack";
        case SEEK:      return "seek";
        case NEED_PATH: return "need_path";
        default:        return "undefined";
    }
}


} // namespace Units
