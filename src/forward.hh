#ifndef REALM_FORWARD_DECL_HH_
#define REALM_FORWARD_DECL_HH_

class Config;
struct World;
struct RenderApi;
struct Path;
struct UIElement;

namespace Realm {
class Runtime;
class Prototype;
};

namespace gfx {
    class Camera;
    class Element;
    struct Overlay;
    class Scene;
    struct Sprite;
}

namespace input {
    struct InputHandler;
}


namespace txt {
    struct Font;
}
#endif //_forward_h_
