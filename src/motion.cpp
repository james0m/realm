#include "common.h"

#include "grimoire/log.h"
#include "grimoire/grimoire_math.hpp"
#include "grimoire/stringlib.h"

#include "motion.hpp"
#include "runtime.hpp"
#include "action.h"
#include "entity/components.h"
#include "entity/world.hpp"
#include "events.hpp"

using events::EntityMotion;

namespace Motion
{

static Realm::Runtime*        _rt;
static World*                 _world;
static Array<idtype>           _moving_entities;

static float t; // timestep used for updating the sim

//@TODO: use this for updating the indexes of each unit, instead of allocating a vector
static GridIndexer::QuadList adjacency_buffer;


static void remove_from_index(Spatial& body);

static void on_entity_destroyed(const ecs::EntityDestroyed& ev)
{
    auto entity = ev.eid;
    // check for dead guy in selection
    auto itr = search(_moving_entities, ev.eid);
    if (itr != _moving_entities.end())
    {
        pop_erase(_moving_entities, itr);
    }

    auto& body = _world->bodies[entity];

    remove_from_index(body);
}



static void remove_from_index(Spatial& body)
{
    ASSERT(body.entity != NULL_ID);

    // remove from current quads via body.quads indices, if any
    for(auto i=0u; i<body.quad_count; ++i) {        
        _world->index.erase_at_index(body.quads[i], body.entity);        
    }

    body.clear_quads();
}

template <class C, class T>
void push_if_not_contained(C& container, const T& value)
{
    const auto itr = std::find(container.begin(), container.end(), value);
    if (itr == container.end())
    {
        container.push_back(value);
    }
}
/**
 *
 */
static void update_spatial_index(Spatial& body)
{
    remove_from_index(body);

    // stab our location and place in index
    const auto& quad   = _world->index.at_point(body.location);

    body.quads[0] = quad.index;
    body.quad_count = 1;

    _world->index.insert_at_index(quad.index, body.entity);

    // check neibhgor intersection
    // insert into each intersected neighbor
    // record in body.quads
    //
    // IMPORTANT:  this assumes that no body will be larger than
    // one quad and it's immediate neighbors. anything bigger will
    // not be considered

    _world->index.neighbors_of(body.location, adjacency_buffer);

    for(auto cell : adjacency_buffer)
    {
        if (rect_intersects_circle(cell->region, body.location, body.radius))
        {
            body.add_quad_index(cell->index);
            _world->index.insert_at_index(cell->index, body.entity);
        }
    }

    adjacency_buffer.clear();
}

//static void on_component_attached(const ecs::ComponentAttached& e)
static void on_component_attached(const idtype& e)
{
    if (_world->has_component<Spatial>(e))
    {
        auto& world = *_rt->world();
        auto& body = world.get<Spatial>(e);
        update_spatial_index(body);
    }
};

static void on_component_detached(const ecs::ComponentDetached& e)
{
    auto& world = *_rt->world();
    if (e.cid == tl::type_id<Spatial>())
    {
        const auto &body = world.get<Spatial>(e.eid);
        world.index.erase_at_index( world.index.at_point(body.location).index, body.entity);
    }
};


void init(Realm::Runtime& rt) {

    _rt = &rt;
    _world = rt.world();

    //s_world->component_attached.connect(on_component_attached);
    events::entity_spawned.connect(on_component_attached);

    _world->entity_destroyed.connect(on_entity_destroyed);

    _world->component_detached.connect(on_component_detached);
    t = globals.PHYSICS_TIMESTEP;

    adjacency_buffer.reserve(9);
}

void cleanup()
{
    _world = nullptr;
    _rt = nullptr;

    adjacency_buffer = GridIndexer::QuadList();
}


void frame()
{
//    for(auto& ev : events::entity_deaths)
//    {
//        remove_from_index(s_world->bodies[ev.entity]);
//        s_world->bodies[ev.entity].entity = NULL_ID;
//    }

    if (globals.physics_clock < globals.EIGHTYHz)
        return;
    
    events::collisions.clear();
    update_entity_movement(_rt->world()); // -> collision events
    globals.physics_clock = 0;
}


bool is_moving(idtype entity)
{
    return contained_in(_moving_entities, entity);
}

static vec2f calc_eject_vector(vec2f ca, float ra, vec2f cb, float rb)
{
    // TODO: review and maybe find alternative to calc_eject_vector, i doubt this is correct
    auto eject_length = (ra + rb - distance(ca, cb));
    auto eject_dir    = norm(ca - cb); // move away from b

    return eject_dir * eject_length;
}


static inline bool is_stationary(const Spatial& body)
{
    return (body.vel == norm(body.vel) );
}


void update_entity_movement(World* world)
{
    events::motion_events.clear();

    for (auto& body : _world->bodies)
    {
        if (body.entity == NULL_ID || is_stationary(body))
            continue;

        if ( !is_number(body.vel) )
        {
            stop_movement(body.entity);
            continue;
        }

        vec2f prev_loc    = body.location;
        auto new_location = body.location + body.vel * t;
        auto world_dims   = world->map->pixel_dimensions();
        auto quad         = _world->index.at_point(new_location);
                
        for(auto e : quad.entities)
        {
            //if (e == ent) // you are always colliding with yourself
            if (e == body.entity)
                continue;

            auto& test_body = _world->bodies[e];

            if (test_body.entity == NULL_ID)
                continue;

            if (circles_intersect(new_location, body.radius, test_body.location, test_body.radius)) {
                // what counter-force would prevent this collision?
                const auto eject = calc_eject_vector(
                    new_location, body.radius,test_body.location, test_body.radius
                );
                new_location += eject;
                events::collisions.emplace_back(globals.time, body.entity, e, body, test_body);
            }
        }

        // keep moving bodies from falling off the map

        // where we should be = where we are - edge of map +- body radius
        if ((new_location.x - body.radius) < 0)
        {
            new_location.x = body.radius;
        }

        else if ((new_location.x + body.radius) >= static_cast<float>(world_dims.x))
        {
            new_location.x = static_cast<float>(world_dims.x) - body.radius;
        }

        if ((new_location.y - body.radius) < 0)
        {
            new_location.y = body.radius;
        }

        else if((new_location.y + body.radius) >= static_cast<float>(world_dims.y))
        {
            new_location.y = static_cast<float>(world_dims.y) - body.radius;
        }

        if (!is_number(new_location)) // check for NAN
        {
            auto msg = sl::format("update_movement: new_location resolved to NAN. entity: %d location: (%d, %d)",
                                  body.entity, body.location.x, body.location.y);
            Log::e(TAG, msg);
            continue; // something went horribly wrong at runtime, skip this
        }

        {
            const auto tile_coord = world->tile_coords(new_location);
            if (! world->terrain_is_passable(tile_coord))
            {
                events::terrain_collisions.emplace_back(globals.time,
                                                        body.entity,
                                                        tile_coord,
                                                        &world->map->terrain_at(tile_coord));
                new_location = prev_loc;
            }
        }

        // update the entity's position
        body.location = new_location;

        update_spatial_index(body);
        events::motion_events.emplace_back(body.entity, 0, prev_loc, body.location);
    }
}


void place_body(idtype entity, vec2f pos)
{
    auto& body = _rt->world()->get<Spatial>(entity);

    events::motion_events.emplace_back(entity, 0, body.location, pos);
    body.location = pos;

    update_spatial_index(body);
}

void start_movement(idtype entity, vec2f vel)
{
    auto& spatial = _rt->world()->get<Spatial>(entity);
    //spatial.accel = 1;
    spatial.vel = vel;

    if (! contained_in(_moving_entities, entity) )
    {
        _moving_entities.push_back(entity);
    }
}

void stop_movement(idtype entity)
{
    // first, negate any forces on the body
    auto& body = _rt->world()->get<Spatial>(entity);
    //body.accel = 0;
    body.vel = normalize(body.vel);

    // quick delete from movers list
    erase_if_contained(_moving_entities, entity);
}

} // namespace Motion
