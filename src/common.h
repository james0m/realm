#ifndef _common_h_
#define _common_h_

#include "grimoire/grimoire_math.hpp"
#include "grimoire/grimoire_util.h"
#include "grimoire/log.h"

#include "grimoire/rect.h"
#include "grimoire/fontlib.h"

#include "grimoire/stringlib.h"

using MapLocation = vec2f;

template <class T>
using EventQueue = Array<T>;

struct Globals {
    u32 time = 0;
    u32 frames = 0;
    long render_clock = 0;
    long physics_clock = 0;
    long units_clock = 0;
    const long SIXTYHz = 1000 / 60;
    const long EIGHTYHz = 1000 / 80;
    const long THIRTYHz = 1000 / 30;
    const long TENHz = 1000 / 10;

    const float PHYSICS_TIMESTEP = 1.0 / 80.0f;
    Unique<txt::Font> ui_font = nullptr;
};

extern Globals globals;

constexpr idtype NULL_ENTITY = -1;
constexpr idtype NULL_ID = -1;

// need to define a hash function type that
// hashes vector
namespace std
{
template <>
struct hash<vec2<int>> {
    inline size_t operator()(const vec2<float> &v) const {
        return (size_t)v.x * 10397531131ul + (size_t)v.y;

		// TODO: a better hash function; fewer collisions, better spread
		//       based on the size of the map
		
		// return (v.x * 10397531131 + v.y*11) % n*n
		//
		// the above hash function seems to have a much better spread and fewer collisions
		// need to test
    }

	 
};
} // std


#endif //_common_h_
