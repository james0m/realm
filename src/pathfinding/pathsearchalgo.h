#ifndef _pathsearchalgo_h_
#define _pathsearchalgo_h_

#include "common.h"
#include "path.hpp"



enum SearchState {
    NotStarted = 0,
    Started,     // search has begun
    Updated,         // algo has added at least one more node
    Complete,        // the path is finished
    PartialComplete, // the path could not be done as-is, improvised
    Failed           // could not find a path at all
};


using RankedLocation = pair<int, vec2<int>>;

// create a special gt comparator for pairs of priority/vector
struct my_greater_func {
    bool operator()(const RankedLocation& a, const RankedLocation& b) {
        return a.first > b.first;
    }
};

using LocationQueue = std::priority_queue<RankedLocation,
                                          Array<RankedLocation>,
                                          my_greater_func>;

namespace tmx {
    class TiledMap;
}



struct PathSearch
{
    uint              id;                // search id; usually an entity id
    SearchState       status;
    Path              path;              // the path itself
    tmx::TiledMap*    map;               // the map we are searching through
    uint              last_compute_node; // last node found in the search

    vec2<float>       real_start;
    vec2<float>       real_goal;
    vec2<int>         start;            // start location in grid coordinates
    vec2<int>         goal;             // goal location in grid coordinates

    // internal state for search algo

    LocationQueue         frontier; // yet to be explored map cells
    HashMap<vec2i, vec2i> trail;         // accumulation table of traversed nodes
    HashMap<vec2i, int>   cost_table;      // running cost for the given path node

    void init() {
        frontier = LocationQueue();
        trail.clear();
        cost_table.clear();
    }
};

//using SearchAlgorithm = void (*)(PathSearch& );



/* the no-search search algorithm
 * just beeline toward the destination
 * 
 * for debugging only
 */
//void LinearSearch(PathSearch &pathop);

/*
 * spiral out on the map from the starting point until
 * the destination is reached. No guarantee on
 * finding the shortest path
 */
//void BreadthFirstSearch(PathSearch &pathop);

/*
 * best first measures the distance from each of the neighbors
 * of the current node, and selects the one that is closest
 * to the goal
 */
//void GreedyBestFirst(PathSearch &pathop);

/*
 * dijkstra considers the local cost 
 */
//void Dijkstra(PathSearch &pathop);
/*
 */
void AStar(PathSearch &pathop);

#endif //_pathsearchalgo_h_
