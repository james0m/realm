#ifndef _path_h_
#define _path_h_

#include <list>

#include "common.h"

struct Path
{
    using List = std::list<MapLocation>;
    using Iterator = List::iterator;
    using ConstIterator = List::const_iterator;
    using InitList = std::initializer_list<MapLocation>;
    using SizeType = List::size_type;

    mutable Iterator current;
    List nodes;

    Path() {
        current = nodes.begin();
    }

    Path(const Path& o) {
        *this = o;
        current = begin();
    }

    Path& operator = (const Path& o)
    {
        if (this != &o) {
            nodes = o.nodes;

        }
        return *this;
    }
    Path(const InitList&& rval):
        nodes(rval)
    {
        current = nodes.begin();
    }

    SizeType size() const {
        return nodes.size();
    }

    bool empty() const {
        return nodes.empty();
    }

    bool completed() const {
        return *current == back();
    }

    MapLocation next() const {
        ++current;
        return *current;
    }

    void push_front(const MapLocation& loc) {
        nodes.push_front(loc);
        current = nodes.begin();
    }

    void push_back(const MapLocation& loc) {
        nodes.push_back(loc);
    }

    Iterator begin() {
        return nodes.begin();
    }

    Iterator end() {
        return nodes.end();
    }

    ConstIterator begin() const {
        return nodes.begin();
    }

    ConstIterator end() const {
        return nodes.end();
    }

    bool complete() const {
        return current == std::end(nodes);
    }

    /**
     * The 'front' of the path is the first
     * node that hasn't been visited yet.
     */
    const MapLocation& front() const {
        return *current;
    }

    const MapLocation& back() const {
        return nodes.back();
    }

    const MapLocation& goal() const {
        return nodes.back();
    }

    void clear() {
        nodes.clear();
        current = std::begin(nodes);
    }

};
#endif //_path_h_
