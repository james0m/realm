#include "grimoire/tiledmap.h"
#include "pathsearchalgo.h"


using tmx::TiledMap;

// the 'null' algorithm; just makes a beeline from the start to the goal
//void LinearSearch(PathSearch& pathop)
//{
//    auto start = pathop.start;
//    auto dest = pathop.goal;
//    auto midpt_delta = vec2f((dest.x - start.x)/2, (dest.y - start.y)/2 );
//    auto midpt = start + midpt_delta;
//    pathop.path = Path { start, midpt, dest };
//    pathop.status = SearchState::Complete;
//}

//const vec2f DIRS[] = {
//    {0, -1}, { -1, 0}, {1, 0}, {0, 1}
//};

//template <class Tp>
//Array<vec2<Tp>> neighbors(vec2<Tp> location)
//{
//    Array<vec2<Tp>> result;

//    for(auto dir : DIRS)
//    {
//        auto neighbor = location + dir;
//        if (neighbor.x < 0 || neighbor.y < 0)
//            continue;

//        result.push_back(neighbor);
//    }
//    return result;
//}


//int cost(vec2f a, vec2f b) {
//    return distance(a, b);
//}

int move_cost(const TiledMap &map, u16 cell)
{
    // all tiles in RTS games are either passable or not
    return map.tile_info(cell).resistance == 255 ? 255 : 1;
}


void AStar(PathSearch& pathop)
{
    auto &frontier      = pathop.frontier;
    auto &trail         = pathop.trail;
    auto &cost_table    = pathop.cost_table;
    auto &map           = *pathop.map;
    const auto &terrain = map.layer_at(0).data;

    if (pathop.status == SearchState::NotStarted) {
        frontier.emplace( 0, pathop.start);

        cost_table[pathop.start] = 0;
        pathop.status = SearchState::Started;
    }

    while( !frontier.empty())
    {
        auto node = frontier.top().second;
        frontier.pop();

        if (node == pathop.goal) {
            pathop.status = SearchState::Complete;
            break;
        }

        vec2i adjacents[4] = {
            node + vec2i(0, -1),
            node + vec2i(1, 0),
            node + vec2i(0, 1),
            node + vec2i(-1, 0)
        };

        const auto map_bounds = rectf({0,0}, pathop.map->dimensions());

        for(auto neighbor : adjacents) {
            // we're gonna access map data, don't visit any nodes
            // that are out of range
            if( ! map_bounds.collide(neighbor))
                continue;

            auto tile_cost = move_cost(map, terrain.cell(neighbor.x, neighbor.y));
            auto new_cost = tile_cost + cost_table[node];

            if( ! cost_table.count(neighbor) || new_cost < cost_table[neighbor]) {
                cost_table[neighbor] = new_cost;
                trail[neighbor] = node;

                const auto dist = manhattan_distance(pathop.goal, neighbor);
                frontier.emplace(dist + new_cost, neighbor);
            }

        }
    }
}


// void BreadthFirstSearch(PathSearch& pathop)
// {
//     auto &frontier = pathop.frontier;
//     auto &trail    = pathop.trail;
//     auto &map      = *pathop.map;

//     auto visited = 0u;
//     auto loops = 0u;
//     // put first node into frontier
//     frontier.emplace(0, pathop.start);

//     trail[pathop.start] = {-1, -1};

//     // while frontier is not empty
//     while(frontier.size() > 0)
//     {
//         auto node = frontier.top().second;
//         frontier.pop();

//         ++visited;
//         ++loops;
//         assert ( visited < map.size() && "BreadthFirstSearch: visited nodes exceed size of map");
//         assert ( loops < map.size() && "BreadthFirstSearch: loops exceed size of map");
//         if (node == pathop.goal) {
//             pathop.status = SearchState::Complete;
//             break;
//         }

//         vec2i adjacents[4] = {
//             node + vec2i(0, -1),
//             node + vec2i(1, 0),
//             node + vec2i(0, 1),
//             node + vec2i(-1, 0)
//         };

//         const auto MAP_BOUNDS = rect<int>(
//             {0,0},
//             pathop.map->dimensions()
//         );

//         for(auto &position : adjacents) {
//             if( !MAP_BOUNDS.collide(position))
//                 continue;

//             if( ! trail.count(position)) {
//                 frontier.emplace(visited, position);
//                 trail[position] = node;
//             }
//         }
//     }

// }

//void GreedyBestFirst(PathSearch& pathop)
//{
//    auto &frontier   = pathop.frontier;
//    auto &trail      = pathop.trail;
//    auto &cost_table = pathop.cost_table;
//    auto &map        = *pathop.map;
//    auto &terrain    = map.layer_at(0);
//
//    if (pathop.status == SearchState::NotStarted) {
//        frontier.emplace( 0, pathop.start);
//
//        cost_table[pathop.start] = 0;
//        pathop.status = SearchState::Started;
//    }
//
//    while( frontier.size() > 0)
//    {
//        auto node = frontier.top().second;
//        frontier.pop();
//
//        if (node == pathop.goal) {
//            pathop.status = SearchState::Complete;
//            break;
//        }
//
//        vec2i adjacents[4] = {
//            node + vec2i(0, -1),
//            node + vec2i(1, 0),
//            node + vec2i(0, 1),
//            node + vec2i(-1, 0)
//        };
//
//        const auto map_bounds = rectf({0,0}, pathop.map->dimensions());
//
//        for( auto neighbor : adjacents) {
//            // we're gonna access map data, don't visit any nodes
//            // that are out of range
//            if( ! map_bounds.collide(neighbor))
//                continue;
//
//            auto dist        = distance(pathop.goal, neighbor);
//            auto cell_value  = terrain.data.cell(neighbor.x, neighbor.y);
//
//            auto new_cost = move_cost(map, cell_value) + dist;
//            if( ! trail.count(neighbor) || new_cost < cost_table[node])
//            {
//                frontier.emplace(new_cost, neighbor);
//                trail[neighbor] = node;
//            }
//
//        }
//    }
//}
//
//void Dijkstra(PathSearch& pathop)
//{
//    auto &frontier   = pathop.frontier;
//    auto &trail      = pathop.trail;
//    auto &cost_table = pathop.cost_table;
//    auto &map        = *pathop.map;
//    auto &terrain    = map.layer_at(0);
//
//    if (pathop.status == SearchState::NotStarted) {
//        frontier.emplace( 0, pathop.start);
//
//        cost_table[pathop.start] = 0;
//        pathop.status = SearchState::Started;
//    }
//
//    while( frontier.size() > 0)
//    {
//        auto node = frontier.top().second;
//        frontier.pop();
//
//        if (node == pathop.goal) {
//            pathop.status = SearchState::Complete;
//            break;
//        }
//
//        vec2i adjacents[4] = {
//            node + vec2i(0, -1),
//            node + vec2i(1, 0),
//            node + vec2i(0, 1),
//            node + vec2i(-1, 0)
//        };
//
//        const auto map_bounds = rectf({0,0}, pathop.map->dimensions());
//
//        for( auto neighbor : adjacents) {
//            // we're gonna access map data, don't visit any nodes
//            // that are out of range
//            if( ! map_bounds.collide(neighbor))
//                continue;
//
//            auto cell_value  = terrain.data.cell(neighbor.x, neighbor.y);
//
//            auto new_cost = move_cost(map, cell_value) + cost_table[node];
//
//            if( ! cost_table.count(neighbor) || new_cost < cost_table[neighbor])
//            {
//                cost_table[neighbor] = new_cost;
//                trail[neighbor] = node;
//                frontier.emplace(new_cost, neighbor);
//            }
//        }
//    }
//}
