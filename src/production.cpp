#include "grimoire/log.h"
#include "grimoire/stringlib.h"

#include "action.h"
#include "asset/assets.hpp"
#include "runtime.hpp"
#include "production.hpp"
#include "entity/prototype.hpp"
#include "entity/world.hpp"

using Realm::Runtime;

namespace Production
{

static Runtime*               _rt;
static World*                 _world;

static BuildQueue             _builds;
EventQueue<BuildCompleted> completed_builds;
EventQueue<BuildProgress> build_progresses;


BuildOp::BuildOp(idtype src, const Realm::Prototype* proto, long start, long dur, vec2f dest) {
    source      = src;
    prototype   = proto;
    start_t     = start;
    duration    = dur;
    progress    = 0.0f;
    destination = dest;
}


bool
BuildOp::operator==(const BuildOp& o) const {
    return source == o.source && prototype == o.prototype && start_t == o.start_t;
}


void
init(Runtime& rt) {
    _rt = &rt;
    _world = rt.world();
    _builds = BuildQueue(0);
}


void
cleanup() {
    _rt = nullptr;
    _world = nullptr;    
    _builds = BuildQueue(0);
}



void
frame() {
    auto t = globals.time;
    completed_builds.clear();

    // check progress of every build operation, update progress, or spawn the entity
    for(auto b=0u; b<_builds.size(); ++b)
    {
        auto& op = _builds[b];

        if ( t >= op.start_t + op.duration)
        {
            auto spatial = _world->get<Spatial>(op.source);
            auto spawner = (Spawner*)_world->spawners.get(op.source);
            op.destination = spatial.location + spawner->spawn_point;

            auto new_entity = _world->spawn_entity(*op.prototype, op.destination);

            --spawner->builds;
            completed_builds.emplace_back(op.source, new_entity, op.prototype, op.destination);
            pop_erase(_builds, _builds.begin() + b);
        }
        else
        {
            op.progress = float(t - op.start_t) / op.duration;
            build_progresses.emplace_back(&op);
        }
    }
}



bool
try_build(idtype player_id, ActionId action, idtype builder_id, const sl::string& type) {
    Log::i(TAG, sl::format("try_build() was called with args: player: %d | action: %s | builder: %d | type: %s",
                           player_id, action_string(action), builder_id, type));

    auto& player = _world->players[player_id];
    

    const Realm::Prototype* prototype = assets::get_prototype(type.c_str());
    // check for tech tree pre-requisites
    if (prototype->prerequisites.size() > 0)
    {
        for(auto req : prototype->prerequisites) {
            if( player.acquired_tech.find(req) == player.acquired_tech.end()) {
                // TODO: alert the system with events, not just a log message
                Log::i(TAG, sl::format("could not build %s, tech up some more", prototype->name));
                return false;
            }
        }
    }

    // check for sufficient credits and materials
    if (prototype->materials_cost > player.materials) {
        // TODO: alert the system with events, not just a log message
        Log::i(TAG, sl::format("cannot build %s: not enough materials", prototype->name));
        return false;
    }

    if (prototype->credits_cost > player.credits) {
        // TODO: alert the system with events, not just a log message
        Log::i(TAG, sl::format("cannot build %s: not enough credits", prototype->name));
        return false;
    }
    
    {
        const auto& spawner = _world->get<Spawner>(builder_id);
        if (spawner.builds >= spawner.max_builds)
        {
            Log::i(TAG, "cannot build. Entity has too many builds going already!");
            return false;
        }
    }
    // TODO: alert the world we are building a unit or a structure

    if (prototype->inherit == "infantry") {
        actions::build_unit.emplace_back(globals.time,
                                         player_id,
                                         builder_id, prototype->name.c_str());
    }
    else if (prototype->inherit == "structure") {
        actions::build_structure.emplace_back(globals.time,
                                              player_id,
                                              builder_id, prototype->name.c_str());
    }
    else {
        Log::e(TAG, sl::format("prototype named '%s' had invalid inherit property: %s",
                               prototype->name, prototype->inherit));
        return false;
    }

    incur_costs(player_id, prototype->credits_cost, prototype->materials_cost);
    start_build(builder_id, prototype);
    return true;
}


void
start_build(idtype source, const Realm::Prototype* prototype) {
    auto& spawner = _world->get<Spawner>(source);
    assert(spawner.builds < spawner.max_builds);
    _builds.emplace_back(
        source,
        prototype,
        globals.time,
        prototype->build_time,
        spawner.spawn_point
    );

    ++spawner.builds;
}


void
incur_costs(idtype player, int credits, int materials) {
    // TODO: alert the world that money is being spent
    _world->players[player].credits -= credits;
    _world->players[player].materials -= materials;
}

} // namespace Production
