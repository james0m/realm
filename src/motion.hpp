#ifndef _physics_hpp_
#define _physics_hpp_

#include "common.h"
#include "forward.hh"

#include "entity/components.h"
#include "grimoire/ecs.hpp"
#include "grimoire/componentpool.h"

/**
 * The motion module is a lower level system
 * that only deals with moving spatial bodies about the map,
 * according to velocity and deals with immediate collisions.
 * Other systems must be used to implement control logic for
 * the different kinds of moving entities, such as units
 * and missiles
 */

namespace Motion
{
constexpr const char* TAG = "Motion";
    


void init(Realm::Runtime& rt);
void cleanup();
void frame();

rectf world_bounds();

void start_movement(idtype entity, vec2f vel);    
void stop_movement(idtype entity);

/** move an entity's spatial body directly to the given position, */        
void place_body(idtype entity, vec2f pos);    
    
void update_entity_movement(World* world);

/** 
 * check to see if the given entity is actively being moved
 */
bool is_moving(idtype entity);

//void test_collisions();
//void handle_collisions();

}
#endif
