#include <algorithm>

#include "grimoire/filesystem.h"
#include "grimoire/sdlutils.h"
#include "grimoire/log.h"
#include "grimoire/renderapi_sdl.hpp"
#include "grimoire/scene.h"
#include "grimoire/sprite.h"

#include "config.hpp"
#include "renderer.hpp"
#include "action.h"
#include "runtime.hpp"
#include "input/input.hpp"

#include "ui/overlay/collisionoverlay.hpp"
#include "ui/overlay/overlay.hpp"
#include "ui/overlay/pathdebugoverlay.hpp"

#include "ui/overlay/debuginfo.hpp"
#include "unitcontrol.h"
#include "combat.hpp"


static Runtime *                    s_runtime;
static World *                      s_world;
static Unique<RenderApi>            s_render;
static Unique<Display>              s_display;
static Unique<gfx::Camera>          s_main_camera;
static Unique<gfx::Camera>          s_minimap_camera;
static Array<rect_def>              s_object_halos;
Array<Unique<HaloNode>>             s_halos;

static Unique<gfx::Scene>           s_ui = nullptr;
static Unique<gfx::Scene>           s_scene_graph = nullptr;
static Unique<gfx::TiledMapElement> s_mapelem = nullptr;
OverlayTable                        s_overlays;

static
Unique<DebugInfo>        debug_info;

static
Unique<PathDebugOverlay> path_overlay;

static
Unique<CollisionOverlay> collision_overlay;

static
bool
draw_waypoint_indicators = false;

//const colorf YELLOW = { 1.0f, 1.0f, 0.0f, 1.0f };
//const colorf RED = { 1.0f, 0, 0, 1.0f };


static
void
on_entity_destroyed(const ecs::EntityDestroyed& ev)
{
    if(s_world->has_component<gfx::Sprite>(ev.eid))
    {
        // TODO: will need some code to deal with child entities as well
        auto sprite = (gfx::Sprite*)s_world->sprites.get(ev.eid);
        s_scene_graph->detach(sprite);
    }
}


void
HaloNode::draw(RenderApi& r)
{
    r.draw_circle(point + bounds().topleft(), radius, color);
    if(fadeout) {
        color.a = interpolate(linear, 1.0, 0.0, float(globals.time - start_t) / 1000);
    }
}

namespace Render {

void
on_toggle_terrain()
{
    s_mapelem->toggle_terrain();
}


void
on_toggle_map_grid()
{
    s_mapelem->toggle_map_grid();
}


void
on_toggle_waypoint_indicators()
{
    draw_waypoint_indicators = !draw_waypoint_indicators;
}


void
on_unit_spawned(const idtype& entity)
{
    assert(s_world->has_component<gfx::Sprite>(entity));
    assert(s_world->has_component<Spatial>(entity));

    auto& sprite = s_world->get<gfx::Sprite>(entity);
    auto& spatial = s_world->get<Spatial>(entity);
    auto area = sprite.bounds();

    sprite.set_bounds(area.x - spatial.radius, area.y - spatial.radius,
                      spatial.radius*2, spatial.radius*2);

    s_scene_graph->attach(&sprite);
}


void
init(Realm::Runtime& rt) {
    s_runtime = &rt;
    s_world = rt.world();

    {
        Log::i(TAG, "initializing text and font rendering");
        txt::init();
        globals.ui_font = txt::Font::from_file(
            get_api(),
            s_runtime->config().get_string("game.ui_font"),
            static_cast<int>(s_runtime->config().get_float("game.ui_font_size"))
        );
    }

    {
        Log::i(TAG, "initializing UI scene graph");
        if (! s_scene_graph)
            create_scene();

        s_ui = gfx::Scene::create("UI");

        s_ui->set_bounds(
                0, 0,
                static_cast<float>(s_display->viewport.w),
                static_cast<float>(s_display->viewport.h)
        );

        s_ui->follow_camera(false); // the ui stays fixed to the screen viewport

    }
    {
        Log::i(TAG, "initializing event handlers");
        events::entity_spawned.connect(on_unit_spawned);
        actions::toggle_map_grid.connect(on_toggle_map_grid);
        actions::toggle_terrain.connect(on_toggle_terrain);
        actions::scroll_map.connect(on_map_scrolled);
        s_world->entity_destroyed.connect(on_entity_destroyed);
    }

}

void
cleanup() {
    events::entity_spawned.disconnect(on_unit_spawned);
    actions::toggle_map_grid.disconnect(on_toggle_map_grid);
    actions::toggle_terrain.disconnect(on_toggle_terrain);
    actions::scroll_map.disconnect(on_map_scrolled);

    s_main_camera.reset(nullptr);
    s_minimap_camera.reset(nullptr);

    s_halos.clear();
    s_halos.shrink_to_fit();

    s_object_halos.clear();
    s_object_halos.shrink_to_fit();

    s_scene_graph.reset(nullptr);
    s_ui.reset(nullptr);

    txt::cleanup();
    // we may or may not want to delete the window and renderer
    //s_render.reset(nullptr);
    //s_display.reset(nullptr);
}


gfx::Scene*
create_scene()
{
    s_scene_graph = gfx::Scene::create("");
    s_scene_graph->set_bounds(
        0, 0,
        static_cast<float>(s_display->viewport.w),
        static_cast<float>(s_display->viewport.h)
    );
    return s_scene_graph.get();
}


vec2f
screen_to_world_coords(int pixel_x, int pixel_y)
{
    const auto &vp = s_main_camera->bounds();
    return {
        static_cast<float>(pixel_x) + vp.x,
        static_cast<float>(pixel_y) + vp.y
    };
}


vec2f
world_to_pixel_coords(float world_x, float world_y)
{
    const auto &vp = s_main_camera->bounds();
    return {
        world_x - vp.x,
        world_y - vp.y
    };
}


vec2f
world_to_pixel_coords(vec2f world_coords)
{
    const auto &vp = s_main_camera->bounds();
    return {
        world_coords.x - vp.x,
        world_coords.y - vp.y
    };
}


vec2i
world_to_grid_coords(float world_x, float world_y)
{
    auto tile_size = s_mapelem->map()->tile_dimensions();
    return {
        static_cast<int>(world_x) / tile_size.x,
        static_cast<int>(world_y) / tile_size.y
    };
}


vec2i
world_to_grid_coords(vec2f world_coords)
{
    return world_to_grid_coords(world_coords.x , world_coords.y);
}

//vec2f grid_to_world_coords(int grid_x, int grid_y)
//{
//    auto tile_size = s_mapelem->map()->tile_dimensions();
//    return vec2f( tile_size.x * grid_x, tile_size.y * grid_y);
//}


void
create_display(int w, int h, int xoffset, int yoffset)
{
    s_display = util::make_unique<Display>();

    w = (w > -1) ? w : 800;
    h = (h > -1) ? h : 600;

    xoffset = (xoffset > -1) ? xoffset : SDL_WINDOWPOS_CENTERED;
    yoffset = (yoffset > -1) ? yoffset : SDL_WINDOWPOS_CENTERED;

    u32 flags = SDL_WINDOW_RESIZABLE;

    s_display->window = sdl::window_ptr{
        SDL_CreateWindow("scrappy", xoffset, yoffset, w, h, flags)
    };

    assert(s_display->window != nullptr);

    SDL_GetWindowSize(s_display->window.get(),
                      &s_display->viewport.w,
                      &s_display->viewport.h);

    SDL_GetWindowPosition(s_display->window.get(),
                          &s_display->viewport.x,
                          &s_display->viewport.y);

    s_render = RenderApi_SDL::create(s_display->window.get());
}


void
on_map_scrolled(const vec2i& delta) {
    s_main_camera->translate(delta);
}


gfx::Camera&
main_camera()
{
    return *s_main_camera;
}


RenderApi&
get_api()
{
    return *s_render;
}


gfx::Scene&
get_ui() {
    return *s_ui;
}


gfx::Scene&
get_scene() {
    return *s_scene_graph;
}


const OverlayTable&
get_overlays() {
    return s_overlays;
}


bool
has_overlay(const String& name) {
    return s_overlays.find(name) != end(s_overlays);
}


void
add_element(Element* root, Element* elem) {
    root->attach(elem);
}


void
add_element(Element* elem) {
    add_element(s_scene_graph.get(), elem);
}

//void drop_element(Element* elem)
//{
//    drop_element(s_scene_graph.get(), elem);
//}

void
drop_element(Element* root, Element* elem) {
    root->detach(elem);
}


struct Rifle_Fire_Node : HaloNode {
    Rifle_Fire_Node(vec2f s, vec2f e):
        HaloNode()
    {
        start = s;
        end = e;
        color = { 1.0f, .7f, 0, 1.0f};
    }

    vec2f start; vec2f end;

    void
    draw(RenderApi& r) override {
        auto t = globals.time;

        if (rand() % 100 < 25)
            r.draw_line(start, end, color);

        r.draw_filled_circle(start + (end - start) * .055, 4, {1,1,1, color.a});
        color.a = interpolate(linear, 1.0, 0.0, float(t - start_t) / 250.0);
    }
};


static
void
draw_rifle_fire(vec2f src_point, vec2f impact_pt)
{
    const auto xform = main_camera().transform();
    auto true_point = src_point - xform;
    auto true_impact = impact_pt - xform;

    auto node = util::make_unique<Rifle_Fire_Node>(true_point, true_impact);
    add_element(node.get());
    s_halos.push_back(std::move(node));
}


void
frame()
{
    // first handle any updates to the world
    for(auto &ev : events::motion_events) {
        auto& sprite = s_world->sprites[ev.entity];
//        auto& body   = s_world->bodies[ev.entity];

        // @TODO: stop using spatial body and radius here, just set location
        sprite.place(ev.current_loc.x, ev.current_loc.y);
    }

    for(auto& attack : events::entity_attacks)
    {
        //@TODO: check type of weapon and add muzzle flare, particles, etc
        auto attacker_unit = (Unit*) s_world->units.get(attack.attacker);
        auto target_unit = (Unit*) s_world->units.get(attack.target);

        draw_rifle_fire(attacker_unit->location, target_unit->location);
    }

    // make sure to delete the faded out click halos outside the render
    for(auto h=0; h < s_halos.size(); ++h)
    {
        if (s_halos[h]->color.a <= 0.0f) {
            drop_halo(s_halos[h].get());
        }
    }


    // we only want to draw about 60 times per second, so early exit if we can
    if (globals.render_clock < globals.SIXTYHz)
        return;

    // check for selected units and add haloes

    for(auto entity : input::selected_units())
    {
        auto sprite = &s_world->get<gfx::Sprite>(entity);

        rect_def cmd;
        cmd.r = sprite->bounds();
        cmd.color = input::selection_color();

        s_object_halos.push_back(cmd);

#ifdef DEBUG
        if (draw_waypoint_indicators)
        {
            // draw waypoint indicators for moving units
            auto unit = &s_world->get<Unit>(entity);
            cmd.r = rectf(world_to_pixel_coords(unit->waypoint), {16, 16});
            cmd.color = { 0.0f, 6.0f, 0.0f, 1.0f};

            s_object_halos.push_back(cmd);
        }
#endif
    }

    s_render->frame_begin();
    {
        s_scene_graph->draw(*s_render, *s_main_camera);
        s_render->draw_rect(input::selection_area(), input::selection_color());
        s_render->batch_rects(pbegin(s_object_halos), size(s_object_halos));

       for (auto entity : input::selected_units())
       {
           auto& unit = s_world->units[entity];

           auto screen_pos = Render::world_to_pixel_coords(unit.location);

           auto y = screen_pos.y - globals.ui_font->line_height * 2;

           txt::render_text(*s_render, *globals.ui_font,
                            std::to_string(entity), screen_pos.x, y, {0,0,0,0});

           y = screen_pos.y - globals.ui_font->line_height;

           txt::render_text(*s_render, *globals.ui_font,
                            Units::state_string(unit.current_state()),
                            screen_pos.x, screen_pos.y, {0,0,0,0});
       }
        s_ui->draw(*s_render, *s_main_camera);
    }
    s_render->frame_end();

    s_object_halos.clear();
    globals.render_clock = 0;
}


texid
make_blank_texture(int w, int h, colorf fill)
{
    assert(w > 0); assert(h>0);
    auto texture = s_render->create_texture(w, h, PixelFormat::RGBA, AccessMode::STATIC);
    auto tmpsurface = sdl::surface_create(w, h, 32);

    color<u8> intcolor = fill.convert<u8>();

    u32 fillvalue = SDL_MapRGBA(tmpsurface->format,
                                intcolor.r, intcolor.g, intcolor.b, intcolor.a);
    u32* pixels = static_cast<u32*>(tmpsurface->pixels);
    u32* end = pixels + w * h;
    std::fill(pixels, end, fillvalue);

    s_render->bind_texture(texture);
    s_render->update_texture(pixels);

    return texture;
}


//tmx::Tileset
//make_blank_tileset()
//{
//    auto ts = tmx::Tileset();
//    ts.atlas.set_texture(make_blank_texture(32,32));
//    ts.atlas.add_region(rectf(0,0,32,32));
//    ts.firstgid = 0;
//    ts.lastgid = 0;
//    ts.tilewidth = 32;
//    ts.tileheight =32;
//    return ts;
//}


void
resize_display(int w, int h) {
    s_display->viewport.w = w;
    s_display->viewport.h = h;
    s_ui->set_bounds(0, 0, w, h);
    main_camera().set_dimensions(w,h);

    events::view_resized(w,h);
}


/**
 * preconditions:
 * - current scene is valid
 */
void
load_map_data(tmx::TiledMap* mapdata)
{
    s_mapelem = util::make_unique<gfx::TiledMapElement> (s_scene_graph.get(), mapdata);
    s_main_camera = gfx::Camera::create(s_display->viewport.w,
                                        s_display->viewport.h,
                                        s_mapelem.get());

    auto &tileset = s_mapelem->map()->tileset(0);

    auto tsfile = io::open(tileset.src);
    auto tsdata = util::make_unique<u8[]>(tsfile->size());

    tsfile->read( (void*)(tsdata.get()));

    auto s = sdl::image_load_from_memory(tsdata.get(), tsfile->size());

    auto texture = s_render->create_texture(s->w, s->h,
                                            from_sdl_format(s->format),
                                            AccessMode::STATIC);
    s_render->bind_texture(texture);
    s_render->update_texture(s->pixels);

    tileset.atlas.set_texture(texture);

    auto regions = util::rects_from_grid(
        tileset.tilewidth,
        tileset.tileheight,
        s->w,
        s->h,
        tileset.spacing,
        tileset.margin
    );

    tileset.atlas.add_regions(pbegin(regions), size(regions));

    // @TODO overlays should be generated somewhere else. could update map data refs here maybe
    {
        debug_info = DebugInfo::create(*s_runtime);
        path_overlay = PathDebugOverlay::create(*s_runtime, s_mapelem.get());
        collision_overlay = CollisionOverlay::create(*s_runtime, s_scene_graph.get());

        add_overlay(DebugInfo::TAG, debug_info.get(), s_ui.get());
        add_overlay(PathDebugOverlay::TAG, path_overlay.get(), s_mapelem.get());
        add_overlay(CollisionOverlay::TAG, collision_overlay.get(), s_scene_graph.get());

#ifdef DEBUG
        enable_overlay(DebugInfo::TAG);
        //enable_overlay(CollisionOverlay::TAG);
#endif
    }

    s_scene_graph->attach(s_mapelem.get());
}


/**
 * register a new overlay to the pool of available overlays
 */
void
add_overlay(const String& name, gfx::Overlay* overlay, Element* root)
{
    if (s_overlays.find(name) != end(s_overlays)) {
        Log::d(TAG, sl::format(
                   "attempted to overwrite an existing overlay: %s. Ignoring.", name));
        return;
    }

    Log::i(TAG, sl::format("adding overlay: %s", name));
    s_overlays[name] = std::make_pair(overlay, root);
}


/**
 * disable and forget about an overlay
 */
//void
//drop_overlay(const String& name)
//{
//    auto itr = s_overlays.find(name);
//    if(itr == end(s_overlays)) {
//        Log::e(TAG, sl::format("attempt to drop unknown overlay: %s", name));
//        return;
//    }
//
//    Log::d(TAG, sl::format("dropping overlay: %s", name));
//    disable_overlay(name);
//    s_overlays.erase(itr);
//}


/**
 * initialize and start rendering the registered overlay
 */
void
enable_overlay(const String& name) {
    const auto itr = s_overlays.find(name);
    if( itr == end(s_overlays)) {
        Log::d(TAG, sl::format("attempt to enable unknown overlay: %s", name));
        return;
    }

    auto* overlay = itr->second.first;
    if( overlay->enabled ) {
        Log::i(TAG, sl::format("overlay already active: %s", name));
        return;
    }

    Log::i(TAG, sl::format("enabling overlay: %s", name));
    auto* root_element = itr->second.second;
    overlay->init(*s_runtime);
    root_element->attach(overlay);

    overlay->enabled = true;
}


/**
 * stop drawing the registered overlay
 */
void
disable_overlay(const String& name) {
    auto itr = s_overlays.find(name);
    if( itr == end(s_overlays))
    {
        Log::d(TAG, sl::format("attempt to disable unknown overlay: %s", name));
        return;
    }
    Log::d(TAG, sl::format("disabling overlay: %s", name));

    auto* overlay = itr->second.first;
    auto* root_element = itr->second.second;

    overlay->enabled = false;

    if (root_element->has_child(overlay))
        root_element->detach(overlay);
}


void
add_halo(Element *root, vec2f point, float radius, colorf color, bool fade)
{
    auto halo = util::make_unique<HaloNode>();

    halo->point = point;
    halo->radius = radius;
    halo->color = color;
    halo->start_t = globals.time;
    halo->fadeout = fade;

    root->attach(halo.get());
    s_halos.push_back(std::move(halo));
}


void
add_halo(vec2f point, float radius, colorf color, bool fade) {
    add_halo(s_scene_graph.get(), point, radius, color, fade);
}


void
drop_halo(HaloNode *halo) {
    for(auto itr=s_halos.begin(); itr != s_halos.end(); ++itr)
    {
        if ( (*itr).get() == halo )
        {
            s_scene_graph->detach((*itr).get() );
            //Log::d(TAG, sl::format("halo @ %#012x removed", (*itr).get()));
            pop_erase(s_halos, itr);

            return;
        }
    }
}

} // namespace
