#ifndef PRODUCTION_H
#define PRODUCTION_H


#include "grimoire/ecs.hpp"
#include "grimoire/componentpool.h"

#include "forward.hh"
#include "common.h"
#include "entity/components.h"



//enum Build_Fail_Reason
//{
//    FAIL_CREDITS,
//    FAIL_MATERIALS,
//    FAIL_POWER,
//    FAIL_TECHLEVEL
//};

//struct BuildFailed {
//    Build_Fail_Reason reason;
//    idtype player;
//    ActionId action;
//};



/**
 * EntityProduction
 *
 * defines the unit and building production system
 */

namespace Production {

static constexpr const char* TAG = "Production";

struct BuildOp {
    idtype                source;
    const Realm::Prototype* prototype;
    long                  start_t;
    long                  duration;
    float                 progress;
    vec2f                 destination;

    BuildOp() = default;
    BuildOp(idtype src, const Realm::Prototype* proto, long start, long dur, vec2f dest);

    bool
    operator==(const BuildOp& o) const;
};


struct BuildCompleted {
    idtype           source;
    idtype           new_entity;
    const Realm::Prototype* prototype;
    vec2f            destination;

    BuildCompleted(idtype src, idtype new_ent, const Realm::Prototype* proto, vec2f dest) {
        source = src;
        new_entity = new_ent;
        prototype = proto;
        destination = dest;
    }
};

struct BuildProgress {
    const BuildOp* build;

    BuildProgress(const BuildOp* op) {
        build = op;
    }
};

using BuildQueue = Array<BuildOp>;

void
init(Realm::Runtime& rt);

void
frame();

void
cleanup();

void
incur_costs(idtype player_id, int credits, int materials);


/**
 * check to make sure player has enough credits/materials, tech prereqs, etc
 * before starting the build. If the build can be done, it is started. If 
 * not, notify the other modules with an event.
 */
bool
try_build(idtype player_id, ActionId action, idtype builder_id, const sl::string& type);
    
/**
* queue up an entity build
*/
void
start_build(idtype builder_id, const Realm::Prototype* prototype);

}

#endif