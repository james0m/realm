#ifndef _action_h_
#define _action_h_

#include <map>

#include "grimoire/ecs.hpp"

#include "common.h"
#include "entity/components.h"
#include "grimoire/stringlib.h"

enum ActionId {
    A_NULL,
    A_CANCEL,
    A_SHOW_MENU,
    A_UNIT_MOVE,
    A_UNIT_STOP,
    A_UNIT_ATTACK,
    A_UNIT_TRAIN,
    A_BUILD_STRUCTURE,
    A_BUILD_UPGRADE,
    A_TOGGLE_CONSOLE,

    DEBUG_ENABLE_MAP_GRID,
    DEBUG_TOGGLE_TERRAIN,
    DEBUG_TOGGLE_WAYPOINT_INDICATORS
};

enum Action_Type
{
    ACTION, // a single action, meant to be triggered now
    MENU    // an action requiring an off-map choice from a menu
};


/** get the human readable name of an action */
const char* action_string(ActionId action);

inline bool has_several_options(ActionId action) {
    return (action == A_BUILD_STRUCTURE ||
            action == A_BUILD_UPGRADE   ||
            action == A_UNIT_TRAIN);
}

/**
 * Actions represent player-issued commands in the game
 */
struct Action {
    ActionId id = A_NULL;                // what sort of action was this?
    long     time = 0;                   // event timestamp
    idtype   player = ecs::NULL_ENTITY;  // player who issued the action

    Action(ActionId type, long t, idtype p):
        id(type),
        time(t),
        player(p)
    {}
};

struct Action_UnitMove : Action {
    vec2f  goal   = vec2f::nan();

    Action_UnitMove(long t, idtype p, idtype o, vec2f dest):
        Action(A_UNIT_MOVE, t, p),
        goal(dest)
    {}
};

struct Action_UnitStop : Action {

    Action_UnitStop(long t, idtype p):
        Action(A_UNIT_STOP, t, p)
    {}
};

struct Action_UnitAttack : Action {
    idtype attacker = ecs::NULL_ENTITY; // entity who is attacking
    idtype target = ecs::NULL_ENTITY;   // the target of the attack
    vec2f  goal = vec2f::nan();         // (optional) a map location to attack

    Action_UnitAttack(long t, idtype p, idtype o, vec2f dest):
        Action(A_UNIT_ATTACK, t, p),
        attacker(o),
        goal(dest)
    {}

    Action_UnitAttack(long t, idtype p, idtype o, idtype tgt):
        Action(A_UNIT_ATTACK, t, p),
        attacker(o),
        target(tgt)
    {}
};

struct Action_UnitDefend : Action {
    idtype defender = ecs::NULL_ENTITY; // entity who is defending
    idtype target = ecs::NULL_ENTITY;   // the entity we are defending
    vec2f  goal = vec2f::nan();         // (optional) a static map location to defend

    Action_UnitDefend(long t, idtype p, idtype o, vec2f dest):
        Action(A_UNIT_ATTACK, t, p),
        defender(o),
        goal(dest)

    {}

    Action_UnitDefend(long t, idtype p, idtype o, idtype tgt):
        Action(A_UNIT_ATTACK, t, p),
        defender(o),
        target(tgt)
    {}
};

struct Action_TrainUnit : Action {

    idtype player = ecs::NULL_ENTITY;  // player initiating the build
    idtype builder = ecs::NULL_ENTITY;  // entity doing the build
    const char* prototype;             // type of entity being built

    Action_TrainUnit(long t, idtype p, idtype o, const char* proto):
        Action(A_UNIT_TRAIN, t, p),
        builder(o),
        prototype(proto)
    {}
};

struct Action_BuildStructure : Action {

    idtype builder;         // entity doing the build
    const char* prototype; // type of entity being built

    Action_BuildStructure(long t, idtype p, idtype b, const char* proto):
        Action(A_BUILD_STRUCTURE, t, p),
        builder(b),
        prototype(proto)
    {
    }
};


namespace actions {

//actions for debugging
extern signal<void> toggle_path_visuals;
extern signal<void> toggle_map_grid;
extern signal<void> toggle_terrain;

extern signal<Realm::Prototype> spawn_entity;
extern signal<idtype> kill_entity;

extern signal<> toggle_console;

// in-game actions
extern signal<Action_UnitMove> unit_move;
extern signal<Action_UnitStop> unit_stop;
extern signal<Action_UnitAttack> unit_attack;
extern signal<Action_UnitDefend> unit_defend;
extern signal<vec2i> scroll_map;

extern Array<Action_TrainUnit>      build_unit;
extern Array<Action_BuildStructure> build_structure;


} // actions


#endif //_action_h_
