
#include "grimoire/sdlutils.h"
#include "grimoire/log.h"
#include "grimoire/stringlib.h"
#include "grimoire/sprite.h"

#include "action.h"
#include "input.hpp"
#include "production.hpp"
#include "entity/prototype.hpp"
#include "renderer.hpp"
#include "runtime.hpp"

#include "ui/hud/hud.hpp"

using namespace std;

using gfx::Sprite;

namespace input
{

constexpr int KEY_SCROLL_DISTANCE = 64;
constexpr int MOUSE_SCROLL_SCALE_FACTOR = 3;

using Keymap = std::map<unsigned, ActionId>;

static InputHandler default_focus = {
    key_press,
    key_release,
    mouse_press,
    mouse_release,
    mouse_motion
};

constexpr int FOCUS_STACK_LEVELS = 16;

struct Focus_Stack {
    int top = -1;
    InputHandler* stack[FOCUS_STACK_LEVELS];
};

Focus_Stack _focus_stack;

static bool focus_stack_empty()
{
    return _focus_stack.top < 0;
}

static void focus_stack_push(InputHandler* new_focus)
{
    ASSERT(_focus_stack.top < FOCUS_STACK_LEVELS - 2);
    _focus_stack.stack[++_focus_stack.top] = new_focus;
}

static void focus_stack_pop()
{
    if (!focus_stack_empty())
        _focus_stack.top -= 1;
}

static InputHandler* focus_stack_top()
{
    return _focus_stack.stack[_focus_stack.top];
}

static World  *              _world = nullptr;
static Keymap                _keymap;
static bool                  _dragging = false;
static bool                  _mouse_drag_scrolling = false;
static vec2f                 _drag_start;
static rectf                 _drag_region = rectf::null_rect();

static ActionId              _input_action;
const char*                  _input_option = ""; // for build-menu actions

static HUD                   _hud;

static ecs::EntityList       _unit_selection;
static Array<ActionId>       _exposed_actions;
static colorf                _select_color;

/**
 * local object for tracking interaction with the HUD UI
 */
struct UI_State
{
	UIElement* hovered = nullptr;
};

static UI_State ui_state;


static void on_entity_destroyed(const ecs::EntityDestroyed& ev)
{
    // check for dead guy in selection
    auto itr = search(_unit_selection, ev.eid);
    if (itr != _unit_selection.end())
    {
        _unit_selection.erase(itr);
    }
}


/**
 * Helper functions for testing input state
 */
static bool is_arrow_key(u32 key)
{
    return (key == SDLK_LEFT || key == SDLK_RIGHT || key == SDLK_UP || key == SDLK_DOWN);
}

static bool key_is_pressed(u32 key)
{
    return SDL_GetKeyboardState(0)[SDL_GetScancodeFromKey(key)] != 0;
}

static bool mouse_button_pressed(u32 button)
{
    return SDL_GetMouseState(0,0) & SDL_BUTTON(button);
}


/**
 * check if the x y coordinates hover over the given UI element
 */
static bool hud_element_hovered(UIElement* elem, int x, int y)
{
    return elem->bounds().collide(x,y);
}

/**
 *  check for hover, clicks, etc, and update the widget and global ui state
 */
static void hud_update_pointer_interaction(UIElement* root, vec2i pos)
{
    auto ui = stab_element(root, pos.x, pos.y);
    if (ui)
    {
        Log::d(TAG, "thing is hovered");

        if (!ui_state.hovered) {
            ui_state.hovered = ui;
            ui->update_pointer_interaction(pos.x, pos.y, SDL_GetMouseState(0,0));
        }
        else if (ui_state.hovered && ui_state.hovered != ui)
        {
            ui_state.hovered->exit(pos.x, pos.y);
            ui_state.hovered = ui;
            ui->enter(pos.x, pos.y);
        }
        else
            ui->update_pointer_interaction(pos.x, pos.y, SDL_GetMouseState(0,0));
    }
}

void init(Realm::Runtime &rt)
{
    _world = rt.world();
    _hud.init(&Render::get_ui());
    _select_color = { 1.0f, 0.0f, 0.5f, 1.0f };

    _hud.minimap.set_map(_world->map.get());
    steal_focus(&default_focus);
    _world->entity_destroyed.connect(on_entity_destroyed);
}

colorf selection_color() { return _select_color; }
rectf selection_area() { return _drag_region; }

void cleanup()
{
    _world = nullptr;

    _unit_selection.clear();
}

void frame()
{

}


void key_press(unsigned key)
{
    // check for key binding
    ActionId action = A_NULL;
    try {
        action = _keymap[key];
        Log::i(TAG, sl::format("hotkey action: %s", action_string(action)));
    }
    catch (std::out_of_range &e) {
        Log::e(TAG, sl::format("there was an error: %s", e.what()));
        return;
    }

    if (_input_action != A_NULL && action == A_CANCEL)
    {
        Log::i(TAG, sl::format("Cancelling previous action: %s\n",  action_string(action)));

        if (_hud.actionpanel.panel_stack.size() > 1)
        {
            _hud.actionpanel.pop_current_panel();
        }

        _input_action = A_NULL;

        return;
    }

    if (action == A_TOGGLE_CONSOLE) {
        actions::toggle_console();
    }

    if (action == DEBUG_ENABLE_MAP_GRID)
    {
        Log::d(TAG, "map grid lines enabled");
        //actions::toggle_map_grid();
        Render::on_toggle_map_grid();
        return;
    }

    if (action == DEBUG_TOGGLE_TERRAIN) {
        //actions::toggle_terrain();
        Render::on_toggle_terrain();
        return;
    }

    if (action == DEBUG_TOGGLE_WAYPOINT_INDICATORS) {
        //actions::toggle_terrain();
        Render::on_toggle_waypoint_indicators();
        return;
    }

    if ( is_arrow_key(key) )
    {
        int dx = 0, dy = 0;
        if(key_is_pressed(SDLK_LEFT))
            dx = -KEY_SCROLL_DISTANCE;

        else if (key_is_pressed(SDLK_RIGHT))
            dx = KEY_SCROLL_DISTANCE;

        if (key_is_pressed(SDLK_UP))
            dy = -KEY_SCROLL_DISTANCE;

        else if (key_is_pressed(SDLK_DOWN))
            dy = KEY_SCROLL_DISTANCE;

        actions::scroll_map(dx, dy);
    }


    begin_game_action(action);
}


void begin_game_action(ActionId action)
{
    // make sure we can execute this action
    if (!contained_in(_exposed_actions, action))
    {
        Log::d(TAG, sl::format("action is currently unavailable: %s", action_string(action)));
        return;
    }

    if (action == A_UNIT_STOP)
    {
        Log::i(TAG, "stopping units");
        actions::unit_stop(globals.time, 0);
        return;
    }

    if (has_several_options(action))
    {
        // add the options defined by the entity's spawner component
        // FIXME: this causes a crash if the user is about to select a build
        //        and the unit is killed
        ASSERT(_unit_selection.size() == 1); // only one entity can do this at a time

        auto entity = _unit_selection.front();
        ASSERT(_world->has_component<Spawner>(entity));

        const auto& spawner = _world->get<Spawner>(entity);

        // push a new panel
        _hud.actionpanel.push_panel();
        for(auto& s : spawner.options)
            expose_action_with_option(action, s.c_str());
    }

    _input_action = action;
    return;
}

void clear_action_state()
{
    _exposed_actions.clear();
    _hud.actionpanel.clear();
    _input_option = "";
    _input_action = A_NULL;

    if( _unit_selection.size() > 0)
    {
        selection();
    }
}

void end_game_action(const ActionId action, vec2f world_loc)
{
    switch(action)
    {
    case A_CANCEL: {
        clear_action_state();
        return;
    }

    case A_UNIT_MOVE: {
        Log::i(TAG, sl::format("moving to (%f, %f)", world_loc.x, world_loc.y));
            actions::unit_move(globals.time, 0, 0, world_loc);
            clear_action_state();
            return;
        }

        case A_UNIT_ATTACK: {

            // todo: do we need to pass the current group in?
            Log::i(TAG, sl::format("attacking location (%d, %d)", world_loc.x, world_loc.y));
            actions::unit_attack(globals.time, 0, 0, world_loc);

            clear_action_state();
            return;
        }

        case A_UNIT_TRAIN:
        case A_BUILD_STRUCTURE:
        case A_BUILD_UPGRADE:
        {
            ASSERT(_unit_selection.size() == 1);
            ASSERT( strcmp(_input_option, "")!= 0);

            auto prototype_to_make = _input_option;
            auto player = 0; //temporary, this would be whatever id the local player has
            auto builder_entity = _unit_selection[0];

            Production::try_build(player, _input_action,
                                 builder_entity, prototype_to_make);

            // return to default state
            clear_action_state();
        }
        default: {
            break;
        }
    }
}


void key_release(unsigned key)
{
}

void mouse_press(unsigned button, int x, int y)
{

    const auto world_loc = Render::screen_to_world_coords(x, y);
    const auto screen_loc = vec2f(x,y);

#ifdef DEBUG
    Render::add_halo(world_loc, 16, { 0.0f, 1.0f, 0.0f, 1.0f });
#endif

    if (hud_element_hovered(&_hud.actionpanel, x, y))
    {
        Log::d(TAG, sl::format("unit action panel was clicked: (%d, %d)", x, y));

        // handle UI input
        auto clicked_item = stab_element(&_hud.actionpanel, x, y);
        if (!clicked_item)
            return;

        clicked_item->update_pointer_interaction(x,y, SDL_GetMouseState(0,0));

        if (clicked_item->active())
        {
            if (clicked_item->type() == UI_Type::PANEL_OPTION)
            {
                auto panel_option = static_cast<PanelOption*>(clicked_item);
                Log::d(TAG, sl::format("panel option was clicked: %s", panel_option->option));

                if ( ! _input_action)
                    begin_game_action(panel_option->action);
                else {
                    _input_option = panel_option->option.c_str();
                    end_game_action(panel_option->action, vec2f::nan());
                }
            }
        }
        return;
    }

    // don't allow clicks off the map
    if (!_world->contains_point(world_loc))
    {
        Log::e(TAG, sl::format("mouse_press: coordinates were off-world: (%f, %f)",
                               world_loc.x, world_loc.y));
        return;
    }

    // first check to see if we are completing an action
    if (_input_action != A_NULL)
    {
        end_game_action(_input_action, world_loc);
    }

    // no action, check for select/deselect
    auto keystate = SDL_GetKeyboardState(nullptr);
    bool ctrl_click = keystate[SDL_SCANCODE_LCTRL] || keystate[SDL_SCANCODE_RCTRL];


    if(button == SDL_BUTTON_LEFT)
    {
        if (!ctrl_click)
            deselect();

        // query the world via quadtree using world coordinates
        auto& quad = _world->index.at_point(world_loc);
        for(auto& e : quad.entities) {

            if (e == NULL_ID)
                continue;
            // test for hit using screen coordinates
            const auto& sprite = _world->get<gfx::Sprite>(e);

            if (sprite.bounds().collide(screen_loc))
            {
                auto itr = search(_unit_selection, e);

                if (itr != _unit_selection.end())
                {
                    if (ctrl_click)
                    { // ctrl click deselects the already selected unit
                        _unit_selection.erase(itr);
                    }
                }
                else
                {
                    _unit_selection.push_back(e);
                }
                break;
            }
        }

        if (_unit_selection.size())
            selection();
    }

    else if (button == SDL_BUTTON_MIDDLE)
    {
        _mouse_drag_scrolling = true;
        return;
    }

    else if (button == SDL_BUTTON_RIGHT)
    {
        Log::i(TAG, sl::format("moving to (%f, %f)", world_loc.x, world_loc.y));

        actions::unit_move(globals.time, 0, 0, world_loc);
        _input_action = A_NULL;
    }
}

void deselect() {
    _unit_selection.clear();
    _hud.actionpanel.clear();
    clear_action_state();
}

void mouse_release(unsigned button, int x, int y)
{
    if (hud_element_hovered(&_hud.actionpanel, x, y))
    {
        hud_update_pointer_interaction(&_hud.actionpanel, vec2f(x, y));
    }
    else if (hud_element_hovered(&_hud.minimap, x, y))
    {
        hud_update_pointer_interaction(&_hud.minimap, vec2f(x, y));
    }

    if (button == SDL_BUTTON_MIDDLE)
        _mouse_drag_scrolling = false;

    if (_dragging && _drag_region.valid())
        handle_drag_select(_drag_region);

    _drag_region = rectf::null_rect();
    _dragging = false;
}


void mouse_motion(vec2i pos_prev, vec2i pos_now)
{
    if(_mouse_drag_scrolling)
    {
        auto delta = -((pos_prev - pos_now) * MOUSE_SCROLL_SCALE_FACTOR);
        actions::scroll_map(delta.x, delta.y);
        return;
    }

    if (_hud.actionpanel.bounds().collide(pos_now))
    {
        Log::d(TAG, sl::format("actionpanel is hovered: (%d, %d)", pos_now.x, pos_now.y));
        hud_update_pointer_interaction(&_hud.actionpanel, pos_now);
    }
    else if (_hud.minimap.bounds().collide(pos_now))
    {
        Log::d(TAG, sl::format("minimap is hovered: (%d, %d)", pos_now.x, pos_now.y));
        hud_update_pointer_interaction(&_hud.minimap, pos_now);
    }
    else
    {
        if (ui_state.hovered)
        {
            ui_state.hovered->exit(0,0);
            ui_state.hovered = nullptr;
        }
    }

    // if the left button is down, start a drag
    if( mouse_button_pressed(SDL_BUTTON_LEFT))
    {
        if(!_dragging)
        {
            _drag_start = pos_now;
            _dragging = true;
        }

        _drag_region.set(
            min(_drag_start.x, float(pos_now.x)),
            min(_drag_start.y, float(pos_now.y)),

            abs(_drag_start.x - pos_now.x),
            abs(_drag_start.y - pos_now.y)
        );
    }
}


void selection()
{
    if (!_unit_selection.size())
        return;

    // build a set of actions common to all selected

    auto action_set = std::set<ActionId>();

    for(auto eid : _unit_selection) {
        const auto& entity_actions = _world->prototype_of(eid)->actions;
        action_set.insert(entity_actions.begin(), entity_actions.end());

//        Render::add_halo(unit->location, unit->sight_range, {1,1,0,1});
    }

    for(auto action : action_set)
    {
        for(auto eid : _unit_selection)
        {
            const auto& entity_actions = _world->prototype_of(eid)->actions;

            if (!contained_in(entity_actions, action))
            {
                action_set.erase(action);
            }
        }
    }

    // expose the availabe actions to the outside...
    _exposed_actions.clear();
    expose_actions(action_set.begin(), action_set.end());
    action_set.clear();
}

InputHandler *focus() {
    return focus_stack_top();
}

InputHandler* steal_focus(InputHandler* new_focus)
{
    focus_stack_push(new_focus);
    return focus_stack_top();
}

InputHandler* return_focus()
{
    focus_stack_pop();
    return focus_stack_top();
}


const Array<ActionId> &exposed_actions()
{
    return _exposed_actions;
}

void expose_action(ActionId action)
{
    _exposed_actions.push_back(action);

    if(_hud.actionpanel.panel_stack.size() < 1)
        _hud.actionpanel.push_panel();

    Panel& panel = _hud.actionpanel.panel_stack.top();
    panel.add_option(action, action_string(action));
}

void expose_action_with_option(ActionId action, const char *option)
{
    _hud.actionpanel.expose_option(action, option);
}


void handle_drag_select(rectf region)
{
    //FIXME: this should be a query to the scene, via a quadtree test
    auto entities = _world->with_components( tl::type_mask<Sprite>());

    int selected_entities = 0;
    for (auto e : entities) {
        auto& sprite = _world->get<Sprite>(e);

        if (region.collide(sprite.bounds())) {
            _unit_selection.push_back(e);
            ++selected_entities;
        }
    }

    // if we didn't select anything, interpret as a deselect
    if (!selected_entities) {
        deselect();
        return;
    }
    selection();
}

void bind_key(unsigned key, ActionId action)
{
    _keymap[key] = action;
}

void bind_keys(std::initializer_list<std::pair<u32, ActionId> > bindings)
{
    for(auto& b : bindings) {
        _keymap[b.first] = b.second;
    }
}

const ecs::EntityList &selected_units() { return _unit_selection; }

} // namespace Input
