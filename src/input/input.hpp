#ifndef _inputcontrol_h_
#define _inputcontrol_h_

#include "grimoire/vec2.h"
#include "grimoire/rect.h"

#include "action.h"
#include "runtime.hpp"

/**
 * the main input system for Realm.
 * handles key and mouse bindings,
 * dragging, and runs actions.
 */
namespace input
{

using key_func = void(*)(unsigned key);
using mousebutton_func = void(*)(unsigned, int, int);
using mousemotion_func = void(*)(vec2i, vec2i);


/**
 * callback interface used to pass system input events
 * to whatever has input focus.
 *
 * by default, focus is on the input module itself
 *
 * to steal input focus, instantiate this struct and
 * pass it the appropriate function pointers, and
 * call steal_focus(&your_inputhandler_struct);
 *
 * return focus when done by calling return_focus(). 
 * Focus will go back to the last object who had it before 
 * calling steal_focus()
 * 
 */

struct InputHandler
{
    key_func key_press;
    key_func key_release;
    mousebutton_func mouse_press;
    mousebutton_func mouse_release;
    mousemotion_func mouse_motion;
};


static constexpr const char* TAG = "Input";


void init(Realm::Runtime& rt);
void cleanup();

void frame();

void key_press(unsigned key);
void key_release(unsigned key);
void mouse_press(unsigned button, int x, int y);
void mouse_release(unsigned button, int x, int y);
void mouse_motion(vec2i pos_prev, vec2i pos_now);   

void on_console_toggled();

void bind_key(unsigned key, ActionId action);
void bind_keys(std::initializer_list<std::pair<u32, ActionId>> bindings);

colorf selection_color();
rectf selection_area();

const ecs::EntityList& selected_units();

void handle_drag_select(rectf region);
void deselect();
void selection();

InputHandler* focus();
InputHandler* steal_focus(InputHandler* new_focus);
InputHandler* return_focus();

/**
 * get a list of action types currently available to the player
 */
const Array<ActionId>& exposed_actions();

/**
 * make an action type availabe to the player
 */ 
void expose_action(ActionId action);
    
/**
 * make a submenu action available to the player
 * this is used to populate build options menus
 */
void expose_action_with_option(ActionId action, const char* option);

/**
 * expose a batch of actions at once, via iterator range
 */ 
template <class InputIter>
void expose_actions(InputIter begin, InputIter end);

/**
 * start activation of an action. 
 *
 * most actions require an activation, and then a click on the world
 * to select an argument... either an entity or a world location.
 *
 * actions like stop will trigger at once, while move, attack, and the 
 * like will be stored in the _input_action member, and will not 
 * complete until the corresponding end_game_action() call
 */ 
void begin_game_action(ActionId action);

/**
 * complete the action
 */
void end_game_action(ActionId action, vec2f world_loc);

/**
 * resets the input tracking state to default. 
 *
 * all interface will be cleared, and if there is a selection,
 * it will be reprocessed for available actions
 */
void clear_action_state();



    
   
template <class InputIter>
void expose_actions(InputIter begin, InputIter end)
{
    auto itr = begin;
    while (itr != end)
    {
        expose_action(*itr++);
    }
}

} // namespace Input
#endif //_inputcontrol_h_
