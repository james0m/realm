#ifndef _binding_h_
#define _binding_h_

#include <map>

namespace io {

class Action;

/** a single flat list of all input types; keyboard, mouse, joystick, touch, etc*/
enum InputCode {
    INPUT_NONE = 0,
    INPUT_KEY_1 = 1,
    INPUT_KEY_2 = 2,
    INPUT_KEY_3 = 3,
    INPUT_KEY_4 = 4,
    INPUT_KEY_5,
    INPUT_KEY_6,
    INPUT_KEY_7,
    INPUT_KEY_8,
    INPUT_KEY_9,

    INPUT_KEY_SPACE,
    INPUT_KEY_RETURN,
    INPUT_KEY_ESCAPE,

    INPUT_MOUSE_LEFT_BUTTON,
    INPUT_MOUSE_MIDDLE_BUTTON,
    INPUT_MOUSE_RIGHT_BUTTON
};

struct InputBinding {
    InputCode input;
    Action   *action;
};

class InputMapping {
public:

    void expose_actions(Action *actions, int n) {
        
    }
    
    void bind(InputCode input, Action *action) {
        _bindings[input] = action;
    }

    const InputBinding &get_binding(InputCode input) const {
        return _bindings.at(input);
    }
    
    InputBinding &get_binding(InputCode input) {
        return _bindings[input];
    }
    
private:
    using InputMap = std::map<InputCode, InputBinding>;

    InputMap _bindings;
};

} // input

#endif //_binding_h_
