#include "stb/stb_image.h"
#include "grimoire/filesystem.h"

#include "assets.hpp"
#include "entity/prototype.hpp"
#include "renderer.hpp"


static HashMap<sl::string, Realm::Prototype> s_prototypes;

/**
 * TODO: these are hard-coded initializers for the various entity types
 * in the game. They should be loaded from data files at scenario start
 */
static
void
make_prototypes();

static
void
inherit_prototype_values(Realm::Prototype& proto);

/** buildings */
static
void
make_structure_prototype(Realm::Prototype& proto);

static
void
make_hq_prototype(Realm::Prototype& proto);

static
void
make_barracks_prototype(Realm::Prototype& proto);

static
void
make_powerstation_prototype(Realm::Prototype& proto);

/** unit definitions */
static
void
make_infantry_prototype(Realm::Prototype& proto);

static
void
make_marine_prototype(Realm::Prototype& proto);

static
void
make_engineer_prototype(Realm::Prototype& proto);


namespace assets
{

constexpr const char* TAG = "Assets";

static Realm::Runtime*            s_runtime;
static RenderApi*                 s_render;
static HashMap<sl::string, texid> s_textures;

struct Image {
    const char* name = "";

    int width = 0;
    int height = 0;
    int bpp = 0;
    PixelFormat format = PixelFormat::PF_NULL;
    unsigned char* data = nullptr;

    ~Image() {
        name   = "";
        width  = 0;
        height = 0;
        format = PixelFormat::PF_NULL;

        if(data)
        {
            stbi_image_free(data);
            data = nullptr;
        }
    }
};

static HashMap<sl::string, Image> s_image_data;


void
init(Realm::Runtime& rt) {
    s_runtime = &rt;
    s_render = &Render::get_api();
    make_prototypes();
}


void
cleanup() {
    s_runtime = nullptr;
    s_render = nullptr;

    s_image_data = HashMap<sl::string, Image>();
}


bool
load_image_asset(const char* file, bool upload_texture) {
    sl::string data;
    try {
        data = io::open(file)->as_string();
    }
    catch (io::FileNotFoundError &) {
        Log::e(TAG, sl::format("yo dawg, %s ain't no damned file!", file));
        return false;
    }    
    
    int w, h, nc;
    const auto buffer = stbi_load_from_memory(
        (const u8*) data.c_str(), data.size(), &w, &h, &nc, 4);      

    if (!buffer)
    {
        Log::e(TAG, "Failed to load image data");
        Log::e(TAG, sl::format("stb_image: %s", stbi_failure_reason()));
        return false;
    }

    auto& image_store = s_image_data[file];
    image_store.name = file;
    image_store.data = buffer;
    image_store.width = w;
    image_store.height = h;
    image_store.bpp = nc;
    image_store.format = PixelFormat::RGBA;

    if (upload_texture)
        load_texture(file);

    return true;
    
}


texid
load_texture(const char* file) {
    if (s_image_data.find(file) == s_image_data.end()) {
        Log::e(TAG, sl::format("Could not find image data loaded for file '%s'", file));
        return NULL_TEXTURE;
    }

    Image& image = s_image_data[file];
    auto id = s_render->create_texture(image.width, image.height,
                                       ABGR, // @HACK: Have to use ABGR because SDL is an asshole
                                         // and insists on interpreting endianness of data for you.
                                         // as a result, big-endian RGBA comes out of stb, because
                                         // PNG std requires network byte order and stb doesn't care
                                         // either way, but SDL will interpret things backwards on
                                         // intel to compensate.
                                   AccessMode::STATIC);

    if (id == NULL_TEXTURE)
    {
        Log::e(TAG, "failed to load texture");
        return NULL_TEXTURE;
    }
    
    s_render->bind_texture(id);
    s_render->update_texture((void*)image.data);

    s_textures[file] = id;

    TextureInfo tinfo;
    s_render->get_texture_info(id, tinfo);

    return id;
}


texid
texture(const char* handle)
{
    auto itr = s_textures.find(handle);
    if (itr == s_textures.end())
    {
        Log::e(TAG, sl::format("cannot find texture: %s", handle));
        return NULL_TEXTURE;
    }
    
    return (*itr).second;
}


const Realm::Prototype*
get_prototype(const char* name) {
    return &s_prototypes[name];
}


} // namespace Assets

static
void
load_prototype_assets(Realm::Prototype& proto)
{
    if (proto.using_component(tl::type_id<Sprite>()))
    {
        if (sl::is_empty_string(proto.sprite.image))
            return;
        
        assets::load_image_asset(proto.sprite.image, true);
        proto.sprite.texture = assets::texture(proto.sprite.image);
    }
}


static
void
make_prototypes() {
    make_structure_prototype(s_prototypes["structure"]);
        
    make_hq_prototype(s_prototypes["Hq"]);
    load_prototype_assets(s_prototypes["Hq"]);
    
    make_barracks_prototype(s_prototypes["Barracks"]);
    load_prototype_assets(s_prototypes["Barracks"]);
    
    make_powerstation_prototype(s_prototypes["PowerStation"]);
    load_prototype_assets(s_prototypes["PowerStation"]);
    
    make_infantry_prototype(s_prototypes["infantry"]);
    load_prototype_assets(s_prototypes["infantry"]);
    
    make_marine_prototype(s_prototypes["Marine"]);
    load_prototype_assets(s_prototypes["Marine"]);
    
    make_engineer_prototype(s_prototypes["Engineer"]);
    load_prototype_assets(s_prototypes["Engineer"]);
}


void
make_structure_prototype(Realm::Prototype& proto) {
    proto.name = "structure";
    proto.components = tl::type_mask<Spatial, Sprite, Vitals>();

    proto.spatial.radius = 32;
    proto.spatial.soft_radius = 48;
    proto.spatial.max_speed = 0.0f;
}


static
void
make_hq_prototype(Realm::Prototype& proto) {
    proto.name = "Hq";
    proto.inherit = "structure";

    inherit_prototype_values(proto);

    proto.components |= tl::type_mask<Spawner>();

    proto.spatial.radius = 64.0f;

    proto.sprite.image = "hq.png";
    // proto.sprite.image = "cmd_center.gif";
    proto.sprite.texture = -1;
    proto.sprite.color = { 0, 0, 0, 1.0f };

    proto.actions = { A_UNIT_TRAIN };

    proto.spawner.spawn_type = ST_UNIT;
    proto.spawner.max_builds = 1;
    proto.spawner.options = sl::stringlist ({ "Engineer" });
    proto.spawner.spawn_point = { 0, 64 };

}


static
void
make_barracks_prototype(Realm::Prototype& proto) {
    proto.name = "Barracks";
    proto.inherit = "structure";

    inherit_prototype_values(proto);

    proto.components |= tl::type_mask<Spawner>();

    proto.spatial.radius = 50.0f;
    proto.spatial.soft_radius = 75.0f;
    proto.sprite.image = "sc_barracks.png";    
    
    proto.actions = { A_UNIT_TRAIN };

    proto.spawner.spawn_type = ST_UNIT;
    proto.spawner.max_builds = 1;
    proto.spawner.options = sl::stringlist ({ "Marine" });
    proto.spawner.spawn_point = { 0, 64 };

}


static
void
make_powerstation_prototype(Realm::Prototype& proto) {
    proto.name = "PowerStation";
    proto.inherit = "structure";

    inherit_prototype_values(proto);

    proto.sprite.image = "powerstation.png";  
}


static
void
make_infantry_prototype(Realm::Prototype& proto) {
    proto.name = "infantry";
    proto.components = tl::type_mask<Spatial, gfx::Sprite, Vitals, Unit>();

    proto.spatial.radius = 8.0f;
    proto.spatial.soft_radius = 12.0f;
    proto.spatial.max_speed = 120.0f;

    proto.vitals.hp = 100;
    proto.vitals.mp = 10;
    proto.vitals.xp = 0;
    proto.vitals.lvl = 1;
    proto.vitals.defense = .01;

    proto.unit.sight_range = 120;
}


static
void
make_marine_prototype(Realm::Prototype& proto) {
    proto.name = "Marine";
    proto.inherit = "infantry";
    proto.components = tl::type_mask<Spatial, gfx::Sprite, Vitals, Unit>();
    inherit_prototype_values(proto);

    proto.credits_cost = 20;
    proto.materials_cost = 10;
    proto.build_time = 3000;

    proto.unit.sight_range = 400;
    
    proto.sprite.image = "marine.png";
    proto.sprite.color = { 0.2f, 0.2f, 0.2f, 1.0f };

    proto.vitals.weapon = 1;
    proto.vitals.defense = .3;
    
    proto.actions = { A_UNIT_MOVE, A_UNIT_ATTACK, A_UNIT_STOP };
}

static
void
make_engineer_prototype(Realm::Prototype& proto) {
    proto.name = "Engineer";
    proto.inherit = "infantry";

    inherit_prototype_values(proto);

    proto.components = tl::type_mask<Spatial, gfx::Sprite, Vitals, Unit, Spawner>();

    proto.credits_cost = 30;
    proto.materials_cost = 0;
    proto.build_time = 5000;

    proto.sprite.color = { 0.8f, 0.8f, 0.2f, 1.0f };

    proto.vitals.hp = 85;
    proto.vitals.mp = 100;
    proto.vitals.xp = 0;
    proto.vitals.lvl = 1;
    proto.vitals.weapon = 0;

    proto.spawner.spawn_type = ST_STRUCTURE;
    proto.spawner.max_builds = 1;
    proto.spawner.options = sl::stringlist ({ "Hq", "Barracks", "PowerStation" });

    proto.actions = { A_UNIT_MOVE, A_UNIT_STOP, A_BUILD_STRUCTURE };
}


static
void
inherit_prototype_values(Realm::Prototype& proto) {
    auto itr = s_prototypes.find(proto.inherit);
    if ( itr == end(s_prototypes)) {
        Log::e("SCHLOCKY", sl::format("unrecognized super-type: %s", proto.inherit));
        return;
    }

    auto& base = itr->second;
    proto.components |= base.components;

    // loop over proto.components and initialize each one
    for (int cmp_id=0; cmp_id < ecs::MAX_COMPONENTS; ++cmp_id) {
        if (proto.components & (1 << cmp_id)) {

            if (cmp_id == tl::type_id<gfx::Sprite>()) {
                proto.sprite = base.sprite;
            }

            else if (cmp_id == tl::type_id<Spatial>()) {
                proto.spatial = base.spatial;
            }

            else if (cmp_id == tl::type_id<Unit>()) {
                proto.unit = base.unit;
            }

            else if (cmp_id == tl::type_id<Vitals>()) {
                proto.vitals = base.vitals;
            }

            else if (cmp_id == tl::type_id<Spawner>()) {
                proto.spawner = base.spawner;
            }
        }
    }
}

