#ifndef _assets_h_
#define _assets_h_

#include "forward.hh"

namespace assets
{

void
init(Realm::Runtime& rt);

void
cleanup();

bool
load_image_asset(const char* file, bool upload_texture=false);

idtype
load_texture(const char* file);

idtype
texture(const char* handle);

const Realm::Prototype*
get_prototype(const char* name);

} 
#endif
