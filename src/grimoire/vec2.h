#ifndef _vec2_h_
#define _vec2_h_

#include <cassert>
#include <cmath>
#include <initializer_list>
#include <limits>

template <typename T>
struct vec2 {

	typedef vec2<T> Self;

    T x, y;

    /**
      return a vec2 whose components are both non-numbers... surprisingly useful
    */
    static vec2<T> nan() {
        static const auto vec_nan = vec2<T>(std::numeric_limits<T>::quiet_NaN(),
                                            std::numeric_limits<T>::quiet_NaN());
        return vec_nan;
    }


    /**
     * the default ctor;
     */
	constexpr vec2(T x=0, T y=0):
		x(x), y(y)
	{}

    /**
     *  enable notation like vec2 v = { 12, 12 }
     */
	vec2(const std::initializer_list<T> &l) {
        assert(l.size() == 2);
		auto p = l.begin();
		x = *p++;
		y = *p;
	}

	template <typename U>
	vec2(const vec2<U> other) {
		x = other.x;
		y = other.y;
	}

	Self& operator = (const Self &other) {
		if(this != &other) {
			x = other.x;
			y = other.y;
		}
		return *this;
	}

    /**
     * directly set component values
     */
    Self& set(T ix=0, T iy=0) {
        x = ix; y = iy;
        return *this;
    }

    /**
     * hacky way of allowing things like float x = vec2[0];
     */
	inline T operator[](int idx) {
        return reinterpret_cast<T*>(this)[idx];
    }

    /**
     * offset by vector
     */
    template <class U>
	Self& operator += (const vec2<U> &other) {
		x += other.x;
		y += other.y;
		return *this;
	}

    /** 
     * negate this vector
     */
    inline Self operator - () const {
        return Self(-x, -y);
    }
    
    /**
     * offset by the scalar
     */
    template <class U>
    Self& operator += (U scalar) {
        x += scalar; y+=scalar;
        return *this;
    }

    /**
     * offset the vector with the negative of the vector
     */
    template <class U>
	Self &operator -= (const vec2<U> &other) {
		x -= other.x;
		y -= other.y;
		return *this;
	}

    /**
     * offset the vector by the negative of the scalar
     */
    template <class U>
    Self& operator -= (U scalar) {
        x -= scalar;
        y -= scalar;
        return *this;
    }

    template <class U>
    Self& operator *= (const vec2<U>& v) {
        x *= v.x;
        y *= v.y;
        return *this;
    }
    template <class U>
    Self& operator *= (U v) {
        x *= v;
        y *= v;
        return *this;
    }

    template <class U>
    Self& operator /= (const vec2<U>& v) {
        x /= v.x;
        y /= v.y;
        return *this;
    }
    template <class U>
    Self& operator /= (U v) {
        x /= v;
        y /= v;
        return *this;
    }
    /**
     * pre-increment
     */
    Self& operator++() {
        ++x;
        ++y;
        return *this;
    }

    /** pre-decrement
     */
    Self& operator--() {
        ++x;
        ++y;
        return *this;
    }

    /**
     * post-increment
     */
    Self operator++(int) {
        Self prev(*this);
        ++x;
        ++y;
        return prev;
    }

    /** post-decrement
     */
    Self operator--(int) {
        Self prev(*this);
        ++x;
        ++y;
        return prev;
    }

    /**
     *
     */
    /**
       unary -, for negation
     */
    template <class T2>
    vec2<T2> operator-() {
        return vec2<T2>(-x, -y);
    }
};


template <typename T, typename U>
bool operator == (const vec2<T>& lhs, const vec2<U>& rhs) {
	return (lhs.x==rhs.x && lhs.y==rhs.y);
}

template <typename T, typename U>
bool operator != (const vec2<T>& lhs, const vec2<U>& rhs) {
	return (lhs.x!=rhs.x || lhs.y!=rhs.y);
}

template<typename T, typename U>
vec2<T> operator + (const vec2<T> &lhs, const vec2<U> &rhs) {
	return vec2<T>(
		lhs.x + rhs.x,
		lhs.y + rhs.y
	);
}

template<typename T, typename U>
vec2<T> operator + (const vec2<T> &lhs, U scalar) {
	return vec2<T>(
		lhs.x + scalar,
		lhs.y + scalar
	);
}

template<typename T, typename U>
vec2<T> operator + (U scalar, const vec2<T>& v) {
	return vec2<T>(
        scalar + v.x,
		scalar + v.y
	);
}

template<typename T, typename U>
vec2<T> operator - (const vec2<T> &lhs, const vec2<U> &rhs) {
	return vec2<T>(
		lhs.x - rhs.x,
		lhs.y - rhs.y
	);
}

template<typename T, typename U>
vec2<T> operator - (const vec2<T> &lhs, U scalar) {
	return vec2<T>(
		lhs.x - scalar,
		lhs.y - scalar
	);
}

template<typename T, typename U>
vec2<T> operator - (U scalar, const vec2<T>& v) {
	return vec2<T>(
        scalar - v.x,
		scalar - v.y
	);
}

template<typename T, typename U>
vec2<T> operator * (const vec2<T> &lhs, const vec2<U> &rhs) {
	return vec2<T>(
		lhs.x * rhs.x,
		lhs.y * rhs.y
	);
}

template<typename T, typename U>
vec2<T> operator * (const vec2<T> &lhs, U scalar) {
	return vec2<T>(
		lhs.x * scalar,
		lhs.y * scalar
	);
}

template<typename T, typename U>
vec2<T> operator * (U scalar, const vec2<T>& v) {
	return vec2<T>(
        scalar * v.x,
		scalar * v.y
	);
}

template<typename T, typename U>
vec2<T> operator / (const vec2<T> &lhs, const vec2<U> &rhs) {
	return vec2<T>(
		lhs.x / rhs.x,
		lhs.y / rhs.y
	);
}

template<typename T, typename U>
vec2<T> operator / (const vec2<T> &lhs, U scalar) {
	return vec2<T>(
		lhs.x / scalar,
		lhs.y / scalar
	);
}

template<typename T, typename U>
vec2<T> operator / (U scalar, const vec2<T>& v) {
	return vec2<T>(
        scalar / v.x,
		scalar / v.y
	);
}


typedef vec2<int>           vec2i;
typedef vec2<float>         vec2f;


#endif //_vec2_h_
