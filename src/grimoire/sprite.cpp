#include "sprite.h"


namespace gfx {

Unique<Sprite> Sprite::create(Element *root) {
    return std::unique_ptr<Sprite> (new Sprite(root));
}

Sprite::Sprite(Element *root):
    gfx::Element(root)
{}

Sprite::Sprite(const Sprite &o):
    Element(const_cast<Element*>(o.parent())),
    color(o.color)
{

}

void Sprite::place(float x, float y) {
    auto diff_x = _bounds.center().x - _bounds.x;
    auto diff_y = _bounds.center().y - _bounds.y;
    Element::place(x - diff_x, y - diff_y);
}

vec2f Sprite::position() const {
    return bounds().center();
}

void Sprite::draw(RenderApi &r) {

    auto b = bounds();

    if (texture != NULL_TEXTURE)
    {
        r.draw_texture(texture, region, b);
    }

    else
    {
        r.draw_filled_circle(b.center(), b.w / 2, color);
        r.draw_circle(b.center(), b.w / 2, {0,0,0,1});
    }


    if (elements().size() > 0)
    {
        for(auto e : elements())
            e->draw(r);
    }
}

} // gfx
