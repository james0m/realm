#ifndef _sdl_renderer_impl_h_
#define _sdl_renderer_impl_h_

#include "sdlutils.h"
#include "renderapi.hpp"


struct SDL_Texture;

Uint32 get_sdl_format(PixelFormat fmt);
PixelFormat from_sdl_format(SDL_PixelFormat *format);

class RenderApi_SDL : public RenderApi {
public:
    RenderApi_SDL(SDL_Window *window=nullptr);
    virtual ~RenderApi_SDL();

    static std::unique_ptr<RenderApi> create(SDL_Window *);
    
	virtual void set_context(SDL_Window* window) override;

	void clear();
	virtual void frame_begin() override;
	virtual void frame_end() override;

    virtual void draw_point(float x, float y, const colorf &color) override;
    virtual void draw_line(vec2f p1, vec2f p2, const colorf& color) override;
	virtual void draw_rect(const rectf &r, const colorf &color) override;
	virtual void draw_filled_rect(const rectf &r, const colorf &color) override;

	virtual void draw_circle(const vec2f &c, float radius, const colorf &color) override;
	virtual void draw_filled_circle(const vec2f &c, float radius, const colorf &color) override;

    
    virtual void draw_texture(texid texture,
                              const rectf& src = rectf::null_rect(),
                              const rectf& dst = rectf::null_rect()) override;
    
	virtual void batch_lines(const line_def *lines, int n=1) override;
	virtual void batch_circles(const circle_def* circles, int n=1) override;
	virtual void batch_rects(const rect_def* rects, int n = 1) override;
	virtual void batch_textures(const texture_def *textures, int n=1) override;

	virtual void bind_texture(texid texture) override;
	virtual texid create_texture(int w, int h, PixelFormat fmt, AccessMode access) override;
	virtual void update_texture(const void* data, const rectf *region=nullptr, int pitch=0) override;

    void get_texture_info(texid tex, TextureInfo& info) override;

private:
	using sdl_texture_table = HashMap<texid, SDL_Texture*>;
    using format_table = HashMap<texid, Uint32>;
    
	void save_draw_color() { 
		SDL_GetRenderDrawColor (_ctx.get(), 
			&_color_store.r,
			&_color_store.g,
			&_color_store.b,
			&_color_store.a 
		);
	}

	void restore_draw_color() {
		SDL_SetRenderDrawColor(_ctx.get(),
			_color_store.r,
			_color_store.g,
			_color_store.b,
			_color_store.a
		);
	}

	color<Uint8>              _color_store;

	sdl::renderer_ptr         _ctx = nullptr;

	sdl_texture_table         _textures;
    format_table              _texture_format;
	texid                     _bound_texture = 0;
};

#endif
