#ifndef _filesystem_h_
#define _filesystem_h_

#include "types.h"
#include "file.h"
#include "stringlib.h"

using sl::stringlist;

namespace io 
{


FileHandle open(const String &path, AccessMode mode=ACCESS_READ);
FileHandle mkfile(const String &path);
FileHandle mkdir(const String &path);
bool       exists(const String &path);
bool       isfile(const String &path);
bool       isdir(const String &path);

String     dirname(const String& path);

void       mount(const String& path);

class FileSystem
{
public:

    ////////////////////////////////////////////////////////////////////////
    // get actual running filesystem object
    static FileSystem  &get();

    ////////////////////////////////////////////////////////////////////////
    // mount a directory to the filesystem, and specify its mount point
    //
    // @param path the path to add
    // @param mountpt (optional) specify the name used to access the path
    // @return true on success, false on fail 
    static bool         mount(const String &path, const String &mountpt="");
    
    ////////////////////////////////////////////////////////////////////////
    // get the name of the filesystem object's implementation
    // most likely will be "PhysFSFileSystem"
    virtual const char* type() const = 0;

    /////////////////////////////////////////////////////////////////////////
    // open a new file 
    //
    // @param path path to the file
    // @param mode read / write mode 
    // @return handle to the new file, or an empty handle if failed
    virtual FileHandle  open_file(const String &path, AccessMode mode) const = 0;
    
    ////////////////////////////////////////////////////////////////////////
    // create a file, similar to 'touch' on unix-y systems
    virtual FileHandle  create_file(const String &path) const = 0;

    ////////////////////////////////////////////////////////////////////////
    // create a directory
    virtual FileHandle  create_dir(const String &path) const = 0;
    
    ////////////////////////////////////////////////////////////////////////
    // test if a file or directory exists
    virtual bool        exists(const String &path) const = 0;

    ////////////////////////////////////////////////////////////////////////
    // test if the path is a real file
    virtual bool        isfile(const String &path) const = 0;

    ////////////////////////////////////////////////////////////////////////
    // test if the path is a real directory
    virtual bool        isdir(const String &path) const = 0;

    ////////////////////////////////////////////////////////////////////////
    // add_path
    // add a new path/mount point to the filesystem
    //
    // @param path - the full path to the directory
    // @param mountpt - optional mount point name
    // @return true if successfully added , false otherwise 
    virtual bool        add_path(const String &path, const String &mountpt="") = 0;

    ////////////////////////////////////////////////////////////////////////
    // return the root of the filesystem
    virtual String      root() const = 0;

    /////////////////////////////////////////////////////////////////////
    // get a list of all directories available to read from
    virtual const stringlist& search_path() const = 0;
};

class FileSystemError : public std::runtime_error
{
public:
    FileSystemError(const String &msg): 
        std::runtime_error(msg)
    {
    }

    virtual ~FileSystemError() throw() {}
};

class FileNotFoundError : public FileSystemError
{
public:
    FileNotFoundError(const String &filename, const String &extra=""):
        FileSystemError("FileNotFoundError: " + filename + ":" + extra),
        _filename(filename)
    {
    }

private:
    String _filename;
};

}; // io

#endif
