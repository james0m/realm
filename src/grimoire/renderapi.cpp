#include "renderapi.hpp"


RenderApi::RenderApi(SDL_Window *window) {
	_window = window;
}

colorf RenderApi::clear_color() const {
	return _clear_color;
}

void RenderApi::set_clear_color(const colorf &color) {
	_clear_color = color;
}

void RenderApi::set_context(SDL_Window *window) {
	_window = window;
}
