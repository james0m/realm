#ifndef TMX_MAP_READER_H_
#define TMX_MAP_READER_H_

#include <exception>
#include <map>
#include "3p/rapidxml/rapidxml.hpp"

#include "grimoire/ioutils.h"
#include "grimoire/stack.h"
#include "config.hpp"

#include "tiledmap.h"

namespace io {


typedef rapidxml::xml_document<>   XmlDocument;
typedef rapidxml::xml_node<>       XmlNode;
typedef rapidxml::xml_attribute<>  XmlAttr;

using AnyMap = std::map<String, Any>;


Unique<tmx::TiledMap>
load_tmx_map(const String &tmxfile);


struct TmxMapReader {
    explicit
	TmxMapReader(const String &file="");

	Unique<tmx::TiledMap>
	read(const String &data="");

protected:
	bool
	extract_data(const String &data);

	void
	extract_map();

	void
	extract_tileset();

	void
	extract_external_tileset(u32 tsid, const String &file);

    void
	extract_tileset_image(u32 tsid);

    void
	extract_tileset_terraintypes(u32 tsid);

    void
	extract_tileset_terrain(u32 tsid);

    void
	extract_tileset_tileoffset(u32 tsid);

    void
	extract_tileset_tile(u32 tsid);

    void
	extract_tileset_inner(u32 tsid);

    AnyMap
    extract_properties();

    void
	extract_property(XmlNode *owner, AnyMap& out);

	void
	extract_layer();

	void
	extract_objectgroup();

	void
	extract_imagelayer();

    void
	enter_node(XmlNode *node);

    void
	exit_node();

private:
    Unique<XmlDoc>         _document = nullptr;
    XmlNode*               _node = nullptr;
    util::Stack<XmlNode *> _scope;

	Unique<tmx::TiledMap>   _map = nullptr;
};


class TmxReadError : std::exception {
public:
    explicit
	TmxReadError(const char *msg=""):
        _what(msg)
	{}

	const char *
    what() const noexcept override { return _what; }

private:
    const char *_what;
};


}; // namespace io
#endif
