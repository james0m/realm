#include "grimoire/logger.h"
#include "grimoire/grimoire_util.h"

using namespace std;

namespace util {

Logger::Logger(std::ostream &stream)
{
    _streams.push_back(&stream);    
}

void clear_string_stream(std::stringstream& ss) {
    ss.str(String());
}

void Logger::out(char lvl, const char* tag, const char* msg)
{
    _ss << lvl << ": [" << tag << "] " << msg << endl;
    auto s = _ss.str();

    for(auto output_stream : _streams) {
        *output_stream << s;
    }   

    clear_string_stream(_ss);
}

void Logger::add_output_stream(std::ostream& stream) {
    if (std::find(_streams.begin(), _streams.end(), &stream) == _streams.end())
        _streams.push_back(&stream);
        
}

void Logger::drop_output_stream(const std::ostream& stream)
{
    auto itr = std::find(_streams.begin(), _streams.end(), &stream);
    if (itr != _streams.end())
        _streams.erase(itr);
}

}// util
