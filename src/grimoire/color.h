#ifndef _color_h_
#define _color_h_

#include <initializer_list>
#include <limits>

template <typename T>
struct color {
    T r=0,
      g=0,
      b=0,
      a=0;

    color(T r=0, T g=0, T b=0, T a=1):
        r(r), g(g), b(b), a(a)
    {}

    template <typename O>
    color(const color<O> &other) {
        r = other.r;
        g = other.g;
        b = other.b;
        a = other.a;
    }

    color(const std::initializer_list<T> &in_components) {
        if(in_components.size() == 4) {
            const T* ccomp = in_components.begin();
            r = *ccomp++;
            g = *ccomp++;
            b = *ccomp++;
            a = *ccomp;
        }
    }

    template <typename OtherType>
    color<T>& operator=(const color<OtherType>& other) {
        r = other.r;
        g = other.g;
        b = other.b;
        a = other.a;
        return *this;
    }

    template <typename OtherType>
    bool operator == (const color<OtherType>& other) {
        return (r == other.r && g == other.g && b == other.b && a == other.a);
    }

    template <typename OtherType>
    bool operator != (const color<OtherType> &other) {
        return !(operator==(other));
    }

    T& operator[] (int channel) {
        return this[channel];
    }
};


class colorf : public color<float> {
public:

    colorf(float r=0, float g=0, float b=0, float a=1):
        color<float>(r,g,b,a)
    {
    }

    colorf& normalize() {
        r /= 255;
        g /= 255;
        b /= 255;
        a /= 255;

        return *this;
    }

    colorf normalized() const {
        return colorf(r/255.0f, g/255.0f, b/255.0f, a/255.0f);
    }

    // convert the normalized color value to a fixed-point/integer format
    template <typename IntType>
    color<IntType> convert() const {
        auto component_max = std::numeric_limits<IntType>::max();
        return color<IntType>( r * component_max,
                               g * component_max,
                               b * component_max,
                               a * component_max);
    }    

};

#include <iostream>
template <class Color>
std::ostream &operator << (std::ostream &out, const color<Color> &c)
{
    out << "color { " << c.r << ", " << c.g << ", " << c.b << ", " << c.a << " }\n";
    return out;
}

#endif
