#ifndef _types_h_
#define _types_h_


#include <string>

// @TODO: replace base types with std::intxx_t/std::uintxx_t
typedef unsigned int uint;
typedef int                     s32;
typedef unsigned int            u32;
typedef short                   s16;
typedef unsigned short          u16;
typedef char                    s8;
typedef unsigned char           u8;

typedef unsigned int            idtype;
typedef u32        				aspect_mask;
typedef u32                     ComponentMask;

typedef unsigned                texid;

#endif
