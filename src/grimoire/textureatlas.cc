#include <stdio.h>
#include <algorithm>

#include "grimoire_util.h"
#include "textureatlas.h"
#include "ioutils.h"
#include "filesystem.h"
#include "sdlutils.h"



using namespace std;
//using namespace grimoire;
using region_list_t = Array<SDL_Rect>;
//struct compare_rect_x { bool operator()(const rectf &lhs, const rectf rhs) { return lhs.x < rhs.x; }};

struct match_rect_x_func {
    int value;

    match_rect_x_func(int x): value(x) {}

    bool operator() (const SDL_Rect &rect) {
        return (rect.x == value);
    }
};

/*
parses subregions of an image marked with special colors
(control pixels) defined in the first row of pixels.

The alg rolls over each pixel in an image _once_, looking
for either of two colors: a start region color, or an end
region color.

Each region will be marked with one start pixel on its
top-left, and two end point pixels placed outward
horizontally and vertically from each start point.

Regions do not have to be aligned to a grid or flush with
one another, but there can be no overlap.

Once a start point is found, a temp region object is created
with the current x,y values. The alg then expects to find an
end pixel on the same row, denoting region width else an error
is thrown. Once it finds that end pixel, it sets the width,
adds the region to the partials list, and nulls the temp object.

If an end pixel is found, and we aren't trying to close the width of
a new region, the alg looks for the partial region with a matching x coordinate.
It sets the height, moves the region to the results list. If none is found
throw an error.

This process allows for a single scan to pick up any subregions denoted by
the given control pixel values.
*/
void image_find_marked_regions(SDL_Surface *image, u32 startcolor, u32 endcolor, region_list_t &regions) {
    if(image == NULL || startcolor == endcolor) {
        return;
    }

    pixel_iterator<u32> itr, end;

    itr                 = sdl::surface_get_iterator<u32>(image);
    end                 = sdl::surface_get_end<u32>(image);
    int scanline_width  = itr.stride();

    itr.move_down();
	
    int x = 0;
    int y = 1;
    Array<SDL_Rect> partial_regions;
    SDL_Rect tmp_region;

    while(itr != end) {

        if(*itr == startcolor) {
            sdl::rect_set_position(tmp_region, itr.x(), itr.y());
        }
        else if (*itr == endcolor) {

            if(! sdl::rect_is_null(tmp_region)) {
                // found the width end pixel for the region located on the current row
                tmp_region.w = itr.x() - tmp_region.x;
                partial_regions.push_back(tmp_region);

                // printf("Added new partial region: Rect(%d, %d, %d, %d)\n", tmp_region.x, tmp_region.y, tmp_region.w, tmp_region.h);
                sdl::rect_set_null(tmp_region);
            }
            else {
                // either the height of an incomplete region,
                // or we have an error

                match_rect_x_func  match_rect_x(x);

                auto region = std::find_if(
                    partial_regions.begin(),
                    partial_regions.end(),
                    match_rect_x
                );

                if(region != partial_regions.end()) {
                    // this is the height of a region,
                    // set the height, add to atlas, and pop from queue
                    region->h = itr.y() - region->y;
                    region->x += 1;
                    region->w -= 1;
                    regions.push_back(*region);
                    // printf("Added complete region: Rect(%d, %d, %d, %d)\n", region->x, region->y, region->w, region->h);
                    partial_regions.erase(region);
                }
                else {
                    // error, should have found a matching x coordinate
                    printf("error @ (%ld, %ld): found end pixel. Should have found a partial region with matching x-coordinate\n", itr.x(), itr.y());
                }
            }
        }

        ++itr;
        ++x;

        if( x >= scanline_width) {
            x = 0;
            ++y;
        }
    }
}

std::pair<u32, u32> image_read_control_pixels(SDL_Surface *image) {

    std::pair<u32, u32> result(0,0);
    if(image ) {
        pixel_iterator<u32> itr;

        itr = sdl::surface_get_iterator<u32>(image);

        result.first = *itr;
        result.second = *(itr + 1);
    }
    return result;
}

TextureAtlas::TextureAtlas(texid texture):
	m_texture(texture),
    m_active_region(0),
    m_regions()
{}

TextureAtlas::TextureAtlas(const TextureAtlas &o):
	m_active_region(0),
	m_regions()
{
	*this = o;
}

TextureAtlas::~TextureAtlas() {

}

TextureAtlas& TextureAtlas::operator = (const TextureAtlas& o) {
	if(this != &o)
		copy(o);

	return *this;
}

void TextureAtlas::copy(const TextureAtlas &o) {

	m_texture = o.m_texture;
	m_regions = o.m_regions;
	m_active_region = o.m_active_region;
}

bool TextureAtlas::valid() const {
	return m_regions.size() > 0;
}

rectf TextureAtlas::region(unsigned int idx) const {
	ASSERT_BOUNDS_CHECK(idx, m_regions.size());
	return m_regions[idx];
}
void TextureAtlas::region_lookup(unsigned int idx, rectf *region) const {
	ASSERT_BOUNDS_CHECK(idx, m_regions.size());
	*region = m_regions[idx];
}

texid TextureAtlas::get_texture() const {
	return m_texture;
}

void TextureAtlas::get_rect(rectf &out) const {
	out = m_regions[m_active_region];
}

void TextureAtlas::add_region(const rectf &region) {
	m_regions.push_back(region);
}

void TextureAtlas::_reset() {
	m_regions.clear();
	m_active_region = 0;
}
int TextureAtlas::load_marked_image(const String &file) {

    auto data = io::open(file)->as_string();
	sdl::surface_ptr handle = sdl::image_load_from_memory(data);
	SDL_Surface *tmp = handle.get();

	if(tmp != nullptr) {
		region_list_t img_regions;
		std::pair<u32, u32> control = image_read_control_pixels(tmp);

		image_find_marked_regions(
			tmp,
			control.first,
			control.second,
			img_regions
		);

		add_regions(&img_regions[0], img_regions.size());

		//compare_rect_x comp;
		std::sort(m_regions.begin(), m_regions.end(), [](const rectf &lhs, const rectf rhs) { return lhs.x < rhs.x; });
	}
	else
		printf("texture_atlas::load_marked_image(): image loading FAILED - %s\n", SDL_GetError());

	return m_regions.size();
}

size_t TextureAtlas::region_count() const {
	return m_regions.size();
}

size_t TextureAtlas::set_active_region(size_t idx) {
	m_active_region = idx;
	return m_active_region;
}
