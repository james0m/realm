// -*- c++ -*-
#ifndef _wzrd_element_h_
#define _wzrd_element_h_

#include "grimoire/grimoire_util.h"
#include "grimoire/renderapi.hpp"
#include "grimoire/rect.h"
#include "grimoire/sig.h"

namespace gfx {

class Element;
class Scene;

struct Clicked {
    long time;
    int button;
    int x;
    int y;
    Element *element;
};


class Element {
public:

    using ElementList = Array<Element*>;

    virtual ~Element() {}

    const Element* parent() const;
    Element* parent();

    const Scene*        scene() const;
    void                set_position(float x, float y) { _bounds.x = x; _bounds.y = y; }
    void                set_bounds(rectf bounds);
    void                set_bounds(float x, float y, float w , float h);
    void                set_dimensions(float w, float h);
    
    rectf               bounds() const;
    rectf               bounds_local() const;

    float               width() const { return _bounds.w; }
    float               height() const { return _bounds.h; }
    
    vec2f               position() const;
    virtual void        place(float x, float y);
    void                place(vec2f location);

    virtual void        translate(float dx, float dy);
    virtual void        translate(vec2f delta);

    virtual void        draw(RenderApi &r) = 0;

    virtual void        attach(Element *child);
    virtual void        detach(Element *child);

    virtual void        attached(Element &parent);

    virtual void        set_anchor(float dx, float dy);
    void                set_anchor(vec2f pt);

    vec2f               anchor() const;

    ElementList &       elements();
    const ElementList & elements() const;

    float opacity() const { return _opacity; }

    bool has_child(Element* e) const;

protected:
    Element(Element *root);

    rectf       _bounds;
    vec2f       _anchor;
    float       _opacity = 1.0f;
    Element    *_parent;
    ElementList _elements;
};


} // grfx
#endif
