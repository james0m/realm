#pragma once
#include <assert.h>


/**
 * simple range type that allows for python-like syntax when 
 * iterating over a range of numbers
 *
 * eg: for (auto x : range<int>(10)) ...  -> iterates thru 0 - 9 
 */
template <class C>
struct numeric_range {

    struct iterator {

        C _val, _incr;

        iterator(C v, C incr) {
            _val = v;
            _incr = incr;
        }

        bool operator ==(const iterator& i) {
            return _val == i._val;
        }

        bool operator !=(const iterator& i) {
            return !operator==(i);
        }
        
        void operator++() {
            _val += _incr;
        }

        
        C operator*() { return _val; }
        
        iterator operator++(int) {
            iterator i(_val);
            _val += _incr;
            return i;
        }
    };

    using const_iterator = const iterator;

    
    const_iterator begin() const { return const_iterator(_b, _incr); }
    const_iterator end() const { return const_iterator(_end, _incr); }

    numeric_range() {
        _b = _end = _incr = 0;
    }
    
    numeric_range(C c) {               
        _b = 0;
        _end = c;
        _incr = 1;
    }

    numeric_range (C b, C e, C incr) {
        _b = b;
        _end = e;
        _incr = (incr == 0) ? 1 : incr;

    }

    numeric_range(const numeric_range &o) {
        *this = o;
    }

    numeric_range& operator= (const numeric_range& o) {
        if (this != &o) {
            _b = o._b;
            _end = o._end;
            _incr = o._incr;
        }
        return *this;
    }

    void set(C e) {
        _b = 0;
        _end = e;
        _incr = 1;
    }

    void set (C b, C e, C incr) {
        _b = b;
        _end = e;
        _incr = incr;
    }
private:
    C _b, _end, _incr;
};

template <class T>
numeric_range<T> range(T c) {
    return numeric_range<T>(c);
}

template <class T>
numeric_range<T> range(T b, T e, T incr) {
    return numeric_range<T>(b, e, incr);
}
