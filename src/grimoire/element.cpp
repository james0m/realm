#include "element.h"
#include "scene.h"

namespace gfx {

Element::Element(Element *parent)
{
    _parent = parent;
}

void Element::set_bounds(rectf bounds) {
    _bounds = bounds;
}

void Element::set_bounds(float x, float y, float w , float h)
{
    _bounds.set(x,y,w,h);
}

void Element::set_dimensions(float w, float h)
{
    _bounds.w = w;
    _bounds.h = h;
}

rectf Element::bounds() const
{
    if(_parent) 
        return _bounds.translated(_parent->bounds().topleft());
    
    return _bounds;    
}

rectf Element::bounds_local() const {
    return _bounds;
}

vec2f Element::position() const {
    return bounds().topleft();
}

void Element::place(float x, float y)
{
    _bounds.x = x;
    _bounds.y = y;
}

void Element::place(vec2f location)
{
    _bounds.x = location.x;
    _bounds.y = location.y;
}

void Element::translate(float dx, float dy) {
    translate(vec2f(dx, dy));
}

void Element::translate(vec2f delta)
{
    _bounds.x += delta.x;
    _bounds.y += delta.y;
}

void Element::attach(Element *child) {
    child->_parent = this;
    _elements.push_back(child);
    child->attached(*this);
}

void Element::attached(Element& parent) {
    // default impl is a no-op
}
void Element::detach(Element *child) {
    auto itr = std::find(begin(_elements), end(_elements), child);
    if (itr != _elements.end())
        _elements.erase(itr);
}

void Element::set_anchor(float dx, float dy) {
    _anchor.set(dx, dy);
}

void Element::set_anchor(vec2f offset) {
    _anchor = offset;
}

vec2f Element::anchor() const {
    return _anchor;
}

Element::ElementList &Element::elements() {
    return _elements;
}

const Element::ElementList &Element::elements() const {
    return _elements;
}

bool Element::has_child(Element *e) const {
    return std::find(begin(_elements), end(_elements), e) != end(_elements);
}

const Element* Element::parent() const {
    return _parent;
}

const Scene* Element::scene() const {
    auto *node = _parent;
    while(node && node->parent() != nullptr) {
        node = node->parent(); 
    }

    return static_cast<Scene*>(node);
}
Element* Element::parent() {
    return _parent;
}





}


