#ifndef _asset_h_
#define _asset_h_

#include <exception>
#include <string>
#include <map>
#include <memory>

#include "grimoire_util.h"


namespace io 
{

enum AccessMode {
    ACCESS_CLOSED,
    ACCESS_READ,
    ACCESS_WRITE,
    ACCESS_READ_BINARY,
    ACCESS_WRITE_BINARY
};

const char *AccessModeStr(AccessMode mode);

class File;
typedef std::shared_ptr<File> FileHandle;

class File
{
public:
    static const size_t ALL;

    File(const String &path="", AccessMode mode=ACCESS_READ);

    virtual ~File();

    

    ///////////////////////////////////////////////////////////////////
    // get a copy of the asset's file path
    virtual String path() const
    {
        return _path;
    }
    virtual bool isopen() const;
    virtual bool exists() const;
    virtual bool isfile() const;
    virtual bool isdir()  const;
    
    ///////////////////////////////////////////////////////////////////
    // get the asset's filename 
    String name() const;

    ///////////////////////////////////////////////////////////////////
    // get the access mode of this file
    AccessMode mode() const { return _mode; }

    ///////////////////////////////////////////////////////////////////
    // get the file's extension
    String ext() const;

    ///////////////////////////////////////////////////////////////////
    // close the file
    virtual void close();

    ///////////////////////////////////////////////////////////////////
    // get the current read position in bytes
    virtual size_t readpos() const { return _readpos; };

    ///////////////////////////////////////////////////////////////////
    // move the read position
    virtual void seek(size_t bytes) = 0;
    ///////////////////////////////////////////////////////////////////
    // restore the read position to beginning of file
    virtual void reset() { _readpos = 0; }

    ///////////////////////////////////////////////////////////////////
    // Read given number of bytes into a new string object, starting 
    // at current readpos
    //
    // @param bytes: number of bytes to read ALL/-1 for default
    // @return new String with the requested data
    virtual String as_string(size_t bytes=File::ALL) const;

    ///////////////////////////////////////////////////////////////////
    // Read given number of bytes into dst, starting at current
    // readpos
    //
    // @param dst: pointer to writable memory
    // @param bytes: number of bytes to read ALL/-1 for default
    // @return number of bytes actually read 
    virtual size_t read(void *dst, size_t bytes=File::ALL ) const;

    virtual void write(void *data, size_t bytes);
    virtual void writestr(const String &data);

    ///////////////////////////////////////////////////////////////////
    // get the size of the asset data in bytes
    virtual size_t size() const;

protected:
    AccessMode _mode;
    String     _path = "";
    size_t     _readpos = 0;
};



} // io


#endif
