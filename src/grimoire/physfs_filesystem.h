#ifndef _physics_fs_filesystem_h_
#define _physics_fs_filesystem_h_

#include "types.h"
#include "filesystem.h"
#include "file.h"
#include "physfs.h"

namespace io
{

struct PhysFSFile : File
{
	PhysFSFile(const String &path, AccessMode mode=ACCESS_READ);
	virtual ~PhysFSFile();

	virtual bool exists() const;
	virtual bool isfile() const;
	virtual bool isdir() const;

	virtual size_t size() const;

	virtual void seek(size_t inc);
	virtual void reset();

	virtual String as_string(size_t bytes=File::ALL) const;

	virtual size_t read(void *dst, size_t bytes=File::ALL ) const;
	virtual void write(void *data, size_t bytes);
	virtual void writestr(const String &data);

	virtual void close();
	
private:
	PHYSFS_File *_handle = NULL;
};

class PhysFSFileSystem : public FileSystem
{
public:
    PhysFSFileSystem(const String &root);
	virtual ~PhysFSFileSystem();

	virtual const char *type() const;

	virtual FileHandle open_file(const String &path, AccessMode mode) const;
    virtual FileHandle create_file(const String &path) const;
    virtual FileHandle create_dir(const String &path) const;
    
    virtual bool add_path(const String &path, const String &mountpt="");
    virtual String root() const;

    virtual bool exists(const String &path) const;
    virtual bool isfile(const String &path) const;
    virtual bool isdir(const String &path) const;

    virtual const stringlist& search_path() const ;

protected:
    stringlist list_search_paths() const;

    stringlist _search_paths;
};

}// io

#endif
