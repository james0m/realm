// -*- c++ -*-
#ifndef _wzrd_sprite_h_
#define _wzrd_sprite_h_


#include "element.h"
#include "renderapi.hpp"

namespace gfx {

struct Sprite : public gfx::Element
{
    idtype entity = -1;
    const char* image = "";
    
    colorf color;

    idtype texture = NULL_TEXTURE;
    rectf region;
    
    Sprite(gfx::Element *root=nullptr);
    Sprite(const Sprite& o);

    static Unique<Sprite> create(Element* root=nullptr);

    virtual void place(float x, float y) override;

    vec2f position() const;
    void draw(RenderApi &r) override;
};


} // gfx
#endif

