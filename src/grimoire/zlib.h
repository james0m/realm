#ifndef _zlib_h_
#define _zlib_h_

#include "types.h"

namespace util {

void zlib_inflate(char *out, const char *in);
void zlib_deflate(char *out, const char *in);

}
#endif // !_zlib_h_

