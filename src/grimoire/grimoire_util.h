#ifndef GRIMOIRE_UTILITY_H_
#define GRIMOIRE_UTILITY_H_

#include <cassert>
#include <algorithm>
#include <initializer_list>
#include <iostream>
#include <map>
#include <memory>
#include <queue>
#include <set>
#include <sstream>
#include <type_traits>
#include <utility>

#include <boost/any.hpp>

#include "types.h"


using Any = boost::any;
using std::pair;
using std::vector;



template <class T>
using Unique = std::unique_ptr<T>;

//template <class T>
//using Shared = std::shared_ptr<T>;


template <class T>
using Array = std::vector<T>;

#include <unordered_map>
template <class K, class V>
using HashMap = std::unordered_map<K, V>;

//template <class KV>
//using Set = std::set<KV>;


#include <unordered_set>
template <class KV>
using HashSet = std::unordered_set<KV>;

using String = std::string;


#define pbegin(obj)(&(obj)[0])
#define ALL_OF(c)std::begin((c)), std::end((c))
#ifdef DEBUG
#define ASSERT(cond) (assert((cond)) )
#else
#define ASSERT(cond)
#endif

//#define for_each_ptr(a,b)for(auto (a)=&(b)[0], endp=&(b)[0]+(b).size(); (a)!=endp; ++(a))

template <class Ct>
size_t size(const Ct& obj) {
    return obj.size();
}

template <class It>
size_t sum(It itr, It end) {
    size_t n = 0;
    while(itr != end)
        n += *itr++;
    return n;
}

/**
 * erase from a vector by swapping to the end and popping off the end element
 * assumes you don't care about element order
 **/
template <class T, class Iter>
void pop_erase(Array<T>& seq, Iter elem)
{
    std::iter_swap(elem, std::end(seq) - 1);
    seq.pop_back();
}

template <class T>
void erase_if_contained(Array<T>& seq, const T& val) {
    auto itr = find(ALL_OF((seq)), val);
    if (itr != end(seq)) {
        pop_erase(seq, itr);
    }
}

template <class II, class T>
bool contained_in(II begin, II end, const T& value)
{
    return std::find(begin, end, value) != end;
}

/**
 * a shorthand way to check if a value is in a standard container type
 */
template <class C, class T>
bool contained_in(const C& container, const T& value)
{
    return contained_in(ALL_OF((container)), value);
}

/**
 * search
 * 
 * shorthand for std::find()
 */
template <class C, class T>
typename C::const_iterator search(const C& container, const T& value)
{
    return std::find(container.begin(), container.end(), value);
}

/**
 * search 
 *  shorthand for std::find()
 *  non-const version
 */
template <class C, class T>
typename C::iterator search(C& container, const T& value)
{
    return std::find(container.begin(), container.end(), value);
}

//template <class II, class T>
//II b_search_inner(II begin, II end, const T& value)
//{
//    const auto N = std::distance(begin, end);
//
//    II mid = begin;
//    std::advance(mid, N / 2);
//
//    if (*mid == value)
//        return mid;
//
//    else if (*mid > value)
//        return b_search_inner(begin, mid, value);
//
//    else
//        return b_search_inner(++mid, end, value);
//}

//template <class C, class T>
//typename C::iterator b_search(C& container, const T& value)
//{
//    auto n = container.size();
//    return b_search_inner(container.begin(), container.end(), value);
//}


namespace util {

#define ASSERT_BOUNDS_CHECK(index, capacity) (assert( (index) >= 0 && (index) < (capacity) ));
//#define ASSERT_IN_RANGE(lower, value, upper) (assert( (value) >= (lower) && (value) < (upper)));

using std::unique_ptr;
using std::remove_extent;

template<class T>
struct Unique_if {
	typedef unique_ptr<T> Single_object;
};

template<class T>
struct Unique_if<T[]> {
	typedef unique_ptr<T[]> Unknown_bound;
};

template<class T, size_t N>
struct Unique_if<T[N]> {
	typedef void Known_bound;
};

template<class T, class... Args>
typename Unique_if<T>::Single_object
make_unique(Args&&... args)
{
	return unique_ptr<T>(new T(std::forward<Args>(args)...));
}

template<class T>
typename Unique_if<T>::Unknown_bound
make_unique(size_t n)
{
	typedef typename remove_extent<T>::type U;
	return unique_ptr<T>(new U[n]());
}

template<class T, class... Args>
typename Unique_if<T>::_Known_bound
make_unique(Args&&...) = delete;

} // namespace util

#endif
