#ifndef __stringlib_h_
#define __stringlib_h_

#include <cstring>
#include "grimoire_util.h"

namespace sl {
    /*  */
using string = std::string;


//////////////////////////////////////////////////////////////////////////
//StringFilter should be a function that converts the entire string
// at once into a new char buffer.
typedef void(*stringfilter)(char *, const char*);

typedef Array<sl::string>   stringlist;

//////////////////////////////////////////////////////////////////////////
// explode the string into multiples based on delimiter
int        split(const string &s, char delim, stringlist &output);
stringlist split(const string& s, char delim);

inline bool is_empty_string(const char* s)
{
    return strcmp(s, "") == 0;
}
//////////////////////////////////////////////////////////////////////////
// Manipulate an entire string at once, instead of char by char
void transform_string(string &s, stringfilter f);

template <class T>
T fmtarg(T val) noexcept {
    return val;
}

template <class Ch>
const Ch* fmtarg(const std::basic_string<Ch> &str) {
    return str.c_str();
}

template <class ...Args>
sl::string

format(const char* fmt, const Args&... args) {
	constexpr auto BUFSZ = 1024;
	char buf[BUFSZ];
	const auto len = snprintf(buf, BUFSZ, fmt, fmtarg(args)...);
    return (len > 0 && len < BUFSZ) ? sl::string(buf, len) : sl::string();
}

}


#endif
