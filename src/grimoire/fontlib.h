#ifndef _fontlib_h_
#define _fontlib_h_
#include <freetype2/ft2build.h>
#include FT_FREETYPE_H

#include "grimoire/grimoire_util.h"
#include "grimoire/rect.h"
#include "grimoire/renderapi.hpp"


namespace txt {

void init();
FT_Library lib();
void cleanup();

struct Glyph {
    char  id = -1;      // character code
    u32   index = -1;   // font-specific glyph index
    texid texture = -1; // texture id
    rectf region;  // texture coordinates
    vec2f advance; // distance needed to move clear the glyph for the next one
    vec2f bearing; // space from the origin to the glyph bounding box

    static const Glyph NULL_GLYPH;
};



class GlyphAtlas {
public:
    using glyph_map = std::map<char, Glyph>;
    using iterator = glyph_map::iterator;
    
    Glyph& operator[](char ch);

    const Glyph& glyph(char ch) const;
    size_t size() const;
    iterator begin();
    iterator end();

private:
    glyph_map _glyphs;
};


struct Font
{
    int          size;
    String  filepath;
    FT_Face      face;
    GlyphAtlas   glyphs;

    int line_height = 0;
    
    Font(String path="", int pt=14);
    ~Font();
    
    static Unique<Font> from_file(RenderApi& renderer,
                                  String path,
                                  int pt=10);
    
    bool         has_glyph_atlas() const;

    const Glyph& glyph(char ch) const;
    void         init(RenderApi& renderer, String file);    
    int          faces() const;    
};


struct GlyphPosition
{
    u8    glyph;
    rectf extent;
};



struct Layout
{
    const Font* font;
    int      lines;
    rectf    extent;
    std::vector<GlyphPosition> slots;
};



/**
 * build the atlas for rendering font glyphs
 */
GlyphAtlas generate_atlas(RenderApi &render,
                          Font &font,
                          const String& chars); 

/** 
 * render a single string of text starting at the given screen coords
 * with a single color, on a single line.
 */
void render_text(RenderApi &r,
                 const Font &font,
                 const String &text,
                 int x, int y,
                 colorf color);

void render_text(RenderApi &r,
                 const Font &font,
                 const String& text,
                 rect<float> area,
                 colorf color);

Layout build_text_layout(const Font &font,
                         const String& text,
                         rectf area);
                    
void render_layout(RenderApi &         r,
                   const String& text,
                   const Layout &     layout,
                   colorf             color);
/*
 * measure the width and height requirements to render 
 * the text string on a single line
 *
 * @TODO: generalize this to measure for n lines
 */
vec2f measure(const String& word, const Font &font);
    
// get readable version of FT's error code
const char* ft_error_string(FT_Error err);


} // ftl

#endif //_fontlib_h_
