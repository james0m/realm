#include "uielement.hpp"
#include "renderapi.hpp"

UIElement::UIElement(gfx::Element *root):
    Element(root)
{
}

UIElement::~UIElement() 
{	
}


void UIElement::draw(RenderApi& r)
{
	const auto& elems = elements();
    for(auto i=0u; i<elems.size(); ++i)
    {
        auto* el = elems[i];        
        el->draw(r);
    }
}


void UIElement::align(Alignment alignment)
{
    auto pbounds = parent()->bounds();
    auto b = bounds();

    float x = b.x, y = b.y;

    if (alignment & TOP)
    {
        y = 0;

        if (alignment & CENTER)
        {
            x = pbounds.right() / 2 - b.w / 2;
        }
    }
    else if (alignment & BOTTOM)
    {
        y = pbounds.bottom() - b.h;

        if (alignment & CENTER)
        {
            x = pbounds.right() / 2 - b.w / 2;
        }
    }

    if (alignment & LEFT)
    {
        x = 0;

        if (alignment & CENTER)
        {
            y = pbounds.bottom() / 2 - b.h / 2;
        }
    }

    else if (alignment & RIGHT)
    {
        x = pbounds.right() - b.w;

        if (alignment & CENTER)
        {
            y = pbounds.bottom() / 2 - b.h / 2;
        }
    }

    place(x,y);
}

void UIElement::enter(int x, int y) {
    _hovered = true;
}

void UIElement::exit(int x, int y)
{
    _hovered = false;
}

void UIElement::update_pointer_interaction(int x, int y, int button_state)
{
    if (bounds().collide(x,y))
    {
        if (!_hovered)
        {
            enter(x, y);
        }

        // set clicked state...
        // TODO: this 
        if ((button_state))
            _active = true;
        else
            _active = false;
    }
    else
    {
        exit(x,y);
    }
}
bool UIElement::hovered() const {
    return _hovered;
}

bool UIElement::active() const {
    return _active;
}
// inner recursive call for stab,
// avoids growing cost in calculating bounds() every time we call this
static UIElement* stab_element_inner(Element* elem, rectf elem_bounds, int x, int y)
{
    UIElement* result = nullptr;

    if (! elem_bounds.collide(x, y))
        return result;

    result = (UIElement*)elem;

    if (elem->elements().size())
    {
        for(auto* e : elem->elements())
        {
            auto b = e->bounds_local().translated(elem_bounds.topleft());
            auto stabbed_child = stab_element_inner(e, b, x, y);
            if (stabbed_child)
            {
                // oh no, what have I done!?
                result = stabbed_child;
                break;
            }
        }
    }
    return result;
}

UIElement *stab_element(UIElement* elem, int x, int y)
{
    auto b = elem->bounds();
    auto e = stab_element_inner(elem, b, x, y);
    return e;
}
