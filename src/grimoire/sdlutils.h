#ifndef _sdlutil_h_
#define _sdlutil_h_

#include <string>
#include <stdexcept>
#include <SDL2/SDL.h>

#include "grimoire_util.h"
#include "types.h"
#include "pixel_iterator.hpp"


namespace sdl 
{


struct delete_surface  { 
    void operator()(SDL_Surface *s) {
        SDL_FreeSurface (s); s=NULL;
    }
};

struct delete_texture  { 
    void operator()(SDL_Texture *t) {
        SDL_DestroyTexture (t); t=NULL;
    }
};

struct delete_window   { 
    void operator()(SDL_Window *w)  {
        SDL_DestroyWindow(w); w=NULL;
    }
};

struct delete_renderer { 
    void operator()(SDL_Renderer* r){
            SDL_DestroyRenderer(r); r=NULL;
    }
};



template <class T, typename Del>
using resource_ptr = std::unique_ptr<T, Del>;

using window_ptr   = resource_ptr<SDL_Window, delete_window>;
using renderer_ptr = resource_ptr<SDL_Renderer, delete_renderer>;
using texture_ptr  = resource_ptr<SDL_Texture, delete_texture>;
using surface_ptr  = resource_ptr<SDL_Surface, delete_surface>;


void    init(u32 flags=SDL_INIT_EVERYTHING);
void    quit(); 

String  eventtype_string(const SDL_Event &e);
String  pixelformat_string(Uint32 format);
String  textureaccess_string(int access);

void 	renderer_info(std::ostream &out, SDL_Renderer *ren);
void 	texture_info(std::ostream &out, SDL_Texture *tex);
std::string	surface_info(SDL_Surface *surf);

void 	wait_for_quit(SDL_Keycode hotkey);

void    limit_framerate(int frame_time, int target_fps);
void    limit60hz(int frame_time);

///////////////////////////////////////////////////////////////////////////
// Scoped SDL Resource Allocation
///////////////////////////////////////////////////////////////////////////
sdl::surface_ptr surface_create(int width, int height, 
								   int depth, 
								   u32 rmask=0, 
								   u32 gmask=0, 
								   u32 bmask=0, 
								   u32 amask=0);

sdl::surface_ptr image_load (const String &file);

sdl::surface_ptr image_load_from_memory(const String& data);
sdl::surface_ptr image_load_from_memory(const unsigned char* data, size_t bytes);

sdl::window_ptr	window_create (const char *title, 
					               int width, 
					               int height,
                                   u32 flags = SDL_WINDOW_RESIZABLE);

sdl::window_ptr	window_create (const char *title, 
					               int width, 
					               int height, 
					               int posx, 
					               int posy, 
					               u32 flags);

sdl::renderer_ptr renderer_create (SDL_Window* win, 
								     int driveridx = 0, 
								     u32 flags     = SDL_RENDERER_ACCELERATED);

sdl::texture_ptr texture_create (SDL_Renderer *ren, 
	                                u32 format, 
	                                int access, 
	                                int w, int h);

sdl::texture_ptr texture_from_surface(SDL_Renderer *ren, 
	                                     SDL_Surface *surface);

/////////////////////////////////////////////////////////////////////////////
// SDL_Rect helper functions
/////////////////////////////////////////////////////////////////////////////
void    rect_copy(const SDL_Rect &src, SDL_Rect &tgt);
void    rect_set(SDL_Rect &r, Sint16 x, Sint16 y, Uint16 w, Uint16 h);
void    rect_set_position(SDL_Rect &r, Sint16 x, Sint16 y);
void    rect_set_size(SDL_Rect &r, Uint16 w, Uint16 h);
void    rect_set_null(SDL_Rect &r);
bool    rect_is_null(const SDL_Rect &r);
Sint16  rect_left(const SDL_Rect &r);
Sint16  rect_right(const SDL_Rect &r);
Sint16  rect_top(const SDL_Rect &r);
Sint16  rect_bottom(const SDL_Rect &r);
bool    rect_collide(const SDL_Rect &r, Sint16 x, Sint16 y);
bool    rect_collide(const SDL_Rect &r1, const SDL_Rect &r2);


///////////////////////////////////////////////////////////////////////////
// Generic surface operations
///////////////////////////////////////////////////////////////////////////

template <typename Pixel> inline
pixel_iterator<Pixel> surface_get_iterator(SDL_Surface *surface) {
	return pixel_iterator<Pixel>(surface);
}
template <typename Pixel> inline
pixel_iterator<Pixel> surface_get_end(SDL_Surface *surface) {
	return pixel_iterator<Pixel>(surface) + static_cast<Pixel>(surface->w * surface->h);
}

int get_texture_pitch(SDL_Texture *texture);

////////////////////////////////////////////////////////////////////////////////
// SDL specific exception types
////////////////////////////////////////////////////////////////////////////////

class SDLError : public std::runtime_error
{
public:
	SDLError(const String &msg):
		std::runtime_error(msg)
	{}

	virtual ~SDLError() throw() {}
};

class SDLInitError : public SDLError
{
public:
	SDLInitError(const String &msg):
		SDLError(msg)
	{}
};

class SDLResourceError : public SDLError
{
public:
	SDLResourceError(const String &msg):
		SDLError(msg)
	{}
};

}// sdl wrapper namespace
#endif
