#ifndef _renderer_h_
#define _renderer_h_

#include "rect.h"
#include "vec2.h"
#include "color.h"

struct circle_def {
	vec2f center;
	float radius;
	colorf color;
};

struct line_def {
	vec2f start;
	vec2f end;
	colorf color;
};

struct rect_def {
	rectf  r;
	colorf color;
};

struct texture_def {
	texid        texture;
	rectf        src_region;
	rectf        dst_region;
};

const texid NULL_TEXTURE = -1;

enum AccessMode {
    STATIC,
    STREAM
};

enum PixelFormat {
    PF_NULL,
    // 32 bit - alpha channel
    ARGB,
    RGBA,
    ABGR,
    BGRA,

    // 32 bit - no alpha
    BGRX,
    RGBX,

    // 24 bit no alpha
    RGB,
    BGR
};

struct TextureInfo
{
    texid id = NULL_TEXTURE;
    int w = -1;
    int h = -1;
    int bpp = 0;
    PixelFormat format = PF_NULL;
    AccessMode  access_mode = STATIC;
};

struct SDL_Window;

/////////////////////////////////////////////////////////////////////////////////////////
// a very simple rendering interface for drawing batched primitives and 
// texture sprites
class RenderApi {
public:
    virtual ~RenderApi() {}

	// virtual texid create_textures(texid *ids, int n) = 0;
	// virtual texid bind_texture(texid texture) = 0;

	virtual void   	set_clear_color(const colorf &color);
	colorf       	clear_color() const;
	
	virtual void    set_context(SDL_Window *window);
	
	virtual void  	frame_begin() = 0;
	virtual void  	frame_end() = 0;

    virtual texid 	create_texture(int w, int h, PixelFormat fmt, AccessMode access) = 0;
	virtual void  	update_texture(const void* data, const rectf *region=nullptr, int pitch=0) = 0;
	virtual void  	bind_texture(texid texture) = 0;

    virtual void    get_texture_info(texid tex, TextureInfo& info) = 0;
    virtual void    draw_point(float x, float y, const colorf &color) = 0;
    virtual void    draw_line(vec2f p1, vec2f p2, const colorf& color) = 0;
	virtual void 	draw_rect(const rectf &r, const colorf &color) = 0;
	virtual void 	draw_filled_rect(const rectf &r, const colorf &color) = 0;

	virtual void    draw_circle(const vec2f &c, float radius, const colorf &color) = 0;
	virtual void    draw_filled_circle(const vec2f &c, float radius, const colorf &color) = 0;

    virtual void draw_texture(texid texture,
                              const rectf& src = rectf::null_rect(),
                              const rectf& dst = rectf::null_rect())=0;
    
	virtual void 	batch_rects(const rect_def* rects, int n = 1) = 0;
	virtual void 	batch_lines(const line_def* lines, int n = 1) = 0;
	virtual void 	batch_circles(const circle_def *circles, int n = 1) = 0;
	virtual void 	batch_textures(const texture_def *textures, int n = 1) = 0;

protected:
    RenderApi(SDL_Window *window=nullptr);

	colorf     		 _clear_color;
	SDL_Window 		*_window;
};

#endif
