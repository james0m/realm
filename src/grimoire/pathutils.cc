
#include "pathutils.h"

#include <cassert>

namespace io 
{
namespace path 
{

///////////////////////////////////////////////////////////////////////////////
// get the file name from a path, if path designates a file
String filename(const String &path) {
    assert( path.size() );

    size_t split_idx = 0;

    for(size_t i=path.size() - 1; i!=0; --i) {
        if( path[i] == io::path::sep)
        {
            split_idx = i;
            break;
        }
    }
    return (split_idx > 0) ? path.substr(split_idx + 1, path.size() - split_idx) : path; 
}

///////////////////////////////////////////////////////////////////////////////
// get the file extension as a substring of (path)
String ext(const String &path) {
    size_t dotidx = 0;

    auto chitr = path.begin(), 
         chend = path.end();
    for(; chitr != chend; ++chitr) {
        if( *chitr == '.') {
            dotidx = chitr - path.begin();
            break; 
        }
    }

    String extstr = "";

    if (dotidx > 0) {
        extstr = path.substr(dotidx + 1, path.size() - dotidx);
    }
    return extstr;
}

/////////////////////////////////////////////////////////////////////
//strip a single part of the filename from the path
String strip(const String &path, const String &comp) {
    size_t pos = path.find(comp);
    
    if(pos >= path.size()) {
        return path;
    }

    String lhs = path.substr(0, pos);
    String rhs = path.substr(pos + comp.size());

    return lhs + rhs;
}

/////////////////////////////////////////////////////////////////////
//join the two paths more-or-less intelligently 
String join(const String &lhs, const String &rhs) {
    char lhs_lastchar = lhs[lhs.size() - 1];

    if(lhs_lastchar != io::path::sep && rhs[0] != io::path::sep) {
        return lhs + io::path::sep + rhs;
    }

    if(lhs_lastchar == io::path::sep && rhs[0] == io::path::sep) {
        return lhs + rhs.substr(1);
    }

    return lhs + rhs;
}

String prefix(const String &path)
{
    return strip(path, filename(path));
}

} // path
} // io