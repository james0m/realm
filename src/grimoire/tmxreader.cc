#include <cassert>
#include <cstring>
#include <algorithm>

#include "grimoire/base64.h"
#include "grimoire/filesystem.h"
#include "grimoire/log.h"
#include "grimoire/range.hpp"
#include "grimoire/zlib.h"

#include "tmxreader.h"


using tmx::TiledMap;
using namespace rapidxml;

namespace io {


TmxMapReader::TmxMapReader(const String &file)
{
	if ( path::is_file(file)) {
        _document = util::make_unique<XmlDoc>();
        _document->parse(load_into_string(file));
    }
}


TiledMap::Ref
TmxMapReader::read(const String &data) {

    _map = util::make_unique<TiledMap>();

	if (!data.empty())
        extract_data(data);

    return TiledMap::Ref(_map.release());
}


bool
TmxMapReader::extract_data(const String &data)
{
    _document = io::parse_xml_document(data);
    _node = _document->dom.first_node("map");
    extract_map();

    // post-processing to calculate correct tile resistances
    for(auto i=0; i<_map->tile_type_count(); ++i)
    {
        TileInfo& ti = _map->tile_info(i);
        if (ti.resistance == TR_IMPASSABLE)
            continue;

        ti.resistance = 0;
        for(const u32 j : range(4))
            ti.resistance += _map->terrain(ti.terrain[j]).resistance;

        ti.resistance /= 4;
    }
    return (_map->tileset_count() && _map->layer_count());
}


void
TmxMapReader::enter_node(XmlNode *node)
{
    _scope.push(_node);
    _node = node;
}


void
TmxMapReader::exit_node()
{
    _node = _scope.get();
    _scope.pop();
}


void
TmxMapReader::extract_map()
{
    assert( strcmp(_node->name(), "map") == 0);

    // get all available node attributes
    XmlAttr *attr = _node->first_attribute();

    while ( attr )
    {
        char *attr_name = attr->name();
        if (strcmp(attr_name, "version") == 0)
        {
            //ignore
        }
        else if (strcmp(attr_name, "orientation") == 0)
        {
            //ignore
        }
        else if (strcmp(attr_name, "renderorder") == 0)
        {
            // ignore
        }
        else if (strcmp(attr_name, "width") == 0)
        {
            _map->set_width (atoi (attr->value ()));
        }
        else if (strcmp(attr_name, "height") == 0)
        {
            _map->set_height (atoi (attr->value ()));
        }
        else if (strcmp(attr_name, "tilewidth") == 0)
        {
            _map->set_tilewidth(atoi (attr->value ()));
        }
        else if (strcmp(attr_name, "tileheight") == 0)
        {
            _map->set_tileheight(atoi (attr->value ()));
        }
        else if (strcmp(attr_name, "hexsidelength") == 0)
        {
            // ignore
        }
        else if (strcmp(attr_name, "staggeraxis") == 0)
        {
            //ignore
        }
        else if (strcmp(attr_name, "staggerindex") == 0)
        {
            //ignore
        }
        else if (strcmp(attr_name, "backgroundcolor") == 0)
        {
            //ignore
        }
        else if (strcmp(attr_name, "nextobjectid") == 0)
        {
            //ignore
        }
        attr = attr->next_attribute();
    }

    // descend into child nodes
    XmlNode *child = _node->first_node();

    while (child)
    {
        enter_node (child);

        char *node_name = _node->name();

        if (strcmp(node_name, "tileset") == 0)
        {
            extract_tileset();
        }
        else if (strcmp(node_name, "properties") == 0)
        {
            extract_properties();
        }
        else if (strcmp(node_name, "layer") == 0)
        {
            extract_layer();
        }
        else if (strcmp(node_name, "objectgroup") == 0)
        {
            extract_objectgroup();
        }
        else if (strcmp(node_name, "imagelayer") == 0)
        {
            extract_imagelayer();
        }
        else
        {
            // this is an error
            throw TmxReadError("Invalid tmx data was found!");
        }

        exit_node ();
        child = child->next_sibling();
    }


}


void
TmxMapReader::extract_external_tileset(u32 tsid, const String &file)
{
    io::FileHandle mapfile = io::open(file);
    assert( mapfile->exists());

    auto tilesetdoc = io::load_xml_document(file);

    XmlNode *tsroot = tilesetdoc->dom.first_node("tileset");
    enter_node(tsroot);
    extract_tileset_inner(tsid);
    exit_node();
}


void
TmxMapReader::extract_tileset_image(u32 tsid)
{
    assert(strcmp(_node->name(), "image") == 0);

    _map->tileset(tsid).src = _node->first_attribute("source")->value();
}


void
TmxMapReader::extract_tileset_terraintypes(u32 tsid)
{
    assert( strcmp (_node->name(), "terraintypes") == 0);

    for (auto* child = _node->first_node(); child != nullptr; child = child->next_sibling()) {
        enter_node(child);
        extract_tileset_terrain(tsid);
        exit_node();
    }
}


void
TmxMapReader::extract_tileset_terrain(u32 tsid)
{
    assert(strcmp(_node->name(), "terrain") == 0);

    Terrain terr;

    {
        // check for and assign name, error if no name given
        auto *attr_name = _node->first_attribute("name");
        assert(attr_name &&
               "extract_tileset_terrain: no name given for terrain type");

        terr.name = attr_name->value();
    }
    {
        // check for and assign tile id, error if none given
        auto *attr_tile_id = _node->first_attribute("tile");
        assert (attr_tile_id &&
                "extract_tileset_terrain: no name given for terrain type");
        terr.tile_id = std::atoi(attr_tile_id->value());
    }
    {
        // check for properties, and extract them
        auto *props = _node->first_node("properties");
        if (props)
        {
            enter_node(props);
            const auto attr_map = extract_properties();
            for( auto &itr : attr_map)
            {
                if (itr.first == "resistance") {
                    terr.resistance = boost::any_cast<int>(itr.second);
                }
            }
            exit_node();
        }
    }
    _map->add_terrain(terr);
}


void
TmxMapReader::extract_tileset_tile(u32 tsid)
{
    assert( strcmp( _node->name(), "tile") == 0);

    int id = atoi( _node->first_attribute("id")->value());
    if (id > _map->tileset(tsid).lastgid)
        _map->tileset(tsid).lastgid = id;

    auto attr_terrain = _node->first_attribute("terrain");
    assert(attr_terrain &&
           "extract_tileset_tile: no terrain string given for tile");

    auto tile_terrains = std::string(attr_terrain->value());
    auto strings = sl::split(tile_terrains, ',');

    u8 resistance = 0;
    u8 values[4];
    values[0] = std::atoi(strings[0].c_str());
    values[1] = std::atoi(strings[1].c_str());
    values[2] = std::atoi(strings[2].c_str());
    values[3] = std::atoi(strings[3].c_str());

    if (_node->first_node("properties") )
    {
        enter_node(_node->first_node("properties"));
        std::map<String, Any> tile_props = extract_properties();
        for( auto itr : tile_props) {
            if (itr.first == "resistance") {
                resistance = static_cast<u8>(Config::cast<int>(itr.second));
            }
        }
        exit_node();
    }
    _map->add_tile_info(id, values, resistance);
}


void
TmxMapReader::extract_tileset_inner(u32 tsid)
{
    assert(strcmp(_node->name(), "tileset") == 0);

     // get all attributes
    XmlAttr *attr = _node->first_attribute();
    while (attr)
    {
        char *attrname = attr->name();
        if (strcmp(attrname, "source") == 0)
        {
            throw TmxReadError("TmxMapReader::extract_tileset_inner() recieved an external tileset reference");
        }
        else if (strcmp(attrname, "firstgid") == 0) {
            // pass thru, we already have grabbed this in the top level extract_ method
        }
        else if (strcmp(attrname, "name") == 0)
        {
            _map->tileset(tsid).name = String(attr->value());
        }
        else if (strcmp(attrname, "tilewidth") == 0)
        {
            _map->tileset(tsid).tilewidth = atoi(attr->value());
        }
        else if (strcmp(attrname, "tileheight") == 0)
        {
            _map->tileset(tsid).tileheight = atoi(attr->value());
        }
        else if (strcmp(attrname, "spacing") == 0)
        {
            _map->tileset(tsid).spacing = atoi(attr->value());
        }
        else if (strcmp(attrname, "margin") == 0)
        {
            _map->tileset(tsid).margin = atoi(attr->value());
        }
        else if (strcmp(attrname, "tilecount") == 0)
        {
           /* grimoire::Tileset &ts = _map->tileset(tsid);
            ts.lastgid = ts.firstgid + atoi(attr->value()) - 1;*/
        }
        else if (strcmp(attrname, "columns") == 0)
        {
            //
        }
        else
        {
            throw TmxReadError("Tileset has bad data");
        }

        attr = attr->next_attribute();
    }

    XmlNode *child = _node->first_node();
    enter_node(child);

    while (child)
    {
        char *node_name = child->name();
        if (strcmp(node_name, "tileoffset") == 0)
        {
            _map->tileset(tsid).offset = {
                atoi( child->first_attribute("x")->value()),
                atoi( child->first_attribute("y")->value())
            };
        }
        else if (strcmp(node_name, "image") == 0)
        {
            extract_tileset_image (tsid);
        }
        else if (strcmp(node_name, "properties") == 0)
        {
            //extract_properties ();
        }
        else if (strcmp(node_name, "terraintypes") == 0)
        {
            extract_tileset_terraintypes (tsid);
        }
        else if (strcmp(node_name, "tile") == 0)
        {
            extract_tileset_tile (tsid);
        }
        else
        {
            printf("TmxMapReader::extract_tileset_inner() - Tileset xml had erroneus element type: %s\n", node_name);
        }

        exit_node();
        child = child->next_sibling();
        enter_node(child);
    }

}


void
TmxMapReader::extract_tileset()
{
    assert( strcmp(_node->name(), "tileset") == 0);

    // create a new tileset
    u32 tsid = _map->add_tileset(tmx::Tileset());

    _map->tileset(tsid).firstgid = atoi(_node->first_attribute("firstgid")->value());

    if( _node->first_attribute("source") != nullptr )
        extract_external_tileset(tsid, _node->first_attribute("source")->value());
    else
        extract_tileset_inner(tsid);

}


AnyMap
TmxMapReader::extract_properties()
{
    assert( strcmp(_node->name(), "properties") == 0);

    XmlNode *owner    = _scope.get();
    XmlNode *property = _node->first_node();

    auto conf = std::map<String, Any>{};

    while (property)
    {
        enter_node(property);
        extract_property(owner, conf);
        exit_node();
        property = property->next_sibling();
    }
    return conf;
}

void
TmxMapReader::extract_property(XmlNode *owner, AnyMap &conf)
{
    assert (owner != 0 && strcmp (_node->name(), "property") == 0);

    Any obj;

    auto name = _node->first_attribute("name");
    if (!name) {
        Log::e("TmxMapReader", "extract_property(): property with unspecified name");
        return;
    }

    auto value = _node->first_attribute("value");
    if (!value) {
        Log::e("TmxMapReader","extract_property(): property with unspecified value");
        return;
    }

    auto type = _node->first_attribute("type");
    if(!type) {
        Log::e("TmxMapReader", "extract_property(): property with unspecified type");
        return;
    }

    auto type_str = type->value();
    if (strcmp(type_str, "string") == 0 || strcmp(type_str, "file") == 0 )
        obj = value->value();

    else if (strcmp(type_str, "int") == 0)
        obj = std::atoi(value->value());

    else if (strcmp(type_str, "float") == 0)
        obj = std::atof(value->value());

    else if (strcmp(type_str, "bool") == 0)
        obj = strcmp(value->value(), "true") == 0 ? true : false;

    else if (strcmp(type_str, "color") == 0) {
        Log::i("TmxMapReader",
               "extract_property: \"color\" property type not supported" );
    }

    assert( ! obj.empty() &&
            "extract_property: could not capture any value for property");

    conf.emplace(name->value(), obj);
}

//////////////////////////////////////////////////////////////////////////////////////
// check for compression type and inflate the data string in place
void
check_and_inflate(const String &compression, String &raw)
{
    if (compression == "zlib") {

        sl::transform_string(raw, util::zlib_deflate);
    }
    else if (compression == "bzip") {
        throw TmxReadError("Cannot inflate bzip compressed data!");
    }
    else {
        // unknown compression
        throw TmxReadError("Unknown Compression type was found");
    }
}


// extracts the tiles in sequence from tilenode and each sibling
void
extract_tiles(tmx::MapLayer &layer, XmlNode *tilenode)
{
    unsigned idx = 0;
    while (tilenode)
    {
        layer.data[idx++] = atoi(tilenode->first_attribute("gid")->value()) - 1;
        tilenode = tilenode->next_sibling();
    }
}


void
TmxMapReader::extract_layer()
{
    assert( strcmp(_node->name(), "layer") == 0);

    String layername = _node->first_attribute("name")->value();
    XmlNode *child = _node->first_node();

    unsigned int layerid = _map->add_layer(layername);
    _map->layer_at(layerid).data.set_dimensions(_map->width(), _map->height() );

    while (child)
    {
        enter_node(child);

        if (strcmp(child->name(), "properties") == 0)
        {
            extract_properties();
        }
        else if (strcmp(child->name(), "data") == 0)
        {
            // check for encoding and encryption
            XmlAttr *attr_encoding    = child->first_attribute("encoding"),
                    *attr_compression = child->first_attribute("compression");

            String raw = child->value();

            bool base64_encoded = attr_encoding != nullptr && strcmp(attr_encoding->value(), "base64") == 0;
            bool csv_encoded    = attr_encoding != nullptr && strcmp(attr_encoding->value(), "csv") == 0;

            if (base64_encoded)
            {
                // base64 encoded xml, almost as easy
                sl::transform_string(raw, util::base64_decode);

                if (attr_compression) {
                    String comp(attr_compression->value());
                    check_and_inflate(comp, raw);
                }
                // TODO: ok, we can AT LEAST encode/decode base64 tile data
                throw TmxReadError("TmxMapReader::extract_layer() - Cannot decode base64... so sorry");

                // once here, we have a string of <tile> elements to parse
                char *buf = strdup(raw.data());
                XmlDocument tiledoc;
                tiledoc.parse<0>(buf);

                XmlNode *tilenode = tiledoc.first_node();
                extract_tiles(_map->layer_at(layerid), tilenode);
                free (buf);
            }
            else if (csv_encoded)
            {
                // csv encoding, super easy.
                if( attr_compression) {
                    String comp(attr_compression->value());
                    check_and_inflate(comp, raw);
                }

                // explode the comma-separated-string into a list
                auto gids = sl::split(raw, ',');

                for (unsigned i = 0; i < gids.size(); ++i) {
                    _map->layer_at (layerid).data[i] = atoi ( gids[i].c_str() );
                }
            }
            else {
                // if here, unencoded, already-parsed xml tiles
                extract_tiles(_map->layer_at(layerid), child->first_node());
            }

        }

        exit_node();
        child = child->next_sibling();
    }
}


void
TmxMapReader::extract_objectgroup() {
    assert( strcmp(_node->name(), "objectgroup") == 0);
}


void
TmxMapReader::extract_imagelayer() {
    assert( strcmp(_node->name(), "imagelayer") == 0);
}


Unique<TiledMap>
load_tmx_map(const String &file) {
    assert( io::exists(file) );

	// @CLEANUP: not sure what is the problem, but I have to use strdup, and
	//          an explicit free() here to get the parsing done.
	//          tried using a unique_ptr to char[], just passing the string directly,
	//          and each time, got errors and junk data. Somewhere in the call stack,
	//          the resource is going out of scope.
    auto str = io::open(file)->as_string();
    char* buf = strdup(str.c_str());

    TmxMapReader reader;
    auto map = reader.read(buf);
	free(buf);
	return map;
}


}; // namespace io
