
#include "fontlib.h"
#include "grimoire/grimoire_util.h"
#include "grimoire/filesystem.h"
#include "grimoire/log.h"
#include "grimoire/stringlib.h"
#include "grimoire/sdlutils.h"
#include "grimoire/vec2.h"
//#include "grimoire/renderapi.hpp"

using namespace std;


constexpr const char* const TAG = "fontlib";
static FT_Library ftlib;

namespace txt {

const Glyph Glyph::NULL_GLYPH = Glyph();

void init() {
    int err = FT_Init_FreeType(&ftlib);

    if( err ) {
        Log::e(TAG, sl::format("freetype failed to initialize. error: %d", err));
    }
}

FT_Library lib(){
    return ftlib;
}

void cleanup()
{
    if (lib())
    {
        FT_Done_FreeType(lib());
    }
}

void blit_glyph_to_rgba(FT_GlyphSlot glyph, void* dst, int x, int y, int pitch)
{
    assert (dst);    
    assert (pitch > 0);
        
    auto bitmap = glyph->bitmap;
    int  i, j, p, q;
    
    x *= 4; // expand x value to accomodate 4 bytes per pixel
    
    int  x_max = x + bitmap.width * 4;
    int  y_max = y + bitmap.rows;

    u8* img = (u8*) dst;
    
    for ( j = y, q = 0; j < y_max; j++, q++ )
    {
        for ( i = x, p = 0; i < x_max; i += 4, p++ )
        {
            auto outidx   = i + j * pitch;
            auto inidx    = p + q * bitmap.width;
            u8 font_pixel = bitmap.buffer[inidx];
            
            img[outidx]   = font_pixel;
            img[outidx+1] = font_pixel;
            img[outidx+2] = font_pixel;
            img[outidx+3] = font_pixel;
        }
    }
}

GlyphAtlas generate_atlas(RenderApi &renderer, Font &font, const std::string& chars) {

    constexpr const auto TEXTURE_WIDTH = 512;
    
    auto glyphs = Array< Glyph >(chars.size());
    
    ///////////////////////////////////////////////////////////////
    // 1. first we look at each glyph in our set of characters,
    // and determine how big our texture needs to be. also,
    // generate src and destination rects for blitting in #2    
    ////////////////////////////////////////////////////////////////
    
    int x = 0,
        y = 0,
        image_height = 0,
        next_line_y = 0;
    
    for(u32 i=0; i<chars.size(); ++i) {

        char ch = chars[i];
        auto ch_index = FT_Get_Char_Index(font.face, static_cast<FT_ULong>(ch));

        FT_Load_Glyph(font.face, ch_index, FT_LOAD_RENDER);

        auto glyph = font.face->glyph;
        
        int glyph_h = glyph->bitmap.rows;
        int glyph_w = glyph->bitmap.width;

        next_line_y = std::max(next_line_y, y + glyph_h);
        
        if (x + glyph_w > TEXTURE_WIDTH) {
            x = 0;
            y = next_line_y;
    
        }              

        glyphs[i].id = ch;
        glyphs[i].index = ch_index;
        glyphs[i].texture = NULL_TEXTURE; // to be updated in the second pass
        glyphs[i].region.set(x, y, glyph_w, glyph_h);
        glyphs[i].advance.set(glyph->advance.x >> 6, glyph->advance.y >> 6);
        glyphs[i].bearing.set(
            glyph->metrics.horiBearingX >> 6, glyph->metrics.horiBearingY >> 6);
    
        x += glyph_w;
    }
    image_height = next_line_y;
    
    ////////////////////////////////////////////////////////////////
    // now create a temporary chunk of memory to copy into using
    // the calculated dimensions. then blit all glyphs into it
    auto buffer = Array<u8>(TEXTURE_WIDTH * image_height * 4, 0);


    ////////////////////////////////////////////////////////
    // create the texture now so we can use the id to write
    // our glyph metadata to the atlas
    auto tex_id = renderer.create_texture(TEXTURE_WIDTH,
                                          image_height,
                                          RGBA,
                                          STATIC);

    if (tex_id == NULL_TEXTURE) {
        Log::e(TAG, sl::format("failure creating font atlas texture: %s\n",
                                 SDL_GetError()));
        assert (false);
    }
    
    //auto ga     = GlyphAtlas(tex_id, TEXTURE_WIDTH, image_height);
    auto ga = GlyphAtlas();

    ////////////////////////////////////////////////////////////////////////////
    // step 2 render the glyphs onto a temporary surface, expanding to
    // 32bit rgba
    ////////////////////////////////////////////////////////////////////////////

    for(u32 i = 0; i < chars.size(); ++i) {
        auto ch = chars[i];       
        glyphs[i].texture = tex_id;
        
        FT_Load_Glyph(font.face,
                      FT_Get_Char_Index(font.face, static_cast<FT_ULong>(ch)),
                      FT_LOAD_RENDER);       

        int error = FT_Render_Glyph(font.face->glyph, FT_RENDER_MODE_NORMAL);
        
        if (error) {
            Log::e(TAG, sl::format("error rendering glyph: %s", ft_error_string(error)));
            assert(false && "error during glyph rendering");
        }

        // create an 8bit surface
        auto glyph = font.face->glyph;

        // blit to the surface
        blit_glyph_to_rgba(glyph,
                           &buffer.front(),
                           glyphs[i].region.x,
                           glyphs[i].region.y,
                           4 * TEXTURE_WIDTH);

        ga[ch] = glyphs[i]; // store the glyph data in the atlas
    }
////////////////////////////////////////////////////////////////
// 3. upload the texture data
////////////////////////////////////////////////////////////////    

    renderer.bind_texture(tex_id);
    renderer.update_texture(&buffer.front());
    
    return ga;
}
    
void render_text(RenderApi &r, const Font &font, const string &text, int x, int y, colorf color)
{
    assert (font.has_glyph_atlas());

    auto pen_position = vec2f{ float(x), float(y) };
    auto cmds         = Array<texture_def>(text.size());

    const Glyph* prev_glyph = nullptr;
        
    for(auto i=0u; i<text.size(); ++i) {
        // generate the commands from the fonts glyph info

        auto &glyph = font.glyph(text[i]);

        if (FT_IS_FIXED_WIDTH(font.face) && prev_glyph) {

            FT_Vector kern_delta;
            FT_Get_Kerning(font.face,
                           prev_glyph->index, glyph.index,
                           FT_KERNING_DEFAULT,
                           &kern_delta);
            
            pen_position.x += kern_delta.x >> 6;
        }

        pen_position.x += glyph.bearing.x;

        // fill the render command fields 
        cmds[i].texture = glyph.texture;
        cmds[i].src_region = glyph.region;

        cmds[i].dst_region.x = pen_position.x;
        cmds[i].dst_region.y = pen_position.y - glyph.bearing.y;
        cmds[i].dst_region.w = glyph.region.w;
        cmds[i].dst_region.h = glyph.region.h;

        // @TODO: this doesn't account for available line length, RTL, or vertical
        // fonts
        pen_position += glyph.advance;
        prev_glyph = &glyph;
    }

    r.batch_textures(&cmds[0], text.size());
}

vec2f measure(const string& word, const Font &font) {
    auto result = vec2f();
    for(auto ch : word) {
        result.x += font.glyph(ch).advance.x;
        result.y = max(font.glyph(ch).region.h, result.y);
    }
    return result;
}

Layout build_text_layout(const Font& font,
                         const string& text,
                         rectf area )
{
    Layout result;
    result.font = &font;
    
    float min_x = area.x,
          min_y = area.y,
          max_x = area.right();

    auto pen_position = vec2f{ min_x, min_y };
    auto words = sl::split(text, ' ');
    
    result.lines = 1;
    for(auto word : words)
    {
        auto delta = measure(word, font);

        if (pen_position.x + delta.x > max_x) {
            // to the next line
            pen_position.x = min_x;
            pen_position.y += font.line_height;
            ++result.lines;
        }

        const Glyph* prev_glyph = nullptr;
        
        // now render the glyphs in the word
        for(auto i = 0u; i < word.size(); ++i) {

            const auto &glyph = font.glyph(word[i]);
            
            if (prev_glyph && !FT_IS_FIXED_WIDTH(font.face))
            {
                FT_Vector kern_delta;
                FT_Get_Kerning(font.face,
                               prev_glyph->index, glyph.index,
                               FT_KERNING_DEFAULT,
                               &kern_delta);
            
                pen_position.x += kern_delta.x >> 6;
            }


            // fill the render command fields
            GlyphPosition cmd;
            cmd.glyph = word[i];            
            cmd.extent.x = pen_position.x;
            cmd.extent.y = pen_position.y - glyph.bearing.y;
            cmd.extent.w = glyph.region.w;
            cmd.extent.h = glyph.region.h;

            result.slots.push_back(cmd);

            result.extent = union_rect(result.extent, cmd.extent);

            pen_position.x += glyph.advance.x - glyph.bearing.x;
            prev_glyph = &glyph;
        }
        // add a space after each word
        auto glyph = font.glyph(' ');

        pen_position.x += glyph.advance.x;
        
        GlyphPosition ws_cmd;
        ws_cmd.glyph = ' ';        
        ws_cmd.extent = rectf{
                pen_position.x,
                pen_position.y,
                glyph.region.w,
                glyph.region.y
        };
        
        result.slots.push_back(ws_cmd);
        result.extent = union_rect(result.extent, ws_cmd.extent);
        if (result.extent.x < min_x) result.extent.x = min_x;
        if (result.extent.y < min_y) result.extent.y = min_y;
        prev_glyph = nullptr;        
    }

    return result;

}
void render_text(RenderApi &r,
                 const Font &font,
                 const string &text,
                 rect<float> area,
                 colorf color)
{
    auto layout = build_text_layout(font, text, area);
    render_layout(r, text, layout, color);
}

void render_layout(RenderApi&          r,
                   const std::string& text,
                   const Layout &     layout,
                   colorf             color)
{
    const auto N = layout.slots.size();
    static auto commands = Array<texture_def>(N);

    for(auto i=0u; i<N; ++i)
    {
        auto &slot = layout.slots[i];
        auto &glyph = layout.font->glyph(slot.glyph);

        commands[i].texture    = glyph.texture;
        commands[i].src_region = glyph.region;
        commands[i].dst_region = slot.extent;
    }
    r.batch_textures(&commands.front(), N);    
};


const char* ft_error_string(FT_Error err)
{
    #undef __FTERRORS_H__
    #define FT_ERRORDEF( e, v, s )  case e: return s;
    #define FT_ERROR_START_LIST     switch (err) {
    #define FT_ERROR_END_LIST       }
    #include FT_ERRORS_H
    return "(Unknown error)";
}

Glyph &GlyphAtlas::operator[](char ch) {
    return (ch != '\n') ? _glyphs[ch] : *const_cast<Glyph*>(&Glyph::NULL_GLYPH);
}

const Glyph &GlyphAtlas::glyph(char ch) const
{
    try {
        return (ch != '\n') ? _glyphs.at(ch) : *(&Glyph::NULL_GLYPH);
    }
    catch(std::out_of_range&) {
        return _glyphs.at(' ');
    }
}

size_t GlyphAtlas::size() const {
    return _glyphs.size();
}

GlyphAtlas::iterator GlyphAtlas::begin() {
    return _glyphs.begin();
}

GlyphAtlas::iterator GlyphAtlas::end() {
    return _glyphs.end();
}


};
