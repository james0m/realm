#ifndef _texture_atlas_h_
#define _texture_atlas_h_

#include "grimoire_util.h"

#include "SDL2/SDL.h"

#include "rect.h"
#include "renderapi.hpp"


// texture atlas definition file
// should sit right next to the image file in the same dir

// so if the file is "demon.png",
//  the atlas file is "demon.atl"
/*
{
    "image" : "nameoffile.png",
    "animations" : {

        "idle" : [ 
            {0,0, 32,32}, 
            {32,0,32,32},
            {64,0,32,32},
            {96,0,32,32} 
        ],

        "walk" : [

        ],

        "run" : [
        ],

        "jump" : [],
        "hit" : [],
        "attack-melee": [],
        "attack-sword": [],
        "attack-beam": []
    }
}
*/
class TextureAtlas {
public:
    TextureAtlas(texid texture=NULL_TEXTURE);
    TextureAtlas(const TextureAtlas &o);

    virtual ~TextureAtlas();

    TextureAtlas& operator= ( const TextureAtlas& o);

    ///////////////////////////////////////////////////////////////////////////////
    // load the image using load_image() and then parse its contents 
    // for denoted regions defined by control pixels. See image.h for 
    // more info.
    // FIXME: this should not be necessary
    int                       load_marked_image(const String &file);

    ///////////////////////////////////////////////////////////////////////////////
    // ensure that this object is usable in it's current state.
    bool                      valid() const;

    ///////////////////////////////////////////////////////////////////////////////
    // The total bounding rect of the texture; mandatory inherit from renderable
    virtual void              get_rect(rectf &out) const;

    ///////////////////////////////////////////////////////////////////////////////
    // Get the number of regions defined in this atlas
    size_t                    region_count() const;

    ///////////////////////////////////////////////////////////////////////////////
    // Assigns to this object via a deep-copy of the other
    void                      copy(const TextureAtlas &o);

    ///////////////////////////////////////////////////////////////////////////////
    // Define a single sub-region 
    void                      add_region(const rectf &region);

    ///////////////////////////////////////////////////////////////////////////////
    // Fill an input rect reference with a copy of the region at idx 
    void                      region_lookup(unsigned int idx, rectf *region) const;
    rectf                     region(unsigned int idx) const;

    ///////////////////////////////////////////////////////////////////////////////
    // Return a pointer to this atlas' SDL_Texture.
    virtual texid             get_texture() const;
    void                      set_texture(texid texture) {m_texture = texture;}
    ///////////////////////////////////////////////////////////////////////////////
    // Assign the region that will be used to draw this texture 
    // when passed into video_system::render()
    size_t                    set_active_region(size_t idx);
    size_t                    active_region() const { return m_active_region; }
    ///////////////////////////////////////////////////////////////////////////////
    // Define multiple regions by array of T. 
    //
    // T is assumed to be a rect-like object, possessing attributes for
    // x, y, w, and h; 
    template <typename T> int add_regions(const T *regions, int n);

private:
    ///////////////////////////////////////////////////////////////////////////////
    // Free texture and surface memory, nullify pointers, and clear() all regions.
    void                      _reset();

    String                    m_file;
    texid                     m_texture;
    size_t                    m_active_region;
    Array<rectf>              m_regions;
};

template <typename T>
int TextureAtlas::add_regions(const T *regions, int n) {
    int added=0;
    rectf tmp = {0,0,0,0};
    for(auto i=0; i<n; ++i) {

        tmp.x = regions[i].x;
        tmp.y = regions[i].y;
        tmp.w = regions[i].w;
        tmp.h = regions[i].h;
        
        m_regions.push_back(tmp);
        ++added;
    }
    return added;
}

#endif
