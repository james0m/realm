#include "filesystem.h"
#include "ioutils.h"

namespace io {

void
XmlDoc::parse(String s)  {
	dom.clear();
	src = std::move(s);
	dom.parse<0>(pbegin(src));
}


Unique<XmlDoc>
load_xml_document(const String &file)
{
    auto fhandle = io::open(file);
    auto document = parse_xml_document(fhandle->as_string());
    return document;
}


Unique<XmlDoc>
parse_xml_document(const String &data)
{
    auto doc = util::make_unique<XmlDoc>();
    doc->parse(data);
    return doc;
}

}
