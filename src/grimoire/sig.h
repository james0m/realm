#ifndef _signal_h_
#define _signal_h_

/*
* maps a list of callback functions to an event id.

TODO: template specialization on void datatype

example:
    this will create a signal expecting an <int> data
    argument to trigger() and all callbacks.

signal<int> sig1;

    this will connect a static callback function to
    be executed on trigger(), and then calls trigger()
    with an event

sig1.connect(&some_function_from_elsewhere);
int event = SOME_EVENT_CONST
sig1.trigger(event)


*/
#include "grimoire_util.h"
#include "fastdelegates/FastDelegate.h"

using fastdelegate::FastDelegate1;
using fastdelegate::FastDelegate0;

template <typename E>
struct signal_traits {
    typedef const E&                                data_type;
    typedef FastDelegate1<data_type>                callback_type;
    typedef Array<callback_type>                    callback_list;
    typedef typename callback_list::iterator        cb_iterator;
    typedef typename callback_list::const_iterator  const_cb_iterator;
};

template <>
struct signal_traits<void> {
    typedef void                                    data_type;
    typedef FastDelegate0<void>                     callback_type;
    typedef Array<callback_type>                    callback_list;
    typedef typename callback_list::iterator        cb_iterator;
    typedef typename callback_list::const_iterator  const_cb_iterator;
};

template <typename E>
class signal_base {
public:
    using data_type         = typename signal_traits<E>::data_type;
    using callback_type     = typename signal_traits<E>::callback_type;
    using callback_list     = typename signal_traits<E>::callback_list;
    using cb_iterator       = typename signal_traits<E>::cb_iterator;
    using const_cb_iterator = typename signal_traits<E>::const_cb_iterator;

    signal_base():
        m_callbacks()
    {}

    void connect(const callback_type &cb) {
        auto itr = std::find(m_callbacks.begin(), m_callbacks.end(), cb);
        if (itr == m_callbacks.end())
            m_callbacks.push_back(cb);
    }

    template <typename Class, typename F>
    void connect(Class* instance, F f) {
        connect(fastdelegate::MakeDelegate(instance, f));
    }

    void disconnect(const callback_type &cb) {
        cb_iterator itr = _find_callback(cb);
        if(itr != end(m_callbacks)) {
            m_callbacks.erase(itr);
        }
    }
    size_t size() const {
        return m_callbacks.size();
    }

    bool has_callback(const callback_type &cb) const {
        return (_find_callback(cb) != end(m_callbacks) ? true : false );
    }
protected:

    cb_iterator _find_callback(const callback_type &cb)  {
        return std::find(begin(m_callbacks), end(m_callbacks), cb);
    }

    callback_list m_callbacks;
};


/*
*   Signal front-end - trigger() takes a data_type parameter
*/
template <typename E=void>
class signal : public signal_base<E> {
public:
    using data_type         =  typename signal_traits<E>::data_type;
    using callback          =  typename signal_traits<E>::callback_type;
    using callback_list     =  typename signal_traits<E>::callback_list;
    using cb_iterator       =  typename signal_traits<E>::cb_iterator;
    using const_cb_iterator =  typename signal_traits<E>::const_cb_iterator;

    void trigger( const data_type &data) {
        for (auto &f : this->m_callbacks) {
            f(data);
        }
    }

    template <typename ...Args>
    void operator()(Args && ...args) {
        trigger(std::forward<Args>(args)...);
    }

    template <typename ...Args>
    void trigger(Args && ...args) {
        auto e = E( std::forward<Args>(args) ...);
        for (auto f : this->m_callbacks) {
            f(e);
        }
    }
};

/*
*   Signal front-end - void data_type specialization
*/
template <>
struct signal<void> : public signal_base<void> {
    using callback = typename signal_traits<void>::callback_type;
    void operator()() {
        trigger();
    }
    void trigger() {
        for (auto &f : this->m_callbacks) {
            f();
        }
    }
};

#endif
