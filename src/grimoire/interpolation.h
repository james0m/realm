#ifndef _interpolation_h_
#define _interpolation_h_

#include <map>
#include "types.h"
#include "vec2.h"
typedef float (*LerpFunc)(float);

struct lerp_state
{
    vec2f    p1;
    vec2f    p2;
    LerpFunc func;
    float    t;
};

using LerpTable = std::map<idtype, lerp_state>;

float interpolate(LerpFunc lerp, float start, float end, float time);

float linear (float time);

float quad_in(float time);
float quad_out(float time);
float quad_inout(float time);

float sine_in(float time);
float sine_out(float time);

float smoothstep(float time);

using Interpolator = float (*)(float);

#endif
