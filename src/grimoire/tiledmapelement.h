#ifndef _tiledmapelement_h_
#define _tiledmapelement_h_

#include "grimoire/grimoire_util.h"
#include "grimoire/tiledmap.h"
#include "grimoire/renderapi.hpp"

#include "element.h"
#include "camera.h"

namespace gfx {


    
class TiledMapElement : public Element {
public:
    TiledMapElement(Element *root, tmx::TiledMap* map=nullptr);
    virtual ~TiledMapElement();
        
    void draw(RenderApi &r);
 
    tmx::TiledMap *map() {
        return _map;
    }

    const tmx::TiledMap* map() const {
        return _map;
    }

    void toggle_terrain() { _draw_terrain = !_draw_terrain; }
    void toggle_map_grid() { _draw_grid = !_draw_grid; }
    void enable_map_grid()  { _draw_grid = true; }
    void disable_map_grid() { _draw_grid = false; }
private:

    bool _draw_grid = false;
    bool _draw_terrain= false;
    Array<texture_def> _cmd_buffer;
    tmx::TiledMap* _map;

};


} //gfx
#endif //_tiledmapelement_h_
