#ifndef _typeid_h_
#define _typeid_h_

#include "grimoire/types.h"

namespace tl {

extern idtype type_id_counter;


template <class A>
idtype type_id()
{
    static const idtype aspect_type_id = tl::type_id_counter++;
    return aspect_type_id;
}

template <class A>
u32 type_unmask()
{
    return ~(1 << tl::type_id<A>());
}

namespace detail {
 
template <class ...Args>
struct MaskImpl; // needed for compiler bug


template <class T, class ...Args>
struct MaskImpl<T, Args...> // recursive case, partial specialization
{
    static u32 val()
    {
        return (1 << type_id<T>()) | MaskImpl<Args...>::val();
    }
};

template <>
struct MaskImpl<> // base case (no more Args) specialization
{
    static u32 val() { return 0; }
};

}// detail

template <class ...Args>
u32 type_mask() // TODO: this could be constexpr
{
    return detail::MaskImpl<Args...>::val();
}

} // tl namespace
#endif //_typeid_h_
