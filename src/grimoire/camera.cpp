#include "camera.h"

namespace gfx {

Unique<Camera> Camera::create(int w, int h, Element *root) {
    return Unique<Camera> { new Camera(root, w, h) };
}
    
Camera::Camera(Element *root, int width, int height):
    Element(root)
{
    set_bounds( { 0,0,
                static_cast<float>(width),
                static_cast<float>(height)
                });
    set_viewport(0,0,width, height);
}
Camera::~Camera() {}

void Camera::set_viewport(int x, int y, int w, int h) {
    _viewport.set(x,y,
                  w > 0 ? w : _viewport.w,
                  h > 0 ? h : _viewport.h);   
}

void Camera::set_viewport( rect<int> region) {
    _viewport = region;
}

void Camera::lock_to_parent_bounds(bool do_lock) {
    _lock_to_parent = do_lock;
}

vec2f Camera::transform() const
{
    return bounds().topleft();
}

rect<int> Camera::viewport() const {
    return _viewport;
}

rectf Camera::bounds() const {
    return _bounds;
}

void Camera::draw(RenderApi& r) {}
} // gfx
