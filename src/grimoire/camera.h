#ifndef _camera_h_
#define _camera_h_


#include "element.h"

namespace gfx {
/**
 * Camera describes a viewing area for the scene it is attached to.
 * 
 * The Element::bounds() method returns a rect that describes the 
 * area of the scene that is made visible by this camera.  
 *
 * The Camera::viewport() method returns a rect describing where
 * on the screen to render its view to.
 */
class Camera : public Element {
public:
    Camera(Element *root, int width, int height);
    
    virtual ~Camera();

    static Unique<Camera> create(int w, int h, Element *root=nullptr);
    
    /**
     * setting this property to true will force the 
     * bounds() rect to always be contained by its parent
     * element rect. 

     * this means that translate() place() and so on will 
     * all be checked against this constraint
     */
    void lock_to_parent_bounds(bool do_lock=true);

    
    /** set the viewport area */
    void set_viewport(int x, int y, int w=0, int h=0);
    void set_viewport( rect<int> region);
    
    vec2f transform() const;       
    rect<int> viewport() const;
    rectf bounds() const;
    void draw(RenderApi &r);
private:    
    
    bool _lock_to_parent = false;
    rect<int> _viewport = {0,0,0,0}; // where to render on screen
};

} // gfx
#endif //_camera_h_

