#ifndef _path_utils_h_
#define _path_utils_h_

#include "types.h"

#include "grimoire_util.h"

namespace io 
{
namespace path 
{
// the os-dependent file path separator
extern const char sep;

/////////////////////////////////////////////////////////////////////
//tests for file/directory status
bool        exists(const String &path);
bool        is_file(const String &path);
bool        is_dir(const String &path);
bool        is_link(const String &path);
bool        is_absolute(const String &path);
bool        is_relative(const String &path);

///////////////////////////////////////////////////////////////////////////////
// get the file extension as a substring of (path)
String ext(const String &path);

///////////////////////////////////////////////////////////////////////////////
// get the file name from a path, if path designates a file
String filename(const String &path);

///////////////////////////////////////////////////////////////////////////////
// remove the filename component from the path
String prefix(const String &path);

/////////////////////////////////////////////////////////////////////
//join the two paths more-or-less intelligently 
String join(const String &lhs, const String &rhs);

/////////////////////////////////////////////////////////////////////
//strip a single part of the filename from the path
String strip(const String &path, const String &component);

}
}
#endif
