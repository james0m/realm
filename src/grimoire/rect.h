#ifndef _rect_h_
#define _rect_h_

#include <initializer_list>

#include "types.h"
#include "vec2.h"

template <typename T>
struct rect {

    T x, y, w, h;

    static rect<T> null_rect() {
        static rect<T> null_rect_obj = rect<T> (0,0,0,0);
        return null_rect_obj;
    }

    static rect<T> from_quad(T x1, T y1, T x2, T y2) {
        return rect<T>{ x1, y1, (x2 - x1), (y2 - y1) };
    }
    static rect<T> from_quad(vec2<T> tl, vec2<T> br) {
        return from_quad(tl.x, tl.y, br.x, br.y);
    }
    
    rect(T x=0, T y=0, T w=0, T h=0):
        x(x),
        y(y),
        w(w),
        h(h)
    {}

    rect(const vec2<T> &offset, const vec2<T> &size) {
        x = offset.x;
        y = offset.y;
        w = size.x;
        h = size.y;
    }

    rect (const std::initializer_list<T> &list) {
        const T* p = list.begin();
        x = *p++;
        y = *p++;
        w = *p++;
        h = *p;
    }

    template <typename T2>
    rect (const rect<T2> &other) {
        *this = other;
    }

    template <typename T2>
    rect<T> & operator = (const rect<T2>& other) {
        x = other.x;
        y = other.y;
        w = other.w;
        h = other.h;
        return *this;
    }
    

    void set(T pos=0, T sz=0);
    void set(T x, T y, T w, T h);
    void set_position(T ix, T iy);
    void set_position(const vec2<T> &xy);
    void set_size(T iw, T ih);
    void set_size(const vec2<T> &xy) {
        set_size(xy.x, xy.y);
    }
    inline T left()   const { return x; }
    inline T right()  const { return x + w; }
    inline T top()    const { return y; }
    inline T bottom() const { return y + h; }

    inline T centerx() const { return x + w / 2; }
    inline T centery() const { return y + h / 2; }
    
    inline vec2<T> size() const { return vec2<T>(w, h); }
    inline vec2<T> center() const { return vec2<T>{centerx(), centery()}; }
    inline vec2<T> topleft() const { return vec2<T>(x, y); }
    inline vec2<T> topright() const { return vec2<T>(x+w,y); }
    inline vec2<T> bottomleft() const { return vec2<T>(x,y+h); }
    inline vec2<T> bottomright() const { return vec2<T>(x+w,y+h); }
    inline vec2<T> dimensions() const { return vec2<T>(w, h); }

    inline bool valid() const { return w > 0 && h > 0; }
    inline bool collide(T x, T y) const {
        return (x >= left() && x < right()) && (y>=top()&&y<bottom());
    };

    template <typename T2>
    inline bool collide(const vec2<T2> &xy) const {
        return (xy.x >= left() && xy.x<right() && xy.y >= top() && xy.y < bottom());
    }
    template <typename T2>
    inline bool collide(const rect<T2> &r) const {
        s32 test_x = std::min(x + w, r.x + r.w) - std::max(x, r.x);
        s32 test_y = std::min(y + h, r.y + r.h) - std::max(y, r.y);
        return test_x >= 0 && test_y >= 0;
    }

    /** 
     * get a rect that represents the shared area between two rectangles
     * 
     * algo from pygame's implementation of rect_clip()
     */
    rect<T> intersection(const rect<T> &other) const {
        rect<T> newrect;

        // left
        if ((x >= other.x) && (x < (other.right())))
            newrect.x = x;
        else if ((other.x >= x) && (other.x < (right())))
            newrect.x = other.x;
        else
            return null_rect();

        // right
        if ((right() > other.x) && (right() <= other.right()))
            newrect.w = right() - newrect.x;
        
        else if ((other.right() > x) && (other.right() <= right()) )
            newrect.w = other.right() - newrect.x;
        else
            return null_rect();
        
        // top
        if ((y >= other.y) && (y < (other.bottom())) )
            newrect.y = y;
        
        else if ((other.y >= y) && (other.y < (bottom()) ))
            newrect.y = other.y;
        
        else
            return null_rect();
        
        // bottom
        if ((bottom() > other.y) && (bottom() <= other.bottom()))
            newrect.h = bottom() - newrect.y;
        
        else if ((other.bottom() > y) && (other.bottom() <= bottom()))
            newrect.h = other.bottom() - newrect.y;
        
        else
            return null_rect();
        
        return newrect;
    }


    rect<T> &translate(T dx, T dy) {
        x += dx; y += dy;
        return *this;
    }
    rect<T> &translate(const vec2<T> &delta) {
        return translate(delta.x, delta.y);
    }
    rect<T> translated(T dx, T dy) const {
        rect<T> ret(x + dx , y + dy, w, h);
        return ret;
    }
    rect<T> translated(const vec2<T> &delta) const {
        return translated(delta.x, delta.y);
    }

    // get a rect<T> where dimenstions are grown by dw, dh
    rect<T> expanded(T dw, T dh) {
        return rect<T>(x, y, w + dw, h + dh);
    }

    rect<T>& expand(T dw, T dh) {
        w += dw;
        h += dh;
        return *this;
    }

    rect<T>& expand(vec2<T> delta) {
        return expand(delta.x, delta.y);
    }

    rect<T>& inflate(vec2<T> delta) {
        auto half_dx = delta.x / 2;
        auto half_dy = delta.y / 2;
        x += -half_dx;
        y += -half_dy;
        w += half_dx;
        h += half_dy;

        return *this;
    }
    // get a new rect scaled by a factor of n
    rect<T> scaled(T n) {
        return rect<T>(x, y, w*n, h*n);
    }
    
    // scale this rect by a factor of n
    rect<T> &scale(T n) {
        w *= n;
        h *= n;
        return *this;
    }

    template <typename T2>
    bool contains(const rect<T2> &other) {
        // return true if no part of {other} is outside of {this}
        return collide(other.topleft()) && collide(other.bottomleft()) &&
               collide(other.topright()) && collide(other.bottomright());
    }

    /** cuts the rect to fit within the other rect */
    template <typename U>
    rect<T>& clip(const rect<U>& other)
    {
        *this = intersection(other);
        return *this;
    }
};


template <typename T>
void rect<T>::set(T pos, T sz) {
    x = pos;
    y = pos;
    w = sz;
    h = sz;
};
template <typename T>
void rect<T>::set(T x, T y, T w, T h) {
    this->x = x;
    this->y = y;
    this->w = w;
    this->h = h;
};

template <typename T>
void rect<T>::set_position(T ix, T iy) {
    this->x = ix;
    this->y = iy;
};
template <typename T>
void rect<T>::set_position(const vec2<T> &xy) {
    this->x = xy.x;
    this->y = xy.y;
}
template <typename T>
void rect<T>::set_size(T iw, T ih) {
    this->w = iw;
    this->h = ih;
};

template <typename T1, typename T2>
bool operator == (const rect<T1> &r1, const rect<T2> &r2) {
    return r1.x == r2.x && r1.y == r2.y && r1.w == r2.w && r1.h == r2.h;
}

template <class T1, class T2>
bool operator != (const rect<T1> &r1, const rect<T2> &r2) {
    return ! (r1 == r2);
}

template <class T>
rect<T> union_rect(const rect<T>& a, const rect<T>& b) {
    auto result = rect<T>();
    
    result.x = std::min(a.x, b.x);
    result.y = std::min(a.y, b.y);        

    auto x2 = std::max(a.right(), b.right());
    auto y2 = std::max(a.bottom(), b.bottom());

    result.w = x2 - result.x;
    result.h = y2 - result.y;
    
    return result;
}
typedef rect<float> rectf;


template <class T>
rect<T> topleft(rect<T> r) {
    auto x = r.x,
        y = r.y,
        w = r.w / 2,
        h = r.h / 2;
    return rect<T>(x,y,w,h);
}

template <class T>
rect<T> topright(rect<T> r) {
    auto x = r.x + r.w / 2,
        y = r.y,
        w = r.w / 2,
        h = r.h / 2;
    
    return rect<T>(x,y,w,h);
}

template <class T>
rect<T> bottomleft(rect<T> r) {
    auto x = r.x,
         y = r.y + r.w / 2,
         w = r.w / 2,
         h = r.h / 2;

    return rect<T>(x, y, w, h);
}

template <class T>
rect<T> bottomright(rect<T> r) {
    auto x = r.x + r.w / 2,
        y = r.y + r.h / 2,
        w = r.w / 2,
        h = r.h / 2;
    
    return rect<T>(x,y,w,h);
}

#include <ostream>
template <class T>
std::ostream &operator << (std::ostream& out, rect<T> r) {
    out << "rect { " << r.x << ", " << r.y << ", " << r.w << ", " << r.h << " }" << std::endl;
    return out;
}


#include "grimoire_util.h"
namespace util {

template <typename T>
Array< rect<T> > rects_from_grid(T rwidth, T rheight,
								 T overallwidth, T overallheight, T spacing=0, T margin=0)
{
    u32 h_capacity = overallwidth / (rwidth + spacing);
    u32 v_capacity = overallheight / (rheight + spacing);

    u32 grid_width = h_capacity * (rwidth+spacing);
    u32 grid_height = v_capacity * (rheight+spacing);
    
    u32 x_stride = rwidth + spacing;
    u32 y_stride = rheight + margin; 

    Array<rect<T>> regions;
    regions.reserve(h_capacity * v_capacity);

    rect<T> _rect = { 0, 0, rwidth ,rheight };

    for(u32 y=margin; y<grid_height; y+=y_stride) {
        for(u32 x=margin; x<grid_width; x+=x_stride) {
            _rect.x = x;
            _rect.y = y;
            regions.push_back(_rect);
        }
    }
    return regions;
}

}
#endif
