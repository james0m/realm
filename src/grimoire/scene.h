// -*- c++ -*-
#ifndef _wzrd_scene_h_
#define _wzrd_scene_h_

#include "grimoire/types.h"
#include "grimoire/vec2.h"
#include "grimoire/renderapi.hpp"
#include "element.h"
#include "camera.h"

namespace gfx {

class Scene : public Element {
public:

    static std::unique_ptr<Scene> create(String title, bool make_current=false);
    
    Scene(String title);
    virtual ~Scene() {}
        
    const String &title() const;      
    
    void draw(RenderApi &r, const Camera& camera);

    const Camera *camera() const { return _cam; }
    
    void follow_camera(bool follow) { _follow_camera = follow; }
    
private:

    virtual void draw(RenderApi &r) {}

    bool _follow_camera = true;
    const Camera* _cam = nullptr;   
    String _title;
};

}// gfx
#endif 
