#ifdef _WIN32


//#include "filesystem.h"
#include "pathutils.h"

#include <windows.h>

using namespace std;

namespace io {
	namespace path {

		const char sep = '/';

		bool exists(const String &path) {

			LPCTSTR lpctstr = path.c_str();
			
			DWORD dwAttrib = GetFileAttributes(lpctstr);
			return (dwAttrib != INVALID_FILE_ATTRIBUTES && !(dwAttrib & FILE_ATTRIBUTE_DIRECTORY));
		}

		bool is_file(const String &path) {
			return exists(path);
		}

		bool is_dir(const String &path) {
			DWORD dwAttrib = GetFileAttributes(path.c_str());
			return (dwAttrib != INVALID_FILE_ATTRIBUTES && (dwAttrib & FILE_ATTRIBUTE_DIRECTORY));
		}

		bool is_link(const String &path) {
			return false;
		}

		bool is_absolute(const String &path) {
			return false;
		}
		bool is_relative(const String &path) {
			return false;
		}

	} // namespace path
} // namespace io

#endif