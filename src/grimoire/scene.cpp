#include "scene.h"
#include "element.h"

#include "camera.h"

namespace gfx {


std::unique_ptr<Scene> Scene::create(String title, bool make_current)
{
    return util::make_unique<Scene>(title);
}

Scene::Scene(String title):
    Element(nullptr),
    _title(title)
{}

const String &Scene::title() const
{
    return _title;
}


//@TODO: there should be no rendering code in the scene graph itself
void Scene::draw(RenderApi &r, const Camera &camera)
{
    _cam = &camera;

    auto vp = camera.transform();
    if (_follow_camera)
        place( -vp.x, -vp.y);

    const auto& elems = elements();
    const auto N = elems.size();

    for(auto ch=0u; ch<N; ++ch)
    {
        elems[ch]->draw(r);
    }
}

}
