#ifndef _ioutils_h_
#define _ioutils_h_


#include <utility>

#include "grimoire_util.h"
#include "pathutils.h"

#include "3p/rapidxml/rapidxml.hpp"


namespace io {


struct XmlDoc {
    String                   src;
	rapidxml::xml_document<> dom;

    void
    parse(String s);
};

///////////////////////////////////////////////////////////////////////////////
// read a file's data into a new string
String load_into_string(const String &file);

///////////////////////////////////////////////////////////////////////////////
// get the directory of the application's executable
String exe_dir();

///////////////////////////////////////////////////////////////////////////////
// get the current working directory
String working_dir();

Unique<XmlDoc> load_xml_document(const String& file);
Unique<XmlDoc> parse_xml_document(const String& data);


} //namespace io


#endif
