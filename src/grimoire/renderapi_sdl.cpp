#include <cassert>
#include "types.h"
#include "renderapi_sdl.hpp"
#include "sdlutils.h"
#include "grimoire_util.h"
#include "log.h"
#include "stringlib.h"

using namespace std;

constexpr const char* TAG = "RenderApi_SDL";

Uint32 get_sdl_format(PixelFormat fmt) {
	switch(fmt) {
		case ARGB: return SDL_PIXELFORMAT_ARGB8888;
		case RGBA: return SDL_PIXELFORMAT_RGBA8888;
		case ABGR: return SDL_PIXELFORMAT_ABGR8888;
		case BGRA: return SDL_PIXELFORMAT_BGRA8888;
        case RGB:  return SDL_PIXELFORMAT_RGB24;
        case BGR:  return SDL_PIXELFORMAT_BGR24;

		default:
			return SDL_PIXELFORMAT_UNKNOWN;
	}
}

PixelFormat from_sdl_format_u32(u32 format)
{
    switch(format)
    {
        case SDL_PIXELFORMAT_BGRA8888: return BGRA;
        case SDL_PIXELFORMAT_RGBA8888: return RGBA;
        case SDL_PIXELFORMAT_ABGR8888: return ABGR;
        case SDL_PIXELFORMAT_ARGB8888: return ARGB;
        case SDL_PIXELFORMAT_RGB24:    return RGB;

        default:
            throw sdl::SDLError(
                "from_sdl_format(): unsupported pixelformat: " +
                sdl::pixelformat_string(format));
    }
}

PixelFormat from_sdl_format(SDL_PixelFormat *format) {
    return from_sdl_format_u32(format->format);
}


int get_sdl_access_mode(AccessMode access) {
    return (access == AccessMode::STREAM) ? SDL_TEXTUREACCESS_STREAMING :SDL_TEXTUREACCESS_STATIC;
}

RenderApi_SDL::RenderApi_SDL(SDL_Window *window)
{
    if(window != nullptr)
        set_context(window);
}

RenderApi_SDL::~RenderApi_SDL() {
    if (_ctx.get() != nullptr)
        _ctx.reset(nullptr);
}


std::unique_ptr<RenderApi> RenderApi_SDL::create(SDL_Window *win) {
    return util::make_unique<RenderApi_SDL>(win);
}

void RenderApi_SDL::set_context(SDL_Window* window) {
	RenderApi::set_context(window);
	_ctx = sdl::renderer_ptr {SDL_GetRenderer (window)};
	if (!_ctx)
        _ctx = sdl::renderer_create(window, 0, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

    SDL_SetRenderDrawBlendMode(_ctx.get(), SDL_BLENDMODE_BLEND);
}

void RenderApi_SDL::frame_begin() {
	clear();
}

void RenderApi_SDL::clear() {
	save_draw_color();
    auto cc = _clear_color.convert<u8>();

	SDL_SetRenderDrawColor(_ctx.get(),
		cc.r,
		cc.g,
		cc.b,
		cc.a
	);

	SDL_RenderClear(_ctx.get());
	restore_draw_color();
}

void RenderApi_SDL::frame_end() {
	SDL_RenderPresent(_ctx.get());
}

void RenderApi_SDL::batch_lines(const line_def *lines, int n) {

    save_draw_color();
	for(const line_def *i=lines, *end=lines+n; i!=end; ++i) {
	// extract the rect information into something SDL understands...

		// set rendering color, can only use one color for now
		color<u8> c = i->color.convert<u8>();

		SDL_SetRenderDrawColor(_ctx.get(),
			c.r,c.g,c.b,c.a
		);

		SDL_RenderDrawLine(_ctx.get(),
			i->start.x,
			i->start.y,
			i->end.x,
			i->end.y
		);
	}
	restore_draw_color();
}

void RenderApi_SDL::batch_circles(const circle_def* circles, int n) {
	save_draw_color();
	for (auto i=0; i<n; ++i)
	{
		draw_circle(circles[i].center, circles[i].radius, circles[i].color);
	}
	restore_draw_color();
}
void RenderApi_SDL::batch_rects(const rect_def* rects, int n) {
	SDL_Point sdl_points[5];
	save_draw_color();
	for(auto i=&rects[0], end=rects+n; i!=end; ++i) {
	// extract the rect information into something SDL understands...

		// topleft
		sdl_points[0].x = i->r.x;
		sdl_points[0].y = i->r.y;

		//topright
		sdl_points[1].x = i->r.right();
		sdl_points[1].y = i->r.y;

		//bottomright
		sdl_points[2].x = i->r.right();
		sdl_points[2].y = i->r.bottom();

		//bottomleft
		sdl_points[3].x = i->r.x;
		sdl_points[3].y = i->r.bottom();

		sdl_points[4].x = i->r.x;
		sdl_points[4].y = i->r.y;

		// set rendering color, can only use one color for now
		SDL_SetRenderDrawColor(_ctx.get(),
			i->color.r * 255,
			i->color.g * 255,
			i->color.b * 255,
			i->color.a * 255
		);

		SDL_RenderDrawLines(_ctx.get(), sdl_points, 5);
	}
	restore_draw_color();
}

void RenderApi_SDL::draw_texture(texid texture, const rectf& src, const rectf& dst)
{
    if (src == rectf::null_rect() && dst == rectf::null_rect()) {
        SDL_RenderCopy(_ctx.get(), _textures[texture], nullptr, nullptr);
        return;
    }

    SDL_Rect ssrc, sdst;

    SDL_Rect *p_src = nullptr , *p_dst = nullptr;

    if (src != rectf::null_rect()) {
        p_src = &ssrc;
        ssrc.x = src.x;
        ssrc.y = src.y;
        ssrc.w = src.w;
        ssrc.h = src.h;

    }
    if (dst != rectf::null_rect()) {
        p_dst = &sdst;
        sdst.x = dst.x;
        sdst.y = dst.y;
        sdst.w = dst.w;
        sdst.h = dst.h;
    }

  	SDL_RenderCopy(_ctx.get(), _textures[texture], p_src, p_dst);
}

void RenderApi_SDL::batch_textures(const texture_def *textures, int n) {
	for(auto tx=textures, end=textures+n; tx!=end; ++tx) {
		SDL_Rect src, dst;

		src.x = tx->src_region.x;
		src.y = tx->src_region.y;
		src.w = tx->src_region.w;
		src.h = tx->src_region.h;

		dst.x = tx->dst_region.x;
		dst.y = tx->dst_region.y;
		dst.w = tx->dst_region.w;
		dst.h = tx->dst_region.h;

		// draw the texture...
		SDL_Texture *texture = _textures[tx->texture];
		SDL_RenderCopy(_ctx.get(), texture, &src, &dst);
	}
}

texid RenderApi_SDL::create_texture(int w, int h, PixelFormat fmt, AccessMode access) {
	assert( w > 0 && h > 0);

    texid tx = _textures.size();
	_textures[tx] = nullptr;
    _texture_format[tx] = get_sdl_format(fmt);

	SDL_Texture *real_texture = SDL_CreateTexture(_ctx.get(),
		get_sdl_format(fmt),
		get_sdl_access_mode(access),
		w, h
	);

	if( real_texture != nullptr) {
		_textures[tx] = real_texture;
        SDL_SetTextureBlendMode(real_texture, SDL_BLENDMODE_BLEND);
	}
	else {
        Log::e(TAG, sl::format("create_texture: failed creating texture... \nreason: %s",
                                 SDL_GetError()));
        return NULL_TEXTURE;
	}
	return tx;
}
void RenderApi_SDL::update_texture(const void* data, const rectf *region, int pitch) {
    assert( _bound_texture < _textures.size() &&_bound_texture != NULL_TEXTURE );

	Uint32 fmt;
	int access, w, h;

	SDL_Texture *real_texture = _textures[_bound_texture];

	assert(real_texture != nullptr);

	SDL_QueryTexture(real_texture, &fmt, &access, &w, &h);

    if (pitch == 0) // if no pitch given, assume data is in a matching format
        pitch = w * SDL_BYTESPERPIXEL(fmt);

	SDL_Rect sdl_region;

	if (region != nullptr) {
		sdl_region.x = region->x;
		sdl_region.y = region->y;
		sdl_region.w = region->w;
		sdl_region.h = region->h;
		SDL_UpdateTexture(real_texture, &sdl_region, data, pitch);
	}
	else
		SDL_UpdateTexture(real_texture, NULL, data, pitch);

}

void RenderApi_SDL::get_texture_info(texid tex, TextureInfo& info)
{
    int w, h, access;
    u32 fmt;

    SDL_Texture* sdl_texture = _textures[tex];
    if (!sdl_texture)
    {
        return;
    }

    SDL_QueryTexture(sdl_texture, &fmt, &access, &w, &h);

    info.id = tex;
    info.w = w;
    info.h = h;
    info.format = from_sdl_format_u32(fmt);
    if (access == SDL_TEXTUREACCESS_STATIC)
    {
        info.access_mode = STATIC;
    }

    else if (access == SDL_TEXTUREACCESS_STREAMING)
    {
        info.access_mode = STREAM;
    }
}

void RenderApi_SDL::bind_texture(texid texture) {
	assert( texture < _textures.size());
	_bound_texture = texture;
}

void RenderApi_SDL::draw_point(float x, float y, const colorf &color) {
    SDL_Renderer *r = _ctx.get();
    SDL_SetRenderDrawColor(r, color.r * 255.0f, color.g * 255.0f, color.b * 255.0f, color.a * 255.0f);
    SDL_RenderDrawPoint(r, (int)x, (int)y);
}

void RenderApi_SDL::draw_line(vec2f p1, vec2f p2, const colorf& color)
{
    auto _color = color.convert<u8>();
    SDL_SetRenderDrawColor(_ctx.get(), _color.r, _color.g, _color.b, _color.a);
    SDL_RenderDrawLine(_ctx.get(), p1.x, p1.y, p2.x, p2.y);
}
void RenderApi_SDL::draw_rect(const rectf &r, const colorf &color) {
	SDL_Rect region;
	region.x = r.x;
	region.y = r.y;
	region.w = r.w;
	region.h = r.h;

	save_draw_color();
	SDL_SetRenderDrawColor(_ctx.get(),
		color.r * 255,
		color.g * 255,
		color.b * 255,
		color.a * 255
	);
	SDL_RenderDrawRect(_ctx.get(), &region);
	restore_draw_color();
}
void RenderApi_SDL::draw_filled_rect(const rectf &r, const colorf &color) {
	save_draw_color();

	SDL_Rect region;
	region.x = r.x;
	region.y = r.y;
	region.w = r.w;
	region.h = r.h;

    auto cconv = color.convert<u8>();

	SDL_SetRenderDrawColor(_ctx.get(),
        cconv.r,// * 255.0f,
        cconv.g,// * 255.0f,
        cconv.b, //* 255.0f,
        cconv.a// * 255.0f
	);
	SDL_RenderFillRect(_ctx.get(), &region);

	restore_draw_color();
}

void generate_circle_points(const vec2f &c, float radius, Array<SDL_Point> &out) {
    vec2i ci = c;
	int x = int(radius);
	int y = 0;
	int d2 = 1 - x;

	SDL_Point pt;
	while ( y <= x)
	{
		pt.x = x + ci.x;
		pt.y = y + ci.y;
		out.push_back(pt);

		pt.x = -x + ci.x;
		pt.y = y + ci.y;
		out.push_back(pt);

		pt.x = y + ci.x;
		pt.y = x + ci.y;
    	out.push_back(pt);

    	pt.x = -y + ci.x;
    	pt.y = x + ci.y;
    	out.push_back(pt);

    	pt.x = x + ci.x;
    	pt.y = -y + ci.y;
    	out.push_back(pt);

    	pt.x = -x + ci.x;
    	pt.y = -y + ci.y;
    	out.push_back(pt);

    	pt.x = -y + ci.x;
    	pt.y = -x + ci.y;
    	out.push_back(pt);

    	pt.x = y + ci.x;
    	pt.y = -x + ci.y;
    	out.push_back(pt);

		++y;

		if( d2 <= 0) {
			d2 += 2 * y + 1;
		}
		else {
			--x;
			d2 += 2 * (y - x) + 1;
		}
	}

}
void RenderApi_SDL::draw_circle(const vec2f &c,
                                   float radius,
                                   const colorf &color)
{
    static Array< SDL_Point > points;
    generate_circle_points(c, radius, points);

	SDL_SetRenderDrawColor(_ctx.get(),
                           color.r * 255,
                           color.g * 255,
                           color.b * 255,
                           color.a * 255);

	SDL_RenderDrawPoints(_ctx.get(), &points[0], points.size());
    points.clear();
}


void RenderApi_SDL::draw_filled_circle(const vec2f &c,
                                          float radius,
                                          const colorf &color)
{
    static Array<SDL_Point> points;
    generate_circle_points(c, radius, points);

	SDL_SetRenderDrawColor(_ctx.get(),
                           color.r * 255,
                           color.g * 255,
                           color.b * 255,
                           color.a * 255);
	SDL_RenderDrawLines(_ctx.get(),
                        &points[0], points.size());

    points.clear();
}
