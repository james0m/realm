#ifndef _pixel_iterator_h_
#define _pixel_iterator_h_

#include "SDL2/SDL_video.h"
#include "types.h"

//////////////////////////////////////////////
// A random access iterator for SDL_Surfaces
//
// TODO: accessors for each color channel
//       remove SDL_Surface dependency and replace
//       with compile-time image data traits
//
//////////////////////////////////////////////
template <typename Pixel>
class pixel_iterator {

	SDL_Surface   *_surface;
	Pixel         *_position;
	u32            _stride;  // width of pixel row, based on pitch and size of the Pixel type

public:
	typedef pixel_iterator<Pixel> value_type;

	pixel_iterator() : _surface(NULL), _position(NULL), _stride(0) {}
	pixel_iterator(SDL_Surface *s) : 
		_surface(s), 
		_position(static_cast<Pixel *>(_surface->pixels)),
		_stride(s->pitch / sizeof(Pixel))
	{
	}
	pixel_iterator(const value_type &o) :
		_surface(o._surface),
		_position(o._position),
        _stride(o._stride)
	{
	}

	value_type & operator=(const value_type& other) {
		if(this != &other) {
			_surface = other._surface;
			_position = other._position;
			_stride = _surface->pitch / sizeof(Pixel);
		}
		return *this;
	}

	bool operator == (const value_type& other) {
		return this->_position == other._position;
	}
	bool operator != (const value_type& other) {
		return this->_position != other._position;
	}

	size_t index() const {
        return _position - static_cast<Pixel*>(_surface->pixels);
    }
	size_t x()  const { return index() - y() * _stride; }
	size_t y()  const { return index() / static_cast<size_t>(_stride); }
	void move_x(int dx) { _position += dx; }
	void move_y(int dy) { _position += (dy * _stride); }

	void move_up() 		{ _position -= _stride; }
	void move_down() 	{ _position += _stride; }
	void move_left() 	{ --_position; }
	void move_right() 	{ ++_position; }

	u32 stride() const {
		return _stride;
	}
	Pixel get() const { return *_position; }
	
	// de-reference operator	
	Pixel &operator * () {
		return *_position;
	}

	// pre-increment operator
	value_type& operator ++() { 
		++_position; 
		return *this; 
	}	

	// post-increment operator
	value_type operator ++(int) {
		value_type tmp(*this);
		++_position;
		return tmp;
	}

	template <typename ScalarType>
	value_type operator + (ScalarType value) {
		value_type result(*this);
		result._position += value;
		return result;
	}

	template <typename ScalarType>
	value_type operator - (ScalarType value) {
		value_type result(*this);
		result._position -= value;
		return result;
	}

	template <typename ScalarType>
	value_type &operator +=(ScalarType value) {
		_position += value;
		return *this;
	}

	template <typename ScalarType>
	value_type &operator -=(ScalarType value) {
		_position -= value;
		return *this;
	}

protected:
	Pixel *_start() {
		return static_cast<Pixel *>(_surface->pixels);
	}
};

#endif
