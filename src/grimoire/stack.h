#ifndef _stack_h_
#define _stack_h_

#include "grimoire_util.h"

namespace util {

/*
* A adapts an std::vector for use as a stack
*/
template <typename T>
class Stack 
{
    Array<T> _data;

public:
    Stack(size_t n=0): _data(n) {}

	virtual ~Stack() {}

	/* get item from the top of the stack */
	T &get() {
		return _data.back();
	}
	
	/* get item from the top of the stack ~ const-friendly version*/
	T get() const {
		return _data.back();
	}

    T& top(){ return get(); }
    T top() const { return get(); }

    /* emplace an empty (default constructed) item */
    T& push() {
        _data.emplace_back();
        return _data.back();
    }
    
	/* push an item onto the stack */
	void push(const T &data) {
		_data.push_back(data);
	}

    /* push using an r-value */
    void push(T &&data) {
        _data.push_back(std::move(data));
    }

    template <class ...Args>
    void emplace(Args && ...args) {
        _data.emplace(std::forward(args)...);
    }
	/* remove the top item from the stack */
	void pop() {
		_data.pop_back();
	}
	
	/* get size of the stack */
	size_t size() const { 
		return _data.size();
	}

    void clear() {
        _data.clear();
    }
};

}
#endif
