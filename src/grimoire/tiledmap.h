#ifndef GAME_MAP_H_
#define GAME_MAP_H_

#include "grid.h"
#include "textureatlas.h"
#include "terrain.hpp"

namespace tmx {

/*///////////////////////////////////////////////////////////////////////////////
	Tileset

	A texture atlas that provides the tile graphics for a given
	range of terrain cell IDs.
*////////////////////////////////////////////////////////////////////////////////
struct Tileset {

	String         src;
	String         name;
	int            firstgid;
	int            lastgid;
	int            tilewidth;
	int            tileheight;
	int            spacing;
	int            margin;
	vec2<int>      offset;
	TextureAtlas   atlas;
};

typedef util::grid<u16> Grid;

struct MapLayer
{
    size_t id;    // unique identifier for the layer
    size_t lvl;   // sort key, if we need to reorder our layers
    String name;  // unique identifier
    Grid   data; // tile data
};

class TiledMap {
public:
	typedef Unique<TiledMap>     Ref;
    typedef Array< Tileset >        tileset_list;
    typedef Array<MapLayer>         layer_list;
    typedef HashMap<String, unsigned>    layer_id_map;

    using terrain_list = Array<Terrain>;

public:

    explicit
    TiledMap(String name="");

    virtual
    ~TiledMap() = default;

    static Ref                 from_file(const String &file);
    static Ref                 create(int w, int h);

    const String&              name() const;      
    size_t                     size() const;
    int                        pxwidth() const;
    int                        pxheight() const;
    int                        width() const;      
    int                        height() const;     
    vec2<int>                  dimensions() const;
    int                        tile_width() const;  
    int                        tile_height() const; 
    vec2<int>                  tile_dimensions() const;

    vec2<int>                  pixel_dimensions() const { return (dimensions() * tile_dimensions()); }
    void                       set_name(const String &n) { _name = n; }
    void                       set_dimensions(int w, int h);
    void                       set_width(int w);
    void                       set_height(int h);
    u32                        add_tileset(const Tileset &ts);

    Tileset &                  tileset(u32 idx);
    const Tileset &            tileset(u32 idx=0) const;

    void                       set_tile_dimensions(u32 tw, u32 th);
    void                       set_tilewidth(u32 tw);
    void                       set_tileheight(u32 th);
    u32                        tile_atlas_idx(u32 cell) const;
    
    void                       add_terrain(const Terrain& terrain);

    const Terrain&             terrain(idtype terrain_id) const;
    const Terrain&             terrain_at(vec2i location) const;
    ////////////////////////////////////////////////////////////////////////////////
    // perform a linear search and tests idx for inclusion in 
    // a sets GID range.
    //
    // returns the id of the tileset containing the index/gid
    u32                        tileset_index(u32 idx) const;

    ////////////////////////////////////////////////////////////////////////////////
    // add a new tile layer to the map
    // 
    // automatically sets the new layers data dimensions and 
    // allocates the required memory
    //
    // in:      name - name for the new layer
    // returns: layer id 
    unsigned                   add_layer(const String &name);
    
    MapLayer &                 layer_at(unsigned idx);
    const MapLayer &           layer_at(unsigned idx) const;
    MapLayer&                  layer_by_name(const String &id);
    bool                       have_layer(const String &name) const;
    const layer_list&          layers() const;
    layer_list&                layers();
    unsigned                   layer_count() const; 
    unsigned                   tileset_count() const; 

    bool                       valid() const;

    size_t                     tile_type_count() const { return _tile_types.size();}
    size_t                     terrain_type_count() const { return _terrains.size(); }
    
    const TileInfo&            tile_info(u16 gid) const;
    TileInfo&                  tile_info(u16 gid);
    void                       add_tile_info(u8 id, u8 terrain[4], u8 resistance);


private:

    using TileInfoList = Array<TileInfo>;
    
    String                     _name;
    int                        _width;
    int                        _height;
    int                        _tilewidth;
    int                        _tileheight;

    tileset_list               _tilesets;
    terrain_list               _terrains;
    TileInfoList               _tile_types;
    layer_id_map               _layer_id_lookup;
    layer_list                 _layers;
};


} // namespace grimoire
#endif
