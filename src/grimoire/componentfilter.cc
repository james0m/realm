#include "componentfilter.h"

namespace Realm {
	
ComponentFilter& ComponentFilter::require(idtype component_id) {
	required_mask |= (1 << component_id);
	return *this;
}

ComponentFilter& ComponentFilter::include(idtype component_id) {
	inclusion_mask |= (1 << component_id);
	return *this;
}

ComponentFilter& ComponentFilter::exclude(idtype component_id) {
	exclusion_mask |= (1 << component_id);
	return *this;
}

ComponentFilter& ComponentFilter::requirements(std::initializer_list<idtype> components) {
	for (auto id : components) required_mask |= (1 << id);
	return *this;
}
ComponentFilter& ComponentFilter::inclusions(std::initializer_list<idtype> components) {
	for (auto id : components) inclusion_mask |= (1 << id);
	return *this;
}
ComponentFilter& ComponentFilter::exclusions(std::initializer_list<idtype> components) {
	for (auto id : components) exclusion_mask |= (1 << id);
	return *this;
}

bool ComponentFilter::operator()(idtype entity_id) const {
	return test(entity_id);
}

bool ComponentFilter::test(u32 mask) const {
	// if any components are excluded, and any show up in the mask, fail
	if (exclusion_mask && (exclusion_mask & mask) )
		return false;

	// if any components are required, and any don't show up, fail
	if (required_mask && (required_mask & mask) != required_mask)
		return false;

	// if we need one of a group of components, and none show up, fail
	if (inclusion_mask && !(inclusion_mask & mask))
		return false;

	return true;
}

}
