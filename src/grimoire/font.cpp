#include <cassert>
#include "fontlib.h"

#include "grimoire/types.h"
#include "grimoire/log.h"
#include "grimoire/stringlib.h"
#include "grimoire/filesystem.h"
#include "grimoire/ioutils.h"

namespace txt {

constexpr const char* TAG = "fontlib";
    
Font::Font(std::string path, int pt):
    filepath(path),
    size(pt)
{
}


Font::~Font()
{   
    FT_Done_Face(face);
}

Unique<Font> Font::from_file(RenderApi& renderer, std::string path, int pt) {
    auto font = new Font(path, pt);
    font->init(renderer, path);
    return Unique<Font>(font);
}
    
bool Font::has_glyph_atlas() const {
    return glyphs.size() > 0;
}


const Glyph& Font::glyph(char ch) const {
    
    return ch != '\n' ? glyphs.glyph(ch) : Glyph::NULL_GLYPH;
}

constexpr const char* ALPHANUMERICS_PLUS =
    "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890 !@#$%^&*()-+_=[]{}\\|;:'~`?.,<>/";


void Font::init(RenderApi &renderer, std::string file) {
    
    int error = FT_New_Face(txt::lib(),
                            sl::format("%s/%s/%s", io::exe_dir(), "data", file).c_str(),
                            0,
                            &face);
    if (error) {
        Log::e(TAG, sl::format("Font::init(): error loading face: %s", ft_error_string(error)));
        assert(false);
    }

    error = FT_Set_Char_Size(face,
                             size * 64, // char width, followd by height...
                             size * 64, // size is handled in 1/64s of a point
                             96,        // dpi x
                             0);        // dpi y

    if (error) {
        Log::e(TAG, sl::format("Font::init(): error setting character size: %s", ft_error_string(error)));
        assert(false);
    }
  
    filepath = file;
    glyphs = generate_atlas(renderer, *this, ALPHANUMERICS_PLUS);

    // hacky way to get an automatic line_height
    for(auto entry : glyphs ) {
        line_height = fmax(entry.second.region.h + 5, line_height);
    }

}

int Font::faces() const {
    return face->num_faces;
}

    

}//ftl

