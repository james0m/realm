#include "physfs.h"
#include "log.h"
#include "physfs_filesystem.h"
#include "pathutils.h"

using namespace std;

constexpr const char* TAG = "PhysFSFileSystem";

namespace io
{

PhysFSFileSystem::PhysFSFileSystem(const String &root)
{
	PHYSFS_init(root.c_str());
	add_path(PHYSFS_getBaseDir(), "");
}

PhysFSFileSystem::~PhysFSFileSystem()
{
	PHYSFS_deinit();
}

const char* PhysFSFileSystem::type() const
{
	return TAG;
}

FileHandle PhysFSFileSystem::open_file(const String &path, AccessMode mode) const
{
	auto handle = FileHandle {new PhysFSFile (path, mode)};
	return handle;
}

FileHandle PhysFSFileSystem::create_file(const String &path) const
{
	return open_file(path, ACCESS_WRITE);
}

FileHandle PhysFSFileSystem::create_dir(const String &path) const
{
	PHYSFS_mkdir(path.c_str());
	return open_file(path, ACCESS_WRITE);
}

bool PhysFSFileSystem::add_path(const String &path, const String &mountpt)
{
	auto mountpath = path.c_str();
	auto mount = mountpt.empty() || mountpt == " " ? NULL : mountpt.c_str();
    auto res = PHYSFS_mount(mountpath, mount, 1) != 0 ? true : false;

    _search_paths = list_search_paths();
    return res;
}

String PhysFSFileSystem::root() const
{
	return String(PHYSFS_getBaseDir());
}

bool PhysFSFileSystem::exists(const String &path) const
{
	return PHYSFS_exists(path.c_str());
}

bool PhysFSFileSystem::isfile(const String &path) const
{	
    PHYSFS_Stat* stat = nullptr;
    PHYSFS_stat(path.c_str(), stat);
    return stat && stat->filetype == PHYSFS_FILETYPE_REGULAR;
}

bool PhysFSFileSystem::isdir(const String &path) const
{
    PHYSFS_Stat* stat = nullptr;
    PHYSFS_stat(path.c_str(), stat);    
	return stat && stat->filetype == PHYSFS_FILETYPE_DIRECTORY;
}

const stringlist& PhysFSFileSystem::search_path() const {
    return _search_paths;
}

stringlist PhysFSFileSystem::list_search_paths() const
{
    char **raw_path_strings = PHYSFS_getSearchPath();
    auto itr = raw_path_strings;
    auto result = stringlist();
    while(*itr!=NULL) {
        result.push_back(*itr++);
    }

    PHYSFS_freeList(raw_path_strings);
    return result;
}

static
const char* get_last_error()
{
    return PHYSFS_getErrorByCode(PHYSFS_getLastErrorCode());
}

PhysFSFile::PhysFSFile(const String &path, AccessMode mode):
	File(path, mode)
{
	switch (mode)
	{
		case ACCESS_WRITE: 
		{
			String pre = io::path::prefix(path);
			
			String basedir = PHYSFS_getBaseDir();

			PHYSFS_setWriteDir( (basedir + pre).c_str() );
			_handle = PHYSFS_openWrite(path.c_str());
			break;
		}
		
		case ACCESS_READ:
		default: 
		{
			_handle = PHYSFS_openRead(path.c_str());
			break;
		}
	}

	if ( ! _handle) {
		String physfs_err (get_last_error());
		throw FileNotFoundError(path, physfs_err);
	}
}

PhysFSFile::~PhysFSFile()
{
	close();
}

bool PhysFSFile::exists() const
{
	return PHYSFS_exists( path().c_str() ) != 0;
}

bool PhysFSFile::isfile() const
{
    PHYSFS_Stat* stat = nullptr;
    PHYSFS_stat(path().c_str(), stat);    
	return stat && stat->filetype == PHYSFS_FILETYPE_REGULAR;
}

bool PhysFSFile::isdir() const
{    
    PHYSFS_Stat* stat = nullptr;
    PHYSFS_stat(path().c_str(), stat);    
	return stat && stat->filetype == PHYSFS_FILETYPE_DIRECTORY;
}

String PhysFSFile::as_string(size_t bytes) const
{
	bytes = std::min(bytes, size());
    auto buffa = Unique<char[]>(new char[bytes+1]);
    read(buffa.get(), bytes);
    auto result = String(buffa.get(), bytes);

    assert(result.size() == bytes);
    return result;
}

void PhysFSFile::seek(size_t inc) 
{
	PHYSFS_seek(_handle, inc);
    _readpos = static_cast<size_t>(PHYSFS_tell(_handle));
}

void PhysFSFile::reset() 
{
	PHYSFS_seek(_handle, 0);
    _readpos = 0;
}

size_t PhysFSFile::read(void *dst, size_t bytes ) const
{
	assert (_handle != NULL);
    bytes = std::min(bytes, (size_t)PHYSFS_fileLength(_handle));

    const int bytesread = PHYSFS_readBytes(_handle, dst, bytes);

    if(bytesread < 0) {
        Log::e(TAG, sl::format("error reading from file: %s\n",
                               get_last_error()));
        return 0;
    }
    if((size_t)bytesread != bytes && bytes < File::ALL) {
        auto physfs_says = get_last_error();
        Log::e(TAG, sl::format(
                   "PHYSFS_read read only %d bytes; %d bytes reqested\n"
                   "PHYSFS says: %s",
                   bytesread, bytes, physfs_says));
    }
	return bytesread;
}
void PhysFSFile::write(void *data, size_t bytes)
{
	assert(_handle != NULL);
	assert(mode() == ACCESS_WRITE);

	PHYSFS_writeBytes(_handle, data, bytes);
}
void PhysFSFile::writestr(const String &data)
{
	write( (void*)data.c_str(), data.size());
}

size_t PhysFSFile::size() const 
{
	return static_cast<size_t>(PHYSFS_fileLength(_handle));
}

void PhysFSFile::close() 
{
	File::close();

	if ( _handle) {
		PHYSFS_close(_handle);
		_handle = NULL;
	}
}
} // io
