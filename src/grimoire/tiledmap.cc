#include "log.h"
#include "tiledmap.h"
#include "pathutils.h"
#include "tmxreader.h"


namespace tmx {

constexpr const char* TAG = "TmxReader";

TiledMap::TiledMap(String name):
    _name(std::move(name)),
    _width(0),
    _height(0),
    _tilewidth(0),
    _tileheight(0)
{

}


int TiledMap::pxwidth()  const {
    return _width * _tilewidth;
}

int TiledMap::pxheight() const {
    return _height * _tileheight;
}

size_t TiledMap::size() const {
    return _width * _height;
}

const String& TiledMap::name() const {
    return _name;
}

int TiledMap::tile_width() const {
    return _tilewidth;
}

int TiledMap::tile_height() const {
    return _tileheight;
}

vec2<int> TiledMap::tile_dimensions() const
{
    return vec2i(_tilewidth, _tileheight);
}

void TiledMap::set_width(int w)
{
    _width = w;
}
void TiledMap::set_height(int h)
{
    _height = h;
}
int TiledMap::width() const {
    return _width;
}

int TiledMap::height() const {
    return _height;
}
void TiledMap::set_tilewidth(u32 tw)
{
    _tilewidth = tw;
}
void TiledMap::set_tileheight(u32 th)
{
    _tileheight = th;
}
vec2<int> TiledMap::dimensions() const {
    return vec2<int>(_width, _height);
}

const TiledMap::layer_list& TiledMap::layers() const {
    return _layers;
}

TiledMap::layer_list& TiledMap::layers()  {
    return _layers;
}

unsigned TiledMap::layer_count() const {
    return _layers.size();
}

unsigned TiledMap::tileset_count() const {
    return _tilesets.size();
}

void TiledMap::add_terrain(const Terrain& terrain) {
    _terrains.push_back(terrain);
    const auto N = _terrains.size() - 1;
    _terrains[N].id = N;
}

const Terrain& TiledMap::terrain(idtype terrain_id) const
{
    assert(0 <= terrain_id && terrain_id < _terrains.size() &&
           "TiledMap::terrain: terrain index out of bounds");

    return _terrains[terrain_id];
}

const Terrain &TiledMap::terrain_at(vec2i location) const
{
    return _terrains[ layer_at(0).data.cell(location.x, location.y) ];
}

bool TiledMap::valid() const {
    if( ! (width() > 0 && height() > 0 && layer_count() > 0 && tileset_count() > 0)) {
        return false;
    }
    return true;
}

u32 TiledMap::tile_atlas_idx(u32 cell) const {
    return cell;
}

Tileset & TiledMap::tileset(u32 idx) {
    return _tilesets[idx];
}
const Tileset & TiledMap::tileset(u32 idx) const {
    return _tilesets.at(idx);
}

void TiledMap::set_tile_dimensions(u32 tw, u32 th) {
    _tilewidth = tw;
    _tileheight = th;
}
void TiledMap::set_dimensions(int w, int h) {
    _width = w;
    _height = h;
    for(auto l : _layers) {
        l.data.set_dimensions(w, h);
    }
}
/*
* add the tileset to the list, sort, and find its final position
* calculating the index by the difference of its position and the begin() iterator
*/
u32 TiledMap::add_tileset(const Tileset &ts) {
    _tilesets.push_back(ts);

    auto tilesets_start = _tilesets.begin(),
         tilesets_end   = _tilesets.end();

    // make sure tilesets are sorted by the global tile IDs they contain
	std::sort( tilesets_start, tilesets_end,
        [] (const Tileset &t1, const Tileset &t2) -> bool {
            return t1.firstgid < t2.firstgid;
        }
    );

    const auto itr = std::find_if(tilesets_start, tilesets_end, [&](const Tileset& tileset) {
        return tileset.name == ts.name;
    });

    return static_cast<u32>(itr - tilesets_start);
}

TiledMap::Ref TiledMap::from_file(const String &file)  {
    if (io::path::is_file(file)) {

        auto ftype = io::path::ext(file);

        if (ftype != "tmx") {
            printf("game_map::from_file - Can't recognize map file format: %s\n", ftype.c_str());
            return nullptr;
        }

        auto gm = io::load_tmx_map(file);

        // assertions for map validity
        assert(gm != nullptr);
        assert(gm->_tilesets.size());
        assert(gm->_tilesets[0].atlas.get_texture() != NULL_TEXTURE);
        assert(gm->_tilesets[0].atlas.region_count());

        return gm;

    } else {
        Log::i(TAG, "Yo dawg, that wasn't no damned map file... Getcha shit together, man.\n");
        return nullptr;
    }
}

TiledMap::Ref TiledMap::create(int w, int h) {

    auto map = util::make_unique<TiledMap>();
    map->set_dimensions(w,h);

    // generate a blank tileset


    return map;
}


u32 TiledMap::tileset_index(u32 idx) const
// perform a linear search and tests idx for inclusion in
// a tilesets GID range
//
// FIXME: The method returns 0 as an error case, when 0
//       is quite likely a valid tileset id.
{
    if(_tilesets.size() <= 1 || idx == 0) {
        return 0;
    }

    auto ts_count = _tilesets.size();
    for(unsigned tileset=0; tileset<ts_count - 1 ; ++tileset) {
        u32 this_1stgid = _tilesets[tileset].firstgid;
        if(idx == this_1stgid)
            return tileset;

        u32 next_1stgid = _tilesets[tileset+1].firstgid;

        if(idx > this_1stgid && idx < next_1stgid) {
            return tileset;
        }
        else if(idx == next_1stgid) {
            return tileset+1;
        }
        else if(idx > next_1stgid && (tileset + 1 == ts_count - 1)) {
            return tileset + 1;
        }
    }
    return 0;
}


unsigned TiledMap::add_layer(const String &name) {
    if( ! have_layer(name)) {
        MapLayer new_layer;
        new_layer.id = new_layer.lvl = _layers.size();
        new_layer.name = name;
        new_layer.data.set_dimensions(_width, _height);
        _layers.push_back(new_layer);
        _layer_id_lookup[new_layer.name] = new_layer.id;
        return new_layer.id;
    }
    else {
        // error
        Log::i(TAG, sl::format("game_map::add_layer - Error - Already have layer: %s\n", name.c_str()));
        return _layer_id_lookup[name];
    }
}


bool TiledMap::have_layer(const String &layername) const {
    for(auto& layer : _layers) {
        if(layername == layer.name) {
            return true;
        }
    }
    return false;
}

MapLayer &TiledMap::layer_at(unsigned idx) {
    ASSERT_BOUNDS_CHECK(idx, _layers.size());
    return _layers[idx];
}

const MapLayer &TiledMap::layer_at(unsigned idx) const {
    ASSERT_BOUNDS_CHECK(idx, _layers.size());
    return _layers.at(idx);
}

MapLayer &TiledMap::layer_by_name(const String &id) {
    unsigned layer_id = _layer_id_lookup.at(id);
    ASSERT_BOUNDS_CHECK(layer_id, _layers.size());
    return _layers[layer_id];
}


const TileInfo& TiledMap::tile_info(u16 gid) const {
    return _tile_types[gid];
}

TileInfo& TiledMap::tile_info(u16 gid) {
    return _tile_types[gid];
}

void TiledMap::add_tile_info(u8 id, u8 terrain[4], u8 resistance) {
    if( id >= _tile_types.size())
        _tile_types.resize(id);

    _tile_types.emplace(_tile_types.begin()+id,
                        id, terrain, resistance);
}

} // namespace tmx
