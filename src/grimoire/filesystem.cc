#include "filesystem.h"
#include <cassert>
#include <sstream>
#include <fstream>

#include "physfs_filesystem.h"
#include "ioutils.h"
#include "pathutils.h"

#include "grimoire/grimoire_util.h"
#include "grimoire/log.h"
#include "grimoire/stringlib.h"

// os-independent filesystem/path operations

using std::ofstream;

namespace io 
{




void mount(const String& path) {
	FileSystem::mount(path);
}

namespace 
{
    FileSystem *_fs_object = nullptr;
}


constexpr const char* TAG = "FileSystem";

FileSystem &FileSystem::get()
{
    if ( _fs_object == nullptr)
        _fs_object = new PhysFSFileSystem(io::exe_dir());

    return *_fs_object;
}

bool FileSystem::mount(const String &path, const String &mountpt)
{
    bool res = get().add_path(path, mountpt);
    if(mountpt.empty()) {
        Log::d(TAG, sl::format("mounting directory: %s", path));
    }
    else {
        Log::d(TAG, sl::format("mounting directory: %s @ mountpt: %s", path, mountpt));
    }

    auto search = get().search_path();
    return res;
}

FileHandle open(const String &path, AccessMode mode)
{
    return FileSystem::get().open_file(path, mode);
}

FileHandle mkfile(const String &path)
{
    return FileSystem::get().create_file(path);
}

FileHandle mkdir(const String &path)
{
    return FileSystem::get().create_dir(path);
}

bool exists(const String &path)
{
    return FileSystem::get().exists(path);
}

bool isfile(const String &path)
{
    return FileSystem::get().isfile(path);
}

bool isdir(const String &path)
{
    return FileSystem::get().isdir(path);
}

String dirname(const String &path)
{
    return io::path::prefix(path);
}




const size_t File::ALL = -1;

const char *AccessModeStr(AccessMode mode)
{
    switch (mode)
    {
        case ACCESS_CLOSED: return "CLOSED";
        case ACCESS_READ: return "READ";
        case ACCESS_WRITE: return "WRITE";
        case ACCESS_READ_BINARY: return "READ_BINARY";
        case ACCESS_WRITE_BINARY: return "WRITE_BINARY";
    }

    return "UNKNOWN";
}

File::File(const String &path, AccessMode mode): 
    _mode(mode),
    _path(path),
    _readpos(0)
{

}

File::~File() 
{
    if (isopen()) 
        close();
}	

bool File::exists() const
{
	//return io::path::exists(_path);
	return io::exists( _path );
}

bool File::isopen() const
{
	return _mode != ACCESS_CLOSED;
}

bool File::isfile() const
{
	return FileSystem::get().exists(_path);
}
bool File::isdir()  const
{
	return FileSystem::get().isdir(_path);
}

///////////////////////////////////////////////////////////////////
// get the asset's filename 
String File::name() const
{
    return io::path::filename(_path);
}

///////////////////////////////////////////////////////////////////
// get the file's extension
String File::ext() const 
{
    return io::path::ext(_path);
}

size_t File::size() const
{
	assert (exists() && isfile());
	return as_string().size();
}
void File::close()
{
	_mode = ACCESS_CLOSED;
}
///////////////////////////////////////////////////////////////////
// Read given number of bytes into a new string object, starting 
// at current readpos
//
// @param bytes: number of bytes to read ALL/-1 for default
// @return new String with the requested data
String File::as_string(size_t bytes) const
{
	assert( exists() && isfile());

	bytes = std::min(bytes, size());
	String s;
	s.resize(bytes);
	std::ifstream filestream(_path);

	for (size_t i=0; i<bytes && filestream.good(); i++)
	{
		s[i] = filestream.get();
	}

	filestream.close();

	return s;
}

///////////////////////////////////////////////////////////////////
// Read given number of bytes into dst, starting at current
// readpos
//
// @param dst: pointer to writable memory
// @param bytes: number of bytes to read ALL/-1 for default
// @return number of bytes actually read 
size_t File::read(void *dst, size_t bytes ) const
{
	return 0;
}

void File::write(void *data, size_t bytes)
{
	assert (isopen() && _mode == ACCESS_WRITE);

	ofstream stream (_path);

	char* cdata = static_cast<char *>(data);
	for(size_t i=0; i<bytes; ++i)
	{
		stream << cdata[i];
	}

	stream.flush();
}

void File::writestr(const String &data)
{
	assert (isopen() && _mode == ACCESS_WRITE);

	ofstream stream (_path);

	for(size_t i=0; i<data.size(); ++i)
	{
		stream << data[i];
	}

	stream.flush();
}

} // namespace io
