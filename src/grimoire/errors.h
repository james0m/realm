#ifndef __errors_h_
#define __errors_h_

#include <exception>
#include <stdio.h>

namespace ecs
{
    
class EntityError : public std::exception
{
public:
    virtual const char* what() const throw()
    {
        return message.c_str();
    }
    
protected:
    EntityError(String message)
        :message (message)
    {}

    String message;
};
    
class EntityDoesNotExist : public EntityError
{
public:
    EntityDoesNotExist():
        EntityError("EntityDoesNotExist")
    {}
};

class ComponentNotRegistered : public EntityError
{
public:
    ComponentNotRegistered():
        EntityError("ComponentNotRegistered")
    {}
};

class ComponentAlreadyRegistered : public EntityError
{
public:
    ComponentAlreadyRegistered ():
        EntityError("ComponentAlreadyRegistered")
    {
    }
};
    
class NotImplementedError : public std::exception
{
public:
    NotImplementedError(const String scope, const String method)
    {
        char *buf = new char[scope.size() + method.size() + 3];
        sprintf(buf, "%s: %s", scope.c_str(), method.c_str());
        message.assign(buf, strlen(buf));
    }
    
    NotImplementedError(const String &message):
        message(message)
    {}

    const char *what() const throw() {
        return message.data();
    }
    String message;
};
    
class EntityLimitReached : public EntityError
{
public:
    EntityLimitReached():
        EntityError("Entity limit reached")
    {}
};

class InvalidArgument : public EntityError {
public:
    InvalidArgument(const String &message):
        EntityError(message)
    {}
};

}// Realm
#endif
