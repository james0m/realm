#include "stringlib.h"

#include <cassert>
#include <sstream>
#include <algorithm>

using namespace std;

namespace sl {


//////////////////////////////////////////////////////////////////////////
// Manipulate an entire string at once, instead of char by char
void transform_string(sl::string &s, stringfilter f)
{
    assert(!s.empty());

    char *buf = new char[s.size()];
    f(buf, s.c_str());
    s = buf;
    delete[] buf;
}


/**
 @TODO: improve this so that it 'eats' consecutive instances of delim
        e.g. when delim is whitespace and there are several spaces 
             between two actual worlds
*/       
template <class OutputIterator>
int split(const sl::string &s, char delim, OutputIterator output) {
    istringstream sstream(s);
	sl::string token;

    int n = 0;
    while (std::getline(sstream, token, delim)) {
        *output++ = (token);
        ++n;
    }

    return n;
}

stringlist split(const sl::string& s, char delim) {
    // count the instances of the delimiter
    int n = 1;
    for(auto ch : s) {
        if (ch == delim)
            ++n;
    }

    auto result = stringlist(n);
    split(s, delim, result.begin());
    return result;
}

}
