#ifndef _grid_indexer_h_
#define _grid_indexer_h_

#include "grid.h"
#include "common.h"
#include "log.h"
#include "motion.hpp"


/**
 * Given a region, a recursion depth, and a 2d point, find the smallest
 * rectangular subregion that contains the point.
 *
 * Impl'd so that qt.stab(x, y, 0) == qt.region
 */
rectf stab(rectf region, short qdepth, float x, float y);


struct Index_Cell
{
    idtype          index;
    rectf           region;
    ecs::EntityList entities;

    Index_Cell()
    {
        entities.reserve(20);
    }

    Index_Cell(idtype id, rectf r):
        index(id),
        region(r)
    {
        entities.reserve(20);
    }
};


using Grid_Base = util::grid<Index_Cell>;
struct GridIndexer : Grid_Base
{
    using value_type = typename Grid_Base::value_type;
    using QuadList = Array< Index_Cell* >;

    GridIndexer(int depth=3)
    {
        this->depth = depth;
    }

    GridIndexer(int depth, rectf region):
        Grid_Base(pow(2, depth), pow(2, depth))
    {
        this->region = region;
        this->depth = depth;
        init();
    }

    void init(size_t bucket_init_size=10)
    {
        ASSERT(depth && region.w > 1 && region.h > 1);

        int d = depth;
        rectf r = region;
        while(d--)
        {
            r = topleft(r);
        }

        columns = pow(2, depth);
        cell_width = r.w;
        cell_height = r.h;

        for(auto i=0u; i<this->size(); ++i)
        {
            auto& entry = this->at(i);
            entry.index = i;

            auto gy = i / columns;
            auto gx = i - gy * columns;

            entry.region = rectf (
                float(gx) * cell_width,
                float(gy) * cell_width,
                float(cell_width),
                float(cell_height)
            );
        }
    }

    vec2i  grid_cell_of(vec2f point) const
    {
        return vec2i( int(floor(point.x)) / cell_width, int(floor(point.y)) / cell_height);
    }
    idtype index_of(int x, int y) const
    {
        return x + y * columns;
    }    

    value_type& at_point(vec2f point)
    {
        auto index_x = int(floor(point.x)) / cell_width;
        auto index_y = int (floor(point.y)) / cell_height;

        return this->cell(index_x, index_y);
    }

    const value_type& at_point(vec2f point) const
    {
        auto index_x = int(floor(point.x)) / cell_width;
        auto index_y = int(floor(point.y)) / cell_height;

        return this->cell(index_x, index_y);
    }

    const value_type& at_index(u32 index) const
    {
        return this->at(index);
    }

    value_type& at_index(u32 index)
    {
        return this->at(index);
    }

    void insert(vec2f point, idtype value)
    {
        auto idx = index_of(point.x, point.y);

        this->at(idx).entities.push_back(value);
    }

    void insert_at_index(idtype index, idtype value)
    {
#ifdef DEBUG
        auto& storage = this->at(index);
        auto itr = search(storage.entities, value);

        if (itr == storage.entities.end())
            this->at(index).entities.push_back(value);
        else
            Log::e("SpatialIndex", "attempt to double add an entity into a quad");
#else
        this->at(index).entities.push_back(value);
#endif
    }

    void erase_at_index(idtype index, idtype value)
    {
        auto& storage = this->at(index).entities;
        auto itr = std::find(storage.begin(), storage.end(),value);
        if (itr != storage.end())
        {
            pop_erase(storage, itr);
        }
    }


    void neighbors_of( vec2f loc, QuadList& out) const
    {
        const auto& located = at_point(loc);
        const auto rect = located.region;
        const auto cell_size = vec2i(cell_width, cell_height);

        const rectf DIRECTIONS[8] = {
            rect.translated(cell_size * vec2i(-1, -1)),
            rect.translated(cell_size * vec2i( 0, -1)),
            rect.translated(cell_size * vec2i( 1, -1)),
            rect.translated(cell_size * vec2i( 1,  0)),
            rect.translated(cell_size * vec2i( 1,  1)),
            rect.translated(cell_size * vec2i( 0,  1)),
            rect.translated(cell_size * vec2i(-1,  1)),
            rect.translated(cell_size * vec2i(-1,  0))
        };

        for(auto dir : DIRECTIONS)
        {
            if(dir.x >= 0 && dir.x < region.right() &&
               dir.y >= 0 && dir.y < region.bottom())
            {
                auto cell_loc = grid_cell_of(dir.center());

                auto& cell = this->at_index(index_of(cell_loc.x, cell_loc.y));

                out.push_back( const_cast<Index_Cell*>(&cell));
            }
        }

    }
    QuadList neighbors_of( vec2f cell)
    {
        QuadList results;
        neighbors_of(cell, results);
        return results;
    }


    void populated(QuadList& results)
    {
        for(auto& cell : *this)
        {
            if (cell.entities.size() > 0)
                results.push_back(&cell);
        }
    }

    QuadList populated()
    {
        QuadList results;
        populated(results);
        return results;
    }
    
    void within_radius( vec2f point, float radius, QuadList& results) const
    {
        // convert the point and radius to grid space
        auto center_cell = grid_cell_of(point);

        int grid_radius_x = radius / cell_width;
        int grid_radius_y = radius / cell_height;

        // get a grid-space bounding box that contains the circle
        rect<int> grid_bbox (
            std::max(center_cell.x - grid_radius_x, 0),
            std::max(center_cell.y - grid_radius_y, 0),
            2*grid_radius_x,
            2*grid_radius_y
        );
        
        for(auto gy=grid_bbox.y, gy2 = std::min(grid_bbox.bottom(),  grid_height() - 1);
            gy < gy2; ++gy)
        {
            for(auto gx=grid_bbox.x, gx2 = std::min(grid_bbox.right(), grid_width() - 1);
                gx < gx2; ++gx)
            {
                auto index = index_of(gx, gy);

                auto& cell = this->at_index(index);

                // only consider populated quads that intersect the circle
                if (cell.entities.size() &&
                    rect_intersects_circle(cell.region, point, radius))
                {
                    results.push_back(const_cast<Index_Cell*>(&cell));
                }
            }
        }
    }


    QuadList within_radius( vec2f point, float radius) const
    {
        QuadList results;
        within_radius(point, radius, results);
        return results;
    }

    bool empty() const
    {
        return this->size() < 1;
    }

    int grid_width() const
    {
        return region.w / cell_width;
    }

    int grid_height() const
    {
        return region.h / cell_height;
    }

    rectf region;

    int depth;
    int columns;
    int cell_width;
    int cell_height;
};

#endif

#ifdef GRID_INDEXER_IMPL

rectf stab(rectf region, short qdepth, float x, float y)
{
    if (region.collide(x,y)) {
        if (qdepth == 0)
            return region;

        int depth = 0;

        if (!region.collide(x, y))
            return rectf::null_rect();

        auto r = region;

        while(depth++ < qdepth) {

            if (topleft(r).collide(x,y)) {
                r = topleft(r);

            }

            else if (topright(r).collide(x,y)) {
                r = topright(r);

            }

            else if (bottomleft(r).collide(x,y)) {
                r = bottomleft(r);

            }

            else { //(bottomright(r).collide(x,y))
                r = bottomright(r);

            }
        }

        return r;
    }
    else
        return rectf::null_rect();
}

#undef GRID_INDEXER_IMPL
#endif
