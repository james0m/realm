#include "log.h"

#include <iostream>
#include "logger.h"

namespace Log {

util::Logger logger;

void log_msg(char lvl, const char*tag, const char* msg){
    logger.out(lvl, tag, msg);
}

void i(const char* tag, const char* msg){
    log_msg(LOG_LEVEL_INFO, tag, msg);
}

void i(const char* tag, const std::string& msg) {
    log_msg(LOG_LEVEL_INFO, tag, msg.c_str());
}


void w(const char* tag, const char* msg){
    log_msg(LOG_LEVEL_WARN, tag, msg);
}
void w(const char* tag, const std::string& msg){
    log_msg(LOG_LEVEL_WARN, tag, msg.c_str());
}

void d(const char* tag, const char* msg){
    log_msg(LOG_LEVEL_DEBUG, tag, msg);
}

void d(const char* tag, const std::string& msg){
    log_msg(LOG_LEVEL_DEBUG, tag, msg.c_str());
}

void e(const char* tag, const char* msg){
    log_msg(LOG_LEVEL_ERROR, tag, msg);
}
void e(const char* tag, const std::string& msg){
    log_msg(LOG_LEVEL_ERROR, tag, msg.c_str());
}

    
} // log
