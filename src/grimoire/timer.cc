#include "timer.h"
#include <SDL2/SDL.h>

timer::timer():
	_start(0),
	_finish(0),
	_current(0),
	_previous(0),
	_trigger_delta(0),
	_trigger_time(0),
    _active(false),
    _signal()
{
}
unsigned timer::start() {
	_active = true;
	_start = SDL_GetTicks();
	_previous = 0;
	_current = _start;
	return _start;
}
unsigned timer::stop() {
	_finish = SDL_GetTicks();
	_active = false;
	return _finish;
}
void timer::reset() {
	_start = 0;
	_finish = 0;
	_current = 0;
	_previous = 0;
}
unsigned timer::update() {
	if(!_active) return 0;

	_previous = _current;
	_current = SDL_GetTicks();
	_delta = _current - _previous;

	if(_trigger_delta && _delta >= _trigger_delta) {
		_signal.trigger();
		_delta = 0;
	} else if (_trigger_time && _current >= _trigger_time) {
		_signal.trigger();
		stop();
	}
	return _current;
}

void timer::listen(signal<void>::callback_type f) {
	_signal.connect(f);
}

void timer::trigger_delta(unsigned delta) {
	if(_trigger_time)
		_trigger_time = 0;

	_trigger_delta = delta;
}

void timer::trigger_time(unsigned time) {
	if(_trigger_delta) {
		_trigger_delta = 0;
	}
	_trigger_time = time;
}
