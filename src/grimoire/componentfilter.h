#ifndef _component_filter_h_
#define _component_filter_h_

#include "types.h"

namespace Realm {
///////////////////////////////////////////////////////////////////////////////
// ComponentFilter
//
// this is a combination of three bitmasks used for 'searching' through a 
// data provider for specific entity types.
//
// the required mask is like the AND search operator; any components masked here
// _must_ be available on an entity for the filter to pass.
// 
// the inclusion mask is like the OR search operator; this is like saying 
// "give me an entity with one of these components"
//
// the exclusion mask is the NOT operator. If any components masked here show up
// on an entity, the test will not pass
//
// if any mask is set to 0, that part of the test passes. So, a default filter
// with all three masks set to 0 will allow all entities to pass.

class ComponentFilter {
public:
	ComponentFilter(){}

	ComponentFilter& require(idtype component_id);
	ComponentFilter& include(idtype component_id);
	ComponentFilter& exclude(idtype component_id);

	ComponentFilter& requirements(std::initializer_list<idtype> components);
	ComponentFilter& inclusions(std::initializer_list<idtype> components);
	ComponentFilter& exclusions(std::initializer_list<idtype> components);
	
	bool           operator()(idtype entity_id) const;
	bool           test(u32 mask) const;

private:
	u32 required_mask = 0;  // these components must be present to pass
	u32 inclusion_mask = 0; // must have one of these to pass
	u32 exclusion_mask = 0; // none of these can be present 
};

} //namespace grimoire
#endif
