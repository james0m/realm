#include <string>
#include <iostream>
#include <iomanip>
#include <sstream>
#include "filesystem.h"
#include "sdlutils.h"
#include "stringlib.h"

using namespace std;
	
namespace sdl 
{

void init(u32 flags)
{
	if (SDL_Init(flags) != 0)
		throw SDLInitError(SDL_GetError());

//    IMG_Init(IMG_INIT_PNG | IMG_INIT_JPG);
}





void rect_copy(const SDL_Rect &src, SDL_Rect &tgt) {
	tgt.x = src.x;
	tgt.y = src.y;
	tgt.w = src.w;
	tgt.h = src.h;
}

void rect_set(SDL_Rect &r, Sint16 x, Sint16 y, Uint16 w, Uint16 h) {
	r.x = x;
	r.y = y;
	r.w = w;
	r.h = h;
}

void rect_set_position(SDL_Rect &r, Sint16 x, Sint16 y) {
	r.x = x; r.y = y;
}
void rect_set_size(SDL_Rect &r, Uint16 w, Uint16 h) {
	r.w = w; r.h = h;
}
Sint16 rect_left(const SDL_Rect &r) { 
	return r.x; 
}
Sint16 rect_right(const SDL_Rect &r) { 
	return r.x + static_cast<Sint16>(r.w); 
}
Sint16 rect_top(const SDL_Rect &r) { 
	return r.y; 
}
Sint16 rect_bottom(const SDL_Rect &r) { 
	return r.y + static_cast<Sint16>(r.h); 
}
bool rect_collide(const SDL_Rect &r, Sint16 x, Sint16 y) {
	return (x >= r.x && x <= rect_right(r) && y >= r.y && y <= rect_bottom(r));
}
bool rect_collide(const SDL_Rect &r1, const SDL_Rect &r2) {
	return false;
}
void rect_set_null(SDL_Rect &r) {
	r.h = r.w = r.y = r.x = 0;
}
bool rect_is_null(const SDL_Rect &r) {
	return (r.x == r.y) && (r.y == r.h) && (r.h == r.w) && (r.w == 0);
}

sdl::window_ptr	window_create (const char *title, 
					               int width, 
					               int height,
                                   u32 flags)
{
    return window_create(title, width, height, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, flags);
}

sdl::window_ptr window_create(const char *title, int width, int height, int posx, int posy, u32 flags) {
	SDL_Window *win = SDL_CreateWindow(title, posx, posy, width, height, flags);
	if ( !win )
		throw SDLResourceError(String("sdl_window_create() failed: ") + SDL_GetError());

	return sdl::window_ptr {win};
}
sdl::renderer_ptr renderer_create(SDL_Window* win, int driveridx, u32 flags) {
	SDL_Renderer *ren = SDL_CreateRenderer(win, driveridx, flags);
	if(ren == NULL) {
		throw SDLResourceError(String("SDL_CreateRenderer failed: ") + SDL_GetError());
	}
	return sdl::renderer_ptr {ren};
}

void wait_for_quit(SDL_Keycode hotkey) {
	SDL_Event e;
	bool run = true;
	while(run) {
		while(SDL_PollEvent(&e)) {
			switch(e.type) {
				case SDL_QUIT: run = false; break;
				case SDL_KEYDOWN: {
					if (e.key.keysym.sym == hotkey) {
						run = false;
					}
					break;
				} 
			}
		}
	}
}

#define STB_IMAGE_IMPLEMENTATION
#include "3p/stb/stb_image.h"

sdl::surface_ptr image_load(const String &file) {

    auto data = io::open(file)->as_string();
	auto surface = image_load_from_memory(data);
	
	if(surface == nullptr) {
		throw SDLResourceError(
            String("sdl::image_load failed: ") + stbi_failure_reason());
	}

	return surface;
}

sdl::surface_ptr image_load_from_memory(const String& data)
{
    auto buf = Unique<unsigned char[]>((unsigned char*)strdup(data.c_str()));
    return image_load_from_memory(buf.get(), data.size());
}

sdl::surface_ptr image_load_from_memory(const unsigned char* data, size_t bytes)
{
    int w,h,n;
    unsigned char *raw = stbi_load_from_memory(data, bytes, &w, &h, &n, 0);

    if (!raw)
    {
        cout << "sdlutils::image_load_from_memory: totally blew it\n";
        auto reason = stbi_failure_reason();
        cout << "stb_image sez: " << reason << endl;
        return nullptr;
    }
    // TODO: handle other byte depths
    int bpp;
    if(n == 4)
        bpp = 32;
    else if (n == 3)
        assert(false && "unsupported depth: 24bpp");//bpp = 24;
    else if (n == 2)
        assert(false && "unsupported depth: 16bpp"); //bpp = 16;
    else if (n == 1)
        assert(false && "unsupported depth: 8bpp"); //bpp = 8;

    else {
        auto msg = sl::format("image_load_from_memory: Error: byte depth of %d given", n);
        throw SDLResourceError(msg);

    }

    // TODO: better checking into appropriate pixel format for
    // whatever system we are running on. This is hard coded
    // to ABGR for my linux dev machine, but this will likely
    // be wrong on other systems
    auto s = surface_create(w, h, bpp, 0x000000ff,
                                       0x0000ff00,
                                       0x00ff0000,
                                       0xff000000);

    std::copy(raw, raw + (w * h * n), (unsigned char*)s->pixels);
    stbi_image_free(raw);
    return s;
}

sdl::surface_ptr surface_create(int width, int height, int depth,  u32 rmask, u32 gmask, u32 bmask, u32 amask) {

	SDL_Surface *surface = SDL_CreateRGBSurface(0,
		width, 
		height,
		depth,
		rmask,
		gmask,
		bmask,
		amask
	);
	if(surface == NULL) { 
		throw SDLResourceError(String("sdl_image_load failed: ") + SDL_GetError());
	}
	
	return sdl::surface_ptr {surface};
}
sdl::texture_ptr texture_create(SDL_Renderer *ren, u32 format, int access, int w, int h) {
	SDL_Texture *texture = SDL_CreateTexture(ren, format, access, w, h);
	
	if(texture == NULL) { 
		throw SDLResourceError(String("sdl_texture_create failed: ") + SDL_GetError());
	}
	
	return sdl::texture_ptr {texture};
}
sdl::texture_ptr texture_from_surface(SDL_Renderer *ren, SDL_Surface *surface) {
	SDL_Texture *texture = SDL_CreateTextureFromSurface(ren, surface);

	if (texture == NULL) 
		throw SDLResourceError(String("sdl_texture_from_surface failed: ") + SDL_GetError());
	
	return sdl::texture_ptr {texture};
}



string eventtype_string(const SDL_Event &e) {
	switch(e.type) {
		case SDL_DOLLARGESTURE:		return string ("SDL_DOLLARYGESTURE"); break;
		case SDL_DROPFILE:			return string ("SDL_DROPFILE"); break;
		case SDL_FINGERMOTION:		return string ("SDL_FINGERMOTION"); break;
		case SDL_FINGERDOWN:		return string ("SDL_FINGERDOWN"); break;
		case SDL_FINGERUP:			return string ("SDL_FINGERUP"); break;
		case SDL_JOYAXISMOTION:		return string ("SDL_JOYAXISMOTION"); break;
		case SDL_JOYBALLMOTION:		return string ("SDL_JOYBALLMOTION"); break;
		case SDL_JOYHATMOTION:		return string ("SDL_JOYHATMOTION"); break;
		case SDL_JOYBUTTONDOWN:		return string ("SDL_JOYBUTTONDOWN"); break;
		case SDL_JOYBUTTONUP:		return string ("SDL_JOYBUTTONUP"); break;
		case SDL_KEYDOWN: 			return string ("SDL_KEYDOWN"); break;
		case SDL_KEYUP:				return string ("SDL_KEYUP"); break;
		case SDL_MOUSEMOTION: 		return string ("SDL_MOUSEMOTION"); break;
		case SDL_MOUSEBUTTONDOWN: 	return string ("SDL_MOUSEBUTTONDOWN"); break;
		case SDL_MOUSEBUTTONUP: 	return string ("SDL_MOUSEBUTTONUP"); break;
		case SDL_MOUSEWHEEL:		return string ("SDL_MOUSEWHEEL"); break;
		case SDL_MULTIGESTURE:		return string ("SDL_MULTIGESTURE"); break;
		case SDL_QUIT:				return string ("SDL_QUIT"); break;
		case SDL_SYSWMEVENT:		return string ("SDL_SYSWMEVENT"); break;
		case SDL_TEXTEDITING:		return string ("SDL_TEXTEDITING"); break;
		case SDL_TEXTINPUT:			return string ("SDL_TEXTINPUT"); break;
		case SDL_USEREVENT:			return string ("SDL_USEREVENT"); break;
		case SDL_WINDOWEVENT: {
			if(e.window.event == SDL_WINDOWEVENT_SHOWN) {
				return string("SDL_WINDOWEVENT_SHOWN"); 
			} else if (e.window.event == SDL_WINDOWEVENT_HIDDEN) {
				return string("SDL_WINDOWEVENT_HIDDEN");
			} else if (e.window.event == SDL_WINDOWEVENT_EXPOSED) {
				return string("SDL_WINDOWEVENT_EXPOSED");
			} else if (e.window.event == SDL_WINDOWEVENT_MOVED) {
				return string("SDL_WINDOWEVENT_MOVED");
			} else if (e.window.event == SDL_WINDOWEVENT_RESIZED) {
				return string("SDL_WINDOWEVENT_RESIZED");
			} else if (e.window.event == SDL_WINDOWEVENT_MINIMIZED) {
				return string("SDL_WINDOWEVENT_MINIMIZED");
			} else if (e.window.event == SDL_WINDOWEVENT_MAXIMIZED) {
				return string("SDL_WINDOWEVENT_MAXIMIZED");
			} else if (e.window.event == SDL_WINDOWEVENT_RESTORED) {
				return string("SDL_WINDOWEVENT_RESTORED");
			} else if (e.window.event == SDL_WINDOWEVENT_ENTER) {
				return string("SDL_WINDOWEVENT_ENTER");
			} else if (e.window.event == SDL_WINDOWEVENT_LEAVE) {
				return string("SDL_WINDOWEVENT_LEAVE");
			} else if (e.window.event == SDL_WINDOWEVENT_FOCUS_GAINED) {
				return string("SDL_WINDOWEVENT_FOCUS_GAINED"); 
			} else if (e.window.event == SDL_WINDOWEVENT_FOCUS_LOST) {
				return string("SDL_WINDOWEVENT_FOCUS_LOST");
			} else if (e.window.event == SDL_WINDOWEVENT_CLOSE) {
				return string("SDL_WINDOWEVENT_CLOSE");
			}
			else {
				printf("sdl_eventtype_string() - Error: unrecognized window event type\n");
				return string("");
			}
			break;
		}
		default: {
			printf("sdl_eventtype_string() - Error: unrecognized SDL event type\n");
			return string("");
		}
	}
}

string textureaccess_string(int access) {
	switch(access) {
		case SDL_TEXTUREACCESS_STATIC : return string("SDL_TEXTUREACCESS_STATIC");break;
		case SDL_TEXTUREACCESS_STREAMING : return string("SDL_TEXTUREACCESS_STREAMING");break;
		case SDL_TEXTUREACCESS_TARGET : return string("SDL_TEXTUREACCESS_TARGET");break;
		default:
			return string("Unknown TextureAccess type"); break;
	}
}
string pixelformat_string(Uint32 format) {
	switch(format) {
		case SDL_PIXELFORMAT_INDEX1LSB : return string("SDL_PIXELFORMAT_INDEX1LSB"); break;
		case SDL_PIXELFORMAT_INDEX1MSB : return string("SDL_PIXELFORMAT_INDEX1MSB"); break;
		case SDL_PIXELFORMAT_INDEX4LSB : return string("SDL_PIXELFORMAT_INDEX4LSB"); break;
		case SDL_PIXELFORMAT_INDEX4MSB : return string("SDL_PIXELFORMAT_INDEX4MSB"); break;
		case SDL_PIXELFORMAT_INDEX8 : return string("SDL_PIXELFORMAT_INDEX8"); break;
		case SDL_PIXELFORMAT_RGB332 : return string("SDL_PIXELFORMAT_RGB332"); break;
		case SDL_PIXELFORMAT_RGB444 : return string("SDL_PIXELFORMAT_RGB444"); break;
		case SDL_PIXELFORMAT_RGB555 : return string("SDL_PIXELFORMAT_RGB555"); break;
		case SDL_PIXELFORMAT_BGR555 : return string("SDL_PIXELFORMAT_BGR555"); break;
		case SDL_PIXELFORMAT_ARGB4444 : return string("SDL_PIXELFORMAT_ARGB4444"); break;
		case SDL_PIXELFORMAT_RGBA4444 : return string("SDL_PIXELFORMAT_RGBA4444"); break;
		case SDL_PIXELFORMAT_ABGR4444 : return string("SDL_PIXELFORMAT_ABGR4444"); break;
		case SDL_PIXELFORMAT_BGRA4444 : return string("SDL_PIXELFORMAT_BGRA4444"); break;
		case SDL_PIXELFORMAT_ARGB1555 : return string("SDL_PIXELFORMAT_ARGB1555"); break;
		case SDL_PIXELFORMAT_RGBA5551 : return string("SDL_PIXELFORMAT_RGBA5551"); break;
		case SDL_PIXELFORMAT_ABGR1555 : return string("SDL_PIXELFORMAT_ABGR1555"); break;
		case SDL_PIXELFORMAT_BGRA5551 : return string("SDL_PIXELFORMAT_BGRA5551"); break;
		case SDL_PIXELFORMAT_RGB565 : return string("SDL_PIXELFORMAT_RGB565"); break;
		case SDL_PIXELFORMAT_BGR565 : return string("SDL_PIXELFORMAT_BGR565"); break;
		case SDL_PIXELFORMAT_RGB24 : return string("SDL_PIXELFORMAT_RGB24"); break;
		case SDL_PIXELFORMAT_BGR24 : return string("SDL_PIXELFORMAT_BGR24"); break;
		case SDL_PIXELFORMAT_RGB888 : return string("SDL_PIXELFORMAT_RGB888"); break;
		case SDL_PIXELFORMAT_RGBX8888 : return string("SDL_PIXELFORMAT_RGBX8888"); break;
		case SDL_PIXELFORMAT_BGR888 : return string("SDL_PIXELFORMAT_BGR888"); break;
		case SDL_PIXELFORMAT_BGRX8888 : return string("SDL_PIXELFORMAT_BGRX8888"); break;
		case SDL_PIXELFORMAT_ARGB8888 : return string("SDL_PIXELFORMAT_ARGB8888"); break;
		case SDL_PIXELFORMAT_RGBA8888 : return string("SDL_PIXELFORMAT_RGBA8888"); break;
		case SDL_PIXELFORMAT_ABGR8888 : return string("SDL_PIXELFORMAT_ABGR8888"); break;
		case SDL_PIXELFORMAT_BGRA8888 : return string("SDL_PIXELFORMAT_BGRA8888"); break;
		case SDL_PIXELFORMAT_ARGB2101010 : return string("SDL_PIXELFORMAT_ARGB2101010"); break;
		case SDL_PIXELFORMAT_YV12 : return string("SDL_PIXELFORMAT_YV12"); break;
		case SDL_PIXELFORMAT_IYUV : return string("SDL_PIXELFORMAT_IYUV"); break;
		case SDL_PIXELFORMAT_YUY2 : return string("SDL_PIXELFORMAT_YUY2"); break;
		case SDL_PIXELFORMAT_UYVY : return string("SDL_PIXELFORMAT_UYVY"); break;
		case SDL_PIXELFORMAT_YVYU : return string("SDL_PIXELFORMAT_YVYU"); break;
		case SDL_PIXELFORMAT_UNKNOWN : 
		default:
			return string("SDL_PIXELFORMAT_UNKNOWN"); break;
	}
}


void texture_info(std::ostream &out, SDL_Texture *tex) {
	if(! tex) {
		out << "Texture info: \n...recieved null pointer" << endl << endl;
		return;
	}
	Uint32 fmt;
	int access,w,h;
	SDL_QueryTexture(tex, &fmt, &access, &w, &h);
	out 	<< "Texture info: 	" << endl
			<< "pixel format: 	" << pixelformat_string(fmt) << endl
			<< "access type: 	" << textureaccess_string(access) << endl
			<< "width: 			" << w << endl
			<< "height: 		" << h << endl
			<< endl;
}

std::string surface_info(SDL_Surface *surf) {
    std::stringstream out;
    
	if(! surf) {
		out << "Surface info: \n...recieved null pointer" << endl << endl;
		return out.str();
	}
	out	<< "Surface info:     " << endl
		<< "format:           " << pixelformat_string(surf->format->format) << endl
		<< "width:            " << surf->w << endl
		<< "height:           " << surf->h << endl
		<< "pitch:            " << surf->pitch << endl
		<< "bits/pixel:       " << static_cast<int>(surf->format->BitsPerPixel) << endl
		<< "bytes/pixel:      " << static_cast<int>(surf->format->BytesPerPixel) << endl
		<< "rmask:            0x" << std::hex << surf->format->Rmask << endl
		<< "gmask:            0x" << std::hex << surf->format->Gmask << endl
		<< "bmask:            0x" << std::hex << surf->format->Bmask << endl
		<< "amask:            0x" << std::hex << surf->format->Amask << endl

        << "rshift:           " << dec << +surf->format->Rshift << endl
        << "gshift:           " << +surf->format->Gshift << endl
        << "bshift:           " << +surf->format->Bshift << endl
        << "ashift:           " << +surf->format->Ashift << endl
        
		<< endl;
               
    return out.str();               
}
void renderer_info(std::ostream &out, SDL_Renderer *ren) {
	if(! ren) {
		out << "Renderer info: \n...recieved null pointer" << endl << endl;
		return;
	}
    SDL_RendererInfo *info = nullptr;
	SDL_GetRendererInfo(ren, info);

	out << "Renderer info: " << endl;
	if (ren == NULL) {
		out << "Renderer pointer is NULL";
		return;
	}
	if (info->flags & SDL_RENDERER_SOFTWARE) {
		out << "mode:	SDL_RENDERER_SOFTWARE"  << endl;
	} else if (info->flags & SDL_RENDERER_ACCELERATED) {
		out << "mode:	SDL_RENDERER_ACCELERATED" << endl;
	} else {
		out << "mode: 	RENDERER_MODE_UNKNOWN" << endl;
	}

	if (info->flags & SDL_RENDERER_PRESENTVSYNC) {
		out << "vsync:	yes" << endl;
	} else 
		out << "vsync:	no" << endl;

	if (info->flags & SDL_RENDERER_TARGETTEXTURE) 
		out << "texture targets: yes" << endl;
	else
		out << "texture targets: no" << endl;
}

int pixelformat_bits_per_pixel(Uint32 format) {
    return SDL_BITSPERPIXEL(format);
}

int pixelformat_bytes_per_pixel(Uint32 format) {
    return SDL_BYTESPERPIXEL(format);
}
    
int get_texture_pitch(SDL_Texture *texture)
{
    Uint32 format;
    int w;
    SDL_QueryTexture(texture, &format, 0, &w, 0);

    return w * SDL_BYTESPERPIXEL(format);
}



constexpr int FPS_60hz = 1000 / 60;

void limit60hz(int frame_time) {
    limit_framerate(frame_time, FPS_60hz);
}

void limit_framerate(int frame_time, int target_fps) {
    if(frame_time < target_fps) {
        SDL_Delay(target_fps - frame_time);
    }
}



void quit()
{    
    SDL_Quit();
}

} // sdl wrapper namespace
