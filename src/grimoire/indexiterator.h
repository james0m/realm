#ifndef _index_iterator_h_
#define _index_iterator_h_

#include <iterator>

/////////////////////////////////////////////////////////////////////
// index_iterator<T,C>
//
// An iterator that provides access to its container by indexing 
// rather than by direct pointer manipulation. 
//
// Host container type is expected to be vector-like, and must 
// provide valid operator[] and at() methods
template <typename T, class C> 
class index_iterator : public std::iterator<std::random_access_iterator_tag, T> {
public:

    typedef T  value_type;
    typedef T* pointer;
    typedef T& reference;
    typedef C  host;

    typedef index_iterator<T,C> iterator;

    index_iterator(C &host, unsigned idx=0):
        _host(&host),
        _index(idx) 
    {}
 
    index_iterator(const index_iterator &other) {
        *this = other;
    }

    index_iterator& operator=(const index_iterator &other) {
        if(this != &other) {
            _host = other._host;
            _index = other._index;
        }
        return *this;
    }

    size_t index() const { return _index; }
    bool operator==(const index_iterator<T,C> &other) {
        return _host == other._host && _index == other._index;
    }

    bool operator!=(const index_iterator<T,C> &other) {
        return !(operator==(other));
    }
    pointer operator->() { return &(_host->at(_index)); }
    reference operator*() { return _host->at(_index); }

    void operator ++() { ++_index; }
    iterator& operator ++(int) {iterator tmp = *this; ++_index; return tmp; }

    void operator --() { --_index; }
    iterator& operator--(int) {iterator tmp = *this; --_index; return tmp; }

    index_iterator<T,C> &operator+(unsigned i) {
        return index_iterator<T,C>(*_host, _index + i);
    }
    index_iterator<T,C> &operator+(const index_iterator<T,C> &other) {
        return index_iterator<T,C>(*_host, _index + other._index);
    }

    index_iterator<T,C> &operator-(unsigned i) {
        return index_iterator<T,C>(*_host, _index - i);
    }
    index_iterator<T,C> &operator-(const index_iterator<T,C> &other) {
        return index_iterator<T,C>(*_host, _index - other._index);
    }

private:
    host      *_host;
    unsigned   _index;
};

#endif
