#ifdef __linux__

#include <sys/types.h>
#include <sys/stat.h>

#include "pathutils.h"

using namespace std;

namespace io { 
namespace path {

const char sep = '/';

bool exists(const String &path) {
    struct stat info;
    return (stat(path.c_str(), &info) == 0);
}

bool is_file(const String &path) {
	struct stat info;
	stat(path.c_str(), &info);
	return S_ISREG(info.st_mode);
}

bool is_dir(const String &path) {
	struct stat info;
	stat(path.c_str(), &info);
	return S_ISDIR(info.st_mode);
}

bool is_link(const String &path) {
	struct stat info;
	lstat(path.c_str(), &info);
	return S_ISLNK(info.st_mode);
}

bool is_absolute(const String &path) {
	return path.size() && path[0] == io::path::sep;
}
bool is_relative(const String &path) {
	return path.size() && path[0] != io::path::sep;
}

} // namespace path
} // namespace io

#endif