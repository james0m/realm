#ifndef _grid_h_
#define _grid_h_

#include "grimoire_util.h"

#include "rect.h"
#include "indexiterator.h"



using std::initializer_list;

namespace util {

template <typename T>
struct grid_traits {

    typedef T cellvalue;
    typedef T value_type;
    typedef T* pointer;
    typedef T& reference;
    typedef const T* const_pointer;
    typedef const T& const_reference;

    typedef Array<T> cell_list;
    typedef typename Array<T>::iterator iterator;
    typedef typename Array<T>::iterator const_iterator;
};

/////////////////////////////////////////////////////////////////////
// grid
//
// A stdlib compatible container for storing data in a 2 dimensional
// format.
//
// Supports 2d slices called subregions, which can be accessed and
// iterated over just the same as any other container, and without
// needing to worry about index or xy offsets
template <typename T>
class grid {
public:
    typedef grid_traits<T>                   traits;

    typedef typename traits::value_type      value_type;
    typedef typename traits::cellvalue       cellvalue;
    typedef typename traits::pointer         pointer;
    typedef typename traits::const_pointer   const_pointer;
    typedef typename traits::reference       reference;
    typedef typename traits::const_reference const_reference;
    typedef typename traits::cell_list       cell_list;
    typedef typename traits::iterator        iterator;
    typedef typename traits::const_iterator  const_iterator;

    friend class region;

    class region {
    public:
        typedef index_iterator<T, region> iterator;
        typedef index_iterator<const T, region> const_iterator;

        region(grid<T> &g, const rect<int> &r):
            _grid(&g),
            _area(g.area().intersection(r))
        {
        }

        value_type& operator[](u32 idx) {
            return _grid->operator[](idx + _first_offset());
        }
        const value_type& operator[](u32 idx) const {
            return _grid->operator[](idx + _first_offset());
        }

        const value_type& at(u32 idx) const
        {
            u32 real_index = true_index(idx);
            return _grid->at(real_index);
        }

        value_type& at(u32 idx)
        {
            int iy = idx / _area.w,
                ix = idx - iy*_area.w;

            u32 true_index = (_area.x + ix) + (_area.y + iy)*_grid->width();

            return _grid->at(true_index);
        }
                
        const value_type& at(u32 x, u32 y) const {
            return _grid->at( (_area.x + x) + (_area.y + y)*_grid->width() );
        }

        value_type& at(u32 x, u32 y)
        {
            return _grid->at( (_area.x + x) + (_area.y + y)*_grid->width() );
        }
        
        iterator begin() {
            return iterator(*this);
        }
        iterator end() {
            return iterator(*this, size());
        }
        size_t size() {
            return _area.w * _area.h;
        }

        bool empty() const {
            return size() < 1;
        }        

        size_t true_index(size_t region_index) const
        {
            size_t iy = region_index / _area.w,
                   ix = region_index - iy*_area.w;

            return (_area.x + ix) + (_area.y + iy)*_grid->width();
        }           
        
    private:
        u32 _row_offset(u32 y) const { return y * _grid->width() + _area.x; }

        u32 _first_offset() const {
            return _area.x + _row_offset(_area.y);
        }

        u32 _last_offset() const {
            return (_area.right()-1) + _row_offset(_area.bottom()-1);
        }

        u32 _end_offset() const {
            return _area.right() + _row_offset(_area.bottom()-1);
        }

        // pointers to extremes of the grid data;
        pointer _true_data_start() { return &(_grid->_data[0]); }
        pointer _true_data_last() { return &(_grid->_data[_grid->_data.size()-1]); }
        pointer _true_data_end() { return _true_data_last() + 1; }

        grid<T> *_grid;
        rect<int>  _area;
    };

    grid(unsigned w=0, unsigned h=0, const cellvalue &nullval=T()) {
        _width = w;
        _height = h;
        _data.reserve(w * h);
        initialize(nullval);
    }

    grid(unsigned w, const initializer_list<value_type>& values)
    {
        assert( w < values.size());
        const auto N = values.size();
        assert ( N % w == 0);

        _width = w;
        _height = N / w;
        *this = values;
    }

    grid(const grid<T>& other):_width(0), _height(0), _data() {
        *this = other;
    }

    virtual ~grid() {}

    grid<T>& operator= (const grid<T> &other) {
        if(this != &other) {
            _width = other._width;
            _height = other._height;
            _data = other._data;
        }
        return *this;
    }

    grid<T>& operator= (const initializer_list<value_type>& values) {
        _data.resize(values.size());
        std::copy(values.begin(), values.end(), this->begin());
    }

    void                initialize(const T& val=0) { if(size() > 0) _data.assign(size(), val); }
    void                set_dimensions(unsigned w, unsigned h);
    cellvalue &         operator[](unsigned idx);
    const cellvalue &   operator[](unsigned idx) const;
    cellvalue &         at(unsigned idx) { return this->operator[](idx); }

    template <class Integer>
    cellvalue &         cell(Integer x, Integer y);

    template <class Integer>
    cellvalue &         cell(const vec2<Integer>& v);
    const cellvalue &   at(unsigned idx) const { return this->operator[](idx); }

    const cellvalue &   cell(unsigned x, unsigned y) const;

    
    rect<unsigned>      area()     const  { return rect<unsigned>(0,0,_width,_height); }

    
    unsigned            width()    const  { return _width; }
    unsigned            height()   const  { return _height; }
    unsigned            size()     const  { return _width * _height; }
    size_t              datasize() const  { return sizeof(cellvalue) * size(); }

    iterator            begin() { return _data.begin(); }
    iterator            end()   { return _data.end();   }

    region              subregion(const rect<int> &r) { return region(*this, r); }
    const region        subregion(const rect<int> &r) const { return region(*this, r); }
    
    Array<cellvalue>  neighbors(unsigned x, unsigned y) const {

        auto result = Array<cellvalue> {

            cell(x - 1, y - 1),
            cell(x,     y - 1),
            cell(x + 1, y - 1),

            cell(x - 1, y),
            cell(x + 1, y),

            cell(x - 1, y + 1),
            cell(x,     y + 1),
            cell(x + 1, y + 1)
        };

        return result;
    }

private:
    unsigned _width;
    unsigned _height;

    Array<cellvalue> _data;
};

template <typename T>
void grid<T>::set_dimensions(unsigned w, unsigned h) {
    assert(w * h > 0);
    _width = w;
    _height = h;
    _data.resize(_width * _height);
}
template <typename T>
const typename grid<T>::cellvalue &grid<T>::operator[](unsigned idx) const {
    assert(idx>=0 && idx<(_width*_height));
    return _data[idx];
}
template <typename T>
typename grid<T>::cellvalue &grid<T>::operator[](unsigned idx) {
    assert(idx<(_width*_height));
    return _data[idx];
}
template <typename T>
template <typename Integer>
typename grid<T>::cellvalue &grid<T>::cell(Integer x, Integer y) {
    unsigned idx = x + y*_width;
    assert(idx < _width*_height);
    return _data[idx];
}

template <typename T>
template <typename Integer>
typename grid<T>::cellvalue &grid<T>::cell(const vec2<Integer>& v) {
    return cell(v.x, v.y);
}

template <typename T>
const typename grid<T>::cellvalue &grid<T>::cell(unsigned x, unsigned y) const {
    unsigned idx = x + y*_width;
    assert(idx < _width*_height);
    return _data[idx];
}

}// namespace util

#endif
