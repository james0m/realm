#include "tiledmapelement.h"
#include "grimoire/filesystem.h"
#include "grimoire/tmxreader.h"

#include "scene.h"

using tmx::TiledMap;

namespace gfx {

TiledMapElement::TiledMapElement(Element *root, tmx::TiledMap* map):
    Element(root)
{
    _map = map;
    _cmd_buffer.resize(_map->size());
}

TiledMapElement::~TiledMapElement() {

}



using sprite_cmd_buffer = Array<texture_def>;
using command_generator = void (*) (tmx::TiledMap *, const Camera *, sprite_cmd_buffer &);

void flush_command_buffer(RenderApi &r, sprite_cmd_buffer &cmdbuf) {
    r.batch_textures(&cmdbuf[0], cmdbuf.size());
    cmdbuf.clear();
}

/**
 * drawing a TiledMap boils down to two steps:
 * 1. generate commands for drawing a bunch of textured quads
 * 2. flushing those commands to the graphics api
 *
 */
void draw_map_generic(RenderApi &r,
                      TiledMap *map,
                      const Camera *cam,
                      sprite_cmd_buffer &buf,
                      command_generator generator)
{
    generator(map, cam, buf);
    flush_command_buffer(r, buf);
}

/**
 * This is a more ideal algorithm that will use the visible region defined
 * by the Camera to only draw the tiles visible on the screen
 */
void generate_map_commands_fast(TiledMap *map,
                                const Camera *cam,
                                sprite_cmd_buffer &cmdbuf)
{

    // get a bunch of variables from the map
    auto tileset        = map->tileset(0);
    auto map_w          = map->width();
    auto map_h          = map->height();
    auto tile_w         = map->tile_width();
    auto tile_h         = map->tile_height();
    auto visible_region = static_cast<rect<int>>(cam->bounds());
    auto viewport       = cam->bounds();

    // find the region of tiles we want to draw
    // 1. convert the camera coordinates to tile coordinates
    rect<int> visible_tile_region = {
        visible_region.x / tile_w,
        visible_region.y / tile_h,
        visible_region.w / tile_w,
        visible_region.h / tile_h
    };

    // figure out how far inside of the tiles we are, for smooth scrolling
    auto fine_offset = vec2i {
            visible_region.x % tile_w,
            visible_region.y % tile_h
    };

    auto texture = tileset.atlas.get_texture();
    auto grid_tiles = visible_tile_region.intersection(rect<int>{0,0,map_w, map_h});

    for(auto layer : map->layers())
    {
        // 2. get a subregion pointing to the visible tiles
        auto region = layer.data.subregion(grid_tiles);

        int x = viewport.x,
            y = viewport.y;

        texture_def cmd;
        cmd.texture = texture;
        cmd.dst_region.w = static_cast<float>(tile_w);
        cmd.dst_region.h = static_cast<float>(tile_h);

        for(auto tile_id : region) {
            cmd.src_region   = static_cast<rectf>(tileset.atlas.region(tile_id - 1));
            cmd.dst_region.x = static_cast<float>(x - fine_offset.x);
            cmd.dst_region.y = static_cast<float>(y - fine_offset.y);

            cmdbuf.push_back(cmd);

            x += tile_w;
            if( x >= viewport.right()) {
                x = viewport.x;
                y += tile_h;
            }
        }
    }

}

/**
 * a simpler, but inefficient, algorithm that draws every tile in the map
 * every time it is called
 */
void generate_map_commands_slow(TiledMap *map, const Camera *cam, sprite_cmd_buffer &cmdbuf)
{
    // get a bunch of variables from the map
    auto tileset        = map->tileset(0);
    auto map_w          = map->width();
    auto map_h          = map->height();
    auto tile_w         = map->tile_width();
    auto tile_h         = map->tile_height();
    auto layers         = map->layers();
    auto texture        = tileset.atlas.get_texture();

    auto cam_bounds     = cam->bounds();
    auto map_pixel_w    = map_w * tile_w;
    auto map_pixel_h    = map_h * tile_h;

    rect<int> draw_coords = {
        int(-cam_bounds.x),
        int(-cam_bounds.y),
        map_pixel_w,
        map_pixel_h
    };

    int x, y;

    for (auto layer : layers) {

        texture_def cmd;

        cmd.texture = texture;
        cmd.dst_region.w = static_cast<float>(tile_w);
        cmd.dst_region.h = static_cast<float>(tile_h);

        x = draw_coords.x;
        y = draw_coords.y;

        for (auto tile_id : layer.data) {
            cmd.src_region = static_cast<rectf>(tileset.atlas.region(tile_id));
            cmd.dst_region.x = x;
            cmd.dst_region.y = y;

            cmdbuf.push_back(cmd);

            x += tile_w;

            if (x >= draw_coords.right()) {
                x = draw_coords.left();
                y += tile_h;
            }
        }
    }
}


void draw_map_grid(RenderApi &r, TiledMap* map, const Camera* cam)
{
	static Array<line_def> commands;
	
    auto cam_bounds = cam->bounds();

    auto start_x = -cam_bounds.x;
    auto start_y = -cam_bounds.y;
    auto end_x = start_x + map->pxwidth();
    auto end_y = start_y + map->pxheight();

    for(auto col=0; col < map->width(); col++) {
        line_def line;
        line.color = { 0.0f, 1.0f, 1.0f, 1.0f};
        int x = start_x + col * map->tile_width();

        line.start.x = x;
        line.start.y = start_y;

        line.end.x = x;
        line.end.y = end_y;

        commands.push_back(line);
    }

    for (auto row = 0; row < map->height(); row++) {
        line_def line;
        line.color = { 1.0f, 0.0f, 1.0f, 1.0f};
        int y = start_y + row * map->tile_height();

        line.start.x = start_x;
        line.start.y = y;
        line.end.x = end_x;
        line.end.y = y;

        commands.push_back(line);
    }
    r.batch_lines(&commands.front(), commands.size());
    commands.clear();
}

command_generator cmd = generate_map_commands_slow;
void TiledMapElement::draw(RenderApi &r)
{
    const auto* cam = scene()->camera();
    if (_draw_terrain)
        draw_map_generic(r, _map, cam, _cmd_buffer, cmd);

    if (_draw_grid) {
        draw_map_grid(r, _map, cam);
    }

    for(auto *el : elements()) {
        el->draw(r);
    }
}


} // gfx
