#ifndef _grimoire_math_hpp_
#define _grimoire_math_hpp_

#include <cmath>

#include "rect.h"
#include "vec2.h"

// surprisingly useful 
template <class T>
inline T square(T val) {
    return val*val;
}

template <typename T>
struct Constants {
    static constexpr T PI = 3.1415927;
    static constexpr T RADS_PER_DEG = PI / 180;
};

template <typename T>
struct Funcs {
    using fp_func = double(*)(double);
    static constexpr fp_func Sqrt = sqrt;
    static constexpr fp_func Sin = sin;
    static constexpr fp_func Cos = cos;
    static constexpr fp_func Tan = tan;
};

template <>
struct Funcs<float> {
    using fp_func = float(*)(float);
    static constexpr fp_func Sqrt = sqrtf;
    static constexpr fp_func Cos = cosf;
    static constexpr fp_func Sin = sinf;
    static constexpr fp_func Tan = tanf;
};



template <typename T>
inline T distance(T ax, T ay, T bx, T by)
{
    return Funcs<T>::Sqrt( square(bx - ax) + square(by - ay));
}

// 2D distance functions
template <typename T>
inline T distance(const vec2<T> &a, const vec2<T> &b)
{
    return distance(a.x, a.y,  b.x, b.y);
}

template <typename T>
inline T manhattan_distance(T ax, T ay, T bx, T by) {
	return std::abs(ax - bx) + std::abs(ay - by);
}

template <typename T>
inline T manhattan_distance(vec2<T> a, vec2<T> b) {
    return manhattan_distance(a.x, a.y, b.x, b.y);
}



template <class T, class U, class V>
inline bool nearby(T ax, T ay, U bx, U by, V radius) {
    bool x_test = ((bx - radius <= ax) && (ax <= bx + radius));
    bool y_test = ((by - radius <= ay) && (ay <= by + radius));
    return x_test && y_test;
}

/** 
 * test if the two points (repr'd as vectors) are within
 * (radius) units of each other.
 */
template <class T, class U, class V>
inline bool nearby(vec2<T> a, vec2<U> b, V radius) {
    return nearby(a.x, a.y, b.x, b.y, radius);
}


/**
 * get the magnitude of vec2 v
 */
template <class T>
inline T magnitude(vec2<T> v) {
    return Funcs<T>::Sqrt( square(v.x) + square(v.y));
}

/**
 * convert the vector down to unit scale
 */
template <class T>
vec2<T> normalize(vec2<T> v) {
    const auto mag = magnitude(v);
    return vec2<T> {
        v.x / mag,
        v.y / mag
    };
}

template <class T>
vec2<T> norm(vec2<T> v) {
    const auto mag = magnitude(v);
    return vec2<T> {
        v.x / mag,
        v.y / mag
    };
}
/**
 * returns the dot product of the two vectors
 */
template <class T, class U>
T dot(vec2<T> a, vec2<U> b) {
    return a.x * b.x + a.y * b.y;
}

    
// scale vector 1 using vector 2

template <typename T, typename U>
vec2<T> scale(const vec2<T>& v, const U& scalar) {
    return vec2<T>{ v.x * scalar, v.y * scalar};
}

template <typename T, typename U>
vec2<T> scale(const vec2<T>& v1, const vec2<U> v2) {
    return vec2<T>{ v1.x * v2.x, v1.y * v2.y };
}

template <class T, class U>
vec2<T> project(vec2<T> pos, vec2<U> vel, int cycles=1) {
    return pos + (vel * cycles);
}

template <class T>
inline T deg_to_rad(T deg) {
    return deg * Constants<T>::RADS_PER_DEG;
}

template <class T>
inline T rad_to_deg(T radians) {
    return radians / Constants<T>::RADS_PER_DEG;
}


template <class T>
vec2<T> rotate(vec2<T> v, T theta) {
    auto sn = Funcs<T>::Sin(theta);
    auto cs = Funcs<T>::Cos(theta);

    auto result = vec2<T>(
        v.x * cs - v.y * sn,
        v.x * sn + v.y * cs
    );
    
    return result;
}

/* compute the determinant of the 2d matrix comprised of vectors a and b */
template <class T>
T det(vec2<T> a, vec2<T> b) {
    return a.x * b.y - a.y * b.x;
}

template <class T>
bool is_number(const T& x)
{
    return x == x; // NAN will not be equal to itself
}

template <class T>
inline
bool circles_intersect(vec2<T> c1, T r1, vec2<T> c2, T r2)
{
    return distance(c1, c2) < r1 + r2;
}

template <class T>
bool point_in_circle(vec2<T> center, T radius, vec2<T> pt)
{
    return distance(center, pt) < radius;
}

template <class T>
bool vertical_line_intersects_circle(vec2<T> p1, vec2<T> p2, vec2<T> c, T r)
{
    if (p1.y <= c.y && c.y <= p2.y)
    {
        if ( std::abs(p1.x - c.x) < r)
        {
            return true;
        }
    }

    if( (square(p1.y - c.y) + square(p1.x - c.x) < r*r)  ||
         (square(p2.y - c.y) + square(p2.x - c.x) < r*r))
    {
        return true;
    }

    return false;
}

template <class T>
bool horizontal_line_intersects_circle(vec2<T> p1, vec2<T> p2, vec2<T> c, T r)
{
    if (p1.x <= c.x && c.x <= p2.x)
    {
        if (std::abs(p1.y - c.y) < r)
        {
            return true;
        }
    }

    if( (square(p1.x - c.x) + square(p1.y - c.y) < r*r)  ||
        (square(p2.x - c.x) + square(p2.y - c.y) < r*r))
    {
        return true;
    }

    return false;
}


template <class T>
bool rect_intersects_circle(rect<T> r, vec2<T> cc, T cr)
{
    // upper horizontal
    if (horizontal_line_intersects_circle(r.topleft(), r.topright(), cc, cr))
    {
        return true;
    }

    // lower horizontal
    else if (horizontal_line_intersects_circle(r.bottomleft(), r.bottomright(), cc, cr))
    {
        return true;
    }

    // left vert
    else if (vertical_line_intersects_circle(r.topleft(), r.bottomleft(), cc, cr))
    {
        return true;
    }

    // right vert
    else if (vertical_line_intersects_circle(r.topright(), r.bottomright(), cc, cr))
    {
        return true;
    }

    return false;
}

template <class T>
rect<T> rect_from_circle(vec2<T> center, T radius)
{
    return rect<T>(center - vec2f{radius, radius}, vec2f{radius*2, radius*2});
}

#endif //_grimoire_math_hpp_
