#ifndef _quadtree_hpp_
#define _quadtree_hpp_

#include "grimoire/grimoire_util.h"
#include "grimoire/grimoire_math.hpp"
#include "grimoire/range.hpp"
#include "rect.h"




/**
 * Given a region, a recursion depth, and a 2d point, find the smallest
 * rectangular subregion that contains the point.
 *
 * Impl'd so that qt.stab(x, y, 0) == qt.region
 */
rectf stab(rectf region, short qdepth, float x, float y);


/**
 * find the smallest quadtree region that contains the subject
 *
 * @param area - the total area being partitioned
 * @param qdepth - number of subdivisions to make
 * @param region - the region to be contained
 *
 * @return the subquadrant that contains the area, if
 *         it exists. nullrect otherwise
 */
std::pair<quad_id,rectf> clamp(rectf area, short qdepth, rectf r);


/**
 * get a list of all adjacent quadrants
 */
//std::vector<quad_id> neighbors(const quad_id& qid);
//void neighbors(const quad_id& qid, std::vector<quad_id>& results);


/**
 * calculate the number of nodes required at a given depth
 *
 * @TODO: can this be made to be constexpr?
 */
int nodes_per_quad(int depth);
int nodes_per_tree(int depth);


#endif //_quadtree_hpp_

#ifdef QUADTREE_IMPLEMENTATION


inline bool is_top(char id) {
    return id == A || id == B;
}

inline bool is_bottom(char id) {
    return id == C || id == D;
}

inline bool is_left(char id) {
    return id == A || id == C;
}

inline bool is_right(char id) {
    return id == B || id == D;
}


std::pair<quad_id, rectf> stab(rectf region, short qdepth, float x, float y)
{
    if (region.collide(x,y)) {
        if (qdepth == 0)
            return make_pair(quad_id{}, region);

        int depth = 0;

        if (!region.collide(x, y))
            return make_pair(quad_id{}, rectf::null_rect());

        auto r = region;
        auto qid = quad_id(size_t(qdepth));

        while(depth++ < qdepth) {

            if (topleft(r).collide(x,y)) {
                r = topleft(r);
                qid[qdepth - depth] = A;
            }

            else if (topright(r).collide(x,y)) {
                r = topright(r);
                qid[qdepth - depth] = B;
            }

            else if (bottomleft(r).collide(x,y)) {
                r = bottomleft(r);
                qid[qdepth - depth] = C;
            }

            else { //(bottomright(r).collide(x,y))
                r = bottomright(r);
                qid[qdepth - depth] = D;
            }
        }

        return make_pair(qid, r);
    }
    else
        return make_pair(quad_id{}, rectf::null_rect());
}

// find the smallest quadtree region that contains the subject
std::pair<quad_id, rectf> clamp(rectf region, short qdepth, rectf r) {
    auto qid  = quad_id(qdepth);
    auto quad = rectf();

    if (region.collide(r))
    {
        int depth = 0;
        quad = region;


        while(depth++ < qdepth) {
            if (topleft(quad).contains(r)) {
                quad = topleft(quad);
                qid[qdepth - depth] = A;
            }
            else if (topright(quad).contains(r)) {
                quad = topright(quad);
                qid[qdepth - depth] = B;
            }
            else if (bottomleft(quad).contains(r)) {
                quad = bottomleft(quad);
                qid[qdepth - depth] = C;
            }
            else if (bottomright(quad).contains(r)) {
                quad = bottomright(quad);
                qid[qdepth - depth] = D;
            }
            else
                break;
        }
    }
    return make_pair(qid, quad);
}

int _nodes_per_quad(int depth) {
    if (depth < 1)
        return 1;

    auto result = 0;
    for(auto i=0u; i<depth+1; ++i)
        result += pow(4, i);

    return result;
}

int nodes_per_quad(int depth) {
    return depth < 1 ? 1 : _nodes_per_quad(depth);
}

int nodes_per_tree(int depth) {
    return depth < 1 ? 1 : _nodes_per_quad(depth);
}
#undef QUADTREE_IMPLEMENTATION
