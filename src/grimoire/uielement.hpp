#pragma once

#include "element.h"

class RenderApi;
using gfx::Element;

enum Alignment {
    NONE         = 0,
    TOP          = 1,
    LEFT         = 2,
    BOTTOM       = 4,
    RIGHT        = 8,
    CENTER       = 16,
    TOPLEFT      = TOP | LEFT,
    TOPRIGHT     = TOP | RIGHT,
    TOPCENTER    = TOP | CENTER,
    BOTTOMLEFT   = BOTTOM | LEFT,
    BOTTOMRIGHT  = BOTTOM | RIGHT,
    BOTTOMCENTER = BOTTOM | CENTER,
    LEFTCENTER   = LEFT | CENTER,
    RIGHTCENTER  = RIGHT | CENTER
};

struct UIElement : Element
{
    UIElement(Element* root=nullptr);
	virtual ~UIElement() override;

    virtual void   draw(RenderApi &r) override;

    virtual idtype type() const = 0;
    
    void           align(Alignment alignment);

	// for dealing with pointer interaction
    void           enter(int x, int y);
	void           update_pointer_interaction(int x, int y, int button_state);
    
    void           exit(int x, int y);
    
    bool           hovered() const;
    bool           active()  const;

private:
	bool _hovered = false;
    bool _active  = false;
};

/**
 * starting at the given element, search through the scene heirarchy to find the
 * deepest node that collides with the point (x,y).
 */
UIElement *stab_element(UIElement* elem, int x, int y);
