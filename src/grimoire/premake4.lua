
	project "grimoire"
		kind "StaticLib"
		language "C++"

		files { "*.h", "*.cc", "*.cpp"}	

		configuration "debug"
			defines { "DEBUG" }
			flags { "Symbols" }

		configuration "release"
			defines { "NDEBUG" }
			flags { "Optimize" }
