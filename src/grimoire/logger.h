#ifndef __logger_h_
#define __logger_h_

#include "grimoire/grimoire_util.h"


const char LOG_LEVEL_INFO = 'i';
const char LOG_LEVEL_WARN = 'w';
const char LOG_LEVEL_DEBUG = 'd';
const char LOG_LEVEL_ERROR = 'e';

namespace util {


/**
 * Simple logging class; 
 */
class Logger {
public:
    
    Logger(std::ostream &stream=std::clog);
    virtual ~Logger(){}

    /**
     * out
     *
     * writes a message out to the logger's output stream(s).
     *
     * @param lvl the logging level (i=info, w=warn, d=debug, e=error)
     * @param tag simple category of message, ususally a class/module name
     * @param msg the message to be written
     */
    virtual void out(char lvl, const char* tag, const char* msg);    

    /**
     * set the primary output stream for writing
     */
    //void set_output(std::ostream &stream);

    void add_output_stream(std::ostream& stream);
    void drop_output_stream(const std::ostream& stream);
    
    
private:

    std::stringstream         _ss;    
    Array<std::ostream*> _streams;
};

}
#endif
