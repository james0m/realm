#ifndef __log_h_
#define __log_h_

#include <string>

#include "grimoire/logger.h"

namespace Log
{
    void i(const char* tag, const char* msg);
    void i(const char* tag, const std::string& msg);
    
    void w(const char* tag, const char* msg);
    void w(const char* tag, const std::string& msg);
    
    void d(const char* tag, const char* msg);
    void d(const char* tag, const std::string& msg);
    
    void e(const char* tag, const char* msg);
    void e(const char* tag, const std::string& msg);

    extern util::Logger logger;
}


#endif
