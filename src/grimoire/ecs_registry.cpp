#include <cstring>

#include "common.h"
#include "ecs.hpp"
#include "errors.h"
#include "range.hpp"

namespace ecs
{

#define ASSERT_ENTITY_NONVOID(e) ( assert (e != NULL_ENTITY) )

ComponentMask type_mask(idtype component_type)
{
    return 1 << component_type;
}

void add_to_mask(ComponentMask &mask, idtype component)
{
	mask |= (1 << component);
}

void remove_from_mask(ComponentMask &mask, idtype component)
{
	mask &= ~(1 << component);
}

Unique<Registry> Registry::create(size_t max_entities)
{
    return util::make_unique<Registry>(max_entities);
}

Registry::Registry(size_t max_entities):
    _max_entities (max_entities),
    _component_storage(32),
    _entities(max_entities, 0)
{
    std::memset( &_entities[0], 0, sizeof(u32) * (size_t)_entity_count);
    for(auto id : range<idtype>(max_entities))
        _available_ids.insert(id);
}

Registry::~Registry()
{
    
}

idtype Registry::next_free_id()
{
    idtype next_id;
    
    if (_available_ids.size() > 0) {
        auto itr = _available_ids.begin();
        next_id = *itr;
        _available_ids.erase(itr);
    }

    else if (_entity_count < _max_entities) {
        next_id = _entity_count;
    }
    else
        throw EntityLimitReached();
    
    return next_id;
}

void Registry::recycle_id(idtype id)
{
    _available_ids.insert(id);
}

bool Registry::id_in_use(idtype id) const
{
    return _entity_count > 0 && _available_ids.find(id) == end(_available_ids);
}


#ifdef DEBUG
#define ASSERT_COMPONENTS_REGISTERED(c)(ASSERT ((this->_available_components & (c)) == (c)))
#define ASSERT_NOT_NULL(p) (assert ( (p) != nullptr && (p) != NULL))
#else
#define ASSERT_COMPONENTS_REGISTERED(c)
#define ASSERT_NOT_NULL(p)
#endif

void Registry::register_component(idtype component, ComponentProvider *provider)
{
    ASSERT_NOT_NULL (provider);
        
    if (! registered(component)) {
        _component_storage[component] = provider;
        add_to_mask(_available_components, component);
    }
    else {
        throw ComponentAlreadyRegistered();
    }
}
idtype Registry::create_entity(ComponentMask components)
{
    ASSERT_COMPONENTS_REGISTERED(components);
        
    if (_entity_count < _max_entities) {
        idtype eid = next_free_id();
        _entities[eid] |= components;
        ++_entity_count;
        entity_created(eid);
        return eid;
    }
    return NULL_ENTITY;
}

void Registry::destroy_entity(idtype e) {
	ASSERT_ENTITY_NONVOID(e);
	ASSERT_BOUNDS_CHECK(e, _max_entities);
    ASSERT( _available_ids.count(e) < 1);   // shouldn't be killing entities twice!

    detach_all(e, components_for(e));
    
	_entities[e] = VOID_ASPECT_SET;
    recycle_id(e);
    --_entity_count;

}

bool Registry::entity_exists(idtype e) const {
	ASSERT_ENTITY_NONVOID(e);
	ASSERT_BOUNDS_CHECK(e, _max_entities);
	
	return _entities[e] != VOID_ASPECT_SET;
}

bool Registry::registered(idtype component) const
{
    return _component_storage[component] != nullptr;
}

ComponentMask Registry::components_for(idtype e) const {
	ASSERT_ENTITY_NONVOID(e);
	ASSERT_BOUNDS_CHECK(e, _max_entities);
	
	return _entities[e]; 
}


bool Registry::has_component(idtype entity, idtype component) const {
    ASSERT_ENTITY_NONVOID(entity);
	ASSERT_BOUNDS_CHECK(entity, _max_entities);

	return _entities[entity] & type_mask(component) ? true : false;
}


RawComponent Registry::attach(idtype entity, idtype comp) {
	ASSERT_ENTITY_NONVOID(entity);
	ASSERT_BOUNDS_CHECK(entity, _max_entities);

    add_to_mask(_entities[entity], comp);
    add_to_mask(_available_components, comp);
    component_attached.trigger(entity, comp);
    
    auto c = component(entity, comp);
    return c;
}

ComponentMask Registry::attach_all(idtype entity, u32 components) {
	ASSERT_ENTITY_NONVOID(entity);
	ASSERT_BOUNDS_CHECK(entity, _max_entities);

    ComponentMask added_components = 0;

    u32 cmask;
    
	for(auto ctype_id=0; ctype_id<MAX_COMPONENTS; ++ctype_id) {
		cmask = type_mask(ctype_id);
		if( cmask & components ) {            
			_entities[entity] |= cmask;        
            added_components |= cmask;
			component_attached.trigger(entity, ctype_id);          
		}
	}
	return added_components;
}

ComponentMask Registry::detach(idtype entity, idtype component) {
	ASSERT_ENTITY_NONVOID(entity);
	ASSERT_BOUNDS_CHECK(entity, _max_entities);

    if (has_component(entity, component)) {
		remove_from_mask(_entities[entity], component);
		component_detached.trigger(entity, component);
	}

    return components_for(entity);
}

ComponentMask Registry::detach_all(idtype entity, ComponentMask components) {
	ASSERT_ENTITY_NONVOID(entity);
	ASSERT_BOUNDS_CHECK(entity, _max_entities);
	for(auto ctype_id=0; ctype_id<MAX_COMPONENTS; ++ctype_id) {
		u32 cmask = 1 << ctype_id;
		if( ( cmask & components )){
            detach(entity, ctype_id);
		}
	}

    return components_for(entity);
}

RawComponent Registry::component(idtype entity, idtype component)
{
    return _component_storage[component]->get(entity);
}

const RawComponent Registry::component(idtype entity, idtype component) const
{
    return _component_storage[component]->get(entity);
}

    
EntityList Registry::with_components(ComponentMask filter) const {
    auto result = EntityList();
    for(auto i=0u; i<_entities.size(); ++i) {
        if( (_entities[i] & filter) == filter)
            result.push_back(i); 
    }

    return result;
}

} // ecs
