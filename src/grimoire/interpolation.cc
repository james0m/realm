#include "interpolation.h"


float interpolate(LerpFunc lerp, float start, float end, float time) {
	return start + (end - start) * lerp(time);	
}

float linear (float time) {
	return time;
}

float quad_in(float time) {
	return time*time;
}

float quad_out(float time) {
	return time * (2 - time);
}

float quad_inout(float time) {
    auto t = time - 0.5f;
    return 2 - t * t;
}

float sine_in(float time) {
    return time;
}
float sine_out(float time) {
    return time;
}


float smoothstep(float time) {
    return time * time * ( 3 - time*2);
}
    
