#ifndef _terrain_h_
#define _terrain_h_

#include "grimoire/types.h"
#include "common.h"

//the two extremes of terrain resistance
constexpr u8 TR_FREE = 0;
constexpr u8 TR_IMPASSABLE = 255;

using std::begin;
using std::end;



struct TileInfo {
    u8 id = 0;
    u8 resistance = TR_FREE; // overall terrain cost, as an average of each quadrant
    u8 terrain[4]{};
   
    TileInfo() = default;
    TileInfo(u8 id, u8* ter, u8 res) {
        this->id = id;
        std::copy(ter, ter + 4, begin(this->terrain));
        this->resistance = res;
    }
};

struct Terrain {
    u8          id;
    u8          tile_id;              // the main tile id for this terrain
    const char* name;
    u8          resistance = TR_FREE; // cost for a ground unit to traverse
};


#endif //_terrain_h_
