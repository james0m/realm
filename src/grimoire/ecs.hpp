#ifndef _ecs_hpp_
#define _ecs_hpp_


#include "grimoire_util.h"
#include "typelib.hpp"
#include "sig.h"


namespace ecs {

constexpr ComponentMask VOID_ASPECT_SET = 0;
constexpr size_t MAX_COMPONENTS = 32;

using RawComponent = void*;
using EntityList = Array<idtype>;

constexpr idtype NULL_ENTITY = -1;

template <class C>
C* component_cast(RawComponent rawptr)
{
    return static_cast<C*>(rawptr);
}

class ComponentProvider
{
public:
    virtual const RawComponent get(idtype index) const = 0;
    virtual RawComponent get(idtype index)  = 0;
    
    virtual RawComponent create(idtype index) = 0;
    virtual void release(idtype index) = 0;

    virtual size_t size() const = 0;
    virtual size_t memory_usage() const = 0;

    virtual void deallocate() = 0;
};


struct EntityMoved
{
	EntityMoved(int x=0, int y=0):
		x(x), y(y)
	{}

	int x;
	int y;
};

struct EntityCreated
{
    EntityCreated(idtype id = -1):
        eid(id)
    {}
    idtype eid;
};

struct EntityDestroyed
{
    EntityDestroyed(idtype id = -1):
        eid(id)
    {}

    idtype eid;
};

struct ComponentAttached
{
    ComponentAttached(idtype e = -1, idtype c = -1):
        eid(e), cid(c)
    {}

    idtype eid;
    idtype cid;
};

struct ComponentDetached
{
    ComponentDetached(idtype e = -1, idtype c = -1):
        eid(e), cid(c)
    {}

    idtype eid;
    idtype cid;
};


/**
 * Registry
 *
 * The registry manages creation and access to entities and of their individual components
 */
class Registry
{
public:
    static Unique<Registry> create(size_t max_entities=10000);
    
    Registry(size_t max_entities=10000);
    Registry(const Registry&) = delete;
    
    virtual ~Registry();

public: // EntityManager
    
    idtype create_entity(ComponentMask components=0);
    void destroy_entity(idtype eid);
    
    bool entity_exists(idtype eid) const;
    bool has_component(idtype eid, idtype component) const;
    
    template <class C>
    bool has_component(idtype eid) const {
        return has_component(eid, tl::type_id<C>());
    }

    RawComponent attach(idtype eid, idtype component);
    ComponentMask attach_all(idtype eid, ComponentMask components);
    
    ComponentMask detach(idtype eid, idtype component);
    ComponentMask detach_all(idtype eid, ComponentMask components);

    EntityList with_components(ComponentMask filter) const;
      
    template <class C>
    inline C* attach(idtype entity) {
        return ecs::component_cast<C>(this->attach(entity, tl::type_id<C>()));
    }
    
    template <class C>
    inline C* component(idtype entity) {
        return ecs::component_cast<C>(this->component(entity, tl::type_id<C>()));
    }

    template <class C>
    inline const C* component(idtype entity) const {
        return ecs::component_cast<C>(this->component(entity, tl::type_id<C>()));
    }

    template <class C>
    inline C& get(idtype entity) {
        return *(component<C>(entity));
    }

    template <class C>
    inline const C& get(idtype entity) const {
        return *(component<C>(entity));
    }
    
    template <class C>
    inline void components_for(const Array<idtype>& entities,
                               Array<C*> &output)
    {
        for(auto e : entities) {
            assert( has_component(e, tl::type_id<C>() ));
            output.push_back(component<C>(e));
        }
    }
    size_t memory_usage() const {
        size_t result = sizeof(*this);

        for(auto itr : _component_storage) {
            if( itr == nullptr) // already reserved slots for ComponentPool ptrs,
                break;          // so break if we hit un-used storage
            result += itr->memory_usage();
        }
        return result;
    }

    inline size_t entity_count() const {
        return _entity_count;
    }

    inline size_t component_count() const {
        return _component_storage.size();
    }
    
public: //implementation

    using ComponentProviderMap = Array<ecs::ComponentProvider*>;

    template <class C>
    void register_component(ecs::ComponentProvider *source) {
        register_component(tl::type_id<C>(), source);
    }
    void register_component(idtype component, ecs::ComponentProvider *provider);
    bool registered(idtype component) const;

    RawComponent component(idtype entity, idtype comp);
    const RawComponent component(idtype entity, idtype comp) const;
    ComponentMask components_for(idtype e) const;

    bool id_in_use(idtype id) const;

public: // signals
    
	signal<EntityCreated>     entity_created;
	signal<EntityDestroyed>   entity_destroyed;
	signal<ComponentAttached> component_attached;
	signal<ComponentDetached> component_detached;

private:
    
    idtype next_free_id();

    void recycle_id(idtype id);
 
    u32                   _entity_count = 0;
    u32                   _max_entities = 0;
    ComponentMask         _available_components = 0;
    ComponentProviderMap  _component_storage;
    Array<u32>      _entities;
	std::set<idtype>      _available_ids;
};

} 
#endif



