#include <sstream>
#include <iostream>
#include <fstream>

#ifdef __linux__
#include <unistd.h>
#endif

#include "ioutils.h"


namespace io {

std::string
load_into_string(const String &file) {
	std::ifstream filestream(file);
	std::stringstream buf;
	buf << filestream.rdbuf();
	return buf.str();
}

std::string
exe_dir() {
// SEE http://stackoverflow.com/questions/143174/how-do-i-get-the-directory-that-a-program-is-running-from#answer-198099
#ifdef __linux__
    const ssize_t BUF_SIZE = FILENAME_MAX+1;
    char proc_symlink_path[BUF_SIZE] = {0};
	char exe_path[BUF_SIZE] = {0};

	snprintf(proc_symlink_path, BUF_SIZE, "/proc/%d/exe", getpid());
    const ssize_t bytes = readlink(proc_symlink_path, exe_path, BUF_SIZE);
    if (bytes <= 0) {
        fprintf(stderr, "CRITICAL - ioutils.exe_dir() - cannot read from symbolic link path %s", proc_symlink_path);
        exit(ENOENT);
    }

	auto ret = std::string{exe_path, static_cast<size_t>(bytes)};
	//strip off the filename...
    const size_t last_path_separator_index = ret.rfind(io::path::sep);
    if (last_path_separator_index == std::string::npos) {
        return ret;
    }
	return ret.substr(0, last_path_separator_index);
#elif defined _WIN32 || defined _WIN64
#error "[ioutils] exe_dir() - windows not supported"
#endif
}

std::string
working_dir() {
#ifdef __linux__
    char buf[FILENAME_MAX] = {0};
	const char *cwd = getcwd(buf, FILENAME_MAX);
	if(!cwd) {
		fprintf(stderr, "CRITICAL - io::working_dir(_linux): failed to get the current working directory.\n");
		exit(ENOENT);
	}
	return cwd;

#elif defined _WIN32 || defined _WIN64
#error "[ioutils] working_dir() - windows not supported"
#endif

}



} // io
