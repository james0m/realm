#ifndef _component_pool_h
#define _component_pool_h

#include <cassert>
#include "grimoire_util.h"

#include "ecs.hpp"

template <class C>
class ComponentPool : public ecs::ComponentProvider
{
public:

    //using iterator = typename std::vector<C>::iterator;
    using iterator = C*;
    using const_iterator = typename Array<C>::iterator;
    
    ComponentPool(size_t entities=10000)
    {
        _components.resize(entities);
    }

    const ecs::RawComponent get(idtype index) const override
    {
        return (ecs::RawComponent)(&_components[index]);
    }
    
    ecs::RawComponent get(idtype index) override
    {
        return (ecs::RawComponent)(&_components[index]);
    }

    C& operator[](idtype index) {
        return _components[index];
    }

    const C& operator[](idtype index) const {
        return _components[index];
    }
    
    ecs::RawComponent create(idtype index) override
    {
        return get(index);
    }

    void release(idtype index) override
    {

    }

    size_t size() const override {
        return _components.size();
    }

    size_t memory_usage() const override {
        return size() * sizeof(C);
    }

    void deallocate() override
    {
        //@TODO FIXME: this doesn't guarantee complete deallocation
        _components = Array<C>(0);
    }

    inline iterator begin() { return &_components[0]; }
    inline iterator end()   { return (&_components[0] + _components.size()); }
    
private:
    Array<C> _components;
};

template <class C>
struct PackedComponentPool : public ecs::ComponentProvider {
    using pointer = C*;

    using array_list = Array<C>;
    using lookup_table = HashMap<idtype, C*>;

    array_list   array;
    lookup_table lookup;

    using iterator = C*;
    using const_iterator = const C*;

    PackedComponentPool(size_t entities=10000)
    {
        array.reserve(entities);
        lookup.reserve(entities);
    }

    const ecs::RawComponent get(idtype index) const override
    {
        auto itr = lookup.find(index);
        return static_cast<const ecs::RawComponent>((*itr).second);
    }

    C& operator[] (size_t index)
    {
        return array[index];
    }

    const C& operator[] (size_t index) const
    {
        return array[index];
    }

    iterator begin()
    {
        return &array[0];
    }

    iterator end()
    {
        return &array[0] + array.size();
    }

    ecs::RawComponent get(idtype index) override
    {
        auto itr = lookup.find(index);
        
        if (itr == std::end(lookup))
            return create(index);
        
        return static_cast<ecs::RawComponent>((*itr).second);
    }

    ecs::RawComponent create(idtype index) override
    {
        C c;
        c.entity = index;
        array.push_back(c);
        auto* ptr = &array[0] + array.size() - 1;;
        lookup[index] = ptr;
        return (ecs::RawComponent) ptr;
    }

    void release(idtype index) override {
        auto* item_ptr = &(at(index));
        auto list_itr = std::begin(array) + size_t(item_ptr - &(array[0]));

        lookup.erase(index);
        array.erase(list_itr);
    }

    C& at(idtype index) {
        return *(lookup[index]);
    }

    size_t size() const override {
        return array.size();
    }

    size_t memory_usage() const override {
        assert (0);
        return 0;
    }

    void deallocate() override
    {
        //@TODO FIXME: this doesn't guarantee complete deallocation
        array.clear();
        array.shrink_to_fit();
        lookup = lookup_table();
    }
    
    void rebuild_lookup_table() {

        lookup.clear();
        for(auto& component : array) {
            lookup[component.entity] = &component;
        }
    }

    
};
#endif
