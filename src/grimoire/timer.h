#ifndef _timer_h_
#define _timer_h_

#include "sig.h"

class timer {

	unsigned 		_start;
	unsigned		_finish;
	unsigned		_current;
	unsigned		_previous;
	unsigned		_delta;

	unsigned		_trigger_delta;
	unsigned		_trigger_time;

	bool			_active;

	signal<void> 	_signal;

public:
	timer();

	unsigned		start();
	unsigned		stop();
	void			reset();
	unsigned 		update();
	void 			listen(signal<void>::callback_type f);

	void			trigger_delta(unsigned delta); // trigger callbacks every <delta> ticks
	void			trigger_time(unsigned time); // trigger callback once at <time> ticks and stop

};
#endif