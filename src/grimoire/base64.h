#ifndef _base64_h_
#define _base64_h_

#include "grimoire_util.h"

namespace util {

void base64_encode(char *out, const char *in);
void base64_decode(char *out, const char *in);

void base64_decode_string(String& s);

}

#endif
