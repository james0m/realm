#include <cassert>
#include <cstdlib>
#include <time.h>

#include "grimoire/ioutils.h"
#include "grimoire/filesystem.h"
#include "grimoire/fontlib.h"
#include "grimoire/range.hpp"
#include "grimoire/scene.h"
#include "grimoire/sprite.h"
#include "grimoire/tmxreader.h"

#include "common.h"

#include "asset/assets.hpp"
#include "combat.hpp"
#include "config.hpp"
#include "entity/world.hpp"
#include "input/input.hpp"
#include "motion.hpp"
#include "production.hpp"
#include "renderer.hpp"
#include "script/scriptenv.hpp"
#include "ui/console/console.hpp"
#include "ui/overlay/overlay.hpp"
#include "unitcontrol.h"


Globals globals;


class Game_Runtime : public Runtime
{
public:

    static constexpr const char* TAG = "Runtime";

    Game_Runtime();

	virtual
	~Game_Runtime();

    World*
	world() const override {
		return _world.get();
	}

    void
	on_quit();

    void
	init() override;

    void
	cleanup() override;

    void
	frame() override;

    bool
	should_quit() const;

    const Config&
	config() const override;

    float
	framerate() const override {
        return current_frame_rate;
    }

private:
    Unique<World>     _world;
    Config            conf_data;
    bool              _quitting = false;
    float             current_frame_rate;
    Array<u32>        frame_times;
};



bool Game_Runtime::should_quit() const {
    return _quitting;
}

Game_Runtime::Game_Runtime() {
    init();
}

Game_Runtime::~Game_Runtime() {
    cleanup();
}

void Game_Runtime::on_quit() {
    _quitting = true;
}


void Game_Runtime::init()
{
    srand(time(0) * 848482982943989292);

    {
        const auto exedir = io::exe_dir();
        const auto datadir = io::exe_dir() + "/data/";

        Log::i(TAG, sl::format("mounting data dirs:\n%s\n%s", exedir, datadir));
        io::mount(exedir);
        io::mount(datadir);
    }

    sdl::init();

    {
        Log::i(TAG, "initializing lua environment...");
        script::init(*this);
    }

    {
        Log::i(TAG, "loading engine configuration...");
        const auto config = io::open("data/config.lua");
        assert(config->isopen());
        conf_data = script::load_global_config(config);

        _world = util::make_unique<World>(2048);
    }
    {
        Log::i(TAG, "initializing rendering...");
        Log::i(TAG, "creating game window...");
        Render::create_display( conf_data.get<float>("window.width"),
                                   conf_data.get<float>("window.height"));

        Render::create_scene();
        Render::init(*this);
    }

    {
        Log::i(TAG, "initializing asset module");
        assets::init(*this);
    }

    {
        Log::i(TAG, "initializing input...");
        input::init(*this);

        // setup keybindings
        Log::i(TAG, "setting default keybindings...");

        input::bind_keys
        ({
            { SDLK_ESCAPE, A_CANCEL},
            { SDLK_m,      A_UNIT_MOVE },
            { SDLK_s,      A_UNIT_STOP },
            { SDLK_a,      A_UNIT_ATTACK },
            { SDLK_b,      A_BUILD_STRUCTURE },
            { SDLK_g,      DEBUG_ENABLE_MAP_GRID },
            { SDLK_t,      DEBUG_TOGGLE_TERRAIN },
            { SDLK_w,      DEBUG_TOGGLE_WAYPOINT_INDICATORS }
        });
    }

    {

        Log::i(TAG,"loading default map...");

        try
        {
            const char* map_file = conf_data.get_string("game.startup_map");
            _world->map = io::load_tmx_map(map_file);

            input::init(*this);
            if (!_world->map)
            {
                Log::e(TAG, "error loading map file");
                ASSERT(0 && "error loading map file");
                // TODO: exit now, but we should be returning to a menu state instead
            }

            Render::load_map_data(_world->map.get());
        }
        catch (std::exception& ex)
        {
            Log::e(TAG, sl::format("error while loading map data: %s", ex.what()));
            abort();
        }
    }

    {
        Log::i(TAG, "initializing unit control ai...");
        Units::init(*this);
    }
    {
        Log::i(TAG, "initializing physics...");
        Motion::init(*this);
    }
    {
        Log::i(TAG, "initializing production ...");
        //production->init(*this);
        Production::init(*this);
    }
    {
        Log::i(TAG, "initializing combat ...");
        Combat::init(*this);
    }
    {
		Log::i(TAG, "generating test bloodbath scenario ...");
        _world->index = World::SpatialIndex(3, rectf{ {0,0}, _world->map->pixel_dimensions()});

        const float limit = config().get<float>("game.test_entities");

        int spawn_x = 16,
            spawn_y = 16;

        auto units_per_team = limit / 2;

        auto team_1_color = colorf{ .3f,0.3f,1.0f,1.0f};
        auto team_2_color = colorf{ .5f, .2f, .2f, 1.0f};

        // generate a bunch of units
        for(auto y : range<int>(sqrt(units_per_team)))
        {
            for(auto x : range<int>(sqrt(units_per_team)))
            {
                auto id = _world->spawn_entity("Marine", {
                        float(spawn_x + x*20),
                        float(spawn_y + y*20)
                });
                auto& sprite = world()->get<gfx::Sprite>(id);
                sprite.color = team_1_color;

                auto& unit = world()->get<Unit>(id);
                unit.team = 0;
            }
        }

        spawn_x = 600;
        spawn_y = 600;

        // generate a bunch of units
        for(auto y : range<int>(sqrt(units_per_team)))
        {
            for(auto x : range<int>(sqrt(units_per_team)))
            {
                auto id = _world->spawn_entity("Marine", {
                        float(spawn_x + x*20),
                        float(spawn_y + y*20)
                });

                auto& sprite = world()->get<gfx::Sprite>(id);
                sprite.color = team_2_color;

                auto& unit = world()->get<Unit>(id);
                unit.team = 1;
            }
        }


        auto hq = _world->spawn_entity("Hq", { 500, 600 });
        auto& hq_sprite = _world->sprites[hq];
        hq_sprite.texture = assets::texture(hq_sprite.image);

        auto& hq_spawn = _world->spawners[hq];

        hq_spawn.spawn_point = { 90, 90 };

        Render::add_halo(&_world->sprites[hq],
                         hq_spawn.spawn_point,
                         8,
                         {0, .9, 0, 1.0},
                         false);

        Log::i(TAG, sl::format("spawned hq with id: %d", hq));
    }

    {
        Log::i(TAG, "initializing console...");
        txt::init();
        Console::init(*this);
        Render::get_ui().attach(Console::get_ui());
    }

    frame_times.reserve(100);
}

void Game_Runtime::cleanup()
{
    Log::i(TAG, "cleanup...");

    Combat::cleanup();
    assets::cleanup();
    Console::cleanup();
    Production::cleanup();
    Motion::cleanup();
    Units::cleanup();
    input::cleanup();
    Render::cleanup();

    _world = nullptr;

    SDL_Quit();
}


float average(const Array<u32>& vals) {
    u32 sum = 0;
    for(auto x : vals)
        sum += x;
    return float(sum) / vals.size();
}

void Game_Runtime::frame()
{
    u32 frame_start = SDL_GetTicks();

    SDL_Event e;
    while(SDL_PollEvent (&e))
    {
        if (e.type == SDL_QUIT ||
            (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_ESCAPE))
        {
            on_quit();
            break;
        }
        else if (e.type == SDL_KEYDOWN) {
            input::focus()->key_press(e.key.keysym.sym);
        }
        else if (e.type == SDL_KEYUP) {
            input::focus()->key_release(static_cast<unsigned>(e.key.keysym.sym));
        }
        else if (e.type == SDL_MOUSEBUTTONDOWN) {
            input::focus()->mouse_press(e.button.button, e.button.x, e.button.y);
        }
        else if (e.type == SDL_MOUSEBUTTONUP) {
            input::focus()->mouse_release(e.button.button, e.button.x, e.button.y);
        }
        else if (e.type == SDL_MOUSEMOTION) {
            auto pos = vec2<int> (e.motion.x, e.motion.y);
            auto prev = pos - vec2<int>(e.motion.xrel, e.motion.yrel);
            input::focus()->mouse_motion(prev, pos);
        }

        if (e.type == SDL_WINDOWEVENT)
        {
            switch (e.window.event)
            {
            case SDL_WINDOWEVENT_SHOWN:
                Log::i(TAG, sl::format("Window %d shown", e.window.windowID));
                break;
            case SDL_WINDOWEVENT_HIDDEN:
                Log::i(TAG, sl::format("Window %d hidden", e.window.windowID));
                break;
            case SDL_WINDOWEVENT_EXPOSED:
                Log::i(TAG, sl::format("Window %d exposed", e.window.windowID));
                break;
            case SDL_WINDOWEVENT_MOVED:
                Log::i(TAG, sl::format("Window %d moved to %d,%d",
                        e.window.windowID, e.window.data1,
                        e.window.data2));
                break;
            case SDL_WINDOWEVENT_RESIZED:
                Log::i(TAG, sl::format("Window %d resized to %dx%d",
                        e.window.windowID, e.window.data1,
                        e.window.data2));

                Render::resize_display(e.window.data1, e.window.data2);
                break;

            case SDL_WINDOWEVENT_SIZE_CHANGED:
                Log::i(TAG, sl::format("Window %d size changed to %dx%d",
                        e.window.windowID, e.window.data1,
                        e.window.data2));
                break;
            case SDL_WINDOWEVENT_MINIMIZED:
                Log::i(TAG, sl::format("Window %d minimized", e.window.windowID));
                break;
            case SDL_WINDOWEVENT_MAXIMIZED:
                Log::i(TAG, sl::format("Window %d maximized", e.window.windowID));
                break;
            case SDL_WINDOWEVENT_RESTORED:
                Log::i(TAG, sl::format("Window %d restored", e.window.windowID));
                break;
            case SDL_WINDOWEVENT_ENTER:
                Log::i(TAG, sl::format("Mouse entered window %d",
                        e.window.windowID));
                break;
            case SDL_WINDOWEVENT_LEAVE:
                Log::i(TAG, sl::format("Mouse left window %d", e.window.windowID));
                break;
            case SDL_WINDOWEVENT_FOCUS_GAINED:
                Log::i(TAG, sl::format("Window %d gained keyboard focus",
                        e.window.windowID));
                break;
            case SDL_WINDOWEVENT_FOCUS_LOST:
                Log::i(TAG, sl::format("Window %d lost keyboard focus",
                        e.window.windowID));
                break;
            case SDL_WINDOWEVENT_CLOSE:
                Log::i(TAG, sl::format("Window %d closed", e.window.windowID));
                break;
#if SDL_VERSION_ATLEAST(2, 0, 5)
            case SDL_WINDOWEVENT_TAKE_FOCUS:
                Log::i(TAG, sl::format("Window %d is offered a focus", e.window.windowID));
                break;
            case SDL_WINDOWEVENT_HIT_TEST:
                Log::i(TAG, sl::format("Window %d has a special hit test", e.window.windowID));
                break;
#endif
            default:
                Log::i(TAG, sl::format("Window %d got unknown event %d",
                                       e.window.windowID, e.window.event));
                break;
            }
        }
    }
    // update all modules

    Units::frame();

    Production::frame();
    Motion::frame();
    input::frame();
    Console::frame();
    Render::frame();

    Combat::frame(_world.get()); // combat updates last, so everyone can get to view attack events

    // timing
    globals.time = SDL_GetTicks();
    ++globals.frames;
    u32 frame_time = globals.time - frame_start;
//    frame_times.push_back(frame_time);
    globals.render_clock += frame_time;
    globals.physics_clock += frame_time;
    globals.units_clock += frame_time;

    if (globals.time > 1000) {
        current_frame_rate = float(globals.frames) / (float(globals.time) / 1000.0f);
	}
//    if (frame_times.size() == 100) {
//        frame_times.clear();
//    }
}

const Config& Game_Runtime::config() const
{
    return conf_data;
}


int main()
{
    Game_Runtime runtime;
    while( ! runtime.should_quit())
        runtime.frame();

    return 0;
}
