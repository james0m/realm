#include "textbox.h"

namespace txt {

Unique<TextBox> TextBox::create(const Font& font,
                                const String& text,
                                int w,
                                int h)
{
    auto tbptr = Unique<TextBox>( new TextBox(font, nullptr) );

    tbptr->set_bounds({
                0,
                0,
                w > 0 ? float(w) : 1,
                h > 0 ? float(h) : 1
    });
    
    tbptr->set_text(text);

    return tbptr;
}
TextBox::TextBox(const Font& font, gfx::Element* parent)
    :Element(parent),
     _font(&font)
{
    
}

void TextBox::draw(RenderApi &renderer) {
    auto bbox = bounds();
    bbox.y += _font->line_height;
    txt::render_text(renderer, *_font, _text, bbox, _color);
}

void TextBox::set_text(const String &text) {
    if (_text != text) {
        _text = text;
        _layout = txt::build_text_layout(font(), _text, bounds());
    }
    fit_layout();
}

const String &TextBox::text() const {
    return _text;
}

void TextBox::set_font(const Font& font) {
    _font = &font;
}

const Font& TextBox::font() const {
    return *_font;
}

void TextBox::fit_layout() {   
    set_bounds(_layout.extent);
}

}// txt
