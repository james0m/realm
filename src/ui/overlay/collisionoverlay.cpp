#include <algorithm>

#include "collisionoverlay.hpp"
#include "grimoire/scene.h"
#include "action.h"
#include "entity/world.hpp"


CollisionOverlay::CollisionOverlay(const Runtime& rt, Element* root):
    gfx::Overlay(root)
{
    init(rt);
}


Unique<CollisionOverlay>
CollisionOverlay::create(const Runtime& rt, Element* root)
{
    return util::make_unique<CollisionOverlay>(rt, root);
}


// recursive inner call of get_quads. fills the buffer with a
// list of actual regions of the quadtree in breadth first order
void
CollisionOverlay::get_quads_inner(rectf area, int depth)
{
    if(depth < 1)
        return;

    auto tl = topleft(area),
         tr = topright(area),
         bl = bottomleft(area),
         br = bottomright(area);

    rect_commands.push_back({ tl, colors_table[depth] });
    rect_commands.push_back({ tr, colors_table[depth] });
    rect_commands.push_back({ bl, colors_table[depth] });
    rect_commands.push_back({ br, colors_table[depth] });

    get_quads_inner(tl, depth-1);
    get_quads_inner(tr, depth-1);
    get_quads_inner(bl, depth-1);
    get_quads_inner(br, depth-1);
}


// get all quadtree regions from a given rectangle down to the given depth
void
CollisionOverlay::get_quads(rectf area, int depth)
{
    assert(depth);
    assert(depth < 20);

    if (_world->index.empty())
        return;

    rect_commands.push_back({ area, colors_table[0] });
    get_quads_inner(area, depth);
}

void
CollisionOverlay::init(const Runtime& rt)
{
    _rt = &rt;
    _world = rt.world();

    auto buffer = Array<rectf>();

    buffer.reserve(_world->index.size());

    get_quads(_world->index.region, _world->index.depth);
    reverse(begin(rect_commands), end(rect_commands));
}

struct collide_vis {
    long              event_time;
    rectf             r;
    colorf            rect_color;
    pair<vec2f,vec2f> dir;
    colorf            dir_color;

    collide_vis(long et, rectf _r, colorf rc, pair<vec2f,vec2f> d, colorf dc) {
        event_time = et;
        r = _r;
        rect_color = rc;
        dir = d;
        dir_color = dc;
    }
};

const size_t MAX_COLLIDE_VIS = 1000;
Array<collide_vis> collide_vis_list;

void
CollisionOverlay::draw(RenderApi& r)
{
    const vec2f offset = scene()->camera()->bounds().topleft();
    static auto real_commands = Array<rect_def>(rect_commands.size());

    for(auto i=0; i<size(real_commands); ++i) {
        real_commands[i] = rect_commands[i];
        real_commands[i].r.x -= offset.x;
        real_commands[i].r.y -= offset.y;
    }

    r.batch_rects( &real_commands[0], size(real_commands));

    if (size(collide_vis_list) > MAX_COLLIDE_VIS)
        collide_vis_list.clear();

    for (const auto& collision : events::collisions)
    {
        const auto& body1 = collision.sp1;
        auto rect = rect_from_circle(body1.location, body1.radius);

        collide_vis_list.emplace_back(
            collision.time,
            rect,
            collision_color,
            std::make_pair(body1.location, body1.location + body1.vel * globals.PHYSICS_TIMESTEP),
            highlight_color
        );

        const auto& body2 = collision.sp2;
        rect = rect_from_circle(body2.location, body2.radius);

        collide_vis_list.emplace_back(
            collision.time,
            rect,
            collision_color,
            std::make_pair(body2.location, body2.location + body2.vel * globals.PHYSICS_TIMESTEP),
            highlight_color
        );
    }

    static auto vis_garbage = Array<Array<collide_vis>::iterator>{};
    vis_garbage.reserve(collide_vis_list.size());

    for (auto& vis : collide_vis_list)
    {
        const auto alpha = interpolate(
            linear,
            1.0, 0.0,
            float(globals.time - vis.event_time) / 2000
        );

        if (alpha <= 0.0f) {
            auto itr = collide_vis_list.begin() + size_t(&vis - &collide_vis_list[0]);
            vis_garbage.push_back(itr);
            continue;
        }

        vis.rect_color.a = alpha;
        vis.dir_color.a = alpha;

        r.draw_rect({
                vis.r.x - offset.x,
                vis.r.y - offset.y,
                vis.r.w,
                vis.r.h
        }, vis.rect_color);

        r.draw_line(vis.dir.first - offset, vis.dir.second - offset, vis.dir_color);
    }

    for (const auto& itr : vis_garbage) {
        pop_erase(collide_vis_list, itr);
    }
    vis_garbage.clear();
}
