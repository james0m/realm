#pragma once

#include "grimoire/fontlib.h"
#include "ui/textbox.h"
#include "ui/overlay/overlay.hpp"


constexpr const char* fps_display_template = "fps: %.2f";
constexpr const char* entities_template = "entities: %d";


struct DebugInfo : gfx::Overlay {
	const Runtime*       _rt;
    const World*         _world;

    Unique<txt::TextBox> fps_display;
    Unique<txt::TextBox> entity_count_display;
    Unique<txt::TextBox> cursor_pos_display;
    Unique<txt::Font>    _font;

    static
	constexpr
	const char* TAG = "DebugInfo";

    static
	Unique<DebugInfo>
	create(const Runtime& rt);

    DebugInfo(const Runtime& rt):
        gfx::Overlay()
    {
    }

    void
	init(const Runtime& rt) override;

    void
	draw(RenderApi& r) override;
};


Unique<DebugInfo>
DebugInfo::create(const Runtime& rt) {
    return util::make_unique<DebugInfo>(rt);
}


void
DebugInfo::init(const Runtime& rt)
{
    _rt = &rt;
    _world = rt.world();

    _font = txt::Font::from_file(Render::get_api(), "NotoSans-Regular.ttf", 11);
}

void DebugInfo::draw(RenderApi& r)
{
    //TODO: should NOT be allocating strings just to update text fields
    //      would be better to use something that writes in-place

    r.draw_filled_rect({35, 20, 300, 100}, {0,0,0,.6});
    r.draw_rect({35, 20, 300, 100}, {0,0,0,1});


    txt::render_text(r, *_font,
                     sl::format(fps_display_template, _rt->framerate()),
                     50, 50, {1,1,1,1});

    txt::render_text(r, *_font,
                     sl::format(entities_template, _world->entity_count()),
                     50, 80, { 1, 1, 1, 1});

    int x, y;
    SDL_GetMouseState(&x, &y);


    vec2i wxy = Render::screen_to_world_coords(x,y);
    vec2i gxy = Render::world_to_grid_coords(wxy);

    auto index_cell = _world->index.grid_cell_of(wxy);
    auto index = _world->index.index_of(index_cell.x, index_cell.y);
    txt::render_text(r, *_font,
                     sl::format("cursor: (%d) (%d, %d) (%d, %d) (%d, %d)",
                                index, index_cell.x, index_cell.y, gxy.x, gxy.y, wxy.x, wxy.y),
                     50, 110, { 1, 1, 1, 1});
}
