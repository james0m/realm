#include "renderer.hpp"
#include "overlay.hpp"



namespace gfx {

Enable::Enable() {
    name = "enable";
    args_min = 1;
    args_max = 1;
}


String
Enable::exec(const sl::stringlist& args) const {
    assert(size(args) > 1);

    auto ol_name = args[1];
    

    //@TODO CLEANUP: this should just make the call, and let the check happen in Render
    if (Render::has_overlay(ol_name)) {
        Render::enable_overlay(ol_name);
    } else {
        return sl::format("can't find any overlay named %s", ol_name);
    }
    return "";
}


/**
 * deactivate an active overlay
 */

Disable::Disable() {
    name = "enable";
    args_min = 1;
    args_max = 1;
}

String Disable::exec(const sl::stringlist& args) const {
    ASSERT(size(args) > 1);

    auto ol_name = args[1];


    if (Render::has_overlay(ol_name)) {
        Render::disable_overlay(ol_name);
    }
    else {
        return sl::format("can't find any overlay named %s", ol_name);
    }
    return "";
}


/**
 * list all overlays registered in the rendering system
 */
List::List() {
    name = "list";
}


// TODO: give enabled/disabled status in the output
String List::exec(const sl::stringlist& args) const {

    const auto& overlays = Render::get_overlays();

    std::stringstream ss;
    for(auto itr=begin(overlays), last=end(overlays); itr != last; ++itr) {
        ss << itr->first << "\n";
    }

    return ss.str();
}


/**
 * the main command for interacting with overlays. This is the main
 * command that provides the enable, disable, and list sub-commands
 */

Cmd_Overlay::Cmd_Overlay() {
    name = "overlay";
    args_min = 1;
    args_max = 3;

    subcommands.emplace( "enable", util::make_unique<Enable>());
    subcommands.emplace( "disable", util::make_unique<Disable>());
    subcommands.emplace( "list", util::make_unique<List>());
}

String Cmd_Overlay::exec(const sl::stringlist& args) const  {

    const auto itr = subcommands.find(args[0]);
    if (itr == end(subcommands))
        return sl::format("error: no such command", args[1]);

    return itr->second->exec( args);
}

} // gfx
