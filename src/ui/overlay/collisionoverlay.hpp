#ifndef _collision_overlay_h_
#define _collision_overlay_h_

#include "grimoire/renderapi.hpp"
#include "runtime.hpp"

#include "overlay.hpp"

using gfx::Element;
using Realm::Runtime;

class CollisionOverlay : public gfx::Overlay
{
public:
    static constexpr const char* TAG = "Collision";
    
    static Unique<CollisionOverlay> create(const Runtime& rt, Element* root);

    CollisionOverlay(const Runtime& rt, Element* root);
    virtual ~CollisionOverlay(){}
    
public:
    void init(const Runtime& rt) override;
    
    void draw(RenderApi& r) override;
    
    colorf collision_color = { 1.0f, .5f, .5f, .7f };
    colorf highlight_color = { 0.0f, 1.0f, 0.0f, 0.7f };
    
    colorf colors_table[4] = {
        {1.0f, 1.0f, 1.0f, 1.0f}, // root red
         // 3 green
        
        {0.25f, 0.25f, 0.25f, 1.0f},  // 1 cyan
        {0.5f, 0.5f, 0.5f, 1.0f}, // 2 blue
        {0.75f, 0.75f, 0.75f, 1.0f}
    };

    const Runtime* _rt;
    const World* _world;
    Array<rect_def> rect_commands;

private:
    void get_quads_inner(rectf area, int depth);
    void get_quads(rectf area, int depth);
};

#endif
