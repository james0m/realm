#include "grimoire/range.hpp"
#include "grimoire/scene.h"

#include "pathfinding/pathsearchalgo.h"
#include "entity/world.hpp"
#include "runtime.hpp"

#include "pathdebugoverlay.hpp"


Unique<txt::Font> map_location_font;

namespace actions {
signal<void> toggle_path_visuals;
}


Unique<PathDebugOverlay>
PathDebugOverlay::create(const Runtime& rt, gfx::TiledMapElement *root) {
    assert(root);    
    return util::make_unique<PathDebugOverlay>(rt, root);
}


PathDebugOverlay::PathDebugOverlay(const Runtime& rt, gfx::TiledMapElement *root)
    :Overlay(root)
{
    init(rt);
    events::path_complete.connect(this, &PathDebugOverlay::on_path_complete);
    actions::toggle_path_visuals.connect(this,&PathDebugOverlay::toggle_path_visuals);
}


void
PathDebugOverlay::init(const Runtime& rt) {
    _rt = &rt;
    world = rt.world();
    map = world->map.get();
}


void
PathDebugOverlay::on_path_complete(const events::PathCompleted& ev)
{
}


void
PathDebugOverlay::draw(RenderApi &r) {
    if (!_path_visuals_enabled)
        return;
    
    if (map_location_font == nullptr)
        map_location_font = txt::Font::from_file(r, "Oswald-Regular.ttf", 12);


    const gfx::Camera *main_cam = scene()->camera();

    // get camera offset
    const vec2f offset = main_cam->bounds().topleft();

    const colorf trail_color = { 0.0f, 1.0f, 0.0f, .4f };
    const colorf path_color  = { 0.0f, 0.0f, 1.0f, 0.6f};
    const colorf start_color = { 1.0f, 1.0f, 0.0f, 0.7f};
    const colorf goal_color  = { 0.0f, 0.0f, 0.0f, 0.7f};

    const vec2f tilesize = vec2f(map->tile_dimensions());

    for(auto &ev : events::completed_paths) {

        // draw the path trail
        for(auto &kv : ev.pathop->trail) {

            auto region = rectf(
                        kv.first.x * tilesize.x - offset.x ,
                        kv.first.y * tilesize.y - offset.y,
                        tilesize.x,
                        tilesize.y
                        );
            r.draw_filled_rect(region, trail_color);
        }
        // draw the chosen path
        for(auto &node : ev.pathop->path) {
            auto region = rectf(node.x - offset.x, //* tilesize.x - offset.x,
                                node.y - offset.y, //* tilesize.y - offset.y,
                                tilesize.x,
                                tilesize.y);

            r.draw_filled_rect(region, path_color);
        }

        // draw start and end nodes
        r.draw_filled_rect(rectf(ev.pathop->start.x * tilesize.x - offset.x,
                                 ev.pathop->start.y * tilesize.y - offset.y,
                                 tilesize.x, tilesize.y),
                           start_color);

        r.draw_filled_rect(rectf(ev.pathop->goal.x * tilesize.x - offset.x,
                                 ev.pathop->goal.y * tilesize.y - offset.y,
                                 tilesize.x, tilesize.y),
                           goal_color);


    }

    auto &terrain = map->layer_at(0);

    static
    const auto BLACK = colorf{0, 0, 0, 1};
    
    for (auto i : range(terrain.data.size())) {
        auto cell = terrain.data[i];
        auto res = map->tile_info(cell).resistance;
        auto s = std::to_string(res); 

        auto text_sz = vec2<int>();
        for(auto _ : range(s.size())) {
            char ch = s[0];
            auto glyph_sz = map_location_font->glyph(ch).region.dimensions();
            text_sz.x += glyph_sz.x;
            text_sz.y = std::max(text_sz.y, (int)glyph_sz.y);
        }

        //                xform i -> map coords                    centering           adjust for cam position
        const int x = (i % map->width()) * tilesize.x + (tilesize.x/2 - text_sz.x/2) - offset.x;
        const int y = (i / map->width()) * tilesize.y + (tilesize.y/2 + text_sz.y/2) - offset.y;

        txt::render_text(r, *map_location_font, s, x, y, BLACK);
    }
}


