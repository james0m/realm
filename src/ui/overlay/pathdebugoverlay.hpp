#ifndef _pathdebugoverlay_h_
#define _pathdebugoverlay_h_

#include "grimoire/element.h"
#include "grimoire/tiledmapelement.h"
#include "overlay.hpp"
#include "action.h"


/*
 * Here we have a lovely scene node that
 * will draw pathfinder debugging information
 * over top of the map when it is rendered
 */
struct PathDebugOverlay : gfx::Overlay {
    static constexpr const char* TAG = "PathDebug";

    static
    Unique<PathDebugOverlay>
    create(const Runtime& rt, gfx::TiledMapElement *root);

    PathDebugOverlay(const Runtime& rt, gfx::TiledMapElement* root);

    void
    init(const Runtime& world) final override;

    void
    draw(RenderApi &r) override;

    void
    on_path_complete(const events::PathCompleted &ev);

    void
    toggle_path_visuals() { _path_visuals_enabled = !_path_visuals_enabled; }
    
private:
    const Runtime* _rt;
    const World*    world;
    const tmx::TiledMap* map;

    bool _path_visuals_enabled = true;
};


#endif //_pathdebugoverlay_h_
