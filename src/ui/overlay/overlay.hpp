#ifndef _overlay_h_
#define _overlay_h_

#include <sstream>

#include "forward.hh"
#include "grimoire/element.h"
#include "ui/console/console.hpp"

using Realm::Runtime;

namespace gfx {

/**
 * Overlays are scene elements which are meant to display game world
 * information as part of the rendered scene. As such, when activated
 * they are passed a ref to the Runtime via init() to have read-only 
 * access to the state of the system.
 */
struct Overlay : Element {
    bool enabled = false;
    
    Overlay(Element* root=nullptr):
        Element(root)
    {
    }

    virtual
    void
    init(const Runtime& rt) {}
};

/**
 * the main command for interacting with overlays. This is the main
 * command that provides the enable, disable, and list sub-commands 
 */
struct Cmd_Overlay : Console::MultiCommand {
    Cmd_Overlay();

    String
    exec(const sl::stringlist& args) const override;
};

/** 
 * activate a registered overlay
 */
struct Enable : Console::ConsoleCommand {
    Enable();

    String
    exec(const sl::stringlist& args) const override;
};

/**
 * deactivate an active overlay
 */
struct Disable : Console::ConsoleCommand {
    Disable();

    String
    exec(const sl::stringlist& args) const override;
};

/**
 * list all overlays registered in the rendering system
 */
struct List : Console::ConsoleCommand {
    List();

    String
    exec(const sl::stringlist& args) const override;
};


} // gfx
#endif
