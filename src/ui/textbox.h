#ifndef _textbox_h_
#define _textbox_h_

#include "grimoire/element.h"
#include "grimoire/fontlib.h"

namespace txt {

class TextBox: public gfx::Element {
public:

    static Unique<TextBox> create(const Font& font,
                                  const String& text,
                                  int w=0,
                                  int h=0);
    
    TextBox(const Font& font,
            gfx::Element* parent=nullptr);
    
    virtual ~TextBox() {}

public: // queries
    
    const Font& font() const;
    const String &text() const;

public: // element
    
    void draw(RenderApi &renderer) override;

public: // mutators
    void set_text(const String &text);
    void set_font(const Font& font);
    void fit_layout();
    
private:
    
    colorf      _color;
    const Font* _font;
    Layout      _layout;
    String      _text;
};


} // txt
#endif //_textbox_h_
