#ifndef HUD_HH
#define HUD_HH

#include <utility>

#include "grimoire/color.h"
#include "grimoire/stack.h"
#include "grimoire/tiledmap.h"
#include "grimoire/uielement.hpp"

#include "forward.hh"

#include "common.h"
#include "events.hpp"
#include "action.h"


using gfx::Element;
using gfx::Scene;


enum UI_Type : idtype {
    MINIMAP,
    ACTION_PANEL,
    PANEL,
    PANEL_OPTION
};

class Minimap : public UIElement {
public:

    Minimap(Element* root=nullptr);

    void
	set_map(tmx::TiledMap* map);

    void
	draw(RenderApi& r) override;

    idtype
	type() const override {
        return UI_Type::MINIMAP;
    }

private:
    tmx::TiledMap* _map;
    colorf bg_color = { 0.0f, 0.3f, 0.5f, 0.7f };
};


struct PanelOption : UIElement {
    ActionId     action = A_NULL;
    String       option;
    PanelOption* next = nullptr;

    PanelOption() = default;
    PanelOption(ActionId actid, String option_text):
        UIElement(),
        action(actid),
        option(std::move(option_text))
    {
    }

    void
	draw(RenderApi& r) override;

    idtype
	type() const override {
        return UI_Type::PANEL_OPTION;
    }
};


struct Panel : UIElement
{
    Panel(): UIElement(nullptr) {}

    ~Panel() override;

    void
	draw(RenderApi& r) override;

    void
	add_option(ActionId action, const char* option);

    idtype
	type() const override {
        return UI_Type::PANEL;
    }

private:
	Array< PanelOption* > items;
};


/* show command options for the current selected units */
struct ActionPanel : UIElement {
    colorf             bg_color = { 0.0f, 0, 0, 0.7f };
    colorf             txt_color = { 1, 1, 1, 1 };
    Unique<txt::Font>  font;
    util::Stack<Panel> panel_stack;

    explicit
    ActionPanel(Element* root=nullptr);

	idtype
	type() const override {
        return UI_Type::ACTION_PANEL;
    }

    void
	draw(RenderApi& r) override;

    void
	expose_option(ActionId action, const char* option);

    void
	pop_current_panel();

    Panel&
	push_panel();

    void
	clear();
};


struct HUD {
	Array<ActionId> entity_actions;
    Minimap minimap;
    ActionPanel actionpanel;
    Scene* scenegraph = nullptr;

    explicit
	HUD(Scene* graph=nullptr);

    void
	init(Scene* scene);

    void
	on_view_resize(const events::ViewResize& ev);
};

#endif
