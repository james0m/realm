#include "grimoire/camera.h"
#include "grimoire/scene.h"

#include "hud.hpp"
#include "input/input.hpp"




ActionPanel::ActionPanel(Element* root):
    UIElement(root),

    //HACK: need to fix handling of resources (Panel->PanelOptions),
    //      this just avoids a realloc-move by... or just use an array
    panel_stack(5)
{
    _bounds.w = 125;
    _bounds.h = 125;
}

void ActionPanel::expose_option(ActionId action, const char* option="")
{
    panel_stack.top().add_option(action, option);
}

void ActionPanel::clear()
{
    panel_stack.clear();
    _elements.clear();
}

Panel& ActionPanel::push_panel()
{
    if(panel_stack.size())
        detach(&panel_stack.top());

    auto& panel = panel_stack.push();
    panel.set_bounds(0, 0, bounds().w, bounds().h);
    attach(&panel);
    return panel;

}

void ActionPanel::pop_current_panel()
{
    detach(&panel_stack.top());
    panel_stack.pop();
    attach(&panel_stack.top());
}

void ActionPanel::draw(RenderApi &r)
{
    auto b = bounds();
    if (hovered() && active())
        r.draw_filled_rect(b, { 1, 1, 1, 1});

    else if (hovered())
        r.draw_filled_rect(b, { 0.5f, 0.5f, 0.5f, 1.0f});

    else
        r.draw_filled_rect(b, bg_color);

    if (panel_stack.size())
        panel_stack.top().draw(r);
}

Minimap::Minimap(gfx::Element *root):
    UIElement(root)
{
    _bounds.w = 125;
    _bounds.h = 125;
}

void Minimap::set_map(tmx::TiledMap* map)
{
    _map = map;
}

/**
 * @brief draw_minimap
 *
 * @param r  render api instance
 * @param map pointer to the active map
 * @param cam pointer to the scene camera
 * @param region total area and position of the minimap panel
 */
void draw_minimap(RenderApi* r, const tmx::TiledMap* map, const gfx::Camera* cam, rectf region)
{        
    //rectf cam_v = cam->viewport();
    rectf cam_viewport = cam->bounds();

    const float map_width = map->pxwidth();
    const float map_height = map->pxheight();                    

    const float x_ratio = cam_viewport.x / map_width;
    const float y_ratio = cam_viewport.y / map_height;

    const float w_ratio = cam_viewport.w / map_width;
    const float h_ratio = cam_viewport.h / map_height;

    auto frustum = rectf {
        region.x + (region.w * x_ratio),
        region.y + (region.h * y_ratio),
        region.w * w_ratio,
        region.h * h_ratio
    };    
    
    // draw the view "frustum" on the minimap
    r->draw_rect(frustum.clip(region), {1,1,1,1});

    // draw all visible entities as dots
}

void Minimap::draw(RenderApi &r) {
    auto b = bounds();

    if(hovered())
        r.draw_filled_rect(b, { 1.0f, 0.5f, 0.5f, 0.5f});
    else
        r.draw_filled_rect(b, bg_color);

    draw_minimap(&r, _map, scene()->camera(), bounds());
}


void PanelOption::draw(RenderApi &r)
{
    auto b = bounds();

    if (active())
        r.draw_filled_rect(b, {1,1,1,1});
    else if (hovered())
        r.draw_filled_rect(b, { 0.0f, 0.5f, 0.5f, 1.0f});

    float x = b.x,
          y = b.y + globals.ui_font->line_height;
    txt::render_text(r, *globals.ui_font, option, x, y, {1,1,1,1});
}

Panel::~Panel()
{
    for(auto i : items)
        delete i;
}

void Panel::draw(RenderApi& r)
{
    // if (hovered())
    // {
    //     r.draw_filled_rect(bounds(), {0.5, 1.0f, 0.4f, 1.0f});
    // }
    // else
    // {
    //     UIElement::draw(r);
    // }
    UIElement::draw(r);
}

void Panel::add_option(ActionId action, const char* option)
{
    const sl::string option_text = option;

    const float option_width = txt::measure(option_text, *globals.ui_font).x + 5.0f;
    const auto line_height = static_cast<float>(globals.ui_font->line_height);

	auto* panel_opt = new PanelOption(action, option);
    if (items.empty()) {
        panel_opt->set_bounds(5, 5, option_width, line_height + 5);
    } else {
        const auto last_item = items.back();
        panel_opt->set_bounds(5, last_item->bounds_local().bottom(),
              option_width, line_height);
	}

	items.push_back(panel_opt);
    attach( panel_opt );
}


HUD::HUD(gfx::Scene *graph)
{
}

void HUD::init(gfx::Scene *scene)
{
    scenegraph = scene;

    scenegraph->attach(&minimap);
    scenegraph->attach(&actionpanel);

    events::view_resized.connect(this, &HUD::on_view_resize);
    on_view_resize({ 0,0 });
}

void HUD::on_view_resize(const events::ViewResize &ev)
{
    minimap    .align(Alignment::BOTTOMLEFT);
    actionpanel.align(Alignment::BOTTOMRIGHT);
}
