#include "console.hpp"
#include "grimoire/log.h"
#include "input/input.hpp"

using namespace Realm;

namespace Console
{

ConsoleUI::ConsoleUI(Element* root)
    : gfx::Element(root)
{
    line_height = 24;
    _opacity = 0.0f; // defaults to being hidden
    
    events::view_resized.connect(this, &ConsoleUI::on_resize);
}


void ConsoleUI::attached(Element& parent) {
    Log::i(TAG, "on_attached");

    auto s_bounds = parent.bounds();
    set_bounds(0,0, s_bounds.w, (s_bounds.h / 3) * 2);
}

void ConsoleUI::on_resize(const events::ViewResize& e) {
    set_bounds(0,0, e.w, (e.h/3) * 2);
}

void ConsoleUI::draw(RenderApi& r) {
    if (_opacity > 0.0f)
    {
        // render the background
        bgcol.a = _opacity;
        auto bgarea = bounds();
        r.draw_filled_rect( bgarea, bgcol);

        // render the input line background
        auto inputline = bgarea;
        inputline.y = bgarea.h - line_height;
        inputline.h = line_height;

        auto col = bgcol;
        // generate a highlight color for the input line 
        col.r += col.r * .5f;
        col.g += col.g * .5f;
        col.b += col.b * .5f;
    
        r.draw_filled_rect(inputline, col);

        {
            // draw the text history buffer
            int x = bgarea.x + 10, // TODO: margin property
                y = bgarea.bottom() - line_height; 

            const auto& history = Console::get_history();
            for (auto itr = history.rbegin(), end = history.rend();
                 itr != end && y > 0; ++itr)
            {
                txt::render_text(r, *font, *itr, x, y, fgcol);
                y -= line_height;
            }

        }
        // draw the input line
        {
            const auto& buf = Console::get_input_line();
            auto txt = String(begin(buf), end(buf));
            auto box = inputline;
            box.y += 24 - 5;
            txt::render_text(r, *font, txt, box.x, box.y, fgcol);
        }
        // draw the cursor        
        auto cursor = inputline;
        cursor.w = 10.0f;
        cursor.x = cursor_pos * font->glyph('f').advance.x;
        r.draw_filled_rect(cursor, fgcol);
    }
}

void ConsoleUI::show()
{
    _opacity = opacity_setting;
}

void ConsoleUI::set_background_color(colorf c)
{
    bgcol = c;
    opacity_setting = c.a;
}

void ConsoleUI::hide()
{
    _opacity = 0;
}

void ConsoleUI::on_toggle() {

    if( _opacity == 0.0f)
        show();
    else
        hide();

    
    Log::i(TAG, sl::format(
               "console is %s", _opacity > 0 ? "visible" : "invisible"));
}

}// namespace Console
