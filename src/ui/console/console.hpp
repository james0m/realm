#ifndef _CONSOLE_HPP_
#define _CONSOLE_HPP_

#include "common.h"
#include "forward.hh"

#include "grimoire/stringlib.h"
//#include "grimoire/color.h"
//#include "grimoire/fontlib.h"
//#include "grimoire/renderapi.hpp"
#include "grimoire/grimoire_util.h"
#include "events.hpp"
//#include "grimoire/element.h"
//#include "textbox.h"
//#include "renderer.hpp"


namespace Console
{

struct ConsoleCommand {
    const char* name = "";
    int args_min = 0;
    int args_max = 0;

    virtual ~ConsoleCommand() = default;

    String operator() (const sl::stringlist& args) const {
        return exec(args);
    }

    virtual String exec(const sl::stringlist& args) const = 0;
    virtual bool check_args(const sl::stringlist& args) const;
};

using CommandTable = std::map<String, Unique<ConsoleCommand>>;

struct MultiCommand : ConsoleCommand {
    CommandTable subcommands;
};

struct ConsoleUI : public gfx::Element {
    static constexpr const char* TAG = "ConsoleUI";

    Unique<txt::Font> font;
    float  opacity_setting; // a cache for storing the user-set opacity when  hidden
    colorf fgcol;
    colorf bgcol;
    int    line_height;
    int    cursor_pos = 0;

    ConsoleUI(Element* root=nullptr);

    void attached(Element& parent) override;
    void draw(RenderApi& r) override;
    void on_toggle();
    void on_resize(const events::ViewResize& e);
    void hide();
    void show();

    void set_background_color(colorf c);
};


void
init(Realm::Runtime&);

gfx::Element*
get_ui();

void
frame();

void
cleanup();

//input::InputHandler*
//focus();

//bool visible();

//const ConsoleCommand&
//parse_command(const String& line);



void expose_command(const sl::string& cmdname, Unique<ConsoleCommand> cmd);

const sl::stringlist&
get_history();

const Array<char>&
get_input_line();

template <class CommandType>
void
expose(const String& cmdname, Unique<CommandType> cmd) {
    expose_command(cmdname, std::move(cmd));
}



}
#endif //_console_h_
