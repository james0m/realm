#include "SDL2/SDL_keyboard.h"

#include "grimoire/log.h"
#include "grimoire/stringlib.h"
#include "grimoire/scene.h"
#include "grimoire/sprite.h"

#include "asset/assets.hpp"
#include "config.hpp"
#include "console.hpp"
#include "input/input.hpp"
#include "entity/prototype.hpp"
#include "renderer.hpp"
#include "runtime.hpp"
#include "entity/world.hpp"
#include "ui/overlay/overlay.hpp"

namespace Console
{

constexpr
const char* TAG = "Console";

static Realm::Runtime*   _rt;
static World*            _world;
static Unique<ConsoleUI> _ui;
static Array<char>      _input_line;
static sl::stringlist    _history;
static CommandTable      _commands;

static std::stringstream _log_proxy_stream; // for piping log output to console

static
void
key_release(unsigned key)
{}

static
void
mouse_press(unsigned button, int x, int y)
{}

static
void
mouse_release(unsigned button, int x, int y)
{}

static
void
mouse_motion(vec2i pos_prev, vec2i pos_now)
{}

static
void exec();

static
char
key_to_char(u32);


static
void
key_press(unsigned key) {
    if( key == SDLK_BACKQUOTE) {
        actions::toggle_console();
    } else if( key == SDLK_RETURN) {
        _ui->cursor_pos = 0;
        exec();
    } else if (key == SDLK_BACKSPACE && size(_input_line) > 0) {
        _input_line.pop_back();
        --_ui->cursor_pos;
    } else if (key == SDLK_LEFT && _ui->cursor_pos > 0) {
        --_ui->cursor_pos;
    } else if (key == SDLK_RIGHT && _ui->cursor_pos < size(_input_line)) {
        ++_ui->cursor_pos;
    } else if (key == SDLK_UP) {
        // get the last command from history and drop into input_line
    } else if (key == SDLK_LSHIFT || key == SDLK_RSHIFT) {
        return;
    } else {
        char ch = key_to_char(key);
        // if shift is pressed, get the upper case
        if ( SDL_GetModState() & KMOD_SHIFT ) {
            ch = toupper(key);
        }
        _input_line.insert(begin(_input_line) + _ui->cursor_pos, ch);
        ++_ui->cursor_pos;
    }
}


static input::InputHandler console_input = {
    key_press,
    key_release,
    mouse_press,
    mouse_release,
    mouse_motion
};

bool _visible = false;


bool
ConsoleCommand::check_args(const sl::stringlist& args) const {
    const auto N = args.size();
    return !(N < args_min || N > args_max);
}


struct Cmd_Noop : ConsoleCommand {
    String
    exec(const sl::stringlist&) const override {
        return "";
    }
};

/**
 * a command for testing the console system
 * just adds two numbers together and prints out the result
 */
struct Cmd_Add : ConsoleCommand {

    Cmd_Add() {
        name = "add";
        args_min = 2;
        args_max = 2;
    }

    String
    exec(const sl::stringlist& args) const override {
        // @TODO: assert for now, warn and early exit for real commands

        float lhs = std::stof(args[0]);
        float rhs = std::stof(args[1]);

        std::stringstream ss;
        ss << (lhs + rhs);
        return ss.str();
    }
};


static
void
toggle() {
    _visible = !_visible;

    if (_visible)
        input::steal_focus(&console_input);
    else
        input::return_focus();

    _ui->on_toggle();
}

/**
 * spawn
 *
 * spawns a new entity of given type at the provided map location,
 * or at 0,0 if no coords given
 */
struct Cmd_Spawn : ConsoleCommand {
    Cmd_Spawn() {
        name = "spawn";
        args_min = 1;
        args_max = 3;
    }

    String exec(const sl::stringlist& args) const override {
        ASSERT(args_min <= size(args) && size(args) <= args_max);

        const auto& proto_name = args[0];

        
        //const auto& proto = s_world->prototypes.at(proto_name);
        const auto* proto = assets::get_prototype(proto_name.c_str());
        
        if (size(args) != 3) {
            return sl::format("%s: wrong number of arguments", name);
        }

        if(proto->components & tl::type_mask<Spatial>()) {
            auto x = stof(args[1]);
            auto y = stof(args[2]);

            auto id = _world->spawn_entity(*proto, { x,y });
            return sl::format("spawning entity %d of type: %s", id, args[0]);
        }
        return sl::format("%s: prototype was not configured properly", name);
    }
};

/**
 * get (c) (id)
 *
 * gets component data from the current world
 *
 */
struct Cmd_Get : ConsoleCommand {
    Cmd_Get() {
        name = "get";
        args_min = 2;
        args_max = 2;
    }

    String
    exec(const sl::stringlist& args) const override {
        check_args(args);
        return "";
    }
};


void
init(Realm::Runtime& rt) {
    _rt = &rt;
    _world = rt.world();
    input::bind_key(SDLK_BACKQUOTE, A_TOGGLE_CONSOLE);

    auto& config = _rt->config();

    _ui = util::make_unique<ConsoleUI>();
    _ui->font = txt::Font::from_file(Render::get_api(),
                                    config.get_string("console.font"),
                                    config.get_float("console.font_size"));

    _ui->set_background_color({
            config.get_float("console.bg_r"),
            config.get_float("console.bg_g"),
            config.get_float("console.bg_b"),
            config.get_float("console.bg_a"),
    });

    actions::toggle_console.connect(&toggle);
    
    Log::logger.add_output_stream(_log_proxy_stream);

    _commands.emplace("", util::make_unique<Cmd_Noop>());
    _commands.emplace("add", util::make_unique<Cmd_Add>());
    _commands.emplace("spawn", util::make_unique<Cmd_Spawn>());
    _commands.emplace("get", util::make_unique<Cmd_Get>());
    _commands.emplace("ol", util::make_unique<gfx::Cmd_Overlay>());
}

void
cleanup() {
    _rt = nullptr;
    Render::get_ui().detach(get_ui());
    _ui.reset(nullptr);

    _input_line = Array<char>(0);
    _commands = CommandTable();
    _history = sl::stringlist();

    Log::logger.drop_output_stream(_log_proxy_stream);
}


void
frame() {
    // just poll the log proxy for new data, then output to history
    if (_log_proxy_stream.rdbuf()->in_avail() > 0)
    {
        _history.push_back(_log_proxy_stream.str());
        _log_proxy_stream.str(sl::string());
    }
}

// TODO: this is silly but it gets the job done enough for now
static
char
key_to_char(unsigned sym) {
    char ch;

    switch(sym) {
    case SDLK_a: {
        ch = 'a';
        break;
    }

    case SDLK_b: {
        ch = 'b';
        break;
    }
    case SDLK_c: {
        ch = 'c';
        break;
    }
    case SDLK_d: {
        ch = 'd';
        break;
    }
    case SDLK_e: {
        ch = 'e';
        break;
    }
    case SDLK_f: {
        ch = 'f'; break;
    }
    case SDLK_g: { ch = 'g'; break; }
    case SDLK_h: { ch = 'h'; break; }
    case SDLK_i: { ch = 'i'; break; }
    case SDLK_j: { ch = 'j'; break; }
    case SDLK_k: { ch = 'k'; break; }
    case SDLK_l: { ch = 'l'; break; }
    case SDLK_m: { ch = 'm'; break; }
    case SDLK_n: { ch = 'n'; break; }
    case SDLK_o: { ch = 'o'; break; }
    case SDLK_p: { ch = 'p'; break; }
    case SDLK_q: { ch = 'q'; break; }
    case SDLK_r: { ch = 'r'; break; }
    case SDLK_s: { ch = 's'; break; }
    case SDLK_t: { ch = 't'; break; }
    case SDLK_u: { ch = 'u'; break; }
    case SDLK_v: { ch = 'v'; break; }
    case SDLK_w: { ch = 'w'; break; }
    case SDLK_x: { ch = 'x'; break; }
    case SDLK_y: { ch = 'y'; break; }
    case SDLK_z: { ch = 'z'; break; }

    case SDLK_1: { ch = '1'; break; }
    case SDLK_2: { ch = '2'; break; }
    case SDLK_3: { ch = '3'; break; }
    case SDLK_4: { ch = '4'; break; }
    case SDLK_5: { ch = '5'; break; }
    case SDLK_6: { ch = '6'; break; }
    case SDLK_7: { ch = '7'; break; }
    case SDLK_8: { ch = '8'; break; }
    case SDLK_9: { ch = '9'; break; }
    case SDLK_0: { ch = '0'; break; }

    case SDLK_PERIOD: { ch = '.'; break; }

    default: { ch = ' '; }
    }
    return ch;
}
//
//bool
//visible() {
//    return _visible;
//}

static
const ConsoleCommand&
parse_command(const String& line) {
    auto lex = sl::split(line, ' ');
    assert (size(lex) > 0);
    auto command_name = lex[0];

    try {
        return *_commands.at(command_name);
    }
    catch (std::out_of_range &) {
        return *_commands.at("");
    }
}

static
bool
check_arg_count(const ConsoleCommand& cmd, const sl::stringlist& args) {
    return cmd.args_min <= size(args) && cmd.args_max >= size(args);
}

static
void
exec() {
    auto s = String(begin(_input_line), end(_input_line));
    Log::i(TAG, sl::format("executing command: %s", s));

    const auto& cmd = parse_command(s);

    auto args = sl::split(s, ' ');
    args.erase(begin(args));

    String cmd_result;
    if ( strcmp(cmd.name, "") == 0) {
        cmd_result = sl::format("no such command: %s", s);

    }
    else if (!check_arg_count(cmd, args)) {

        if (cmd.args_min == cmd.args_max)
            cmd_result = sl::format("%s requires exactly %d arguments", cmd.name,cmd.args_min);
        else {
            cmd_result = sl::format("%s requires at least %d arguments and no more than %d arguments", cmd.name, cmd.args_min, cmd.args_max);
        }
    }
    else {
        cmd_result = cmd.exec(args);
        _history.push_back(s);

    }
    _history.push_back(cmd_result);
    _input_line.clear();
}


//input::InputHandler*
//focus()
//{
//    return &console_input;
//}

gfx::Element* get_ui()
{
    return _ui.get();
}

const Array<char>& get_input_line()
{
    return _input_line;
}

void expose_command(const sl::string& cmdname, Unique<ConsoleCommand> cmd)
{
    _commands.emplace(cmdname, std::move(cmd));
}

const sl::stringlist& get_history()
{
    return _history;
}

} // namespace Console
