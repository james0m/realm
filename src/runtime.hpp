#ifndef _runtime_h_
#define _runtime_h_


class Config;

struct World;

namespace Realm {

class Runtime {
public:

    /**
     * @brief init
     *
     * does whatever needs to be done prior to clients
     * accessing the rest of the Runtime api.
     *
     * Using world(), module(type), map(), or frame()
     * all expect that init() has already been called
     * successfully.
     *
     */
    virtual void init() = 0;

    /**
     * @brief cleanup
     *
     * do any reclaiming of resources as needed to get
     * back to initial state. This is needed so that
     * the system can be reset and re-initialized
     * without needing to restart the whole program
     */
    virtual void cleanup() = 0;

    /**
     * @brief world
     * get a pointer to the active World object
     *
     * @return pointer to World
     */
    virtual World* world() const = 0;

    /**
     * @brief frame
     * all code that needs to be continually called
     * each frame. This is to be executed in the
     * main loop
     */
    virtual void frame() = 0;

    /**
     * @brief provide read-only access to global 
     * configuration settings
     */
    virtual const Config& config() const = 0;

    virtual float framerate() const = 0;
};
    
}
#endif //_runtime_h_
